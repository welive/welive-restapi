import csv
from util import get_all_resources

resources = get_all_resources(mapping_type='json_schema')

with open('user_resources.csv', 'w') as csvfile:
    fieldnames = ['id', 'name']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()

    for resource in resources:
        writer.writerow({'id': resource['id'], 'name': resource['name']})
