import requests
import json
import sys

from util import get_all_resources, get_info

def get_status():
    status_url = 'https://dev.welive.eu/dev/api/ods/get-status'

    r = requests.get(status_url)
    if r.status_code == 200:
        return r.json()
    else:
        print 'Could not obtain status data. Code: %d' % r.status_code
        sys.exit(0)

def find_status(resource_id, status_list):
    for status in status_list:
        if status['id'] == resource_id:
            return status

    return None

def get_resource_status(resource_id, status):
    for s in status:
        if s['id'] == resource_id:
            return s

    return None

def calculate_stats(resources, status_list):
    counters = {
        'json': {
            'ok': 0,
            'error': 0,
            'tables': 0,
            'total': 0
        },
        'csv': {
            'ok': 0,
            'error': 0,
            'tables': 0,
            'total': 0
        }
    }

    for resource in resources:
        if 'mapping' in resource and len(resource['mapping']) > 0:
            json_mapping = json.loads(resource['mapping'])
            if json_mapping['mapping'] == 'json':
                print 'json',
                table = counters['json']
            elif json_mapping['mapping'] == 'csv':
                print 'csv',
                table = counters['csv']
            else:
                table = None

            if table is not None:
                table['total'] += 1
                mapping_status = get_resource_status(resource['id'], status_list)
                info = get_info(resource['package_id'], resource['id'])
                if mapping_status['status'] == 'OK':
                    table['ok'] += 1
                elif mapping_status['status'] == 'ERROR':
                    table['error'] += 1

                table['tables'] += len(info['tables'])
                print len(info['tables']), resource['id'], mapping_status['message']

    return counters

if __name__ == '__main__':
    status_list = get_status()

    total_oks = 0
    total_errors = 0

    organization_name = 'bilbao-city-council'
    input_name = '%s_resources.json' % organization_name

    resources = {}
    with open(input_name, 'r') as input_file:
        json_data = input_file.read()
        resources = json.loads(json_data)

    print '%d resources loaded' % len(resources)

    counters = calculate_stats(resources, status_list)

    print 'Results'
    print '======='

    print counters
