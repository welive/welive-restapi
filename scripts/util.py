import requests
import json
from config import ODS_URL

def get_packages(organization_name=None):
    if organization_name is None:
        package_list_url = ODS_URL + '/3/action/package_list'

        r = requests.get(package_list_url)
        if r.status_code == 200:
            json_response = r.json()
            packages = json_response['result']
    else:
        organization_show_url = ODS_URL + '/3/action/organization_show'
        params = {'id': organization_name, 'include_datasets': True, 'include_users': False}

        r = requests.get(organization_show_url, params=params)
        if r.status_code == 200:
            json_response = r.json()
            packages = [package['name'] for package in json_response['result']['packages']]

    return packages

def get_resources(package_id, mapping_type=None):
    package_show_url = ODS_URL + '/3/action/package_show'
    params = {'id': package_id}

    r = requests.get(package_show_url, params=params)
    if r.status_code == 200:
        if 'resources' in r.json()['result']:
            json_response = r.json()
            resources = json_response['result']['resources']

            if mapping_type is not None:
                filtered_resources = []
                for resource in resources:
                    if 'mapping' in resource and len(resource['mapping']) > 0:
                        json_mapping = json.loads(resource['mapping'])
                        if 'mapping' in json_mapping and json_mapping['mapping'] == mapping_type:
                            filtered_resources.append(resource)

                resources = filtered_resources

            return resources

def get_all_resources(organization_name=None, mapping_type=None):
    resources = []
    packages = get_packages(organization_name)

    counter = 0
    for package_id in packages:
        counter += 1
        print 'Processing package %s. Total progress: %d/%d' % (package_id, counter, len(packages))
        resources += get_resources(package_id, mapping_type)
    return resources

def get_resource_ids(resources):
    names = []

    for resource in resources:
        names.append(resource['id'])

    return names

def check_resource(resource_id):
    resource = get_resource(resource_id)
    if status in resource:
        return resource['status']
    else:
        return False

def get_resource(resource_id):
    resource_show_url = ODS_URL + '/3/action/resource_show'
    params = {'id': resource_id}

    r = requests.get(resource_show_url, params=params)
    if r.status_code == 200:
        return r.json()

def get_info(package_id, resource_id):
    package_info_url = 'https://dev.welive.eu/dev/api/ods/%s/resource/%s/mapping/info' % (package_id, resource_id)

    r = requests.get(package_info_url)
    if r.status_code == 200:
        return r.json()
