import requests
from util import check_resource

with open('orphan_resources.csv', 'r') as orphan_data:
    for line in orphan_data:
        resource_id = line.strip()
        if check_resource(resource_id):
            print 'Resource "%s" is not orphan' % resource_id
