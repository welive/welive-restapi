import csv
from util import get_all_resources, get_resource_ids
from glob import glob
from os.path import join, basename

def load_ids(root_dir, dir):
    identifiers = set([])
    for file_name in glob(join(root_dir, dir, '*')):
        identifiers.add(basename(file_name).split('.')[0])
    return identifiers

def get_all_db_ids(root_dir):
    dump_ids = load_ids(root_dir, 'dumps')
    user_ids = load_ids(root_dir, 'user')

    all_ids = dump_ids.union(user_ids)

    return all_ids

if __name__ == '__main__':
    root_dir = raw_input('Data directory: ')

    resources = get_all_resources()
    resource_ids = get_resource_ids(resources)
    print 'Distinct resources in CKAN: %d' % len(resource_ids)

    db_ids = get_all_db_ids(root_dir)
    print 'Distinct IDs in database files: %d' % len(db_ids)

    orphans = db_ids.difference(resource_ids)
    print 'Number of orphan database IDs: %d' % len(orphans)

    with open('orphan_resources.csv', 'w') as output_file:
        for resource_id in orphans:
            output_file.write('%s\n' % resource_id)
