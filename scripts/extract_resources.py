if __name__ == '__main__':
    organization_name = 'bilbao-city-council'
    resources = get_all_resources(organization_name=organization_name)
    print '%d resources in "%s"' % (len(resources), organization_name)

    output_json = json.dumps(resources)
    output_name = '%s_resources.json' % organization_name
    with open(output_name, 'w') as output_file:
        output_file.write(output_json)
