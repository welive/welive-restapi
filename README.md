WeLive REST API
===============

![Codeship Badge](https://codeship.com/projects/d82dc300-cf46-0133-00c5-52aba3b897dd/status?branch=develop)

This file contains the basic documentation for the build and testing process of the [WeLive](http://welive.eu) REST API.
This component is part of the WeLive platform. To see the complete installation guide check the following [document](https://dev.welive.eu/documentation/installation/index.html)

Requirements
-------------

The project has the following requirements that must be correctly installed and
configured:

*	Java SE Development Kit 7. Available at [http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html](http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html)
* 	Maven 3. Available at [http://maven.apache.org/](http://maven.apache.org/)

Installation
------------

### Dependencies

First, clone the following repository somewhere outside the welive-restapi directory.

	git clone https://github.com/sciamlab/common-lib

and install the dependency into the local repository by executing the following command inside the `common-lib` directory

	cd common-lib
	mvn install

Repeat the previous steps with the `ckan4j` dependency

	git clone https://github.com/sciamlab/ckan4j
	cd ckan4j
	mvn install

### Build server

First, install the required query-mapper library distributed with the source code
by executing the following instruction inside the root directory.
	
	mvn validate

The following command will download all required dependencies, execute all
automatic tests and generate a deployable war file called *welive-restapi.war*
in the *target* directory.

	mvn install

**Note:** If there are some failing tests the build process will stop and not
generate the deployable war file. In order to build the code ignoring failing
tests run the following command

	mvn install -Dmaven.test.failure.ignore=true

**Note 2:** In order to skip tests, although not recommended, add the following
parameter to the previous command

	mvn install -DskipTests

### Test coverage analysis

The project uses [cobertura](http://cobertura.github.io/cobertura/) to analyze the test coverage and generate the report. To create the report execute

	mvn cobertura:cobertura

inside the root directory of the project. To inspect the results open the generated target/site/cobertura/index.html file in your browser. 

### Generating code reports

It is also possible to generate other reports (duplicate code, code analysis, possible bugs) by running the following command

	mvn site
	
The results are shown by opening the target/site/project-reports.html file in a browser.

### Testing the server

The server can be tested using an embedded Jetty server that automatically starts and deploys the generated war.

Inside the *rest-api* directory run

	mvn jetty:run

After launching the embedded server, the REST methods are described and can be tested by connecting to the following URL

	http://127.0.0.1:8080/welive/swagger/index.html