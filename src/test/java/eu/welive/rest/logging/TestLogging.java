/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.logging;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Entity.class, ClientBuilder.class, Config.class, LoggingService.class })
@PowerMockIgnore("javax.net.ssl.*")
public class TestLogging extends JerseyTest {

	private static LoggingService ls;
	private static final String appId = "testApp";
	private static final String appType = "testSchema";
	private static String token = "FAKE-TOKEN";

	@Override
	protected Application configure() {
		return new TestApplication();
	}

	@Before
	public void setUp() throws WeLiveException {
		
		ls = Mockito.spy(LoggingService.class);
//		ls.setEnabled(true);
		ls.createTimestamp();
	}

	@Test
	public void testUpdateLogSchema() throws Exception {

		JSONObject inputJSONSchema = new JSONObject(String.format(
				"{\"testSchema\": {\"$schema\": \"http://json-schema.org/draft-04/schema#\",\"type\": \"object\",\"properties\": {\"pilot\": {\"type\": \"string\"}},\"required\": [\"pilot\"]}}}"));

		Mockito.doReturn(null).when(ls).doPost(inputJSONSchema.toString(), "/update/schema/" + appId + "/" + appType,
				null, false, token);

		Response response = ls.udpateLogEventSchema(inputJSONSchema.toString(), appId, appType, token);

		assertEquals(HttpStatus.SC_OK, response.getStatus());
		Mockito.verify(ls, Mockito.times(1)).udpateLogEventSchema(inputJSONSchema.toString(), appId, appType, token);
		Mockito.verify(ls, Mockito.times(1)).doPost(inputJSONSchema.toString(),
				"/update/schema/" + appId + "/" + appType, null, false, token);

	}
	
	@Test
	public void testReadLogSchema() throws Exception {

		String schema = String.format(
				"{\"testSchema\": {\"$schema\": \"http://json-schema.org/draft-04/schema#\",\"type\": \"object\",\"properties\": {\"pilot\": {\"type\": \"string\"}},\"required\": [\"pilot\"]}}}");

		Mockito.doReturn(schema).when(ls).doGet("/read/schema/" + appId, null, token);

		Response response = ls.readLogEventSchema(appId, token);

		assertEquals(HttpStatus.SC_OK, response.getStatus());
		Mockito.verify(ls, Mockito.times(1)).readLogEventSchema(appId, token);
		Mockito.verify(ls, Mockito.times(1)).doGet("/read/schema/" + appId, null, token);

	}

	@Test(expected = WeLiveException.class)
	public void testUpdateSchemaException() throws Exception {

		JSONObject inputJSONSchema = new JSONObject(String.format(
				"{\"testSchema\": {\"$schema\": \"\",\"type\": \"object\",\"properties\": {\"pilot\": {\"type\": \"string\"}},\"required\": [\"pilot\"]}}}"));

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_PRECONDITION_FAILED),
				"HttpStatus.SC_PRECONDITION_FAILED")).when(ls)
				.doPost(inputJSONSchema.toString(), "/update/schema/" + appId + "/" + appType, null, false, token);

		ls.udpateLogEventSchema(inputJSONSchema.toString(), appId, appType, token);

		Mockito.verify(ls, Mockito.times(1)).pushLogOp(inputJSONSchema.toString(), appId, token);

	}

	@Test
	public void testPushInvalidLog() throws Exception {

		JSONObject inputLogMsg = new JSONObject(
				String.format("{\"msg\":\"TestSchema\",\"appId\":\"testApp\",\"type\":\"testSchema\",\"timestamp\":"
						+ System.currentTimeMillis() + ",\"custom_attr\": {\"_host\":\"Trento\"}}"));

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_PRECONDITION_FAILED),
				"HttpStatus.SC_PRECONDITION_FAILED")).when(ls)
				.doPost(inputLogMsg.toString(), "/" + appId, null, false, token);
		try {
			ls.pushLog(inputLogMsg.toString(), appId, token);
		} catch (WeLiveException e) {
			assertTrue(e.getStatus().getStatusCode() == HttpStatus.SC_PRECONDITION_FAILED);
		}
		Mockito.verify(ls, Mockito.times(1)).pushLog(inputLogMsg.toString(), appId, token);
	}

	@Test(expected = WeLiveException.class)
	public void testPushInvalidLogException() throws Exception {

		JSONObject inputLogMsg = new JSONObject(
				String.format("{\"msg\":\"TestSchema\",\"appId\":\"testApp\",\"type\":\"testSchema\",\"timestamp\":"
						+ System.currentTimeMillis() + ",\"custom_attr\": {\"_host\":\"Trento\"}}"));

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_PRECONDITION_FAILED),
				"HttpStatus.SC_PRECONDITION_FAILED")).when(ls)
				.doPost(inputLogMsg.toString(), "/" + appId, null, false, token);

		ls.pushLogOp(inputLogMsg.toString(), appId, token);

		Mockito.verify(ls, Mockito.times(1)).pushLog(inputLogMsg.toString(), appId, token);

	}

	@Test
	public void testPushValidLogMsg() throws Exception {
		Long eventTimeStamp = System.currentTimeMillis();
		String logValidMsg = "{\"msg\":\"TestSchema\",\"appId\":\"testApp\",\"type\":\"testSchema\",\"timestamp\":"
				+ eventTimeStamp + ",\"custom_attr\": {\"pilot\":\"Trento\"}}";
		String msg = new JSONObject(logValidMsg).toString();

		Mockito.doReturn(null).when(ls).doPost(msg, "/" + appId, null, false, token);

		Response response = ls.pushLogOp(appId, msg, token);

		assertEquals(HttpStatus.SC_OK, response.getStatus());

		Mockito.verify(ls, Mockito.times(1)).pushLogOp(appId, msg, token);
		Mockito.verify(ls, Mockito.times(1)).doPost(msg, "/" + appId, null, false, token);
	}

	// @Test
	// public void testGetPing() throws Exception {
	//
	// String log = "{\"offset\": 0,\"limit\": 150,\"data\": [{\"appId\":
	// \"testapp\",\"type\": \"testSchema\",\"msg\":
	// \"TestSchema\",\"timestamp\": 1478704049503,\"custom_attr\": {\"pilot\":
	// \"Trento\"}}],\"total_results\": 1}";
	//
	// Mockito.doReturn(log).when(ls).ping();
	//
	// String response = ls.ping();
	//
	// System.err.println(response);
	// assertEquals(response, log);
	//
	// Mockito.verify(ls, Mockito.times(1)).ping();
	// }

	@Test
	public void testGetLogMsg() throws Exception {

		String log = "{\"offset\": 0,\"limit\": 150,\"data\": [{\"appId\": \"testapp\",\"type\": \"testSchema\",\"msg\": \"TestSchema\",\"timestamp\": 1478704049503,\"custom_attr\": {\"pilot\": \"Trento\"}}],\"total_results\": 1}";

		Mockito.doReturn(log).when(ls).doGet("/" + appId, "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0",
				"");

		Response response = ls.query(appId, 1l, 2l, "", "", "", 1, 0, "");

		assertEquals(response.getEntity().toString(), log);

		Mockito.verify(ls, Mockito.times(1)).query(appId, 1l, 2l, "", "", "", 1, 0, "");
		Mockito.verify(ls, Mockito.times(1)).getLog(appId, "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0",
				"");
		Mockito.verify(ls, Mockito.times(1)).doGet("/" + appId,
				"type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", "");
	}

	@Test(expected = WeLiveException.class)
	public void testGetLogException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_PRECONDITION_FAILED),
				"HttpStatus.SC_PRECONDITION_FAILED")).when(ls)
				.doGet("/" + appId, "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", "");

		ls.query(appId, 1l, 2l, "", "", "", 1, 0, "");

	}

	@Test
	public void testGetLogCount() throws Exception {

		String countResponse = "{\"total_results\": 1}";

		Mockito.doReturn(countResponse).when(ls).doGet("/count/" + appId,
				"type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", "");

		Response response = ls.count(appId, 1l, 2l, "", "", "", 1, 0, "");

		System.err.println(response);
		assertEquals(response.getEntity().toString(), countResponse);

		Mockito.verify(ls, Mockito.times(1)).count(appId, 1l, 2l, "", "", "", 1, 0, "");
		Mockito.verify(ls, Mockito.times(1)).doGet("/count/" + appId,
				"type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", "");
	}

	@Test(expected = WeLiveException.class)
	public void testGetLogCountException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_PRECONDITION_FAILED),
				"HttpStatus.SC_PRECONDITION_FAILED")).when(ls)
				.doGet("/count/" + appId, "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", "");

		ls.count(appId, 1l, 2l, "", "", "", 1, 0, "");
		Mockito.verify(ls, Mockito.times(1)).count(appId, 1l, 2l, "", "", "", 1, 0, "");

	}

	@Test
	public void testAgreegate() throws Exception {

		Mockito.doReturn(null).when(ls).doPost("{}", "/aggregate", null, true, "");

		ls.aggregate("{}", "");

		Mockito.verify(ls, Mockito.times(1)).aggregate("{}", "");
		Mockito.verify(ls, Mockito.times(1)).aggregateLog("{}", "");
	}

	@Test(expected = WeLiveException.class)
	public void testAgreegateException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_PRECONDITION_FAILED),
				"HttpStatus.SC_PRECONDITION_FAILED")).when(ls)
				.doPost("{}", "/aggregate", null, true, "");

		ls.aggregate("{}", "");

		Mockito.verify(ls, Mockito.times(1)).aggregate("{}", "");

	}
	
	
//	@Test
//	public void testPOST() throws Exception {
//		
//		CloseableHttpClient mockedClient = Mockito.mock(CloseableHttpClient.class);
//		HttpClientBuilder mocketHttpClientBuilder = Mockito.mock(HttpClientBuilder.class);
//		Mockito.doReturn(mockedClient).when(mocketHttpClientBuilder).build();
//
//		Mockito.doReturn("").when(ls).doPost("{}", "/aggregate", "", true, "");
//		ls.doPost("{}", "/aggregate", "", true, "");
//		Mockito.verify(ls, Mockito.atLeastOnce()).doPost("{}", "/aggregate", "", true, "");
//	}
//
//	@Test
//	public void testGET() throws Exception {
//		Mockito.doReturn("").when(ls).doGet("/aggregate", "", "test token");
//		ls.doGet("/aggregate", "", "test token");
//		Mockito.verify(ls, Mockito.atLeastOnce()).doGet("/aggregate", "", "test token");
//	}
	
	@Test
	public void testPOST() throws Exception {

		CloseableHttpClient mockClient = Mockito.mock(CloseableHttpClient.class);

		HttpResponse mHttpResponseMock = Mockito.mock(HttpResponse.class);
		StatusLine mStatusLineMock = Mockito.mock(StatusLine.class);
		HttpEntity mHttpEntityMock = Mockito.mock(HttpEntity.class);

		Mockito.when(mHttpResponseMock.getStatusLine()).thenReturn(mStatusLineMock);
		Mockito.when(mStatusLineMock.getStatusCode()).thenReturn(HttpStatus.SC_OK);
		Mockito.when(mHttpResponseMock.getEntity()).thenReturn(mHttpEntityMock);
		CloseableHttpResponse mResponse = buildMockResponse();
		Mockito.when(mockClient.execute(Mockito.isA(HttpPost.class))).thenReturn(mResponse);

		ls.setHttpClient(mockClient);
		
		String response = ls.doPost("", "/aggregate", null, true, token);
//		System.out.println(response);
	}

	
	@Test
	public void testGET() throws Exception {
		CloseableHttpClient mockClient = Mockito.mock(CloseableHttpClient.class);

		HttpResponse mHttpResponseMock = Mockito.mock(HttpResponse.class);
		StatusLine mStatusLineMock = Mockito.mock(StatusLine.class);
		HttpEntity mHttpEntityMock = Mockito.mock(HttpEntity.class);

		Mockito.when(mHttpResponseMock.getStatusLine()).thenReturn(mStatusLineMock);
		Mockito.when(mStatusLineMock.getStatusCode()).thenReturn(HttpStatus.SC_OK);
		Mockito.when(mHttpResponseMock.getEntity()).thenReturn(mHttpEntityMock);
		CloseableHttpResponse mResponse = buildMockResponse();
		Mockito.when(mockClient.execute(Mockito.isA(HttpGet.class))).thenReturn(mResponse);

		ls.setHttpClient(mockClient);
		
		String response = ls.doGet("/api/userProfile/", null, token);
			
//		System.out.println(response);
	}
	
	public static CloseableHttpResponse buildMockResponse() throws ClientProtocolException, IOException {
		String body = "{\"ccUserID\": \"314\",\"birthdate\": \"1981-04-12 00:00:00\",\"address\": \"\",\"city\": \"\",\"country\": \"\",\"zipCode\": \"\",\"referredPilot\": \"Trento\",\"languages\": [\"Italian\",\"English\"],\"developer\": false,\"skills\": [],\"userTags\": [\"developer\"]}";

		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse closeableHttpResponse = httpClient.execute(new HttpGet("http://www.test.com"));
		closeableHttpResponse.setEntity(new StringEntity(body));
		closeableHttpResponse.setStatusCode(200);
		return closeableHttpResponse;
	}

	@Test
	public void testSendLog() throws Exception {
		Long eventTimeStamp = System.currentTimeMillis();
		String logValidMsg = "{\"msg\":\"TestSchema\",\"appId\":\"testApp\",\"type\":\"testSchema\",\"timestamp\":"
				+ eventTimeStamp + ",\"custom_attr\": {\"pilot\":\"Trento\"}}";
		JSONObject msg = new JSONObject(logValidMsg);
		ls.sendLog("service", msg);
		Mockito.verify(ls, Mockito.times(1)).sendLog("service", msg);
	}
}