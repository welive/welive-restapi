/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.ckan.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.Test;

public class CKANUserTest {

	@Test
	public void testLoadData() {
		final JSONObject userInfo = new JSONObject();
		userInfo.put(CKANUser.ID, "1234-1234-1234-1234");
		userInfo.put(CKANUser.NAME, "user-name");
		userInfo.put(CKANUser.CAPACITY, "creator");
		
		final CKANUser ckanUser = new CKANUser(userInfo);
		assertEquals("1234-1234-1234-1234", ckanUser.getID());
		assertEquals("user-name", ckanUser.getName());
		assertEquals("creator", ckanUser.getCapacity());
	}
	
	@Test
	public void testLoadDataEmpty() {
		final JSONObject userInfo = new JSONObject();
		
		final CKANUser ckanUser = new CKANUser(userInfo);
		assertTrue(ckanUser.getID().isEmpty());
		assertTrue(ckanUser.getName().isEmpty());
		assertTrue(ckanUser.getCapacity().isEmpty());
	}
}
