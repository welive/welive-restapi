/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.ckan;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedMap;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.sciamlab.ckan4j.CKANApiClient;
import com.sciamlab.ckan4j.CKANApiClient.CKANApiClientBuilder;
import com.sciamlab.ckan4j.exception.CKANException;

import eu.welive.rest.auth.user.anon.AnonUserInfo;
import eu.welive.rest.auth.user.basic.BasicUserAdminInfo;
import eu.welive.rest.config.Config;
import eu.welive.rest.datasource.manager.AccessData.WeLiveRole;
import eu.welive.rest.datasource.manager.CKANDataMocker;
import eu.welive.rest.util.ckan.data.CKANOrganization;
import eu.welive.rest.util.ckan.data.CKANPackage;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.data.CKANUser;
import eu.welive.rest.util.ckan.exception.CKANConnectionException;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientBuilder.class, CKANUtil.class, CKANApiClientBuilder.class, Config.class})
public class CKANUtilTest {
	
	private static final String CKAN_URL = "http://someurl.com/ods";
	
	private static final String USER_ID = "1111-1111-1111-1111";
	private static final String OTHER_USER_ID = "2222-2222-2222-2222";
	private static final String USER_NAME = "user-1";
	private static final String OTHER_USER_NAME = "user-2";
	private static final String EMAIL = "a@a.com";
	private static final String CAPACITY_1 = WeLiveRole.ADMIN.toString();
	private static final String CAPACITY_2 = WeLiveRole.EDITOR.toString();
	
	private CKANApiClient ckanAPIClient;
	private Builder builder;
	private CKANUtil ckanUtil;
	private Client client;
	
	private JSONObject createResourceInfo() {
		final JSONObject resourceInfo = new JSONObject();
		
		resourceInfo.put(CKANResource.ID, CKANDataMocker.RESOURCE_ID);
		resourceInfo.put(CKANResource.PACKAGE_ID, CKANDataMocker.PACKAGE_ID);
		resourceInfo.put(CKANResource.MAPPING, CKANDataMocker.MAPPING);
		resourceInfo.put(CKANResource.PERMISSIONS, CKANDataMocker.PERMISSIONS);
		
		return resourceInfo;
	}
	
	private JSONObject createPackageInfo() {
		final JSONObject packageInfo = new JSONObject();
		
		packageInfo.put(CKANPackage.ID, CKANDataMocker.PACKAGE_ID);
		packageInfo.put(CKANPackage.CREATOR_USER_ID, CKANDataMocker.CREATOR_USER_ID);
		
		final JSONObject organization = new JSONObject();
		organization.put(CKANOrganization.ID, CKANDataMocker.ORGANIZATION_ID);
		packageInfo.put(CKANPackage.ORGANIZATION, organization);
		
		return packageInfo;
	}

	@Before
	public void setUp() throws CKANConnectionException, IOException {
		ckanAPIClient = Mockito.mock(CKANApiClient.class);
		
		builder = Mockito.mock(Builder.class);
		when(builder.header(Matchers.anyString(), Matchers.anyString()))
			.thenReturn(builder);
		
		final WebTarget target = Mockito.mock(WebTarget.class);
		when(target.path(Matchers.anyString()))
			.thenReturn(target);
		
		when(target.request())
			.thenReturn(builder);
		
		client = Mockito.mock(Client.class);
		when(client.target(Matchers.any(URI.class)))
			.thenReturn(target);
		
		PowerMockito.mockStatic(ClientBuilder.class);
		when(ClientBuilder.newClient())
			.thenReturn(client);
		
		PowerMockito.mockStatic(CKANApiClientBuilder.class);
		final CKANApiClientBuilder builder = PowerMockito.mock(CKANApiClientBuilder.class);
		when(CKANApiClientBuilder.init(Matchers.anyString()))
			.thenReturn(builder);
		
		when(builder.build())
			.thenReturn(ckanAPIClient);
		
		final Config config = PowerMockito.mock(Config.class);
		when(config.getCkanSearchApi())
			.thenReturn(CKAN_URL + "/api/search/");
		
		when(config.getCkanEndpoint())
			.thenReturn(CKAN_URL + "/api/3/");
		
		PowerMockito.mockStatic(Config.class);
		when(Config.getInstance())
			.thenReturn(config);
		
		ckanUtil = new CKANUtil();
	}
	
	@Test(expected=CKANConnectionException.class)
	public void testConstructorBuilderError() throws MalformedURLException, CKANConnectionException {
		when(CKANApiClientBuilder.init(Matchers.anyString()))
			.thenThrow(new MalformedURLException());
		
		new CKANUtil();
	}
	
	@Test
	public void testGetResourceInfo() throws CKANUtilException, CKANException {
		when(ckanAPIClient.resourceShow(CKANDataMocker.RESOURCE_ID))
			.thenReturn(createResourceInfo());
		
		final CKANResource resourceInfo = ckanUtil.getResourceInfo(CKANDataMocker.RESOURCE_ID);
		final CKANResource expected = new CKANResource(CKANDataMocker.RESOURCE_ID, CKANDataMocker.PACKAGE_ID, CKANDataMocker.MAPPING, CKANDataMocker.PERMISSIONS);
		
		assertEquals(expected, resourceInfo);
	}
	
	@Test
	public void testGetPackageInfo() throws CKANException, CKANUtilException {
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(createPackageInfo());
		
		final CKANPackage packageInfo = ckanUtil.getPackageInfo(CKANDataMocker.PACKAGE_ID);
		final CKANPackage expected = new CKANPackage(CKANDataMocker.PACKAGE_ID, CKANDataMocker.CREATOR_USER_ID, CKANDataMocker.ORGANIZATION_ID, new ArrayList<CKANResource>());
		
		assertEquals(expected, packageInfo);
	}
	
	private JSONArray createPackageList() {
		final JSONArray packages = new JSONArray();
		packages.put("sample-package-1");
		packages.put("sample-package-2");
		packages.put("sample-package-3");
		return packages;
	}
	
	@Test
	public void testGetAllPackages() throws CKANException, CKANUtilException {
		when(ckanAPIClient.packageList())
			.thenReturn(createPackageList());
		
		final List<String> allPackages = ckanUtil.getAllPackages();
		
		final List<String> expected = new ArrayList<String>();
		expected.add("sample-package-1");
		expected.add("sample-package-2");
		expected.add("sample-package-3");
		
		assertEquals(expected, allPackages);
	}
	
	@Test(expected=CKANUtilException.class)
	public void testGetAllPackagesError() throws CKANException, CKANUtilException {
		when(ckanAPIClient.packageList())
			.thenThrow(new CKANException(new Exception("Some unexpected problem")));
		
		ckanUtil.getAllPackages();
	}
	
	private JSONObject createUser(String id, String name, String capacity) {
		final JSONObject user = new JSONObject();
		user.put(CKANUser.ID, id);
		user.put(CKANUser.NAME, name);
		user.put(CKANUser.CAPACITY, capacity);
		user.put(CKANUser.SUCCESS, true);
		return user;
	}
	
	private JSONObject createUserInvalid() {
		final JSONObject user = new JSONObject();
		user.put(CKANUser.SUCCESS, false);
		return user;
	}
	
	private JSONObject createOrganization() {
		final JSONObject organization = new JSONObject();
		organization.put(CKANOrganization.ID, CKANDataMocker.ORGANIZATION_ID);
		
		final JSONArray users = new JSONArray();
		users.put(createUser(USER_ID, USER_NAME, CAPACITY_1));
		users.put(createUser(OTHER_USER_ID, OTHER_USER_NAME, CAPACITY_2));
		
		organization.put(CKANOrganization.USERS, users);
		
		return organization;
	}
	
	private JSONObject createOrganizationEmptyCapacity() {
		final JSONObject organization = new JSONObject();
		organization.put(CKANOrganization.ID, CKANDataMocker.ORGANIZATION_ID);
		
		final JSONArray users = new JSONArray();
		users.put(createUser(USER_ID, USER_NAME, ""));
		users.put(createUser(OTHER_USER_ID, OTHER_USER_NAME, ""));
		
		organization.put(CKANOrganization.USERS, users);
		
		return organization;
	}
	
	@Test
	public void testGetOrganization() throws CKANException, CKANUtilException {
		when(ckanAPIClient.organizationShow(CKANDataMocker.ORGANIZATION_ID, false))
			.thenReturn(createOrganization());
			
		final CKANOrganization organization = ckanUtil.getOrganization(CKANDataMocker.ORGANIZATION_ID);
		
		assertEquals(CKANDataMocker.ORGANIZATION_ID, organization.getID());
		assertEquals(2, organization.getUsers().size());
	}
	
	@Test(expected=CKANUtilException.class)
	public void testGetOrganizationError() throws CKANException, CKANUtilException {
		when(ckanAPIClient.organizationShow(CKANDataMocker.ORGANIZATION_ID, false))
			.thenThrow(new CKANException(new Exception("Some unexpected problem")));
		
		ckanUtil.getOrganization(CKANDataMocker.ORGANIZATION_ID);
	}
	
	@Test
	public void testGetUser() throws CKANException, CKANUtilException {
		when(ckanAPIClient.userShow(USER_ID))
			.thenReturn(createUser(USER_ID, USER_NAME, CAPACITY_1));
		
		final CKANUser ckanUser = ckanUtil.getUser(USER_ID);
		
		assertEquals(USER_ID, ckanUser.getID());
		assertEquals(USER_NAME, ckanUser.getName());
		assertEquals(CAPACITY_1, ckanUser.getCapacity());
	}
	
	@Test(expected=CKANUtilException.class)
	public void testGetUserError() throws CKANException, CKANUtilException {
		when(ckanAPIClient.userShow(USER_ID))
			.thenThrow(new CKANException(new Exception("Some unexpected problem")));
		
		ckanUtil.getUser(USER_ID);
	}
	
	@Test
	public void testGetUserRoleEmptyUser() throws CKANUtilException, CKANException {
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(createPackageInfo());
		
		final AnonUserInfo userInfo = new AnonUserInfo();
		final WeLiveRole role = ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, userInfo.getName());
		
		assertEquals(WeLiveRole.NONE, role);
	}
	
	@Test
	public void testGetUserRoleAdmin() throws CKANUtilException, CKANException {
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(createPackageInfo());
		
		final BasicUserAdminInfo userInfo = new BasicUserAdminInfo();
		final WeLiveRole role = ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, userInfo.getName());
		
		assertEquals(WeLiveRole.ADMIN, role);
	}
	
	@Test
	public void testGetUserRoleCreator() throws CKANUtilException, CKANException {
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(createPackageInfo());
		
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createUser(USER_ID, USER_NAME, ""));
		
		final WeLiveRole role = ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_NAME);
		
		assertEquals(WeLiveRole.CREATOR, role);
	}
	
	@Test
	public void testGetUserRoleCreatorDifferent() throws CKANUtilException, CKANException {
		final JSONObject jsonPackage = createPackageInfo();
		jsonPackage.remove(CKANPackage.ORGANIZATION);
		
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(jsonPackage);
		
		when(ckanAPIClient.organizationShow(CKANDataMocker.ORGANIZATION_ID, false))
			.thenReturn(createOrganization());
		
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createUser(OTHER_USER_ID, USER_NAME, ""));
				
		final WeLiveRole role = ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_NAME);
		
		assertEquals(WeLiveRole.NONE, role);
	}
	
	@Test
	public void testGetUserRoleCreatorOrganization() throws CKANUtilException, CKANException {
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(createPackageInfo());
		
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createUser(OTHER_USER_ID, USER_NAME, ""));
		
		when(ckanAPIClient.organizationShow(CKANDataMocker.ORGANIZATION_ID, false))
			.thenReturn(createOrganization());
				
		final WeLiveRole role = ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_NAME);
		
		assertEquals(WeLiveRole.EDITOR, role);
	}
	
	@Test
	public void testGetUserRoleCreatorOrganizationInvalidCapacity() throws CKANUtilException, CKANException {
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(createPackageInfo());
		
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createUser(OTHER_USER_ID, USER_NAME, ""));
		
		when(ckanAPIClient.organizationShow(CKANDataMocker.ORGANIZATION_ID, false))
			.thenReturn(createOrganizationEmptyCapacity());
				
		final WeLiveRole role = ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_NAME);
		
		assertEquals(WeLiveRole.NONE, role);
	}
	
	@Test
	public void testGetUserRoleCreatorEmptyPackage() throws CKANUtilException, CKANException {
		final JSONObject jsonPackage = new JSONObject();		
		jsonPackage.put(CKANPackage.ID, CKANDataMocker.PACKAGE_ID);
		
		when(ckanAPIClient.packageShow(CKANDataMocker.PACKAGE_ID))
			.thenReturn(jsonPackage);
		
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createUser(USER_ID, USER_NAME, ""));
		
		final WeLiveRole role = ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_NAME);
		
		assertEquals(WeLiveRole.NONE, role);
	}
	
	@Test
	public void testGetUpdatedPackagesNullRevision() throws CKANUtilException, CKANException {
		when(ckanAPIClient.packageList())
			.thenReturn(createPackageList());
	
		final List<String> allPackages = ckanUtil.getUpdatedPackages(null);
		
		final List<String> expected = new ArrayList<String>();
		expected.add("sample-package-1");
		expected.add("sample-package-2");
		expected.add("sample-package-3");
		
		assertEquals(expected, allPackages);
	}
	
	private JSONObject createRevision() {
		final JSONObject revision = new JSONObject();
		final JSONArray packages = new JSONArray();
		packages.put("sample-package-1");
		packages.put("sample-package-2");
		packages.put("sample-package-3");
		
		revision.put("packages", packages);
		
		return revision;
	}
	
	@Test
	public void testGetUpdatedPackages() throws CKANUtilException, CKANException, IOException {
		when(builder.get(String.class))
			.thenReturn(createPackageList().toString());
		
		when(ckanAPIClient.actionGET(Matchers.eq("revision_show"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createRevision());
		
		final List<String> updated = ckanUtil.getUpdatedPackages(new DateTime(2016, 10, 27, 14, 50, 00));
		
		ArgumentCaptor<URI> uriCaptor = ArgumentCaptor.forClass(URI.class);
		verify(client).target(uriCaptor.capture());
		
		assertEquals(Config.getInstance().getCkanSearchApi() + "/revision?since_time=2016-10-27T14:50:00" , uriCaptor.getValue().toString());
		
		final List<String> expected = new ArrayList<String>();
		expected.add("sample-package-1");
		expected.add("sample-package-2");
		expected.add("sample-package-3");
		
		assertEquals(expected, updated);
	}
	
	@Test
	public void testGetUpdatedPackagesEmptyPackages() throws CKANUtilException, CKANException {
		when(builder.get(String.class))
			.thenReturn(createPackageList().toString());
		
		when(ckanAPIClient.actionGET(Matchers.eq("revision_show"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(new JSONObject());
		
		final List<String> updated = ckanUtil.getUpdatedPackages(new DateTime(2016, 10, 27, 14, 50, 00));
				
		assertTrue(updated.isEmpty());
	}
	
	@Test(expected=CKANUtilException.class)
	public void testGetUpdatedPackagesError() throws CKANUtilException, CKANException {
		when(builder.get(String.class))
			.thenReturn(createPackageList().toString());
		
		when(ckanAPIClient.actionGET(Matchers.eq("revision_show"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenThrow(new CKANException(new Exception("Some unexpected problem")));
		
		ckanUtil.getUpdatedPackages(new DateTime(2016, 10, 27, 14, 50, 00));
	}
	
	@Test
	public void testGetUserByUserId() throws CKANException, CKANUtilException {
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createUser(USER_ID, USER_NAME, ""));
		
		final CKANUser user = ckanUtil.getUserByUserId(USER_ID);
		
		assertEquals(USER_ID, user.getID());
		assertEquals(USER_NAME, user.getName());
	}
	
	@Test
	public void testGetUserByUserIdInvalidUserId() throws CKANException, CKANUtilException {
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenReturn(createUserInvalid());
		
		final CKANUser user = ckanUtil.getUserByUserId(USER_ID);
		
		assertTrue(user.getID().isEmpty());
		assertTrue(user.getName().isEmpty());
		assertTrue(user.getCapacity().isEmpty());
	}
	
	@Test(expected=CKANUtilException.class)
	public void testGetUserByIdActionGetError() throws CKANException, CKANUtilException {
		when(ckanAPIClient.actionGET(Matchers.eq("user_by_ccuserid"), Matchers.<MultivaluedMap<String, String>>any()))
			.thenThrow(new CKANException(new Exception("Some unexpected error ocurred")));
		
		ckanUtil.getUserByUserId(EMAIL);
	}
}