/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.lum;

import static org.junit.Assert.assertEquals;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientBuilder.class, Entity.class, Config.class, LUM.class, IOUtils.class})
@PowerMockIgnore("javax.net.ssl.*")

public class TestLUM extends JerseyTest{
	
	
	Pilot pilotID;
	Role role;
	Employement empl;
	String companyID = "id";
	
	private static String token = "fake-token";
	
	private static long ccuserid = 653492;
	private static String ccuserID = "userid";
	
	
	
	@Override
	protected Application configure() {
		return new TestApplication();
	}
	
	
	
	@Test
	public void testAddNewUser() throws Exception
	
	
	
		{
			
			role =Role.Academy;
			role = Role.Business;
			role = Role.Entrepreneur;
			role =Role.Citizen;
				
		   role.toString();
			empl = Employement.Unemployed;
			empl.toString();
			
			pilotID = Pilot.Bilbao;
			
			
			
			
			UserCreationBean userData = new UserCreationBean();
			userData.setAddress("sjhdgbds");
			userData.setBirthdayDay(2);
			userData.setBirthdayMonth(12);
			userData.setBirthdayYear(1988);
			userData.setCcUserId(5);
			userData.setCity("pal");
			userData.setCountry("hdjs");
			userData.setDeveloper(true);
			userData.setEmail("dhjsdgs");
			userData.setEmployement(empl);
			userData.setFirstName("first");
			userData.setIsMale(true);
			userData.setLanguages(null);
			userData.setPilot(pilotID);
			userData.setRole(role);
			userData.setSurname("nbxjfdb");
			userData.setTags(null);
			userData.setZipCode("zip");
			userData.getAddress();
			userData.getBirthdayDay();
			userData.getBirthdayMonth();
			userData.getBirthdayYear();
			userData.getCcUserId();
			userData.getCity();
			userData.getCountry();
			userData.getDeveloper();
			userData.getEmail();
			userData.getEmployement();
			userData.getFirstName();
			userData.getIsMale();
			userData.getLanguages();
			userData.getPilot();
			userData.getRole();
			userData.getSurname();
			userData.getTags();
			userData.getZipCode();
			
			String url=Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+ "/add-new-user/"+"ccuser/"+userData.getCcUserId().toString()+"/pilot/"+userData.getPilot().toString()+"/firstname/"+userData.getFirstName()+"/surname/"+userData.getSurname()+"/email/"+userData.getEmail()+"/ismale/"+userData.getIsMale().toString()+"/birthdayday/"+userData.getBirthdayDay().toString()+"/birthdaymonth/"+userData.getBirthdayMonth().toString()+"/birthdayyear/"+userData.getBirthdayYear().toString()+"/isdeveloper/"+userData.getDeveloper().toString()+"/address/"+userData.getAddress()+"/zipcode/"+userData.getZipCode()+"/city/"+userData.getCity()+"/country/"+userData.getCity()+"/-languages"+"/role/"+userData.getRole().toString()+"/employement/"+userData.getEmployement().toString()+"/-tags";
			LUM lum = Mockito.spy(new LUM());
			Response mockedResponse = Mockito.mock (Response.class);
			Mockito.doReturn(mockedResponse).when(lum).post(url, token, null);
			lum.addNewUser(token, userData);
			
			 Mockito.verify(lum,Mockito.times(1)).post(url,token,null);
		}
	
	
	@Test
	public void testInvalidUserIdAddNewUser() throws Exception
	
	
	
		{
			
			role =Role.Academy;
			role = Role.Business;
			role = Role.Entrepreneur;
			role =Role.Citizen;
				
			role.toString();
			empl = Employement.Unemployed;
			empl.toString();
			
pilotID = Pilot.Bilbao;
			
			

			BirthDate birthdate = new BirthDate();
			
			birthdate.setDay(2);
			birthdate.setMonth(12);
			birthdate.setYear(1988);
			
			User user = new User();
			user.setAddress("gdgd");
			user.setAvatar("av");
			user.setBirthDate(birthdate);
			user.setCcUserID(5);
			user.setCity("jyhsfd");
			user.setCountry("jdshd");
			user.setEmail("hdjusgdh");
			user.setEmployment("gyds");
			user.setFirstName("hsf");
			user.setId(545);
			user.setLastName(null);
			user.setIsDeveloper(true);
			user.setIsMale(false);
			user.setLanguages(null);
			user.setLiferayScreenName("njghds");
			user.setRole("hsjhdg");
			user.setZipCode("zip");
			user.setReferredPilot(null);
			user.getAddress();
			user.getAvatar();
			user.getBirthDate();
			user.getCcUserID();
			user.getCity();
			user.getCountry();
			user.getEmail();
			user.getEmployment();
			user.getId();
			user.getIsDeveloper();
			user.getIsMale();
			user.getLanguages();
			user.getLastName();
			user.getLiferayScreenName();
			user.getReferredPilot();
			user.getRole();
			user.getZipCode();
			user.getFirstName();
			GetUserData u = new GetUserData();
			u.setError(true);
			u.getError();
			u.setUser(user);
			u.getError();
			u.getUser();
			
			UserCreationBean userData = new UserCreationBean();
			userData.setAddress("sjhdgbds");
			userData.setBirthdayDay(birthdate.getDay());
			userData.setBirthdayMonth(birthdate.getMonth());
			userData.setBirthdayYear(birthdate.getYear());
			userData.setCcUserId(5);
			userData.setCity("pal");
			userData.setCountry("hdjs");
			userData.setDeveloper(true);
			userData.setEmail("dhjsdgs");
			userData.setEmployement(empl);
			userData.setFirstName("first");
			userData.setIsMale(true);
			userData.setLanguages(null);
			userData.setPilot(pilotID);
			userData.setRole(role);
			userData.setSurname("nbxjfdb");
			userData.setTags(null);
			userData.setZipCode("zip");
			userData.getAddress();
			userData.getBirthdayDay();
			userData.getBirthdayMonth();
			userData.getBirthdayYear();
			userData.getCcUserId();
			userData.getCity();
			userData.getCountry();
			userData.getDeveloper();
			userData.getEmail();
			userData.getEmployement();
			userData.getFirstName();
			userData.getIsMale();
			userData.getLanguages();
			userData.getPilot();
			userData.getRole();
			userData.getSurname();
			userData.getTags();
			userData.getZipCode();
			
			String url=Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+ "/add-new-user/"+"ccuser/"+userData.getCcUserId().toString()+"/pilot/"+userData.getPilot().toString()+"/firstname/"+userData.getFirstName()+"/surname/"+userData.getSurname()+"/email/"+userData.getEmail()+"/ismale/"+userData.getIsMale().toString()+"/birthdayday/"+userData.getBirthdayDay().toString()+"/birthdaymonth/"+userData.getBirthdayMonth().toString()+"/birthdayyear/"+userData.getBirthdayYear().toString()+"/isdeveloper/"+userData.getDeveloper().toString()+"/address/"+userData.getAddress()+"/zipcode/"+userData.getZipCode()+"/city/"+userData.getCity()+"/country/"+userData.getCity()+"/-languages"+"/role/"+userData.getRole().toString()+"/employement/"+userData.getEmployement().toString()+"/-tags";
			
			
			LUM lum = Mockito.spy(new LUM());
			Response mockedResponse = Mockito.mock (Response.class);
			Mockito.doReturn("{\"error\": true, \"message\":\"ccuserid field is missing\"}").when(mockedResponse).readEntity(String.class);
	        Mockito.doReturn(400).when(mockedResponse).getStatus();
			
			Mockito.doReturn(mockedResponse).when(lum).post(url, token, null);
			lum.addNewUser(token, userData);
			Mockito.verify(lum,Mockito.times(1)).post(url, token, null);
			assertEquals(400, mockedResponse.getStatus());
		}
	@Test
	public void testGetUserRoles() throws Exception {
		
		
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath() + "/get-user-roles"
				+ "/ccuserid/"+ ccuserid; 
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
    	try {
			Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	lum.getUserRoles(token, ccuserid);
    	
    	 Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
	}
	
	@Test
	public void testcheckLogin() throws Exception {
		
		String ccuserid = "";
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath() + "/get-user-company-details/ccuserid/" + ccuserid;
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
		try {
			Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		lum.getCompanyDetails(token, ccuserid);
		Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
	
	}
	
	@Test
	public void testCheckLogin2() throws Exception {
		
		String email = "email";
		String password = "pass";
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+"/check-login/email/"+ email + "/pwd/" + password;
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		lum.checklogin(token, email, password);
		Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
		
	}
	
	@Test
	public void testLeader() throws Exception {
		
		
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+"/get-leader-ccuserid-by-organizationid/organization-id/"+ companyID;
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		lum.getCompanyLeader(token, companyID);
		Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
		
	}
	
	@Test
	public void testUserCompany() throws Exception {
		
		
		CompanyLeader l = new CompanyLeader();
		l.setCcUserId(5);
		l.getCcUserId();
		l.setError(true);
		l.getError();
		l.setMessage("dgshj");
		l.getMessage();
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+"/get-user-company-details/ccuserid/"+ ccuserID;
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		lum.getCompanyDetails(token, ccuserID);
		Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
		
	}
	
	@Test
	public void testAllUsersData() throws Exception {
		
		GetUsersData g = new GetUsersData();
		g.setUsers(null);
		g.getUsers();
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+"/get-all-users-data";
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		lum.getAllUsersData(token);
		Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
		
	}
	
	@Test
	public void testUserData() throws Exception {
		
		
		
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+"/get-user-data/cc-user-id/"+ ccuserID;
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		lum.getuserdata(token, ccuserID);
		Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
		
	}
	
	@Test
	public void testInvalidUserData() throws Exception {
		
		
		//ccuserID = null;
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath()+"/get-user-data/cc-user-id/"+ ccuserID;
		LUM lum = Mockito.spy(new LUM());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn("{\"error\": true, \"message\":\"KO-User does not exist\"}").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(400).when(mockedResponse).getStatus();
		
		Mockito.doReturn(mockedResponse).when(lum).invoke(url, token);
		lum.getuserdata(token, ccuserID);
		Mockito.verify(lum,Mockito.times(1)).invoke(url, token);
		assertEquals(400, mockedResponse.getStatus());
	}
	
}