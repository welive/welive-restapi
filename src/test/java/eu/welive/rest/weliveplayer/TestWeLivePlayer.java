/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.weliveplayer;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Entity.class, ClientBuilder.class, Config.class, WeLivePlayerController.class })
@PowerMockIgnore("javax.net.ssl.*")
public class TestWeLivePlayer extends JerseyTest {

	private static WeLivePlayerController wePlayerController;
	private static WeLivePlayerService weLivePlayerService;
	private static String artifactId = "test";
	private static int start = 0;
	private static int count = 1;
	private static String token = "FAKE-TOKEN";
	private static String pilotId = "TestPilot";
	private static String appType = "TestType";

	private static ObjectMapper mapper = new ObjectMapper();

	@Override
	protected Application configure() {
		return new TestApplication();
	}

	@Before
	public void setUp() throws Exception {
		wePlayerController = new WeLivePlayerController();
		weLivePlayerService = Mockito.spy(WeLivePlayerService.class);
		wePlayerController.setWeLivePlayerService(weLivePlayerService);
	}

	@Test
	public void testGetAppComments() throws Exception {

		String appComment = "{\"data\":[]}";
		Mockito.doReturn(appComment).when(weLivePlayerService)
				.doGet("/api/appComments/" + artifactId + "?start=" + start + "&count=" + count, null, token);
		Response responseController = wePlayerController.getAppComments(artifactId, token, start, count);
		assertEquals(appComment, responseController.getEntity().toString());
		Mockito.verify(weLivePlayerService, Mockito.times(1)).findApplicationComments(artifactId, token, start, count);

	}

	@Test(expected = WeLiveException.class)
	public void testGetAppCommentsException() throws Exception {
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(weLivePlayerService)
				.doGet("/api/appComments/" + artifactId + "?start=" + start + "&count=" + count, null, token);
		wePlayerController.getAppComments(artifactId, token, start, count);
	}

	@Test
	public void testGetArtifacts() throws Exception {

		String appsJson = "{\"data\": [{\"comment\": \"Bravi! Non ci aveva pensato ancora nessuno. Il servizio di notifica  molto utile.\",\"authorNode\": \"zacco\",\"publishDate\": \"2016-07-13 13:10:24.172\",\"rating\": 0},{\"comment\": \"Sarebbe utile (abilitando la geolocalizzazione) poter ottenere una notifica nel caso in cui, durante le ore di pulizia di una strada, ci si abbia comunque parcheggiato nonostante i cartelli.\",\"authorNode\": \"adami.giordano\",\"publishDate\": \"2016-07-25 14:39:17.475\",\"rating\": 0},{\"comment\": \"app molto utile\",\"authorNode\": \"isabelmatranga\",\"publishDate\": \"2016-08-02 20:56:16.192\",\"rating\": 0},{\"comment\": \"Sarebbe utile prevedere anche la ricerca per data.Se utilizzo la ricerca per via appare la data in cui prevista la pulizia ma non  chiaro cosa posso o non posso fare a differenza che se l'informazione è visualizzata a partire dal calendario posso transitare? posso sostare in quale orario\",\"authorNode\": \"serena.dallatorre\",\"publishDate\": \"2016-08-03 20:34:09.372\",\"rating\": 0}],\"errorMessage\": null,\"errorCode\": 0}";
		Mockito.doReturn(appsJson).when(weLivePlayerService)
				.doGet("/api/apps/" + pilotId + "/" + appType + "?start=" + start + "&count=" + count, null, token);
		Response responseController = wePlayerController.getArtifacts(pilotId, appType, token, start, count);
		assertEquals(appsJson, responseController.getEntity().toString());
		
//		weLivePlayerService.findApplications(pilotId, appType, token, start, count);
		
		Mockito.verify(weLivePlayerService, Mockito.times(1)).findApplications(pilotId, appType, token, start, count);
//		Mockito.verify(weLivePlayerService, Mockito.atLeastOnce()).doGet("/api/apps/" + pilotId + "/" + appType + "?start=" + start + "&count=" + count, null, token);

	}

	@Test(expected = WeLiveException.class)
	public void testGetArtifactsException() throws Exception {
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(weLivePlayerService)
				.doGet("/api/apps/" + pilotId + "/" + appType + "?start=" + start + "&count=" + count, null, token);
		wePlayerController.getArtifacts(pilotId, appType, token, start, count);
	}

	@Test
	public void testReadUpdateUserProfile() throws Exception {

		String userProfile = "{\"ccUserID\": \"314\",\"birthdate\": \"1981-04-12 00:00:00\",\"address\": \"\",\"city\": \"\",\"country\": \"\",\"zipCode\": \"\",\"referredPilot\": \"Trento\",\"languages\": [\"Italian\",\"English\"],\"developer\": false,\"skills\": [],\"userTags\": [\"developer\"]}";
		Mockito.doReturn(userProfile).when(weLivePlayerService).doGet("/api/userProfile/", null, token);
		Response responseController = wePlayerController.readUserProfile(token);
		assertEquals(userProfile, responseController.getEntity().toString());
		Profile profile = mapper.readValue(responseController.getEntity().toString(), Profile.class);
		profile.setDeveloper(true);
		String updated = weLivePlayerService.getProfileJson(profile);
		Mockito.doReturn(updated).when(weLivePlayerService).doPost("/api/update/userProfile/", updated, token, true);
		Response updateResponse = wePlayerController.updateUserProfile(token, profile);
		Profile updatedProfile = mapper.readValue(updateResponse.getEntity().toString(), Profile.class);
		Assert.assertTrue(updatedProfile.isDeveloper());
		Mockito.verify(weLivePlayerService, Mockito.times(1)).findUserProfile(token);
		Mockito.verify(weLivePlayerService, Mockito.times(1)).updateUserProfile(token, profile);

	}

	@Test(expected = WeLiveException.class)
	public void testReadUserProfileException() throws Exception {
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(weLivePlayerService).doGet("/api/userProfile/", null, token);
		wePlayerController.readUserProfile(token);
	}

//	@Test
//	public void testPOST() throws Exception {
//
//		String body = "{\"ccUserID\": \"314\",\"birthdate\": \"1981-04-12 00:00:00\",\"address\": \"\",\"city\": \"\",\"country\": \"\",\"zipCode\": \"\",\"referredPilot\": \"Trento\",\"languages\": [\"Italian\",\"English\"],\"developer\": false,\"skills\": [],\"userTags\": [\"developer\"]}";
//
//		CloseableHttpClient mockClient = Mockito.mock(CloseableHttpClient.class);
//
//		URIBuilder uriBuilder = new URIBuilder(Config.getInstance().getWeLivePlayerServiceURL() + "/aggregate");
//
//		HttpPost mockHttpPost = new HttpPost(uriBuilder.build());
//		mockHttpPost.setEntity(new StringEntity(body));
//		mockHttpPost.addHeader("Content-Type", "application/json");
//		mockHttpPost.addHeader("Accept", "application/json");
//		mockHttpPost.addHeader("Authorization", token);
//
//		weLivePlayerService.setHttpClient(mockClient);
//
//		Mockito.doReturn(buildMockResponse()).when(mockClient).execute(mockHttpPost);
//
//		String response = weLivePlayerService.doPost("/aggregate", body, token, true);
//		System.out.println(response);
//	}

	
	@Test
	public void testGET() throws Exception {
		CloseableHttpClient mockClient = Mockito.mock(CloseableHttpClient.class);

		HttpResponse mHttpResponseMock = Mockito.mock(HttpResponse.class);
		StatusLine mStatusLineMock = Mockito.mock(StatusLine.class);
		HttpEntity mHttpEntityMock = Mockito.mock(HttpEntity.class);

		Mockito.when(mHttpResponseMock.getStatusLine()).thenReturn(mStatusLineMock);
		Mockito.when(mStatusLineMock.getStatusCode()).thenReturn(HttpStatus.SC_OK);
		Mockito.when(mHttpResponseMock.getEntity()).thenReturn(mHttpEntityMock);
		CloseableHttpResponse mResponse = buildMockResponse();
		Mockito.when(mockClient.execute(Mockito.isA(HttpGet.class))).thenReturn(mResponse);

		weLivePlayerService.setHttpClient(mockClient);

		String response = weLivePlayerService.doGet("/api/userProfile/", null, token);
//		System.out.println(response);
	}
	
	public static CloseableHttpResponse buildMockResponse() throws IOException {
		String body = "{\"ccUserID\": \"314\",\"birthdate\": \"1981-04-12 00:00:00\",\"address\": \"\",\"city\": \"\",\"country\": \"\",\"zipCode\": \"\",\"referredPilot\": \"Trento\",\"languages\": [\"Italian\",\"English\"],\"developer\": false,\"skills\": [],\"userTags\": [\"developer\"]}";

		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse closeableHttpResponse = httpClient.execute(new HttpGet("http://www.test.com"));
		closeableHttpResponse.setEntity(new StringEntity(body));
		closeableHttpResponse.setStatusCode(200);
		return closeableHttpResponse;
	}

}