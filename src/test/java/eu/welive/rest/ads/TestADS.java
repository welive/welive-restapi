/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads;

import java.io.IOException;
import java.text.ParseException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import eu.welive.rest.ads.client.RestClient;
import eu.welive.rest.ads.client.RestClientDefs;
import eu.welive.rest.exception.WeLiveException;

public class TestADS {
	private String userid="1";
	private String pilot =" AND "+GrayLogsTypes.ATRIBUTE_PILOT+":\"Bilbao\"";
	private String kpi="KPI4A";


	@Test
	public final void testGetKPI1_1() throws Exception {
		
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All","","",RestClientDefs.KPI1_1);
        test.getKPI1_1("","");
        Mockito.verify(test, Mockito.times(1)).callCDV("All","","",RestClientDefs.KPI1_1);    
		
	}

	@Test
	public final void testCallCDV() throws Exception {
		
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
		RestClient rc =Mockito.spy(new RestClient());
 		ResponseBuilder rb=Response.ok();
		Response response =rb.build();
        Mockito.doReturn(response).when(rc).getCdvKPI("All", "","",RestClientDefs.KPI1_1);
        test.callCDV("All", RestClientDefs.KPI1_1);
        Mockito.verify(test, Mockito.times(1)).callCDV("All",RestClientDefs.KPI1_1);    
		
		
	}

	@Test
	public final void testGetKPI1_2() throws WeLiveException, ParseException, IOException {
		
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin("type:\""+GrayLogsTypes.TYPE_IDEA_IMPLEMENTATION+"\"","","");
        test.getKPI1_2("", "", false);
        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin("type:\""+GrayLogsTypes.TYPE_IDEA_IMPLEMENTATION+"\"","","");     
        
	}
	

	@Test
	public final void testGetKPI1_2_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(GrayLogsTypes.ATRIBUTE_AUTHORID+":\""+userid+"\" AND  type:\""+GrayLogsTypes.TYPE_IDEA_IMPLEMENTATION+"\"","","");

        test.getKPI1_2_1("1", "","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(GrayLogsTypes.ATRIBUTE_AUTHORID+":\""+userid+"\" AND  type:\""+GrayLogsTypes.TYPE_IDEA_IMPLEMENTATION+"\"","","");  	
    }

	@Test
	public final void testGetKPI1_1String() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All","","",RestClientDefs.KPI1_1);
        test.getKPI1_1("All","","");
        Mockito.verify(test, Mockito.times(1)).callCDV("All","","",RestClientDefs.KPI1_1);  
	}

	@Test
	public final void testGetKPI4_3() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All",RestClientDefs.KPI4_3);
        test.getKPI4_3("","");
        Mockito.verify(test, Mockito.times(1)).callCDV("All","","",RestClientDefs.KPI4_3);  
	}

	@Test
	public final void testGetKPI11_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All",RestClientDefs.KPI11_1);
        test.getKPI11_1();
        Mockito.verify(test, Mockito.times(1)).callCDV("All",RestClientDefs.KPI11_1);  
	}

	@Test
	public final void testGetKPI11_2() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All",RestClientDefs.KPI11_2);
        test.getKPI11_2();
        Mockito.verify(test, Mockito.times(1)).callCDV("All",RestClientDefs.KPI11_2);  
	}

	@Test
	public final void testGetKPI11_3() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All","","",RestClientDefs.KPI11_3);
        test.getKPI11_3("","");
        Mockito.verify(test, Mockito.times(1)).callCDV("All","","",RestClientDefs.KPI11_3);  
	}

	@Test
	public final void testGetKPI11_4() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All","","",RestClientDefs.KPI11_4);
        test.getKPI11_4("","");
        Mockito.verify(test, Mockito.times(1)).callCDV("All","","",RestClientDefs.KPI11_4);  
	}

	@Test
	public final void testGetKPI11_5() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        Mockito.doReturn(resultquery).when(test).callCDV("All","","",RestClientDefs.KPI11_5);
        test.getKPI11_5("","");
        Mockito.verify(test, Mockito.times(1)).callCDV("All","","",RestClientDefs.KPI11_5);  
	}

	@Test
	public final void testGetKPI2_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_DATASHET_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI2_1("","",false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI2_1_delta() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_DATASHET_REMOVED+"\" OR type:\""+GrayLogsTypes.TYPE_DATASHET_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI2_1_delta("","",false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI2_2() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_DATASHET_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI2_2("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI2_3() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_DATASHET_METADATA_ACCESSED+"\" OR type:\""+GrayLogsTypes.TYPE_DATASHET_METADATA_UPDATED+"\""+pilot;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI2_3("","","Bilbao",false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI2_3_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_USERID+":\""
				+ userid + "\" AND  (type:\""
				+ GrayLogsTypes.TYPE_DATASHET_METADATA_ACCESSED
				+ "\" OR type:\""
				+ GrayLogsTypes.TYPE_DATASHET_METADATA_UPDATED + "\")";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI2_3_1("1","","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
	}
	
	@Test
	public final void testGetKPI1_3StringStringStringBoolean() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\""+pilot;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3("","","Bilbao", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}
	

	@Test
	public final void testGetKPI1_3StringStringBoolean() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_3_deltaStringStringBoolean() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_REMOVED+"\" OR type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3_delta("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_3_2StringStringStringBoolean() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_REMOVED+"\""+pilot;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3_2("","","Bilbao", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_3_deltaStringStringStringBoolean() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="(type:\""+GrayLogsTypes.TYPE_ARTEFACT_REMOVED+"\" OR type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\")"+pilot;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3_delta("","","Bilbao", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_3_2StringStringBoolean() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_REMOVED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3_2("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}
	@Test
	public final void testGetKPI1_3_1_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_OWNER_ID+":\""+userid+"\" AND  type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3_1_1("1","","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_3_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_OWNER_ID+":\""+userid+"\" AND  type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_3_1("1","","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_4() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_IDEA_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_4("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_4_rem() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_IDEA_REMOVED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_4_rem("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_4_delta() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_IDEA_REMOVED+"\" OR type:\""+GrayLogsTypes.TYPE_IDEA_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_4_delta("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	
	@Test
	public final void testGetKPI1_4_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_AUTHORID+":\""+userid+"\" AND type:\""+GrayLogsTypes.TYPE_IDEA_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_4_1("1","","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_5() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_CHALLENGE_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_5("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_5_delta() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_CHALLENGE_REMOVED+"\" OR type:\""+GrayLogsTypes.TYPE_CHALLENGE_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_5_delta("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_5_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_AUTHORID + ":\"" + userid
				+ "\" AND type:\""
				+ GrayLogsTypes.TYPE_IDEA_PUBLISHED + "\" AND  _exists_: "+GrayLogsTypes.ATRIBUTE_CHALLENGE_ID;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_5_1("1","", "", true);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_7() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_NEEDPUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_7("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_7_delta() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_NEEDPUBLISHED+"\" OR type:\""+GrayLogsTypes.TYPE_NEEDREMOVED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_7_delta("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI1_7_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_AUTHORID + ":\"" + userid
				+ "\" AND type:\""
				+ GrayLogsTypes.TYPE_NEEDPUBLISHED + "\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI1_7_1("1","", "", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI3_2_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_OWNER_ID+":\""+userid+"\" AND  type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\" AND "+
				GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.PUBLIC_SERVICE_APPLICATION+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI3_2_1("1","", "");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI3_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\" AND "+
				GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.BUILDING_BLOCK+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI3_1("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI3_1delta() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="(type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\" OR type:\""+GrayLogsTypes.TYPE_ARTEFACT_REMOVED+"\") AND "+
				GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.BUILDING_BLOCK+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI3_1delta("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI4_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.PUBLIC_SERVICE_APPLICATION+"\"  "
				+ "AND type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI4_1("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI5_3() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_APP_DOWNLOAD+"\""+pilot;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI5_3("","", "Bilbao", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI5_4() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_APP_OPEN+"\""+pilot;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI5_4("","", "Bilbao", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI6_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query=GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.PUBLIC_SERVICE_APPLICATION+"\" AND "
				+ "type:\""+GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI6_1("","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI12_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_RATED+"\" AND "+
				GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.PUBLIC_SERVICE_APPLICATION+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI12_1("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI12_3() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_RATED+"\" AND "+
				GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.BUILDING_BLOCK+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI12_3("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI12_4() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_RATED+"\" AND "+
				GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE+":\""+GrayLogsTypes.DATASET+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI12_4("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI12_1_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_ARTEFACT_RATED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI12_1_1("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI12_2() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_IDEA_RATED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI12_2("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI12_6() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_NEED_RATED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI12_6("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI13_1() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_COMPONENT_SELECTED+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI13_1("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI13_2() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_CITY_SELECTED+"\""+GrayLogsTypes.PILOT_FILTER;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI13_2("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPI13_3() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_CONTACT_FORM+"\"";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPI13_3("","");

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPIcommon() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_QUESTIONNAIRE+"\" AND custom_kpi:\""+kpi+"\""+" "+GrayLogsTypes.APPNAME_FILTER;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPIcommon(kpi,"","",false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPIcommon2d() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_QUESTIONNAIRE+"\" AND custom_kpi:\""+kpi+"\""+" "+GrayLogsTypes.APPNAME_FILTER;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPIcommon2d(kpi,"","",false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPIcommonPilot() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_QUESTIONNAIRE+"\" AND custom_kpi:\""+kpi+"\" AND custom_pilot: Bilbao "+GrayLogsTypes.APPNAME_FILTER;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPIcommonPilot(kpi,"","","Bilbao",false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPIcommon2dPilot() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\""+GrayLogsTypes.TYPE_QUESTIONNAIRE+"\" AND custom_kpi:\""+kpi+"\" AND custom_pilot: Bilbao "+GrayLogsTypes.APPNAME_FILTER;

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPIcommon2dPilot(kpi,"","","Bilbao",false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}

	@Test
	public final void testGetKPIApp() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\"Objetive\" AND "+GrayLogsTypes.ATRIBUTE_CUSTOM_APP+":\"Auzonet\" ";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPIApp("Objetive","Auzonet","","","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}
	@Test
	public final void testGetKPIAppHelsinky() throws WeLiveException,ParseException, IOException {
		ADSRestfulAPI test = Mockito.spy(new ADSRestfulAPI());
        JSONObject resultquery = new JSONObject(String.format("{\"hits\": {\"hits\":[]}}"));
        String query="type:\"Objetive\" AND "+GrayLogsTypes.ATRIBUTE_APP_ID+":\"helsinkicitybikes\" ";

        Mockito.doReturn(resultquery).when(test).callQueryFromLoggin(query,"","");

        test.getKPIApp("Objetive","helsinkicitybikes","","","", false);

        Mockito.verify(test, Mockito.times(1)).callQueryFromLoggin(query,"","");  	
        
	}
}
