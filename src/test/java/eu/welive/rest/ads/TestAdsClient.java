/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.text.ParseException;

import javax.ws.rs.core.Response;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

import eu.welive.rest.ads.client.RestClient;

public class TestAdsClient {

	@Test
	public final void testGetCdvKPI() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_11.1/All/from/1970-01-01 00:00:00.000/to/1970-01-01 00:00:00.000";

        Mockito.doReturn(mockedResponse).when(test).Get(query);

        test.getCdvKPI("All","1970-01-01 00:00:00.000","1970-01-01 00:00:00.000","/dev/api/cdv/getKPI_11.1/");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
    }

	@Test
	public final void testGetKPI1_1() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_1.1/All";

        Mockito.doReturn(mockedResponse).when(test).Get(query);

        test.getKPI1_1("All");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
    }

	@Test
	public final void testGetKPI4_3() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_4.3/All";

        Mockito.doReturn(mockedResponse).when(test).Get(query);
        
        test.getKPI4_3("All");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
	}

	@Test
	public final void testGetKPI11_1() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_11.1/All";

        Mockito.doReturn(mockedResponse).when(test).Get(query);
        
        test.getKPI11_1("All");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
	}

	@Test
	public final void testGetKPI11_2() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_11.2/All";

        Mockito.doReturn(mockedResponse).when(test).Get(query);
        
        test.getKPI11_2("All");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
	}

	@Test
	public final void testGetKPI11_3() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_11.3/All";

        Mockito.doReturn(mockedResponse).when(test).Get(query);
        
        test.getKPI11_3("All");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
	}

	@Test
	public final void testGetKPI11_4() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_11.4/All";

        Mockito.doReturn(mockedResponse).when(test).Get(query);
        
        test.getKPI11_4("All");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
	}

	@Test
	public final void testGetKPI11_5() throws IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String query="/dev/api/cdv/getKPI_11.5/All";

        Mockito.doReturn(mockedResponse).when(test).Get(query);
        
        test.getKPI11_5("All");

        Mockito.verify(test, Mockito.times(1)).Get(query);	
        
	}

	@Test
	@Ignore
	public final void testGetQueryFromLoggin() throws ParseException, IOException{
		RestClient test = Mockito.spy(new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
        String sUrl="/welive.logging/log/aggregate";
		String hData="{\"size\":100000,\"query\":{\"filtered\":{\"filter\":{\"bool\":{\"must\":{\"range\":{\"timestamp\":{\"include_lower\":true,\"include_upper\":true,\"from\":\"1970-01-01 00:00:00.000\",\"to\":\"2017-01-12 12:47:25.090\"}}}}},\"query\":{\"query_string\":{\"query\":\"\",\"allow_leading_wildcard\":false}}}},\"from\":0,\"sort\":[{\"timestamp\":{\"order\":\"desc\"}}]}";

        Mockito.doReturn(mockedResponse).when(test).Post(sUrl, hData);
        
        test.getQueryFromLoggin("", "1970-01-01 00:00:00.000", "2017-01-12 12:47:25.090");

        Mockito.verify(test, Mockito.times(1)).Post(sUrl, hData);
        
	}
	@Test
	public final void testPost() throws ParseException, IOException {
		//RestClient test = new RestClient("test", "test");
		RestClient test = Mockito.spy( new RestClient("test", "test"));
		Response mockedResponse = Mockito.mock(Response.class);
		Mockito.doReturn(mockedResponse).when(test).Post("", "");
		test.Post("", "");
		Mockito.verify(test, Mockito.times(1)).Post("", "");
		//assertEquals(true, result!=null);

	}

	@Test
	public final void testGet() throws ParseException , IOException {
		//RestClient test = new RestClient();
		RestClient test = Mockito.spy( new RestClient());
		Response mockedResponse = Mockito.mock(Response.class);
		Mockito.doReturn(mockedResponse).when(test).Get("");
		test.Get("");
		Mockito.verify(test, Mockito.times(1)).Get("");//assertEquals(true, result!=null);

	}

/*
	@Test
	public final void testInsertLog() throws ParseException {
		RestClient test = Mockito.spy(new RestClient());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(test).insertLog("","");

        test.insertLog("","");

        Mockito.verify(test, Mockito.times(1)).insertLog("","");   
        
	}

	@Test
	public final void testLogin() {
		RestClient test = Mockito.spy(new RestClient());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(test).login("xxxxxxxxx", "xxxxxxxxxxxxx");

        test.login("xxxxxxxxxxxxxxxx", "xxxxxxxxxxx");

        Mockito.verify(test, Mockito.times(1)).login("xxxxxxxxxxx", "xxxxxxxxxxxxxxxx");
        
	}
*/
}
