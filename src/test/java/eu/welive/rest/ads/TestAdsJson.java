/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Test;

import eu.welive.rest.ads.client.json.Query;

public class TestAdsJson {

	@Test
	public final void testQueryIntIntStringBooleanString() {
		Query test= new Query("");
		assertEquals(true, test.toString()!=null);
	}
	@Test
	public final void testQueryString() {
		Query test= new Query("");
		assertEquals(true, test!=null);
	}

	@Test
	public final void testQueryStringString() throws ParseException {
		Query test= new Query("", "1970-01-01 00:00:00.000");
		assertEquals(true, test!=null);
	}

	@Test
	public final void testQueryStringStringString() throws ParseException {
		Query test= new Query("","1970-01-01 00:00:00.000","1970-01-01 00:00:00.000");
		assertEquals(true, test!=null);
	}

}
