/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads;

import static org.junit.Assert.*;

import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import eu.welive.rest.ads.Utils;

public class TestUtils {
	String testHits="[{\"_index\":\"graylog2_0\",\"_type\":\"message\",\"_source\":{\"gl2_remote_ip\":\"127.0.0.1\",\"custom_appname\":\"Bilbozkatu\",\"gl2_remote_port\":48612,\"streams\":[],\"custom_value\":5,\"custom_kpi\":\"KPI4A\",\"source\":\"127.0.0.1\",\"type\":\"QuestionnaireFilled\",\"message\":\"QuestionnaireFilled KPI4A\",\"gl2_source_input\":\"563736c6e4b0fcce111f2d12\",\"custom_pilot\":\"Bilbao\",\"appId\":\"in-app\",\"gl2_source_node\":\"9270ddb6-613a-452e-9350-0a30a4b0951e\",\"_id\":\"a09600b0-7dd5-11e6-862f-f23c91670a9f\",\"custom_responsesetid\":257,\"timestamp\":\"2016-09-18 19:25:15.000\"},\"_id\":\"a09600b0-7dd5-11e6-862f-f23c91670a9f\",\"sort\":[1474226715000],\"_score\":null},{\"_index\":\"graylog2_0\",\"_type\":\"message\",\"_source\":{\"gl2_remote_ip\":\"127.0.0.1\",\"custom_appname\":\"Bilbon\",\"gl2_remote_port\":46354,\"streams\":[],\"custom_value\":5,\"custom_kpi\":\"KPI4A\",\"source\":\"127.0.0.1\",\"type\":\"QuestionnaireFilled\",\"message\":\"QuestionnaireFilled KPI4A\",\"gl2_source_input\":\"563736c6e4b0fcce111f2d12\",\"custom_pilot\":\"Bilbao\",\"appId\":\"in-app\",\"gl2_source_node\":\"9270ddb6-613a-452e-9350-0a30a4b0951e\",\"_id\":\"8d89a810-7dcf-11e6-862f-f23c91670a9f\",\"custom_responsesetid\":256,\"timestamp\":\"2016-09-18 18:41:46.000\"},\"_id\":\"8d89a810-7dcf-11e6-862f-f23c91670a9f\",\"sort\":[1474224106000],\"_score\":null},{\"_index\":\"graylog2_0\",\"_type\":\"message\",\"_source\":{\"gl2_remote_ip\":\"127.0.0.1\",\"custom_appname\":\"Bilbozkatu\",\"gl2_remote_port\":32914,\"streams\":[],\"custom_value\":4,\"custom_kpi\":\"KPI4A\",\"source\":\"127.0.0.1\",\"type\":\"QuestionnaireFilled\",\"message\":\"QuestionnaireFilled KPI4A\",\"gl2_source_input\":\"563736c6e4b0fcce111f2d12\",\"custom_pilot\":\"Bilbao\",\"appId\":\"in-app\",\"gl2_source_node\":\"9270ddb6-613a-452e-9350-0a30a4b0951e\",\"_id\":\"039c7dc0-6926-11e6-8551-f23c91670a9f\",\"custom_responsesetid\":232,\"timestamp\":\"2016-08-23 11:37:47.000\"},\"_id\":\"039c7dc0-6926-11e6-8551-f23c91670a9f\",\"sort\":[1471952267000],\"_score\":null}]";

	@Test
	public final void testGetStringJSONObjectString() {
		JSONObject object= new JSONObject();
		String key="key";
		String value="value";
		
		object.put(key, value);
		String result=null;
		result=Utils.getString(object, "otro");
		result=Utils.getString(object, key, "otro");
		result=Utils.getString(object, key);
		result=Utils.getString(object, key, value);
		assertEquals(true, result!=null);
	
	}

	@Test
	public final void testGetInt() {
		JSONObject object= new JSONObject();
		String key="key";
		
		object.put(key, 10);
		int result=-1;
		result=Utils.getInt(object, "otro");
		result=Utils.getInt(object, key);
		assertEquals(true, result==10);
	}
/*
	@Test
	public final void testGetStackedHitsPerField() throws ParseException {
		JSONArray hits= new JSONArray(testHits);
		JSONObject result=Utils.getStackedHitsPerField(hits, "custom_appname", "QuestionnaireFilled");
		assertEquals(true, result!=null);
	}
*/
	@Test
	public final void testGet2DDatasetJSONArrayStringString() throws ParseException {
		JSONArray hits= new JSONArray(testHits);
		JSONObject result=Utils.get2DDataset(hits, "custom_appname", "custom_value");
		assertEquals(true, result!=null);
	}

	@Test
	public final void testGet2DDatasetJSONArrayStringStringStringString() throws ParseException {
		JSONArray hits= new JSONArray(testHits);
		JSONObject result=Utils.get2DDataset(hits, "custom_appname", "custom_value", "QuestionnaireFilled", "QuestionnaireFilled");
		assertEquals(true, result!=null);
	}

	@Test
	public final void testGetHitsPerFieldJSONArrayStringStringBooleanStringString() throws ParseException {
		JSONArray hits= new JSONArray(testHits);
		JSONObject result=Utils.getHitsPerField(hits, "custom_appname", "", true, "", "");
		assertEquals(true, result!=null);
	}

	@Test
	public final void testGetHitsPerFieldJSONArrayStringStringStringStringBooleanStringString() throws ParseException {
		JSONArray hits= new JSONArray(testHits);
		JSONObject result=Utils.getHitsPerField(hits, "custom_appname", "", "QuestionnaireFilled", "QuestionnaireFilled", true, "", "");
		assertEquals(true, result!=null);
	}

}
