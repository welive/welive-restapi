/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;
import eu.welive.rest.exception.WeLiveException;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

//import com.codahale.metrics.ConsoleReporter.Builder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.cdv.CDV.AuthType;
import eu.welive.rest.config.Config;
import eu.welive.rest.de.DE;
import eu.welive.rest.mkp.Pilot;



@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientBuilder.class, Entity.class, Config.class, CDV.class, IOUtils.class})
@PowerMockIgnore("javax.net.ssl.*")

public class TestCDV extends JerseyTest{
	
	private static String token = "FAKE-TOKEN";
	Pilot pilotID;
	AuthType cdvType;
	private static String from = "from";
	private static String to = "to";
	private static int ccUserID = 5;
	UpdateProfile profile;
	UserProfile prof;
	CreateProfile profile2;
	//private static String url = "url";

	private static String dataid = "data";
	private static String uid = "uid";
	private static String clientid = "clientid";
	private PushEvent[] profile3; 
	private String tags = "tags";
	
	@Override
	protected Application configure() {
		return new TestApplication();
	}
	
	/*@Test(expected = WeLiveException.class)
	public void testReadConfig() throws WeLiveException{
		PowerMockito.mockStatic(Config.class);
		

		Mockito.spy(new CDV());
	}*/
	
	//@Override
    //protected javax.ws.rs.core.Application configure() {
      //  return new TestApplication();
    //}
	
	
/*	@Test(expected = WeLiveException.class) 
	public void testReadConfig() throws WeLiveException, IOException, Exception {
        PowerMockito.mockStatic(Config.class);
        //PowerMockito.when(Config.getInstance()).thenThrow(IOException.class);
        Mockito.spy(new UpdateProfile());
        Mockito.spy(new PushEvent());
        Mockito.spy(new UserProfile());
        Mockito.spy(new CreateProfile());
        Mockito.spy(new CDV()); 
        
        //Mockito.spy(new CDV());
       
    }*/    
	
	
	@Before
    public void setUp() throws URISyntaxException, IOException {
        CDV cdv = Mockito.spy(CDV.class);
       //doreturn().when(cdv).delete(Matchers.anyString(),Matchers.anyString())).thenReturn(null);
       Mockito.doReturn(null).when(cdv).delete(Matchers.anyString(),Matchers.anyString()); 
       Mockito.doReturn(null).when(cdv).invoke(Matchers.anyString(),Matchers.anyString());
       
        
        
        //UDEUSTO_DE_URL = Config.getInstance().getUdeustoDEURL();
    }
    @Test
    public void testGetKPI_111() throws Exception{
    	
    	String baseurl = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath();
    	KPI_111_Response response = new KPI_111_Response();
    	
    	response.setBilbao(null);
    	response.setHelsinki(null);
    	response.setNovisad(null);
    	response.setTrento(null);
    	response.getBilbao();
    	response.getHelsinki();
    	response.getNovisad();
    	response.getTrento();
    	
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
        pilotID = Pilot.All;
        String url = baseurl + "/getkpi111/pilotid/" + pilotID;
    	 Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
    	 cdv.getKPI_111(token, pilotID);
    	 //System.out.println(mockedResponse); 
    	 Mockito.verify(cdv,Mockito.times(1)).invoke(url, token);	 
	 }
	    
    @Test
	 public void testInvalidGetKPI_111() throws Exception{
	    	CDV cdv = Mockito.spy(new CDV());
	        pilotID = Pilot.All;
	     
	        //String url = "url";
	    	
	    	 Mockito.doReturn(null).when(cdv).getKPI_111(token, pilotID);
	    	 cdv.getKPI_111(token, pilotID);
	    	 //System.out.println(mockedResponse); 
	    	 Mockito.verify(cdv,Mockito.times(1)).getKPI_111(token, pilotID);
	    	 
	 }
    
    @Test
    public void testGetKPI_11() throws Exception{
    	
        pilotID = Pilot.All;
        
        cdvType = AuthType.Basic;
        
    	
    KPI_11_Response resp = new KPI_11_Response ();
    	resp.setBilbao(1);
    	resp.setHelsinki(2);
    	resp.setNovisad(3);
    	resp.setTrento(4);
    	resp.getBilbao();
    	resp.getHelsinki();
    	resp.getNovisad();
    	resp.getTrento();
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getkpi11/pilotid/" + pilotID + "/from/" + from + "/to/" + to;
    	
        CDV cdv = Mockito.spy(new CDV());
        //CDV cdv2 = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	//Response mockedResponse2 = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
    	cdv.getKPI_11(token, pilotID, from, to);
    	 
    	//cdv.invoke(token, token);
        
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url, token);
    	
      
    	
		
    	
    }
    
    
   
    
    @Test
    public void testGetKPI_112() throws Exception{
    	
    	
    
        pilotID = Pilot.All;
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getkpi112/pilotid/" + pilotID;
        CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
    	cdv.getKPI_112(token, pilotID);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url, token);
    }
    
    @Test
    public void testInvalidGetKPI_112() throws Exception{
    	
        pilotID = Pilot.All;
        
        KPI_112_Response r = new KPI_112_Response();
        
        r.setBilbao(null);
        r.setHelsinki(null);
        r.setNovisad(null);
        r.setTrento(null);
        r.getBilbao();
        r.getHelsinki();
        r.getNovisad();
        r.getTrento();
    	
        CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).getKPI_112(token, pilotID);
    	cdv.getKPI_112(token, pilotID);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).getKPI_112(token, pilotID);
    }
    
    @Test
    public void testGetKPI_43() throws Exception{
    	
        pilotID = Pilot.All;
    	String url  = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getkpi43/pilotid/" + pilotID + "/from/" + from + "/to/" + to;
        CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
    	cdv.getKPI_43(token, pilotID, from, to);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url, token);
    }
    
    @Test
    public void testInvalidGetKPI_43() throws Exception{
    	
        pilotID = Pilot.All;
    	
        CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).getKPI_43(token, pilotID, from, to);
    	cdv.getKPI_43(token, pilotID, from, to);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).getKPI_43(token, pilotID, from, to);
    }
    
    @Test
    public void testGetUserMetadata() throws Exception{
    	
        
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath()  + "/getusermetadata/ccuserid/5"; 
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
    	cdv.getUserMetadata(token, ccUserID);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url, token); //invoke(url, token);
    }
    
    @Test
    public void testGetInvalidUserMetadata() throws Exception{
    	
        
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	//String url =  "url";
    	Mockito.doReturn(null).when(cdv).getUserMetadata(token, ccUserID);
    	cdv.getUserMetadata(token, ccUserID);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).getUserMetadata(token, ccUserID);
    }
    
    @Test
    public void testGetUserProfile() throws Exception{
    	
    	prof = new UserProfile();
    	
    	prof.setAddress("address");
    	prof.setBirthdate("birthdate");
    	prof.setCcUserID(23);
    	prof.setCity("city");
    	prof.setCountry("country");
    	prof.setEmail("email");
    	prof.setGender("gender");
    	prof.setIsDeveloper(true);
    	prof.setLanguages(null);
    	prof.setLastKnownLocation(null);
    	prof.setName("name");
    	prof.setProfileData(null);
    	prof.setReferredPilot(null);
    	prof.setSkills(null);
    	prof.setSurname("surname");
    	prof.setUsedApps(null);
    	prof.setUserTags(null);
    	prof.setZipCode(null);
    	prof.getAddress();
    	prof.getBirthdate();
    	prof.getCcUserID();
    	prof.getCity();
    	prof.getCountry();
    	prof.getEmail();
    	prof.getGender();
    	prof.getIsDeveloper();
    	prof.getLanguages();
    	prof.getLastKnownLocation();
    	prof.getName();
    	prof.getProfileData();
    	prof.getReferredPilot();
    	prof.getSkills();
    	prof.getSurname();
    	prof.getUsedApps();
    	prof.getUserTags();
    	prof.getZipCode();
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getuserprofile";
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
    	
    	cdv.getUserProfile(token);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url, token);
    }
    
    @Test
    public void testInvalidGetUserProfile() throws Exception{
    	
    	prof = new UserProfile();
    	
    	prof.setAddress("address");
    	prof.setBirthdate("birthdate");
    	prof.setCcUserID(23);
    	prof.setCity("city");
    	prof.setCountry("country");
    	prof.setEmail("email");
    	prof.setGender("gender");
    	prof.setIsDeveloper(true);
    	prof.setLanguages(null);
    	prof.setLastKnownLocation(null);
    	prof.setName("name");
    	prof.setProfileData(null);
    	prof.setReferredPilot(null);
    	prof.setSkills(null);
    	prof.setSurname("surname");
    	prof.setUsedApps(null);
    	prof.setUserTags(null);
    	prof.setZipCode(null);
    	prof.getAddress();
    	prof.getBirthdate();
    	prof.getCcUserID();
    	prof.getCity();
    	prof.getCountry();
    	prof.getEmail();
    	prof.getGender();
    	prof.getIsDeveloper();
    	prof.getLanguages();
    	prof.getLastKnownLocation();
    	prof.getName();
    	prof.getProfileData();
    	prof.getReferredPilot();
    	prof.getSkills();
    	prof.getSurname();
    	prof.getUsedApps();
    	prof.getUserTags();
    	prof.getZipCode();
    	
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).getUserProfile(token);
    	cdv.getUserProfile(token);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).getUserProfile(token);
    }
    
    @Test
    public void testGetUsersSkills() throws Exception{
    	
       //Response = UserSkill.class
    	UserSkill skill = new UserSkill();
    	
    	skill.setCcUserID(4);
    	skill.getCcUserID();
    	skill.setLanguages(null);
    	skill.getLanguages();
    	skill.setSkills(null);
    	skill.getSkills();
    	//String url = "url";
    	CDV cdv = Mockito.spy(new CDV());
    	String url =  Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getusersskills";
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
    	
    	cdv.getUsersSkills(token);
    	
    	 
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url, token);
    }
    
    @Test
    public void testInvalidGetUsersSkills() throws Exception{
    	
        //Response = UserSkill.class
     	UserSkill skill = new UserSkill();
     	
     	skill.setCcUserID(4);
     	skill.getCcUserID();
     	skill.setLanguages(null);
     	skill.getLanguages();
     	skill.setSkills(null);
     	skill.getSkills();
     	//String url = "url";
     	CDV cdv = Mockito.spy(new CDV());
     	//Response mockedResponse = Mockito.mock (Response.class);
     	Mockito.doReturn(null).when(cdv).getUsersSkills(token);
     	cdv.getUsersSkills(token);
     	
     	 
     	Mockito.verify(cdv,Mockito.times(1)).getUsersSkills(token);
     }
    
    @Test
    public void testUpdateUser() throws Exception{
    	
    	
    	/*profile.setAddress("address");
    	DMYUpdateDate birthdate;
    	birthdate = new DMYUpdateDate();
		profile.setBirthdate(birthdate);
		profile.setCcUserID(2);
		profile.setCity("city");
		profile.setCountry("country");
		profile.setIsMale(true);
		profile.setLanguages(null);
		profile.setUserTags(null);
		profile.setZipCode("zip");
    	profile.getAddress();
    	profile.getCity();
    	profile.getCcUserID();
    	profile.getBirthdate();
    	profile.getCountry();
    	profile.getIsMale();
    	profile.getLanguages();
    	profile.getUserTags();
    	profile.getZipCode();*/
    	
    	CDV cdv = Mockito.spy(new CDV());
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/updateuser";
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).post(url, token, profile);
    	cdv.updateUser(token, profile);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).post(url, token, profile);
    	System.out.println("Invoking "+url);
    	  
}  
    
    @Test
    public void testInvalidUpdateUser() throws Exception{
    	
    	UpdateProfile profile = Mockito.spy(new UpdateProfile());
    	profile.setAddress("address");
    	DMYUpdateDate birthdate;
    	birthdate = new DMYUpdateDate();
		profile.setBirthdate(birthdate);
		profile.setCcUserID(2);
		profile.setCity("city");
		profile.setCountry("country");
		profile.setIsMale(true);
		profile.setLanguages(null);
		profile.setUserTags(null);
		profile.setZipCode("zip");
    	profile.getAddress();
    	profile.getCity();
    	profile.getCcUserID();
    	profile.getBirthdate();
    	profile.getCountry();
    	profile.getIsMale();
    	profile.getLanguages();
    	profile.getUserTags();
    	profile.getZipCode();
   
    	
    	CDV cdv = Mockito.spy(new CDV());
    	//String url =  "url";
    	
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).updateUser(token, profile);
    	cdv.updateUser(token, profile);
    	 
    	Mockito.verify(cdv,Mockito.times(1)).updateUser(token, profile);
     
    	  
}
    
    @Test
    public void testAddUser () throws Exception{
    	
    	DMYdate birthdate = new DMYdate();
    	CreateProfile profile2 =  Mockito.spy(new CreateProfile());
    	List<String> languages = new ArrayList <String>();
    	List<String> userTags = new ArrayList <String>();
    	profile2.setAddress("address");
    	profile2.setCcUserID(ccUserID);
    	profile2.setCity(token);
    	profile2.setCountry("country");
    	profile2.setEmail("email");
    	profile2.setFirstName("name");
    	profile2.setIsDeveloper(true);
    	profile2.setIsMale(false);
    	profile2.setLastName("lastname");
    	profile2.setLiferayScreenName("screen");
    	profile2.setReferredPilot("trento");
    	profile2.setZipCode("90100");
    	profile2.getAddress();
    	profile2.getCcUserID();
    	profile2.getCity();
    	profile2.getCountry();
    	profile2.getEmail();
    	profile2.getFirstName();
    	profile2.getIsDeveloper();
    	profile2.getIsMale();
    	profile2.getLastName();
    	profile2.getLiferayScreenName();
    	profile2.getReferredPilot();
    	profile2.getZipCode();
    	
    	profile2.setBirthdate(birthdate);
    	profile2.setLanguages(languages);
    	profile2.setUserTags(userTags);
    	profile2.getBirthdate();
    	profile2.getLanguages();
    	profile2.getUserTags();
    	
    	profile2.setRole(null);
    	profile2.getRole();
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/adduser";
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).post(url, token, profile2);
    	cdv.addUser(token, profile2);
    	
    	
    	
    	Mockito.verify(cdv,Mockito.times(1)).post(url, token, profile2);
    }
    
    @Test
    public void testInvalidAddUser () throws Exception{
    	
    	CreateProfile profile2 =  Mockito.spy(new CreateProfile());
    	
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).addUser(token, profile2);
    	cdv.addUser(token, profile2);
    	
    	
    	
    	Mockito.verify(cdv,Mockito.times(1)).addUser(token, profile2);
    } 
    
    /*@Test
    public void testdeleteUser() throws Exception{
    	
    	//CreateProfile profile2 =  Mockito.spy(new CreateProfile());
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/userdatadelete/dataid/" + dataid + "/uid/" + uid + "/clientid/" + clientid;
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).delete (url, token);
    	cdv.deleteUserData(token, dataid, uid, clientid);
    	
    	
    	
    	Mockito.verify(cdv,Mockito.times(1)).delete (url, token);
    } */
    
    @Test
    public void testInvalidDeleteUser() throws Exception{
    	
    	//CreateProfile profile2 =  Mockito.spy(new CreateProfile());
    	
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).deleteUserData(token, dataid,uid,clientid);
    	cdv.deleteUserData(token, dataid,uid,clientid);
    	
    	
    	
    	Mockito.verify(cdv,Mockito.times(1)).deleteUserData(token, dataid, uid,clientid);
    } 
    
    @Test
    public void testPush() throws Exception 
    {
    	
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/push";
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).post(url, token, profile3);
    	cdv.push(token, profile3);
    	Mockito.verify(cdv,Mockito.times(1)).post(url, token, profile3);
    	
    }
    
    @Test
    public void testInvalidPush() throws Exception 
    {
    	
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/push";
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).post(url, token, profile3);
    	cdv.push(token, profile3);
    	Mockito.verify(cdv,Mockito.times(1)).post(url, token, profile3);
    	
    }
    
    @Test
    public void testgetKPI113() throws Exception 
    {
    	
    	
    	pilotID = Pilot.All;
    	KPI_113_Response re = new KPI_113_Response();
    	
    	re.setBilbao(null);
    	re.setHelsinki(null);
    	re.setNovisad(null);
    	re.setTrento(null);
    	re.getBilbao();
    	re.getNovisad();
    	re.getTrento();
    	re.getUusimaa();
    			
    	
    	String url =Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getkpi113/pilotid/" + pilotID + "/from/" + from + "/to/" + to;
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url,token);
    	cdv.getKPI_113(token, pilotID, from, to);
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url,token);
    	
    }
    
    @Test
    public void testInvalidgetKPI113() throws Exception 
    {
    	
    	pilotID = Pilot.All;
    	
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).getKPI_113(token, pilotID, from, to);
    	cdv.getKPI_113(token, pilotID, from, to);
    	Mockito.verify(cdv,Mockito.times(1)).getKPI_113(token, pilotID, from, to);
    	
    }
    
    @Test
    public void testgetKPI114() throws Exception
    {
    	
    	KPI_114_Response res = new KPI_114_Response();
    	res.setBilbao(null);
    	res.setHelsinki(null);
    	res.setNovisad(null);
    	res.setTrento(null);
    	res.getBilbao();
    	res.getNovisad();
    	res.getTrento();
    	res.getUusimaa();
    	
    	pilotID = Pilot.All;
    			
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getkpi114/pilotid/" + pilotID + "/from/" + from + "/to/" + to;

    	
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url,token);
    	cdv.getKPI_114(token, pilotID, from, to);
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url,token);
    	
    }
    
    @Test
    public void testInvalidgetKPI114() throws Exception
    {
    	
    	pilotID = Pilot.All;
    	
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).getKPI_114(token, pilotID, from, to);
    	cdv.getKPI_114(token, pilotID, from, to);
    	Mockito.verify(cdv,Mockito.times(1)).getKPI_114(token, pilotID, from, to);
    	
    }
    
    @Test
    public void testgetKPI115() throws Exception
    {
    	
    	
    	pilotID = Pilot.All;
    			
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getkpi115/pilotid/" + pilotID + "/from/" + from + "/to/" + to;
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url,token);
    	cdv.getKPI_115(token, pilotID, from, to);
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url,token);
    	
    }
    
    @Test
    public void testInvalidgetKPI115() throws Exception
    {
    	
    	pilotID = Pilot.All;
    	
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).getKPI_115(token, pilotID, from, to);
    	cdv.getKPI_115(token, pilotID, from, to);
    	Mockito.verify(cdv,Mockito.times(1)).getKPI_115(token, pilotID, from, to);
    	
    }
    
    @Test
    public void testgetUserData() throws Exception
    {
    	
    	
    	pilotID = Pilot.All;
    			
    	
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/userdataget/dataid/" + dataid;
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).invoke(url,token);
    	cdv.getUserData(token, dataid); 
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url,token); 
    	
    }
    
    @Test
    public void testInvalidGetUserData() throws Exception 
    {
    	
    	pilotID = Pilot.All;
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/userdataget/dataid/" + dataid;
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).invoke(url,token);
    	cdv.getUserData(token, dataid); 
    	Mockito.verify(cdv,Mockito.times(1)).invoke(url,token); 
    	
    }
    
    @Test
    public void testUserDataUpdate() throws Exception
    {
    	
    	
    	pilotID = Pilot.All;
    			
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/userdataupdate";
    	
    	CDV cdv = Mockito.spy(new CDV());
    	Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(cdv).post(url, token, null);
    	cdv.userDataUpdate(token, null);
    	Mockito.verify(cdv,Mockito.times(1)).post(url, token, null); 
    	
    }
    
    @Test
    public void testInvalidUserDataUpdate() throws Exception
    {
    	
    	pilotID = Pilot.All;
    	
    	CDV cdv = Mockito.spy(new CDV());
    	//Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(null).when(cdv).userDataUpdate(token, null); 
    	cdv.userDataUpdate(token, null); 
    	Mockito.verify(cdv,Mockito.times(1)).userDataUpdate(token, null);
    	
    }
    
    @Test
    public void testUserDataSearch() throws Exception 
    
    	{
        	
        	
        	pilotID = Pilot.All;
        			
        	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/userdatasearch/tags/" + tags;
        	
        	CDV cdv = Mockito.spy(new CDV());
        	Response mockedResponse = Mockito.mock (Response.class);
        	Mockito.doReturn(mockedResponse).when(cdv).post(url, token, "");
        	cdv.getUserDataSearch(token, tags);
        	Mockito.verify(cdv,Mockito.times(1)).post(url, token, ""); 
        	
        
    	 
    }
    
    @Test
    public void testUserDataDelete() throws Exception 
    
    	{
        	
        	
       
        			
        	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/userdatadelete/dataid/" + dataid + "/uid/" + uid + "/client_id/" + clientid;
        	
        	CDV cdv = Mockito.spy(new CDV());
        	Response mockedResponse = Mockito.mock (Response.class);
        	Mockito.doReturn(mockedResponse).when(cdv).delete(url, token);
        	cdv.deleteUserData(token, dataid, uid, clientid);
        	Mockito.verify(cdv,Mockito.times(1)).delete(url, token);
        	
        
    	 
    }
    
    @Test
    public void testKPI111() throws Exception 
    
    	{
        	
        	
       pilotID=Pilot.All;
        			
        	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath() + "/getkpi111/pilotid/" + pilotID;
        	
        	CDV cdv = Mockito.spy(new CDV());
        	Response mockedResponse = Mockito.mock (Response.class);
        	Mockito.doReturn(mockedResponse).when(cdv).invoke(url, token);
        	cdv.getKPI_111(token, pilotID);
        	Mockito.verify(cdv,Mockito.times(1)).invoke(url, token);
        	
        
    	 
    }
    
   /* @Test
    public void testDelete() throws Exception
    {
    		CDV cdv = Mockito.spy(new CDV());
    		Client client = Mockito.mock(Client.class);
    		WebResource webResource = Mockito.mock(WebResource.class);
    		AuthType auth = Mockito.mock(AuthType.class);
    		ClientResponse clientResp = Mockito.mock(ClientResponse.class);
    		Response resp = Mockito.mock(Response.class);
    		com.sun.jersey.api.client.WebResource.Builder mockedBuilder = Mockito.mock(com.sun.jersey.api.client.WebResource.Builder.class);
    		Mockito.doReturn(webResource).when(client).resource(Matchers.anyString());
    		Mockito.doReturn(clientResp).doReturn(mockedBuilder).when(webResource).accept(MediaType.WILDCARD).delete(ClientResponse.class);
    		Mockito.doReturn(webResource).when(webResource).accept(MediaType.WILDCARD);
    		Mockito.doReturn(clientResp).when(webResource).delete(ClientResponse.class);
    		Mockito.doReturn(resp).when(resp).getEntity();
    		
    		
    		Response response = cdv.delete("url", "token");
    		
    		Mockito.verify(client,Mockito.times(1)).resource(Matchers.anyString());
    		Mockito.verify(webResource,Mockito.times(1)).accept(MediaType.WILDCARD).delete(ClientResponse.class);
    		Mockito.verify(resp,Mockito.times(1)).getEntity();
    		
    		assertEquals(resp, response);
    		
    		
    		
    	
    }
  */
}
