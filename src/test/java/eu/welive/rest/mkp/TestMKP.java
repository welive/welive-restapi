/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.fasterxml.jackson.core.JsonProcessingException;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.mkp.MKP.AuthType;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientBuilder.class, Entity.class, Config.class, MKP.class, IOUtils.class})
@PowerMockIgnore("javax.net.ssl.*")

public class TestMKP extends JerseyTest{
	
	SetupArtefactRequest body;
	NewService service;
	ArtefactType artefactTypes;
	Pilot pilotID;
	private static Integer level = 3;
	private static String datasetId =  "datasetId";
	private static String token = "fake-token";
	private static long artefactid = 37263712;
	private static String ids = "fake-ids";
	private static String keywords = "keywords";
	ValidationRequest request;
	ValidationResponse resp;
	ArtefactResponse respo;
	//private String baseurl = "https://test.welive.eu/api/jsonws//MarketplaceStore-portlet.mkp";	
	@Override
	protected Application configure() {
		return new Application();
	}
	
	@Test
    public void testCreateService() throws Exception{
    	
	   service = new NewService();
	   
	   service.setEndpoint("end");
	   service.setCategory(null);
	   service.getCategory();
       service.getCcUserId();
       service.getDescription();
       service.getEndpoint();
       service.getLanguage();
       service.getProviderName();
       service.getResourceRdf();
       service.getTitle();
       service.getWebpage();
       
       AuthType  auth =  AuthType.Basic;
       
       MKP mkp = Mockito.spy(new MKP());
        
        
        JSONObject inputJSON = new JSONObject(String.format("{\"error\": true, \"message\": \"ccUid is mandatory\"}"));

        String responseString = "{\"error\": true, \"message\": \"ccUid is mandatory\"}";
       
        JSONObject responseJSON = new JSONObject(responseString);
   	    Response mockedResponse = Mockito.mock (Response.class);
   	    String url = "/create-service" + "/title/" + "title";
    	Mockito.doReturn(responseString).when(mockedResponse).readEntity(String.class);
    	 Mockito.doReturn(mockedResponse).when(mkp).newServiceAction(service, token);
    	 mkp.createService(token, service);
    	
    	 Mockito.verify(mkp,Mockito.times(1)).createService(token, service);
    	 }
	
	@Test
    public void testNewService() throws Exception{
		
		service = new NewService();
		   
		ServiceType a;
		a = ServiceType.REST;
		
		
		
		service.setCcUserId(3);
		service.setDescription("descr");
		service.setProviderName("prov");
		service.setResourceRDF("resource");
		service.setTitle("title");
		service.setEndpoint("end");
		   service.setCategory(a);
		   service.getCategory();
	       service.getCcUserId();
	       String description = service.getDescription();
	       service.getEndpoint();
	       //service.getLanguage();
	       String providername=service.getProviderName();
	       service.getResourceRdf();
	       service.getTitle();
	       String webpage = service.getWebpage();
    	
		MKP mkp = Mockito.spy(new MKP());
	     String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/create-service"+"/title/"+service.getTitle()+"/description/"+description+"/category/"+service.getCategory()+"/resource-rdf/"+"cmVzb3VyY2U="+"/endpoint/"+"ZW5k"+"/-webpage/"+"/cc-user-id/"+service.getCcUserId().toString()+"/provider-name/"+providername+"/-language";
	  Response mockedResponse = Mockito.mock (Response.class);
   	Mockito.doReturn(mockedResponse).when(mkp).post(url, token, null);
   	 mkp.newServiceAction(service, token);
   	 Mockito.verify(mkp,Mockito.times(1)).post(url, token, null);
    	 }
	
	@Test
	public void testValidation() throws Exception {
		
		Authentication au = new Authentication();
		
		au.setProtectd(true);
		au.setSecurities(null);
		au.getProtecte();
		au.getSecurities();
		
		SetupType type;
		
		type = SetupType.bblocks;
		type = SetupType.psa;
		
		type.toString();
		
		MKP mkp = Mockito.spy(new MKP());
        
        NewDataset dm = new NewDataset();
        dm.setResourceRDF("");
        dm.getResourceRdf();
        JSONObject inputJSON = new JSONObject(String.format("{\"error\": true, \"message\": \"resourceRdf is mandatory\"}"));

        String responseString = "{\"error\": true, \"message\": \"resourceRdf is mandatory\"}";
       
        JSONObject responseJSON = new JSONObject(responseString);
   	    Response mockedResponse = Mockito.mock (Response.class);
   	    
   	    Mockito.doReturn(responseString).when(mockedResponse).readEntity(String.class);
   	    
	}
	
	
	@Test
    public void testGetAllArtefactsLevel() throws Exception{
    	
	  
	
		  pilotID = Pilot.All;
	      artefactTypes = ArtefactType.All;
	      Integer level = 3;
    	 
	      MKP mkp = Mockito.spy(new MKP());
	     String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/get-all-artefacts/pilot-id/" + pilotID + "/artefact-types/" + artefactTypes + "/level/" + level;
	   	    Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(mkp).invoke(url, token);
    	 mkp.getAllArtefacts(token, pilotID, artefactTypes, level);
    	 Mockito.verify(mkp,Mockito.times(1)).invoke(url, token);
    	 
    	 artefactTypes.toString();
    	 pilotID.toString();
    	 }
	
	@Test
    public void testGetArtefact() throws Exception{
    
		
		
		MKP mkp = Mockito.spy(new MKP());
	   	
   	    Response mockedResponse = Mockito.mock (Response.class);
   	    
   	    String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/get-artefact/artefactid/"+ artefactid;
   	    
    	Mockito.doReturn(mockedResponse).when(mkp).invoke(url, token);
    	 mkp.getArtefact(token, artefactid);
    	 Mockito.verify(mkp,Mockito.times(1)).invoke(url, token);
    	 
    	 }
	
	@Test
    public void testGetArtefactIds() throws Exception{
    
		MKP mkp = Mockito.spy(new MKP());
	   	String url= Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/get-artefacts-by-ids/ids/" + ids;
   	    Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(mkp).invoke(url,token);
    	 mkp.getArtefactsByIds(token, ids);
    	 Mockito.verify(mkp,Mockito.times(1)).invoke(url,token);
    	 
    	 }
	
	/*@Test
    public void testGetArtefactKeywords() throws Exception{
    
		MKP mkp = Mockito.spy(new MKP());
		
		String url = baseurl + "/get-artefacts-by-keywords/keywords/" + keywords + "/artefact-types/" + artefactTypes.toString() + "/pilot/"+ pilotID.toString();
	   	
   	    Response mockedResponse = Mockito.mock (Response.class);
    	Mockito.doReturn(mockedResponse).when(mkp).invoke(url, token);
    	 mkp.getArtefattiByKeywords(token, null, pilotID, artefactTypes);
    	 Mockito.verify(mkp,Mockito.times(1)).invoke(url, token);
    	 
    	 }*/
	
	
	
	@Test 
	public void testCreateDataset () throws WeLiveException, Exception {
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		
		NewDataset dataset = new NewDataset();
		
		Integer ccUserId = 5;
		dataset.setCcUserId(ccUserId);
		String description = "description";
		dataset.setDescription(description);
		dataset.setLanguage(null);
		String providerName = "chiara";
		dataset.setProviderName(providerName);
		String resourceRDF = "rdf";
		dataset.setResourceRDF(resourceRDF);
		String title = "myTitle";
		dataset.setTitle(title);
		String webpage = "webpage";
		dataset.setWebpage(webpage);
		dataset.getCcUserId();
		dataset.getDatasetId();
		dataset.getTitle();
		dataset.getLanguage();
		dataset.getDescription();
		dataset.setDatasetId(datasetId);
		dataset.getProviderName();
		dataset.getResourceRdf();
		dataset.getTitle();
		dataset.getWebpage();
		
		Artefact artefact = new Artefact ("title","desc","ab","trento","cre","page","lang",null,null,null,null);
		
		
		
		String url = "/create-dataset/dataset-id/datasetId/title/title/description/description/resource-rdf/resourceRDF/webpage/webpage/cc-user-id/5/language/language";
		
		String path = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + url;
		JSONObject json = new JSONObject(String.format("{\"error\": true, \"message\": \"datasetId is mandatory\"}"));
		AuthType authType = AuthType.Basic;
		
		authType = mkp.authType(token); 
		 String responseString = "{\"error\": true, \"message\": \"datasetId is mandatory\"}";
	       
	        //JSONObject responseJSON = new JSONObject(responseString);
	   	    
	   	    
	    	Mockito.doReturn(responseString).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(mockedResponse).when(mkp).createDataset(token, dataset);
    	mkp.createDataset(token, dataset);
    	 
  
        
    	Mockito.verify(mkp,Mockito.times(1)).createDataset(token, dataset);
    	
    	ArtefactResponse aa = new ArtefactResponse();
    	
    	aa.setArtefacts(null);
    	aa.getArtefacts();
    	
    	ArtefactObj obj = new ArtefactObj();
    	
    	obj.setArtefactId(null);
    	obj.setName("name");
    	obj.setDescription("description");
    	obj.setInterfaceOperation("int");
    	obj.setType("type");
    	obj.setTypeId(null);
    	obj.setRating(12.32);
    	obj.setUrl("url");
    	obj.setEId(null);
    	obj.setTags(null);
    	obj.setComments(null);
    	obj.setAuthentication(null);
    	obj.setLinkImage("link");
    	obj.setHasmapping(true);
    	obj.getArtefactId();
    obj.getName();
    obj.getDescription();
    obj.getInterfaceOperation();
    obj.getType();
    obj.getTypeId();
    obj.getRating();
    obj.getUrl();
    obj.getEId();
    obj.getTags();
    obj.getComments();
    obj.getAuthentication();
    obj.getLinkImage();
    obj.geHasmapping();

	
	}
	
	@Test
	public void testValiddatasetResquest () throws Exception{
		MKP mkp = Mockito.spy(new MKP());
		NewDataset dataset = new NewDataset();
				
		JSONObject json = new JSONObject(String.format("{\"error\": true, \"message\": \"datasetId is mandatory\"}"));
		dataset.setCcUserId(-1);
		dataset.getCcUserId();
		mkp.isValidNewDatasetRequest(dataset);
	}
	
	
	
	@Test
    public void testValidate() throws Exception{
    
		MKP mkp = Mockito.spy(new MKP());
	   	request=new ValidationRequest();
	   	resp = new ValidationResponse();
	   	
	   
	   	
	   	
	   	Response mockedResponse = Mockito.mock (Response.class);
	   	Response mockedResponse2 = Mockito.mock (Response.class);
	   	request.setResourcerdf("resourse");
   	    request.getResourcerdf();
   	   
   	    resp.setCode(3);
   	    resp.setIsValid(true);
   	    resp.setMessage("message");
   	    resp.setType("type");
   	    resp.getCode();
   	    resp.getIsValid();
   	    resp.getMessage();
   	    resp.getType();
   	    resp.getCode();
   	    resp.getIsValid();
   	    resp.getMessage();
   	    resp.getType();
   	    
    	
    	
    	Mockito.doReturn(mockedResponse2).when(mkp).validateRDFResourceAction(request);
    	mkp.isValidValidationRequest(request);
    	mkp.validateRDFResource(request);
    	
    	Mockito.verify(mkp,Mockito.times(1)).validateRDFResourceAction(request);
    
    	
    	 
    	 
    	 }
	
	
	@Test 
	public void testValidateService () throws WeLiveException, Exception {
		
		ValidationServiceRequest request = new ValidationServiceRequest();
		request.setType(null);
		request.setUrl(null);
		request.getType();
		request.getUrl();
		
		ValidationServiceResponse response = new ValidationServiceResponse();
		
		response.setCode(null);
		response.setMessage(null);
		response.setValid(true);
		response.getCode();
		response.getMessage();
		response.getValid();
		
		
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/validateService";
		
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(mkp).post(url, token, request);
		mkp.validateService(token, request);
		Mockito.verify(mkp,Mockito.times(1)).post(url, token, request);
				
		
	}
	
	@Test 
	public void testUpdateDataset () throws Exception {
		
		ServiceOffering serv = new ServiceOffering();
		serv.setDescription(null);
		serv.setTitle(null);
		serv.setUrl(null);
		serv.getDescription();
		serv.getTitle();
		serv.getUrl();
		
		ServiceOffering serv2 = new ServiceOffering ("title","description","url");
		
		Dataset model = new Dataset();
		String datasetid = "datasetid";
		model.setAbstractDescription("description");
		model.setBusinessRoles(null);
		model.setCreated("created");
		model.setDescription("descritpion");
		model.setLang("lang");
		model.setPage("page");
		model.setPilot("trento");
		model.setServiceOfferings(null);
		model.setTags(null);
		model.setTitle("title");
		model.getAbstractDescription();
		model.getBusinessRoles();
		model.getCreated();
		model.getDescription();
		model.getLang();
		model.getLegalConditions();
		model.setLegalConditions(null);
		model.getPage();
		model.getPilot();
		model.getServiceOfferings();
		model.getTags();
		model.getTitle();
		model.writeAsJsonString();
		
		Dataset model2 = new Dataset ("title","description", "abs","pilot","created","page","lnguage",null,null,null,null);
		
		
		UpdateDatasetRequest request = new UpdateDatasetRequest();
		request.setDatasetid(datasetid);
		request.setHasmapping(true);
		request.setModel(model);
		request.getDatasetid();
		request.getHasmapping();
		request.getModel();
		request.setIsPrivate(true);
		request.getIsPrivate();
		
		
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/update-dataset";
		
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(mkp).post(url, token, request);
		mkp.updateDataset(token,request);
		Mockito.verify(mkp,Mockito.times(1)).post(url, token, request);
		
		
		
	}
	
	@Test 
	public void testGetAllArtefacts () throws WeLiveException, Exception {
		
		
		
		pilotID = Pilot.All;
		artefactTypes = ArtefactType.All;
	
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/get-all-artefacts/pilot-id/" + pilotID +"/artefact-types/"+ artefactTypes;
		
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(mkp).invoke(url, token);
		mkp.getAllArtefacts(token, pilotID, artefactTypes);
		Mockito.verify(mkp,Mockito.times(1)).invoke(url, token);
		
		
	}
	
	@Test 
	public void testGetAllArtefactsByKeywords () throws WeLiveException, Exception {
		
		
		
		pilotID = Pilot.All;
		artefactTypes = ArtefactType.All;
	
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/get-artefacts-by-keywords/keywords/" + keywords + "/artefact-types/"+ artefactTypes.toString() + "/pilot/" + pilotID.toString();
		
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(mkp).invoke(url, token);
		mkp.getArtefactsByKeywords(token, keywords, pilotID, artefactTypes);
		Mockito.verify(mkp,Mockito.times(1)).invoke(url, token);
		
		
	}
	
	@Test 
	public void testGetAllArtefactsByKeywordsLevel () throws WeLiveException, Exception {
		
		
		
		pilotID = Pilot.All;
		artefactTypes = ArtefactType.All;
	
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/get-artefacts-by-keywords/keywords/" + keywords + "/artefact-types/"+ artefactTypes.toString() + "/pilot/" + pilotID.toString() + "/level" + level.toString();
		System.out.print("\nurl di test" + url);
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(mkp).invoke(url, token);
		mkp.getArtefactsByKeywords(token, keywords, pilotID, artefactTypes, level);
		Mockito.verify(mkp,Mockito.times(1)).invoke(url, token);
		
		
	}
	
	@Test 
	public void testSetupArtefact () throws WeLiveException, Exception {
		
		
		body = new SetupArtefactRequest();
			
		body.setCcuserid(null);
		body.setMashupid(null);
		body.setMockupid(null);
		body.setIdeaid(null);
		body.getCcuserid();
		body.getIdeaid();
		body.getMashupid();
		body.getMockupid();
		body.setType(null);
		body.getType();
		body.setLusdl(null);
		body.getLusdl();
		
	
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/setup-artefact";
		
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(mkp).post(url, token, body);
		mkp.setupArtefact(token, body);
		Mockito.verify(mkp,Mockito.times(1)).post(url, token, body);
		
		
	}
	
	@Test 
	public void testDatasetAction () throws WeLiveException, Exception {
		
		NewDataset d = new NewDataset();
		
		d.setDatasetId("dgeu");
		d.setTitle("title");
		d.setDescription("dec");
		d.setWebpage("d2VicGFnZQ==");
		d.setResourceRDF("cmVzb3VyY2U=");
		d.setCcUserId(4);
		d.setProviderName("prov");
		d.setLanguage(null);
		d.getCcUserId();
		d.getDatasetId();
		d.getDescription();
		d.getLanguage();
		d.getProviderName();
		d.getResourceRdf();
		d.getTitle();
		d.getWebpage();
		
		ValidationServiceResponse r = new ValidationServiceResponse();
		
		r.setCode("code");
		r.setMessage("message");
		r.setValid(true);
		r.getCode();
		r.getMessage();
		r.getValid();
	
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath() + "/create-dataset"+"/dataset-id/"+d.getDatasetId()+"/title/"+d.getTitle()+"/description/"+d.getDescription()+"/resource-rdf/"+"Y21WemIzVnlZMlU9"+"/webpage/"+"ZDJWaWNHRm5aUT09"+"/cc-user-id/"+d.getCcUserId()+"/provider-name/"+d.getProviderName()+"/-language";
		MKP mkp = Mockito.spy(new MKP());
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(mkp).post(url, token, null);
		mkp.newDatasetAction(d, token);
		Mockito.verify(mkp,Mockito.times(1)).post(url, token, null);
		
		
	}
	
	
	
}