/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.aac;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.fasterxml.jackson.databind.ObjectMapper;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Entity.class, ClientBuilder.class, Config.class, AACController.class })
@PowerMockIgnore("javax.net.ssl.*")
public class TestAAC extends JerseyTest {

	private static AACController aacController;
	private static AACService aacService;
	private static final String TOKEN = "Bearer 1234-1234-1234-1234";
	private static final String USER_ID = "some_user_id";
	private static final String SCOPE = "profile.basicprofile.me";
	private static final String CLIENT_ID = "some_client_id";
	private static final String CLIENT_SECRET = "some_client_secret";
	private static final String CODE = "some_code";
	private static final String REDIRECT_URI = "http://some_redirect_url.com";
	private static final String GRANT_TYPE = "refresh_token";
	private static final String REFRESH_TOKEN = "some_refresh_token";

	private static ObjectMapper mapper = new ObjectMapper();

	@Override
	protected Application configure() {
		return new TestApplication();
	}

	@Before
	public void setUp() throws Exception {
		aacController = new AACController();
		aacService = Mockito.spy(AACService.class);
		aacController.setAACService(aacService);
	}

	@Test
	public void testGenerateToken() throws Exception {

		String query = "client_id=some_client_id&client_secret=some_client_secret&grant_type=refresh_token&redirect_uri=http://some_redirect_url.com&code=some_code&refresh_token=some_refresh_token";

		Mockito.doReturn(TOKEN).when(aacService).doPost(null, "/oauth/token", query, null, true);
		Response responseController = aacController.generateToken(CLIENT_ID, CLIENT_SECRET, CODE, REDIRECT_URI,
				GRANT_TYPE, REFRESH_TOKEN);
		assertEquals(TOKEN, responseController.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).generateToken(query);

	}

	@Test(expected = WeLiveException.class)
	public void testGenerateTokenException() throws Exception {

		String query = "client_id=some_client_id";

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_BAD_REQUEST),
				"missing required params")).when(aacService)
				.doPost(null, "/oauth/token", query, null, true);
		aacController.generateToken(CLIENT_ID, CLIENT_SECRET, CODE, REDIRECT_URI, GRANT_TYPE, REFRESH_TOKEN);
		Mockito.verify(aacService, Mockito.times(1)).generateToken(query);

	}

	@Test
	public void testAccountProfileMe() throws Exception {

		String profile = "{\"accounts\": {\"google\": {\"eu.trentorise.smartcampus.givenname\": \"Test\",\"eu.trentorise.smartcampus.surname\": \"Test\",\"OIDC_CLAIM_email\": \"test@gmail.com\"}}}";

		Mockito.doReturn(profile).when(aacService).doGet("/accountprofile/me", null, TOKEN);
		Response responseProfile = aacController.findAccountProfile(TOKEN);
		assertEquals(profile, responseProfile.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).findAccountProfile(TOKEN);

	}

	@Test(expected = WeLiveException.class)
	public void testAllAccountProfiles() throws Exception {

		String userIds = "first_user_id," + USER_ID;
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "Not allowed")).when(aacService).doGet("/accountprofile/profiles?userIds=" + userIds, null, TOKEN);
		aacController.findAccountProfiles(TOKEN, userIds);

	}

	@Test(expected = WeLiveException.class)
	public void testAccountProfileException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doGet("/accountprofile/me", null, TOKEN);
		aacController.findAccountProfile(TOKEN);
		Mockito.verify(aacService, Mockito.times(1)).findAccountProfile(TOKEN);

	}

	@Test
	public void testBasicProfileMe() throws Exception {

		String profile = "{\"name\": \"Nawaz\",\"surname\": \"Khurshid\",\"socialId: \"-314\",\"userId\": \"314\"}";

		Mockito.doReturn(profile).when(aacService).doGet("/basicprofile/me", null, TOKEN);
		Response responseProfile = aacController.findProfile(TOKEN);
		assertEquals(profile, responseProfile.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).findProfile(TOKEN);

	}

	@Test
	public void testGetUser() throws Exception {

		String profile = "{\"name\": \"Nawaz\",\"surname\": \"Khurshid\",\"socialId: \"-314\",\"userId\": \"314\"}";

		Mockito.doReturn(profile).when(aacService).doGet("/basicprofile/all/" + USER_ID, null, TOKEN);
		Response responseProfile = aacController.getProfile(TOKEN, USER_ID);
		assertEquals(profile, responseProfile.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).getUser(USER_ID, TOKEN);

	}

	@Test(expected = WeLiveException.class)
	public void testGetUserException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN),
				"Not allowed, missing scope")).when(aacService)
				.doGet("/basicprofile/all/" + USER_ID, null, TOKEN);
		aacController.getProfile(TOKEN, USER_ID);
	}

	@Test
	public void testSearchUser() throws Exception {

		String profile = "{\"name\": \"Nawaz\",\"surname\": \"Khurshid\",\"socialId: \"-314\",\"userId\": \"314\"}";

		Mockito.doReturn(profile).when(aacService).doGet("/basicprofile/all", null, TOKEN);
		Response responseProfile = aacController.searchUsers(TOKEN, null);
		assertEquals(profile, responseProfile.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).searchUsers(null, TOKEN);

	}

	@Test(expected = WeLiveException.class)
	public void testSearcUserException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN),
				"Not allowed, missing scope")).when(aacService)
				.doGet("/basicprofile/all", null, TOKEN);
		aacController.searchUsers(TOKEN, null);
	}

	@Test(expected = WeLiveException.class)
	public void testAllBasicProfiles() throws Exception {

		String userIds = "first_user_id," + USER_ID;
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN),
				"Not allowed, missing scope")).when(aacService)
				.doGet("/basicprofile/profiles?userIds=" + userIds, null, TOKEN);
		aacController.findProfiles(TOKEN, userIds);

	}

	@Test(expected = WeLiveException.class)
	public void testBasicProfileException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doGet("/basicprofile/me", null, TOKEN);
		aacController.findProfile(TOKEN);
		Mockito.verify(aacService, Mockito.times(1)).findProfile(TOKEN);

	}

	@Test
	public void testResourceAccess() throws Exception {

		Mockito.doReturn("true").when(aacService).doGet("/resources/access", "scope=" + SCOPE, TOKEN);
		Response response = aacController.validateToken(TOKEN, SCOPE);
		assertEquals(Boolean.valueOf(response.getEntity().toString()), true);
		Mockito.verify(aacService, Mockito.times(1)).validateToken("scope=" + SCOPE, TOKEN);

	}
	
	@Test
	public void testCheckTokenAccess() throws Exception {

		Mockito.doReturn("true").when(aacService).doGet("/eauth/check_token?token=123", null, null);
		Response response = aacController.checkToken("123");
		assertEquals(Boolean.valueOf(response.getEntity().toString()), true);
		Mockito.verify(aacService, Mockito.times(1)).checkToken("123");

	}
	
	@Test
	public void testGetServicePermissions() throws Exception {

		Mockito.doReturn("true").when(aacService).doGet("/resources/permissions", null, null);
		Response response = aacController.getServicePermissions();
		assertEquals(Boolean.valueOf(response.getEntity().toString()), true);
		Mockito.verify(aacService, Mockito.times(1)).getServicePermissions();

	}

	@Test(expected = WeLiveException.class)
	public void testResourceAccessException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doGet("/resources/access", "scope=" + SCOPE, TOKEN);
		aacController.validateToken(TOKEN, SCOPE);

	}
	

	@Test
	public void testGetClientInfo() throws Exception {

		String clientInfo = "{\"clientId\": \"19bb2e16-9f20-4137-aa5a-76c011a9c2d1\",\"clientName\": \"test\"}";
		Mockito.doReturn(clientInfo).when(aacService).doGet("/resources/clientinfo", null, TOKEN);
		Response response = aacController.getClientInfo(TOKEN);
		assertEquals(clientInfo, response.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).getClientInfo(null, TOKEN);

	}

	@Test
	public void testGetAllClientInfo() throws Exception {

		String allClientInfo = "[{\"clientId\": \"19bb2e16-9f20-4137-aa5a-76c011a9c2d1\",\"clientName\": \"test\"},{\"clientId\": \"19bb2e16-9f20-4137-aa5a-76c011a9c2d1\",\"clientName\": \"test\"},{\"clientId\":\"1a724bac-7515-4e7c-bbf2-8a664b6a6008\",\"clientName\": \"swagger app\"},{\"clientId\": \"499a3674-acf0-4735-aaa7-8ffba94a2958\",\"clientName\": \"BBQ\"},{\"clientId\": \"e4cd499e-81ac-4240-bd5c-1d4680f2f99f\",\"clientName\": \"WeLive Player\"},{\"clientId\": \"19bb2e16-9f20-4137-aa5a-76c011a9c2d1\",\"clientName\": \"test\"}]";

		// OAuth
		Mockito.doReturn(allClientInfo).when(aacService).doGet("/resources/clientinfo/oauth", null, TOKEN);
		Response responseOAuth = aacController.getAllClientInfo(TOKEN);
		assertEquals(allClientInfo, responseOAuth.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).getAllClientInfo(null, TOKEN);

		// Basic Auth.
		Mockito.doReturn(allClientInfo).when(aacService).doGet("/resources/clientinfo/oauth/" + USER_ID, null, TOKEN);
		Response responseBasic = aacController.getAllClientInfoByUserId(TOKEN, USER_ID);
		assertEquals(allClientInfo, responseBasic.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).getAllClientInfoByUserId(USER_ID, TOKEN);

	}

	@Test(expected = WeLiveException.class)
	public void testGetAllClientInfoOAuthException() throws Exception {
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doGet("/resources/clientinfo/oauth", null, TOKEN);
		aacController.getAllClientInfo(TOKEN);

	}

	@Test(expected = WeLiveException.class)
	public void testGetAllClientInfoBasicAuthException() throws Exception {
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doGet("/resources/clientinfo/oauth/" + USER_ID, null, TOKEN);
		aacController.getAllClientInfoByUserId(TOKEN, USER_ID);

	}

	@Test(expected = WeLiveException.class)
	public void testGetClientInfoException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doGet("/resources/clientinfo", null, TOKEN);
		aacController.getClientInfo(TOKEN);
	}

	@Test
	public void testCRUDClientSpec() throws Exception {

		String clientSpec = "{\"clientId\": \"19bb2e16-9f20-4137-aa5a-76c011a9c2d1\",\"clientSecret\": \"992ebc96-0de6-4658-a571-60fed662294c\",\"clientSecretMobile\": \"a01b34c9-e9a6-40aa-bdda-72db2b842dbf\",\"name\": \"test\",\"redirectUris\": [\"http://localhost\"],\"grantedTypes\": [\"implicit\",\"refresh_token\",\"client_credentials\",\"authorization_code\"],\"nativeAppsAccess\": true,\"nativeAppSignatures\": null,\"browserAccess\": true,\"serverSideAccess\": true,\"identityProviders\": [\"google\",\"welive\",\"facebook\"],\"scopes\": [\"cdv.profile.me\",\"profile.basicprofile.me\",\"profile.accountprofile.me\"],\"ownParameters\": []}";
		ClientModel clientModel = mapper.readValue(clientSpec, ClientModel.class);

		// Create
		Mockito.doReturn(clientSpec).when(aacService).doPost(mapper.writeValueAsString(clientModel),
				"/resources/clientspec", null, TOKEN, true);
		Response responsePOST = aacController.createClientSpec(TOKEN, clientModel);
		assertEquals(clientSpec, responsePOST.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).createClientSpec(mapper.writeValueAsString(clientModel), TOKEN);

		// Read
		Mockito.doReturn(clientSpec).when(aacService).doGet("/resources/clientspec", null, TOKEN);
		Response responseGET = aacController.getClientSpec(TOKEN);
		assertEquals(clientSpec, responseGET.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).getClientSpec(TOKEN);

		// Update
		clientModel.setClientId("update_client_id");
		String clientSpecUpdated = mapper.writeValueAsString(clientModel);
		Mockito.doReturn(clientSpecUpdated).when(aacService).doPut(clientSpecUpdated, "/resources/clientspec", null,
				TOKEN, true);
		Response responsePUT = aacController.updateClientSpec(TOKEN, clientModel);
		assertEquals(clientSpecUpdated, responsePUT.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).updateClientSpec(clientSpecUpdated, TOKEN);

	}

	@Test(expected = WeLiveException.class)
	public void testGetClientSpecException() throws Exception {

		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doGet("/resources/clientspec", null, TOKEN);
		aacController.getClientSpec(TOKEN);
	}

	@Test(expected = WeLiveException.class)
	public void testCreateClientSpecException() throws Exception {

		String clientSpec = "{\"clientId\": \"19bb2e16-9f20-4137-aa5a-76c011a9c2d1\",\"clientSecret\": \"992ebc96-0de6-4658-a571-60fed662294c\",\"clientSecretMobile\": \"a01b34c9-e9a6-40aa-bdda-72db2b842dbf\",\"name\": \"test\",\"redirectUris\": [\"http://localhost\"],\"grantedTypes\": [\"implicit\",\"refresh_token\",\"client_credentials\",\"authorization_code\"],\"nativeAppsAccess\": true,\"nativeAppSignatures\": null,\"browserAccess\": true,\"serverSideAccess\": true,\"identityProviders\": [\"google\",\"welive\",\"facebook\"],\"scopes\": [\"cdv.profile.me\",\"profile.basicprofile.me\",\"profile.accountprofile.me\"],\"ownParameters\": []}";
		ClientModel clientModel = mapper.readValue(clientSpec, ClientModel.class);
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService)
				.doPost(mapper.writeValueAsString(clientModel), "/resources/clientspec", null, TOKEN, true);
		aacController.createClientSpec(TOKEN, clientModel);

	}

	@Test(expected = WeLiveException.class)
	public void testUpdateClientSpecException() throws Exception {
		String clientSpec = "{\"clientId\": \"19bb2e16-9f20-4137-aa5a-76c011a9c2d1\",\"clientSecret\": \"992ebc96-0de6-4658-a571-60fed662294c\",\"clientSecretMobile\": \"a01b34c9-e9a6-40aa-bdda-72db2b842dbf\",\"name\": \"test\",\"redirectUris\": [\"http://localhost\"],\"grantedTypes\": [\"implicit\",\"refresh_token\",\"client_credentials\",\"authorization_code\"],\"nativeAppsAccess\": true,\"nativeAppSignatures\": null,\"browserAccess\": true,\"serverSideAccess\": true,\"identityProviders\": [\"google\",\"welive\",\"facebook\"],\"scopes\": [\"cdv.profile.me\",\"profile.basicprofile.me\",\"profile.accountprofile.me\"],\"ownParameters\": []}";
		ClientModel clientModel = mapper.readValue(clientSpec, ClientModel.class);
		clientModel.setClientId("update_client_id");
		String clientSpecUpdated = mapper.writeValueAsString(clientModel);
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doPut(clientSpecUpdated, "/resources/clientspec", null, TOKEN, true);
		aacController.updateClientSpec(TOKEN, clientModel);
	}

	@Test
	public void testCreateUser() throws Exception {
		String extUser = "{\"username\": \"string@string.com\",\"name\": \"string\",\"surname\": \"string\",\"email\": \"string@string.com\"}";
		ExtUser user = mapper.readValue(extUser, ExtUser.class);
		String createdUser = mapper.writeValueAsString(user);

		Mockito.doReturn(createdUser).when(aacService).doPost(createdUser, "/extuser/create", null, TOKEN, true);
		Response responsePOST = aacController.createUser(TOKEN, user);
		assertEquals(createdUser, responsePOST.getEntity().toString());
		Mockito.verify(aacService, Mockito.times(1)).createExtUser(createdUser, TOKEN);

	}

	@Test(expected = WeLiveException.class)
	public void testCreateUserException() throws Exception {
		String extUser = "{\"username\": \"string@string.com\",\"name\": \"string\",\"surname\": \"string\",\"email\": \"string@string.com\"}";
		ExtUser user = mapper.readValue(extUser, ExtUser.class);
		String createdUser = mapper.writeValueAsString(user);
		Mockito.doThrow(new WeLiveException(Response.Status.fromStatusCode(HttpStatus.SC_FORBIDDEN), "invalid token")).when(aacService).doPost(createdUser, "/extuser/create", null, TOKEN, true);
		aacController.createUser(TOKEN, user);
	}
	
	@Test
	public void testRevokeToken() throws Exception {
		Mockito.doReturn(null).when(aacService).doDelete("/eauth/revoke/" + TOKEN, null, null, true);
		Response response = aacController.revokeToken(TOKEN);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		Mockito.verify(aacService, Mockito.times(1)).revokeToken(TOKEN);
	}
	
	@Test
	public void testRevokeTokenUsingClientId() throws Exception {
		Mockito.doReturn(null).when(aacService).doDelete("/eauth/revoke/" + CLIENT_ID + "/" + USER_ID, null, TOKEN, true);
		Response response = aacController.revokeTokenUsingCientIdUserId(TOKEN, CLIENT_ID, USER_ID);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		Mockito.verify(aacService, Mockito.times(1)).revokeTokenUsingClientUserId(CLIENT_ID, USER_ID, TOKEN);
	}
	
	@Test
	public void testResourcePermission() throws Exception {
		Mockito.doReturn("{}").when(aacService).getServicePermissions();
		Response response = aacController.getServicePermissions();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		Mockito.verify(aacService, Mockito.times(1)).getServicePermissions();
	}
	
	@Test
	public void testDeleteUserUsingBasicAuth() throws Exception {
		Mockito.doReturn(null).when(aacService).doDelete("/user/" + USER_ID, "cascade=true", TOKEN, true);
		Response response = aacController.deleteUserDataUsingBasicAuth(TOKEN, USER_ID, true);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
		Mockito.verify(aacService, Mockito.times(1)).deleteUsingBasicAuth(USER_ID, "cascade=true", TOKEN);
	}
	
	@Test(expected = Exception.class)
	public void testPOST() throws Exception {
		aacService.doPost("{}", "/aggregate", null, "", true);
		Mockito.verify(aacService, Mockito.times(1)).doPost("{}", "/aggregate", null, "", true);
	}

	@Test(expected = Exception.class)
	public void testPUT() throws Exception {
		aacService.doPut("{}", "/aggregate", null, "", true);
		Mockito.verify(aacService, Mockito.times(1)).doPut("{}", "/aggregate", null, "", true);
	}

	@Test(expected = Exception.class)
	public void testGET() throws Exception {
		aacService.doGet("/get/", "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", "");
		Mockito.verify(aacService).doGet("/get/", "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", "");
	}

	@Test(expected = Exception.class)
	public void testDELETEException() throws Exception {
		aacService.doDelete("/test/delete/", "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0", null, true);
		Mockito.verify(aacService).doDelete("/test/delete/", "type=&pattern=&msgPattern=&from=1&to=2&limit=1&offset=0",
				null, true);
	}
	
	@Test
	public void testGETMOCK() throws Exception {
		CloseableHttpClient mockClient = Mockito.mock(CloseableHttpClient.class);

		HttpResponse mHttpResponseMock = Mockito.mock(HttpResponse.class);
		StatusLine mStatusLineMock = Mockito.mock(StatusLine.class);
		HttpEntity mHttpEntityMock = Mockito.mock(HttpEntity.class);

		Mockito.when(mHttpResponseMock.getStatusLine()).thenReturn(mStatusLineMock);
		Mockito.when(mStatusLineMock.getStatusCode()).thenReturn(HttpStatus.SC_OK);
		Mockito.when(mHttpResponseMock.getEntity()).thenReturn(mHttpEntityMock);
		CloseableHttpResponse mResponse = buildMockResponse();
		Mockito.when(mockClient.execute(Mockito.isA(HttpGet.class))).thenReturn(mResponse);

		aacService.setHttpClient(mockClient);
		
		String response = aacService.doGet("/api/userProfile/", null, TOKEN);
			
	}
	
	public static CloseableHttpResponse buildMockResponse() throws ClientProtocolException, IOException {
		String body = "{\"ccUserID\": \"314\",\"birthdate\": \"1981-04-12 00:00:00\",\"address\": \"\",\"city\": \"\",\"country\": \"\",\"zipCode\": \"\",\"referredPilot\": \"Trento\",\"languages\": [\"Italian\",\"English\"],\"developer\": false,\"skills\": [],\"userTags\": [\"developer\"]}";

		CloseableHttpClient httpClient = HttpClients.createDefault();
		CloseableHttpResponse closeableHttpResponse = httpClient.execute(new HttpGet("http://www.test.com"));
		closeableHttpResponse.setEntity(new StringEntity(body));
		closeableHttpResponse.setStatusCode(200);
		return closeableHttpResponse;
	}

}