/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.remover;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactory;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactoryException;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANPackage;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CKANUtil.class, UserRemover.class, UpdateableFactory.class, UserRemovalJob.class})
public class UserRemovalJobTest {
	
	private static final String CREATOR_ID = "10";
	private static final String ORGANIZATION_ID = "1";
	
	private final static String MAPPING_JSON_SCHEMA = "{\r\n   \"mapping\":\"json_schema\",\r\n   \"schema\":{\r\n      \"tables\":[\r\n         {\r\n            \"key\":\"key1\",\r\n            \"name\":\"table1\",\r\n            \"table1\":[\r\n               {\r\n                  \"key1\": \"value1\",\r\n                  \"key2\": 10,\r\n                  \"key3\": \"2015-01-01\"\r\n               }\r\n            ]\r\n         },\r\n         {\r\n            \"key\":\"key2\",\r\n            \"name\":\"table2\",\r\n            \"table2\":[\r\n               {\r\n                  \"key1\": \"value1\",\r\n                  \"key2\": \"value2\",\r\n                  \"key3\": 5\r\n               }\r\n            ]\r\n         }\r\n      ]\r\n   }\r\n}";

	private CKANUtil ckanUtil;
	private JobExecutionContext jobContext;
	private UserRemovalJob userRemovalJob;

	@Before
	public void setUp() throws Exception {		
		ckanUtil = PowerMockito.mock(CKANUtil.class);
		
		PowerMockito.whenNew(CKANUtil.class)
			.withNoArguments()
			.thenReturn(ckanUtil);
		
		jobContext = Mockito.mock(JobExecutionContext.class);
		
		userRemovalJob = new UserRemovalJob();
	}
	
	public UserRemover prepareUserRemover() throws UserRemoverException {
		final UserRemover userRemover = PowerMockito.mock(UserRemover.class);
		PowerMockito.mockStatic(UserRemover.class);
		
		when(UserRemover.getInstance())
			.thenReturn(userRemover);
		
		return userRemover;
	}
	
	private Map<String, CKANPackage> createPackages(String mapping) {
		final Map<String, CKANPackage> packages = new HashMap<String, CKANPackage>();
		
		final List<CKANResource> resources = new ArrayList<CKANResource>();
		final CKANResource resourceInfo = new CKANResource("sample-resource-1", "sample-package-1", mapping, "{}");
		resources.add(resourceInfo);
	
		final CKANPackage packageInfo = new CKANPackage("sample-package-1", CREATOR_ID, ORGANIZATION_ID, resources);
		packages.put("sample-package-1", packageInfo);
		
		return packages;
	}
	
	private List<String> createAllPackages(Map<String, CKANPackage> packages) { 
		return new ArrayList<String>(packages.keySet()); 
	} 
	
	@Test
	public void testExecuteNoPendingUsers() throws CKANUtilException, UserRemoverException, JobExecutionException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING_JSON_SCHEMA);
		when(ckanUtil.getAllPackages())
			.thenReturn(createAllPackages(packages));
		
		final UserRemover userRemover = prepareUserRemover();
		
		when(userRemover.getAnonymizedUsers())
			.thenReturn(Collections.<PendingUser> emptySet());
		
		when(userRemover.getErasedUsers())
			.thenReturn(Collections.<PendingUser> emptySet());
		
		userRemovalJob.execute(jobContext);
	}
	
	private Set<PendingUser> createAnonUsers() {
		final Set<PendingUser> anonUsers = new HashSet<PendingUser>();
		anonUsers.add(new PendingUser("user-1", false));
		return anonUsers;
	}
	
	private Set<PendingUser> createErasedUsers() {
		final Set<PendingUser> erasedUsers = new HashSet<PendingUser>();
		erasedUsers.add(new PendingUser("user-1", true));
		return erasedUsers;
	}
	
	@Test
	public void testExecuteAnonUsers() throws CKANUtilException, UserRemoverException, DataSourceManagementException, DatabaseCreationException, DataSourceSecurityManagerException, UpdateableFactoryException, JobExecutionException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING_JSON_SCHEMA);
		when(ckanUtil.getAllPackages())
			.thenReturn(createAllPackages(packages));
		
		when(ckanUtil.getPackageInfo("sample-package-1"))
			.thenReturn(packages.get("sample-package-1"));
		
		final UserRemover userRemover = prepareUserRemover();
		
		when(userRemover.getAnonymizedUsers())
			.thenReturn(createAnonUsers());
		
		when(userRemover.getErasedUsers())
			.thenReturn(Collections.<PendingUser> emptySet());
		
		PowerMockito.mockStatic(UpdateableFactory.class);
		
		final SQLUpdateable updateable = prepareSQLUpdateable();
		
		when(UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq("sample-resource-1"), Matchers.eq(false)))
			.thenReturn(updateable);
		
		userRemovalJob.execute(jobContext);
		
		PowerMockito.verifyStatic(times(1));
		UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq("sample-resource-1"), Matchers.eq(false));
		
		final ArgumentCaptor<String> sqlCaptor = ArgumentCaptor.forClass(String.class);
		verify(updateable, times(1)).executeSQLUpdate(sqlCaptor.capture(), Matchers.eq(DataSourceSecurityManager.ADMIN_USER));
		
		assertEquals("UPDATE table1 SET _row_user_ = '-1' WHERE _row_user_='user-1'", sqlCaptor.getValue());
	}
	
	@Test
	public void testExecuteErasedUsers() throws CKANUtilException, UserRemoverException, DataSourceManagementException, DatabaseCreationException, DataSourceSecurityManagerException, UpdateableFactoryException, JobExecutionException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING_JSON_SCHEMA);
		when(ckanUtil.getAllPackages())
			.thenReturn(createAllPackages(packages));
		
		when(ckanUtil.getPackageInfo("sample-package-1"))
			.thenReturn(packages.get("sample-package-1"));
		
		final UserRemover userRemover = prepareUserRemover();
		
		when(userRemover.getAnonymizedUsers())
			.thenReturn(Collections.<PendingUser> emptySet());
		
		when(userRemover.getErasedUsers())
			.thenReturn(createErasedUsers());
		
		PowerMockito.mockStatic(UpdateableFactory.class);
		
		final SQLUpdateable updateable = prepareSQLUpdateable();
		
		when(UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq("sample-resource-1"), Matchers.eq(false)))
			.thenReturn(updateable);
		
		userRemovalJob.execute(jobContext);
		
		PowerMockito.verifyStatic(times(1));
		UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq("sample-resource-1"), Matchers.eq(false));
		
		final ArgumentCaptor<String> sqlCaptor = ArgumentCaptor.forClass(String.class);
		verify(updateable, times(1)).executeSQLUpdate(sqlCaptor.capture(), Matchers.eq(DataSourceSecurityManager.ADMIN_USER));
		
		assertEquals("DELETE FROM table1 WHERE _row_user_='user-1'", sqlCaptor.getValue());
	}

	private SQLUpdateable prepareSQLUpdateable() throws DataSourceManagementException {
		final List<TableInfo> tables = new ArrayList<TableInfo>();
		tables.add(new TableInfo("table1"));
		
		final DataSourceInfo dataSourceInfo = Mockito.mock(DataSourceInfo.class);
		when(dataSourceInfo.getTables())
			.thenReturn(tables);
		
		final SQLUpdateable updateable = Mockito.mock(SQLUpdateable.class);
		when(updateable.getInfo())
			.thenReturn(dataSourceInfo);
		return updateable;
	}
}
