/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.remover;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PendingUserTest {

	@Test
	public void testGetUserId() {
		final PendingUser user = new PendingUser("10", false);
		assertEquals("10", user.getUserId());
	}

	@Test
	public void testRemoveData() {
		final PendingUser user = new PendingUser("10", true);
		assertTrue(user.isDataRemoved());
	}
	
	@Test
	public void testLoadString() {
		final PendingUser user = new PendingUser();
		user.loadString("10:false");
		
		assertEquals("10", user.getUserId());
		assertFalse(user.isDataRemoved());
		
		user.loadString("20:true");
		
		assertEquals("20", user.getUserId());
		assertTrue(user.isDataRemoved());
	}

	@Test
	public void testEqualsObject() {
		final PendingUser userA = new PendingUser("10", false);
		final PendingUser userB = new PendingUser("10", true);
		
		assertTrue(userA.equals(userB));
		
		final PendingUser userC = new PendingUser("20", false);
		
		assertFalse(userA.equals(userC));
		
		final Object o = new Object();
		assertFalse(userA.equals(o));
	}

	@Test
	public void testToString() {
		final PendingUser userFalse = new PendingUser("10", false);
		assertEquals("10:false", userFalse.toString());
		
		final PendingUser userTrue = new PendingUser("10", true);
		assertEquals("10:true", userTrue.toString());
	}
}
