/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.remover;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UserRemoverTest {

	private UserRemover userRemover;
	
	@Before
	public void setUp() throws UserRemoverException {
		userRemover = UserRemover.getInstance();
		userRemover.setDataDir(".");
		userRemover.clearData();
	}
	
	@After
	public void setDown() throws UserRemoverException {
		UserRemover.getInstance().clearData();
	}
	
	@Test
	public void testAddUser() throws UserRemoverException {
		userRemover.addPendingUser("15", false);
		userRemover.addPendingUser("20", true);
		userRemover.addPendingUser("30", false);
		
		assertEquals(3, userRemover.getPendingUsers().size());
		
		userRemover.addPendingUser("20", true);
		userRemover.addPendingUser("30", true);
		
		assertEquals(3, userRemover.getPendingUsers().size());
	}

	@Test
	public void testRemoveUser() throws UserRemoverException {
		userRemover.addPendingUser("15", false);
		userRemover.addPendingUser("20", true);
		userRemover.addPendingUser("30", false);
		
		assertEquals(3, userRemover.getPendingUsers().size());
		
		userRemover.removePendingUser("20");

		assertEquals(2, userRemover.getPendingUsers().size());
		
		userRemover.removePendingUser("50");

		assertEquals(2, userRemover.getPendingUsers().size());
	}
	
	@Test
	public void testGetAnonymizedUsers() throws UserRemoverException {
		userRemover.addPendingUser("15", false);
		userRemover.addPendingUser("20", true);
		userRemover.addPendingUser("30", false);
		
		assertEquals(2, userRemover.getAnonymizedUsers().size());
	}
	
	@Test
	public void getGetForgottenUsers() throws UserRemoverException {
		userRemover.addPendingUser("15", false);
		userRemover.addPendingUser("20", true);
		userRemover.addPendingUser("30", false);
		
		assertEquals(1, userRemover.getErasedUsers().size());
	}
}
