/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.logging;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URISyntaxException;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;

import eu.welive.rest.logging.LoggingService;

public class DatasourceLoggerTest {

	private LoggingService loggingService;
	private DatasourceLogger datasourceLogger;
	
	private static final String USER_ID = "1"; 
	private static final String RESOURCE_ID = "12345-12345-12345-12345";
	private static final long TIMESTAMP = 1000000; 
	
	ArgumentCaptor<String> stringArgumentCaptor;
	ArgumentCaptor<JSONObject> jsonObjectArgumentCaptor;
	
	@Before
	public void setUp() { 
		loggingService = Mockito.mock(LoggingService.class);
		when(loggingService.createTimestamp())
			.thenReturn(TIMESTAMP);
		
		when(loggingService.createMainPayload(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenCallRealMethod();
		
		datasourceLogger = new DatasourceLogger(loggingService);
		
		stringArgumentCaptor = ArgumentCaptor.forClass(String.class);
		jsonObjectArgumentCaptor = ArgumentCaptor.forClass(JSONObject.class);
	}
	
	@Test
	public void testLogQuery() throws URISyntaxException {		
		final String sqlQuery = "SELECT * from table;";
		
		datasourceLogger.logQuery(RESOURCE_ID, sqlQuery, USER_ID);
		
		verify(loggingService, times(1)).sendLog(stringArgumentCaptor.capture(), jsonObjectArgumentCaptor.capture());
		
		assertEquals("ods", stringArgumentCaptor.getValue());
		
		final JSONObject expectedJSON = new JSONObject();
		expectedJSON.put("msg", "Resource queried");
		expectedJSON.put("appid", "ods");
		expectedJSON.put("type", "ResourceQueried");
		expectedJSON.put("timestamp", TIMESTAMP);
		
		final JSONObject customAttributes = new JSONObject();
		customAttributes.put("resourceid", RESOURCE_ID);
		customAttributes.put("query", sqlQuery);
		customAttributes.put("authorid", USER_ID);
		
		expectedJSON.put("custom_attr", customAttributes);
		
		JSONAssert.assertEquals(expectedJSON, jsonObjectArgumentCaptor.getValue(), true);
	}
	
	@Test
	public void testLogUpdate() throws URISyntaxException {
		datasourceLogger.logUpdate(RESOURCE_ID, USER_ID);
		
		verify(loggingService, times(1)).sendLog(stringArgumentCaptor.capture(), jsonObjectArgumentCaptor.capture());
		
		final JSONObject expectedJSON = new JSONObject();
		expectedJSON.put("msg", "Resource update");
		expectedJSON.put("appid", "ods");
		expectedJSON.put("type", "ResourceUpdated");
		expectedJSON.put("timestamp", TIMESTAMP);
		
		final JSONObject customAttributes = new JSONObject();
		customAttributes.put("resourceid", RESOURCE_ID);
		customAttributes.put("authorid", USER_ID);
		
		expectedJSON.put("custom_attr", customAttributes);
		
		JSONAssert.assertEquals(expectedJSON, jsonObjectArgumentCaptor.getValue(), true);
	}
}
