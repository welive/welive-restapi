/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.mockito.Matchers;

import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANPackage;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

public class CKANDataMocker {
	
	public final static String PACKAGE_ID = "1234-1234-1234-1234";
	public final static String INVALID_PACKAGE_ID = "0000-0000-0000-0000";
	public final static String CREATOR_USER_ID = "1111-1111-1111-1111";
	public final static String ORGANIZATION_ID = "4444-4444-4444-4444";
	
	public final static String RESOURCE_ID = "2222-2222-2222-2222";
	public final static String MAPPING = "{\r\n   \"mapping\": \"json\",\r\n   \"uri\": \"http://someuri/data.json\",\r\n   \"root\": \"results\",\r\n   \"key\": \"_id\",\r\n   \"table\": \"main\",\r\n   \"refresh\": 86400\r\n}";
	public final static String INVALID_MAPPING = "{\r\n   \"mapping\": \"invalid\"\r\n}";
	public final static String PERMISSIONS = "{}";
	
	public static CKANResource prepareValidData(CKANUtil ckanUtil) throws CKANUtilException {
		final CKANPackage ckanPackage = new CKANPackage(PACKAGE_ID, CREATOR_USER_ID, ORGANIZATION_ID, new ArrayList<CKANResource>());;	
		when(ckanUtil.getPackageInfo(Matchers.eq(PACKAGE_ID))).thenReturn(ckanPackage);
		
		final CKANResource ckanResource = new CKANResource(RESOURCE_ID, PACKAGE_ID, MAPPING, PERMISSIONS);
		when(ckanUtil.getResourceInfo(Matchers.eq(RESOURCE_ID))).thenReturn(ckanResource);
		
		return ckanResource;
	}
	
	public static CKANResource prepareUnrelatedPackage(CKANUtil ckanUtil) throws CKANUtilException {
		final CKANPackage ckanPackage = new CKANPackage(PACKAGE_ID, CREATOR_USER_ID, ORGANIZATION_ID, new ArrayList<CKANResource>());;	
		when(ckanUtil.getPackageInfo(Matchers.eq(PACKAGE_ID))).thenReturn(ckanPackage);
		
		final CKANResource ckanResource = new CKANResource(RESOURCE_ID, INVALID_PACKAGE_ID, MAPPING, PERMISSIONS);
		when(ckanUtil.getResourceInfo(Matchers.eq(RESOURCE_ID))).thenReturn(ckanResource);
		
		return ckanResource;
	}
	
	public static CKANResource prepareInvalidMapping(CKANUtil ckanUtil) throws CKANUtilException {
		final CKANPackage ckanPackage = new CKANPackage(PACKAGE_ID, CREATOR_USER_ID, ORGANIZATION_ID, new ArrayList<CKANResource>());;	
		when(ckanUtil.getPackageInfo(Matchers.eq(PACKAGE_ID))).thenReturn(ckanPackage);
		
		final CKANResource ckanResource = new CKANResource(RESOURCE_ID, PACKAGE_ID, INVALID_MAPPING, PERMISSIONS);
		when(ckanUtil.getResourceInfo(Matchers.eq(RESOURCE_ID))).thenReturn(ckanResource);
		
		return ckanResource;
	}
}
