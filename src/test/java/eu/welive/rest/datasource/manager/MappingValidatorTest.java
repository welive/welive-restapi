/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import eu.welive.rest.datasource.manager.MappingStatus.Status;
import eu.welive.rest.exception.WeLiveException;

public class MappingValidatorTest {
	
	private String VALID_MAPPING = "{" +
			"\"mapping\": \"json\"," + 
			"\"uri\": \"/json/data/jsondata1.json\"," +
			"\"root\": \"results\"," + 
			"\"key\": \"key1\"" + 
		"}";	
	
	private String INVALID_MAPPING = "{" +
			"\"mapping\": \"not_valid\"," + 
			"\"uri\": \"/json/data/jsondata1.json\"," +
			"\"root\": \"results\"," + 
			"\"key\": \"key1\"" + 
		"}";

	@Test
	public void testValidateMapping() throws WeLiveException {
		final MappingValidator validator = new MappingValidator();
		final MappingStatus mappingStatus = validator.validateMapping(VALID_MAPPING);
		assertEquals(Status.VALID, mappingStatus.getStatus());
		assertEquals("Connector description valid", mappingStatus.getMessage());
	}
	
	@Test
	public void testValidateMappingInvalid() throws WeLiveException {
		final MappingValidator validator = new MappingValidator();
		final MappingStatus mappingStatus = validator.validateMapping(INVALID_MAPPING);
		assertEquals(Status.ERROR, mappingStatus.getStatus());
	}
}
