/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;

import com.sciamlab.ckan4j.exception.CKANException;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.auth.user.oauth.OAuthUserInfo;
import eu.welive.rest.datasource.manager.AccessData.WeLiveRole;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

public class AccessDataManagerTest {

	private CKANUtil ckanUtil;
	
	private final static String USER_ID = "10";
	private final static String TOKEN = "1234-1234-1234-1234";
	
	private AccessDataManager accessDataManager;
	
	@Before
	public void setUp() {
		ckanUtil = Mockito.mock(CKANUtil.class);
		accessDataManager = new AccessDataManager();
	}

	@Test
	public void testGetAccessData() throws WeLiveException, CKANException, CKANUtilException {
		final CKANResource ckanResource = CKANDataMocker.prepareValidData(ckanUtil);
		
		when(ckanUtil.getUserRole(Matchers.eq(CKANDataMocker.PACKAGE_ID), Matchers.eq(USER_ID))).thenReturn(WeLiveRole.CREATOR);
		
		final UserInfo userInfo = new OAuthUserInfo(USER_ID, TOKEN);
		final AccessData accessData = accessDataManager.getAccessData(ckanUtil, userInfo, CKANDataMocker.PACKAGE_ID, CKANDataMocker.RESOURCE_ID);
		
		assertEquals(userInfo, accessData.getUserInfo());
		assertEquals(ckanResource, accessData.getResourceInfo());
	}
	
	@Test(expected=WeLiveException.class)
	public void testGetAccessDataInvalidMapping() throws WeLiveException, CKANException, CKANUtilException {
		CKANDataMocker.prepareInvalidMapping(ckanUtil);
		
		when(ckanUtil.getUserRole(Matchers.eq(CKANDataMocker.PACKAGE_ID), Matchers.eq(USER_ID))).thenReturn(WeLiveRole.CREATOR);
		
		final UserInfo userInfo = new OAuthUserInfo(USER_ID, TOKEN);
		accessDataManager.getAccessData(ckanUtil, userInfo, CKANDataMocker.PACKAGE_ID, CKANDataMocker.RESOURCE_ID);
	}
		
	@Test
	public void testGetConnector() throws WeLiveException, CKANException, CKANUtilException {
		CKANDataMocker.prepareValidData(ckanUtil);
		
		when(ckanUtil.getUserRole(Matchers.eq(CKANDataMocker.PACKAGE_ID), Matchers.eq(USER_ID))).thenReturn(WeLiveRole.CREATOR);
		
		final UserInfo userInfo = new OAuthUserInfo(USER_ID, TOKEN);
		final AccessData accessData = accessDataManager.getAccessData(ckanUtil, userInfo, CKANDataMocker.PACKAGE_ID, CKANDataMocker.RESOURCE_ID);
		
		final DataSourceConnector connector = accessData.getConnector();
		
		assertEquals(ConnectorType.json, connector.getType());
	}
}
