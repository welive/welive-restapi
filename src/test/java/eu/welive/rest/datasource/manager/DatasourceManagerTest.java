/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.sciamlab.ckan4j.exception.CKANException;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.JSONDumper;
import eu.iescities.server.querymapper.datasource.query.QueriableFactory;
import eu.iescities.server.querymapper.datasource.query.QueriableFactoryException;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactory;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactoryException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.status.DatasetStatus;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;
import eu.welive.rest.auth.MessageResponse;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.auth.user.oauth.OAuthUserInfo;
import eu.welive.rest.datasource.exception.QueryExecutionException;
import eu.welive.rest.datasource.exception.UpdateExecutionException;
import eu.welive.rest.datasource.logging.DatasourceLogger;
import eu.welive.rest.datasource.manager.AccessData.WeLiveRole;
import eu.welive.rest.exception.ForbiddenErrorException;
import eu.welive.rest.exception.UnexpectedErrorException;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ QueriableFactory.class, 
	UpdateableFactory.class, 
	DatasetStatusManager.class,
	JSONDumper.class })
public class DatasourceManagerTest {
	
	private final static String USER_ID = "10";
	private final static String TOKEN = "1234-1234-1234-1234";
	
	private DatasourceManager manager;
	private CKANUtil ckanUtil;
	private DatasourceLogger datasourceLogger;
	private AccessData accessData;
	
	@Before
	public void setUp() throws CKANUtilException, WeLiveException {
		ckanUtil = Mockito.mock(CKANUtil.class);
		datasourceLogger = Mockito.mock(DatasourceLogger.class);
		
		CKANDataMocker.prepareValidData(ckanUtil);
		
		final UserInfo userInfo = new OAuthUserInfo(USER_ID, TOKEN);
		
		final AccessDataManager accessDataManager = new AccessDataManager();
		accessData = accessDataManager.getAccessData(ckanUtil, userInfo, CKANDataMocker.PACKAGE_ID, CKANDataMocker.RESOURCE_ID);
		manager = new DatasourceManager(ckanUtil, accessData, datasourceLogger);
	}
	
	@Test
	public void testGetMapping() throws WeLiveException, CKANException, DataSourceConnectorException, CKANUtilException {
		final CKANResource ckanResource = CKANDataMocker.prepareValidData(ckanUtil);
		when(ckanUtil.getUserRole(Matchers.eq(CKANDataMocker.PACKAGE_ID), Matchers.eq(USER_ID))).thenReturn(WeLiveRole.CREATOR);
		
		final String mapping = manager.getMapping();
		
		final DataSourceConnector expected = ConnectorFactory.load(ckanResource.getMergedMapping());
		final DataSourceConnector loaded = ConnectorFactory.load(mapping);
		
		assertEquals(expected.getMapping(), loaded.getMapping());
	}
	
	private DataSourceInfo createDataSourceInfo() {
		final DataSourceInfo dataSourceInfo = new DataSourceInfo(ConnectorType.json);
		final TableInfo tableInfo = new TableInfo("test_table");
		
		final ColumnInfo columnInfo1 = new ColumnInfo("column1", "INTEGER", true);
		tableInfo.addColumnInfo(columnInfo1);
		
		final ColumnInfo columnInfo2 = new ColumnInfo("column2", "VARCHAR", false);
		tableInfo.addColumnInfo(columnInfo2);
		
		dataSourceInfo.addTable(tableInfo);
		
		return dataSourceInfo;
	}
	
	@Test
	public void testGetInfo() throws WeLiveException, CKANException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, CKANUtilException {
		CKANDataMocker.prepareValidData(ckanUtil);
		
		final SQLQueriable sqlQueriable = Mockito.mock(SQLQueriable.class);
		when(sqlQueriable.getInfo())
			.thenReturn(createDataSourceInfo());
		
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenReturn(sqlQueriable);
		
		final DataSourceInfo dataSourceInfo = manager.getInfo();
		assertEquals(1, dataSourceInfo.getTables().size());
		
		final TableInfo tableInfo = dataSourceInfo.getTable("test_table");
		assertEquals(2, tableInfo.getColumns().size());
		
		assertEquals("INTEGER", tableInfo.getColumn("column1").getType());
		assertTrue(tableInfo.getColumn("column1").isKey());
		
		assertEquals("VARCHAR", tableInfo.getColumn("column2").getType());
		assertFalse(tableInfo.getColumn("column2").isKey());
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testGetInfoForbiddenError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, WeLiveException {
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new DataSourceSecurityManagerException("Unauthorized acces to datasource"));
		
		manager.getInfo();
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testGetInfoUnexpectedError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, WeLiveException {
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new QueriableFactoryException("Could not access database"));
		
		manager.getInfo();
	}
	
	@Test(expected=QueryExecutionException.class)
	public void testGetInfoInternalQueryError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, WeLiveException {
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new DataSourceManagementException("Could query info data"));
		
		manager.getInfo();
	}
	
	private ResultSet createResultSet() {
		final ResultSet resultSet = new ResultSet();
		
		final Row row = new Row();
		row.addColumn("column1", 13);
		row.addColumn("column2", "some_text");
		
		resultSet.addRow(row);
		
		return resultSet;
	}
	
	@Test
	public void testExecuteSQLQuery() throws WeLiveException, CKANException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, TranslationException, CKANUtilException {
		CKANDataMocker.prepareValidData(ckanUtil);
		
		final SQLQueriable sqlQueriable = Mockito.mock(SQLQueriable.class);
		when(sqlQueriable.executeSQLSelect(Matchers.anyString(), Matchers.anyString(), Matchers.any(DataOrigin.class)))
			.thenReturn(createResultSet());
		
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenReturn(sqlQueriable);
		
		final ResultSet resultSet = manager.executeSQLQuery("select * from table", DataOrigin.ANY);
		assertEquals(1, resultSet.getRows().size());
		
		assertEquals(13, resultSet.getRows().get(0).getValue("column1"));
		assertEquals("some_text", resultSet.getRows().get(0).getValue("column2"));
		
		verify(datasourceLogger, times(1)).logQuery(Matchers.anyString(), Matchers.anyString(), Matchers.anyString());
	}
	
	@Test(expected=QueryExecutionException.class)
	public void testExecuteSQLQueryEmptyQuery() throws WeLiveException, CKANException, DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, CKANUtilException {
		CKANDataMocker.prepareValidData(ckanUtil);
		
		final SQLQueriable sqlQueriable = Mockito.mock(SQLQueriable.class);
		
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenReturn(sqlQueriable);
		
		manager.executeSQLQuery("", DataOrigin.ANY);
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testExecuteSQLQueryForbiddenError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, WeLiveException {
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new DataSourceSecurityManagerException("Unauthorized acces to datasource"));
		
		manager.executeSQLQuery("select * from table", DataOrigin.ANY);
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testExecuteSQLQueryUnexpectedError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, WeLiveException {
		PowerMockito.mockStatic(QueriableFactory.class);
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new QueriableFactoryException("Could not create queriable factory"));
		
		manager.executeSQLQuery("select * from table", DataOrigin.ANY);
	}
	
	@Test(expected=QueryExecutionException.class)
	public void testExecuteSQLQueryExecutionError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, QueriableFactoryException, WeLiveException, TranslationException {
		final SQLQueriable sqlQueriable = Mockito.mock(SQLQueriable.class);
		
		when(sqlQueriable.executeSQLSelect(Matchers.anyString(), Matchers.anyString(), Matchers.any(DataOrigin.class)))
			.thenThrow(new TranslationException("Could not translate query"));
		
		PowerMockito.mockStatic(QueriableFactory.class);		
		when(QueriableFactory.createQueriable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenReturn(sqlQueriable);
		
		manager.executeSQLQuery("select * from table", DataOrigin.ANY);
	}
	
	@Test
	public void testExecuteSQLUpdate() throws WeLiveException, CKANException, DataSourceManagementException, DataSourceSecurityManagerException, DatabaseCreationException, UpdateableFactoryException, CKANUtilException {
		CKANDataMocker.prepareValidData(ckanUtil);
		
		final SQLUpdateable sqlUpdateable = Mockito.mock(SQLUpdateable.class);
		when(sqlUpdateable.executeSQLUpdate(Matchers.anyString(), Matchers.anyString()))
			.thenReturn(3);
		
		PowerMockito.mockStatic(UpdateableFactory.class);
		when(UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenReturn(sqlUpdateable);
		
		final UpdateResult updateResult = manager.executeSQLUpdate("update table (id, name) values (1, 'test')", false);
		assertEquals(3, updateResult.getRows());
		
		verify(datasourceLogger, times(1)).logUpdate(Matchers.anyString(), Matchers.anyString());
	}
	
	@Test
	public void testExecuteSQLUpdateTransactional() throws WeLiveException, CKANException, DataSourceManagementException, DataSourceSecurityManagerException, DatabaseCreationException, UpdateableFactoryException, CKANUtilException {
		CKANDataMocker.prepareValidData(ckanUtil);
		
		final SQLUpdateable sqlUpdateable = Mockito.mock(SQLUpdateable.class);
		when(sqlUpdateable.executeSQLTransactionUpdate(Matchers.anyString(), Matchers.anyString()))
			.thenReturn(3);
		
		PowerMockito.mockStatic(UpdateableFactory.class);
		when(UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenReturn(sqlUpdateable);
		
		final UpdateResult updateResult = manager.executeSQLUpdate("update table (id, name) values (1, 'test')", true);
		assertEquals(3, updateResult.getRows());
		
		verify(datasourceLogger, times(1)).logUpdate(Matchers.anyString(), Matchers.anyString());
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testExecuteSQLUpdateForbiddenError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, WeLiveException {
		PowerMockito.mockStatic(UpdateableFactory.class);
		when(UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new DataSourceSecurityManagerException("Unauthorized acces to datasource"));
		
		manager.executeSQLUpdate("update table (id, name) values (1, 'test')", false);
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testExecuteSQLUpdateUnexpectedError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, WeLiveException {
		PowerMockito.mockStatic(UpdateableFactory.class);
		when(UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new UpdateableFactoryException("Could not create queriable factory"));
		
		manager.executeSQLUpdate("update table (id, name) values (1, 'test')", false);
	}
	
	@Test(expected=UpdateExecutionException.class)
	public void testExecuteSQLUpdateExecutionError() throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException, WeLiveException {
		PowerMockito.mockStatic(UpdateableFactory.class);
		when(UpdateableFactory.createUpdateable(Matchers.any(DataSourceConnector.class), Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(false)))
			.thenThrow(new DataSourceManagementException("Data source could not be accessed"));
		
		manager.executeSQLUpdate("update table (id, name) values (1, 'test')", false);
	}
	
	private List<DatasetStatus> createAllStatus() {
		final List<DatasetStatus> status = new ArrayList<DatasetStatus>();
		
		status.add(new DatasetStatus("dataset1", "OK", "Everything is ok", "10-10-2016"));
		status.add(new DatasetStatus("dataset2", "ERROR", "Some error occurred", "1-9-2016"));
		
		return status;
	}
	
	@Test
	public void testGetAllStatus() throws DatasetStatusManagerException, WeLiveException {
		PowerMockito.mockStatic(DatasetStatusManager.class);
		
		final DatasetStatusManager datasetStatusManager = Mockito.mock(DatasetStatusManager.class);
		when(datasetStatusManager.getAll())
			.thenReturn(createAllStatus());
		
		when(DatasetStatusManager.getInstance())
			.thenReturn(datasetStatusManager);
		
		final List<DatasetStatus> status = manager.getAllStatus();
		
		assertEquals(2, status.size());
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testGetAllStatusUnexpectedError() throws DatasetStatusManagerException, WeLiveException {
		PowerMockito.mockStatic(DatasetStatusManager.class);
		
		final DatasetStatusManager datasetStatusManager = Mockito.mock(DatasetStatusManager.class);
		when(datasetStatusManager.getAll())
			.thenThrow(new DatasetStatusManagerException("Cannot access datasource status"));
		
		when(DatasetStatusManager.getInstance())
			.thenReturn(datasetStatusManager);
		
		manager.getAllStatus();
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testResetDataSourceForbidden() throws WeLiveException, CKANUtilException {
		when(ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_ID))
			.thenReturn(WeLiveRole.NONE);
		
		manager = new DatasourceManager(ckanUtil, accessData, datasourceLogger);
		
		manager.resetDataSource();
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testDeleteStoredDataForbidden() throws WeLiveException, CKANUtilException {
		when(ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_ID))
			.thenReturn(WeLiveRole.NONE);
		
		manager = new DatasourceManager(ckanUtil, accessData, datasourceLogger);
		
		manager.deleteStoredData();
	}
	
	@Test
	public void testDeleteStoredData() throws WeLiveException, DatasetStatusManagerException, CKANUtilException {
		when(ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_ID))
			.thenReturn(WeLiveRole.ADMIN);
				
		final DatasetStatusManager datasetStatusManager = Mockito.mock(DatasetStatusManager.class);		
		PowerMockito.mockStatic(DatasetStatusManager.class);
		when(DatasetStatusManager.getInstance())
			.thenReturn(datasetStatusManager);
		
		manager = new DatasourceManager(ckanUtil, accessData, datasourceLogger);
		
		final MessageResponse response = manager.deleteStoredData();
		assertEquals(String.format("Datasource %s data correctly deleted", CKANDataMocker.RESOURCE_ID), response.getResponse());
		
		verify(datasetStatusManager, times(1)).updateStatus(Matchers.eq(CKANDataMocker.RESOURCE_ID), Matchers.eq(DatasetStatusManager.Status.OK), Matchers.eq("Data was manually deleted"));
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testDeleteStoredDataUnexpectedError() throws WeLiveException, DatasetStatusManagerException, IOException, CKANUtilException {
		when(ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_ID))
			.thenReturn(WeLiveRole.ADMIN);
				
		PowerMockito.mockStatic(JSONDumper.class);
		PowerMockito.doThrow(new IOException("Cannot delete data")).when(JSONDumper.class);
		JSONDumper.deleteData(Matchers.eq(CKANDataMocker.RESOURCE_ID));
		
		manager = new DatasourceManager(ckanUtil, accessData, datasourceLogger);
		
		manager.deleteStoredData();
	}
	
	@Test(expected=ForbiddenErrorException.class)
	public void testStopAssociatedJobForbidden() throws WeLiveException, CKANUtilException {
		when(ckanUtil.getUserRole(CKANDataMocker.PACKAGE_ID, USER_ID))
			.thenReturn(WeLiveRole.NONE);
		
		manager = new DatasourceManager(ckanUtil, accessData, datasourceLogger);
		
		manager.deleteStoredData();
	}
}