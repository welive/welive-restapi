/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;

import eu.iescities.server.querymapper.datasource.DataSourceConnector.ConnectorType;
import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.ColumnInfo;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.query.serialization.Row;
import eu.iescities.server.querymapper.util.status.DatasetStatus;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager.Status;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;
import eu.welive.rest.app.TestApplication;
import eu.welive.rest.auth.MessageResponse;
import eu.welive.rest.auth.filter.SecurityRequestFilter;
import eu.welive.rest.auth.securitycontext.BasicSecurityContext;
import eu.welive.rest.auth.securitycontext.OAuthSecurityContext;
import eu.welive.rest.auth.token.TokenManager;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.auth.user.oauth.OAuthUserInfo;
import eu.welive.rest.config.Config;
import eu.welive.rest.datasource.manager.DatasourceManager;
import eu.welive.rest.datasource.manager.MappingStatus;
import eu.welive.rest.datasource.manager.MappingValidator;
import eu.welive.rest.datasource.manager.UpdateResult;
import eu.welive.rest.datasource.remover.UserRemover;
import eu.welive.rest.datasource.remover.UserRemoverException;
import eu.welive.rest.exception.WeLiveException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({DatasourceAPI.class, DatasetStatusManager.class, 
		DatasourceManager.class, MappingValidator.class,
		UserRemover.class, BasicSecurityContext.class,
		SecurityRequestFilter.class, TokenManager.class, 
		Config.class })
public class DatasourceAPITest extends JerseyTest {
	
	private static final String MAPPING = "{\r\n   \"mapping\":\"json_schema\",\r\n   \"schema\":{\r\n      \"tables\":[\r\n         {\r\n            \"key\":\"key1\",\r\n            \"name\":\"table1\",\r\n            \"table1\":[\r\n               {\r\n                  \"key1\": \"value1\",\r\n                  \"key2\": 10,\r\n                  \"key3\": \"2015-01-01\"\r\n               }\r\n            ]\r\n         },\r\n         {\r\n            \"key\":\"key2\",\r\n            \"name\":\"table2\",\r\n            \"table2\":[\r\n               {\r\n                  \"key1\": \"value1\",\r\n                  \"key2\": \"value2\",\r\n                  \"key3\": 5\r\n               }\r\n            ]\r\n         }\r\n      ]\r\n   }\r\n}";
	
	private static final String AUTH_HEADER = "Authorization";
	private static final String OAUTH_TOKEN = "Bearer 1234-1234-1234-1234";
	private static final String BASIC_AUTH_TOKEN = "Basic YWRtaW46cGFzc3dvcmQ="; // admin:password
	
	private static final String USER = "authenticated-user"; 
	private static final String TOKEN = "token";
	
	private DatasetStatusManager datasetStatusManager;
	private DatasourceManager datasourceManager;
	private MappingValidator mappingValidator;
	private TokenManager tokenManager;
	private UserRemover userRemover;
	
	@Override
	protected Application configure() {
		return new TestApplication();
	}
	
	@Before
	public void setUpChild() throws Exception {
		datasetStatusManager = PowerMockito.mock(DatasetStatusManager.class);
		
		PowerMockito.mockStatic(DatasetStatusManager.class);
		when(DatasetStatusManager.getInstance())
			.thenReturn(datasetStatusManager);
		
		datasourceManager = PowerMockito.mock(DatasourceManager.class);
		PowerMockito.whenNew(DatasourceManager.class)
			.withArguments(Matchers.any(UserInfo.class), Matchers.anyString(), Matchers.anyString())
			.thenReturn(datasourceManager);
		
		mappingValidator = PowerMockito.mock(MappingValidator.class);
		PowerMockito.whenNew(MappingValidator.class)
			.withNoArguments()
			.thenReturn(mappingValidator);
		
		tokenManager = PowerMockito.mock(TokenManager.class);
		PowerMockito.whenNew(TokenManager.class)
			.withNoArguments()
			.thenReturn(tokenManager);
		
		when(tokenManager.validateToken(Matchers.anyString()))
			.thenReturn(new OAuthSecurityContext(new OAuthUserInfo(USER, TOKEN)));
		
		userRemover = PowerMockito.mock(UserRemover.class);
		PowerMockito.mockStatic(UserRemover.class);
		when(UserRemover.getInstance())
			.thenReturn(userRemover);
		
		final Config config = PowerMockito.mock(Config.class);
		when(config.getAdminUser())
			.thenReturn("admin");
		when(config.getAdminPass())
			.thenReturn("password");
		
		PowerMockito.mockStatic(Config.class);
		when(Config.getInstance())
			.thenReturn(config);
	}
	
	@Test
	public void testGetMapping() throws WeLiveException {		
		when(datasourceManager.getMapping())
			.thenReturn(MAPPING);

		final String response = target("/ods/some-id/resource/some-id/mapping")
									.request()
									.get(String.class);
		
		final JSONObject expected = new JSONObject(MAPPING);
		final JSONObject jsonMapping = new JSONObject(response);
		
		JSONAssert.assertEquals(expected, jsonMapping, false);
	}
	
	@Test
	public void testValidateMapping() {
		final MappingStatus status = new MappingStatus(MappingStatus.Status.VALID, "Dataset is ok");
		
		when(mappingValidator.validateMapping(Matchers.anyString()))
			.thenReturn(status);
		
		final Entity<String> entity = Entity.entity(MAPPING, MediaType.TEXT_PLAIN);
		
		final MappingStatus response = target("/ods/mapping/validate")
											.request(MediaType.APPLICATION_JSON)
											.post(entity, MappingStatus.class);
		
		verify(mappingValidator).validateMapping(Matchers.eq(MAPPING));
		
		assertEquals(status.getStatus(), response.getStatus());
		assertEquals(status.getMessage(), response.getMessage());
	}
	
	private DataSourceInfo createDataSourceInfo() {
		final DataSourceInfo dataSourceInfo = new DataSourceInfo(ConnectorType.json);
		final TableInfo tableInfo = new TableInfo("test_table");
		
		final ColumnInfo columnInfo1 = new ColumnInfo("column1", "INTEGER", true);
		tableInfo.addColumnInfo(columnInfo1);
		
		final ColumnInfo columnInfo2 = new ColumnInfo("column2", "VARCHAR", false);
		tableInfo.addColumnInfo(columnInfo2);
		
		dataSourceInfo.addTable(tableInfo);
		
		return dataSourceInfo;
	}
	
	@Test
	public void testGetInfo() throws WeLiveException {
		final DataSourceInfo expected = createDataSourceInfo();
		when(datasourceManager.getInfo())
			.thenReturn(expected);
		
		final DataSourceInfo response = target("/ods/some-id/resource/some-id/mapping/info")
									.request(MediaType.APPLICATION_JSON)
									.get(DataSourceInfo.class);
		
		assertEquals(expected.getType(), response.getType());
	}
	
	private List<DatasetStatus> createDatasetStatus() {
		final List<DatasetStatus> statusList = new ArrayList<DatasetStatus>();
		statusList.add(new DatasetStatus("sample-resource-1", Status.OK.toString(), "Dataset is ok", "2016-10-26 09:37"));
		statusList.add(new DatasetStatus("sample-resource-2", Status.ERROR.toString(), "Some problem processing dataset", "2016-10-26 09:37"));
		
		return statusList;
	}

	private DatasetStatus createOneDatasetStatus(String resourceID, String message, String timestamp) {
		return new DatasetStatus(resourceID, Status.OK.toString(), message, timestamp);
	}
	
	private ResultSet createResultSet() {
		final ResultSet resultSet = new ResultSet();
		
		final Row row = new Row();
		row.addColumn("column1", 13);
		row.addColumn("column2", "some_text");
		
		resultSet.addRow(row);
		
		return resultSet;
	}
	
	@Test
	public void testExecuteSQLQuery() throws WeLiveException {
		final String query = "select * from table";
		
		when(datasourceManager.executeSQLQuery(query, DataOrigin.USER))
			.thenReturn(createResultSet());
		
		final Entity<String> queryEntity = Entity.entity(query, MediaType.TEXT_PLAIN);
		final ResultSet response = target("/ods/some-value/resource/some-value/query")
										.queryParam("origin", DataOrigin.USER)
										.request(MediaType.APPLICATION_JSON)
										.post(queryEntity, ResultSet.class);
		
		verify(datasourceManager).executeSQLQuery(Matchers.eq(query), Matchers.eq(DataOrigin.USER));
		
		assertEquals(1, response.getRows().size());
	}
	
	@Test
	public void testExecuteSQLUpdate() throws WeLiveException {
		final String update = "update table set name='a' where id=1";
		
		final UpdateResult updateResult = new UpdateResult();
		updateResult.setRows(3);
		
		when(datasourceManager.executeSQLUpdate(update, true))
			.thenReturn(updateResult);
		
		final Entity<String> updateEntity = Entity.entity(update, MediaType.TEXT_PLAIN);
		final UpdateResult response = target("/ods/some-value/resource/some-value/update")
										.queryParam("transaction", "true")
										.request(MediaType.APPLICATION_JSON)
										.post(updateEntity, UpdateResult.class);
		
		verify(datasourceManager).executeSQLUpdate(Matchers.eq(update), Matchers.eq(true));
		
		assertEquals(3, response.getRows());
	}
	
	@Test
	public void testResetDatasource() throws WeLiveException {
		final MessageResponse message = new MessageResponse("Dataset correctly reset");
		when(datasourceManager.resetDataSource())
			.thenReturn(message);
		
		final MessageResponse response = target("/ods/some-id/resource/some-id/reset")
									.request(MediaType.APPLICATION_JSON)
									.header(AUTH_HEADER, OAUTH_TOKEN)
									.get(MessageResponse.class);
		
		verify(datasourceManager).resetDataSource();
		
		assertEquals(message.getResponse(), response.getResponse());
	}
	
	@Test
	public void testDeleteStoredData() throws WeLiveException {
		final MessageResponse message = new MessageResponse("Dataset correctly deleted");
		when(datasourceManager.deleteStoredData())
			.thenReturn(message);
		
		final MessageResponse response = target("/ods/some-id/resource/some-id/delete-stored-data")
									.request(MediaType.APPLICATION_JSON)
									.header(AUTH_HEADER, OAUTH_TOKEN)
									.get(MessageResponse.class);
		
		verify(datasourceManager).deleteStoredData();
		
		assertEquals(message.getResponse(), response.getResponse());
	}
	
	@Test
	public void testStopAssociatedJob() throws WeLiveException {
		final MessageResponse message = new MessageResponse("Associated job correctly stopped");
		when(datasourceManager.stopAssociatedJob())
			.thenReturn(message);
		
		final MessageResponse response = target("/ods/some-id/resource/some-id/stop-associated-job")
									.request(MediaType.APPLICATION_JSON)
									.header(AUTH_HEADER, OAUTH_TOKEN)
									.get(MessageResponse.class);
		
		verify(datasourceManager).stopAssociatedJob();
		
		assertEquals(message.getResponse(), response.getResponse());
	}
	
	@Test
	public void testGetAllStatus() throws DatasetStatusManagerException {
		when(datasetStatusManager.getAll())
			.thenReturn(createDatasetStatus());
		
		final List<DatasetStatus> status = target("/ods/get-status")
												.request(MediaType.APPLICATION_JSON)
												.get(new GenericType<List<DatasetStatus>>(){});
		
		assertEquals(2, status.size());
	}

	@Test
	public void testStatus() throws DatasetStatusManagerException {
		when(datasetStatusManager.getStatus("resource-id")).thenReturn(createOneDatasetStatus(
				"resource-id", "Job correctly executed", "2016-10-26 09:37"));
		final DatasetStatus status = target("/ods/get-status/resource-id")
										.request(MediaType.APPLICATION_JSON)
										.get(new GenericType<DatasetStatus>(){});
		assertEquals("Job correctly executed", status.getMessage());
		assertEquals("2016-10-26 09:37", status.getTimestamp());
		assertEquals("resource-id", status.getId());
		assertEquals(Status.OK.toString(), status.getStatus());
	}
	
	@Test
	public void testDeleteCurrentUser() throws DatasetStatusManagerException, UserRemoverException {
		PowerMockito.doNothing().when(userRemover).addPendingUser(Matchers.anyString(), Matchers.eq(true));
		
		final MessageResponse response = target("/ods/dataset/user/me")
												.queryParam("cascade", true)
												.request(MediaType.APPLICATION_JSON)
												.header(AUTH_HEADER, OAUTH_TOKEN)
												.delete(MessageResponse.class);
		
		verify(userRemover).addPendingUser(USER, true);
		 
		assertEquals("User authenticated-user removal scheduled", response.getResponse());
	}
	
	@Test
	public void testDeleteUser() throws DatasetStatusManagerException, UserRemoverException {		
		PowerMockito.doNothing().when(userRemover).addPendingUser(Matchers.anyString(), Matchers.eq(true));
		
		final MessageResponse response = target("/ods/dataset/user/100")
												.queryParam("cascade", true)
												.request(MediaType.APPLICATION_JSON)
												.header(AUTH_HEADER, BASIC_AUTH_TOKEN)
												.delete(MessageResponse.class);
		
		verify(userRemover).addPendingUser("100", true);
		 
		assertEquals("User 100 removal scheduled", response.getResponse());
	}
}
