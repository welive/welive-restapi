/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.filter;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.SecurityContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.rest.auth.roles.UserRoles;
import eu.welive.rest.auth.securitycontext.AnonSecurityContext;
import eu.welive.rest.auth.securitycontext.BasicSecurityContext;
import eu.welive.rest.auth.securitycontext.OAuthSecurityContext;
import eu.welive.rest.auth.token.TokenManager;
import eu.welive.rest.auth.user.oauth.OAuthUserInfo;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SecurityRequestFilter.class, Method.class, ResourceInfo.class, TokenManager.class })
public class SecurityRequestFilterTest {

	private static final String BASIC_AUTH = "Basic BYWRtaW46cGFzc3dvcmQ="; // admin:password
	private static final String OAUTH_TOKEN = "Bearer 1234-1234-1234-1234";
	
	private static final String USER_ID = "user-id";
	
	private SecurityRequestFilter securityRequestFilter;
	private ContainerRequestContext requestContext;
	private TokenManager tokenManager;
	private ResourceInfo resourceInfo;
	
	@RolesAllowed(UserRoles.BASIC_USER)
	public void basicAnnotatedMethod() {
		
	}
	
	@RolesAllowed(UserRoles.REGISTERED_USER)
	public void registeredAnnotatedMethod() {
		
	}
	
	public void notAnnotatedMethod() {
		
	}
	
	@Before
	public void setUp() throws Exception {
		tokenManager = Mockito.mock(TokenManager.class);
		PowerMockito.whenNew(TokenManager.class)
			.withNoArguments()
			.thenReturn(tokenManager);
		
		resourceInfo = Mockito.mock(ResourceInfo.class);
		
		securityRequestFilter = new SecurityRequestFilter(resourceInfo);
		requestContext = Mockito.mock(ContainerRequestContext.class);
	}
	
	@Test
	public void testFilterBasicAuth() throws IOException, NoSuchMethodException, SecurityException {
		final Method method = getClass().getMethod("basicAnnotatedMethod", new Class<?>[] {});
		when(resourceInfo.getResourceMethod())
			.thenReturn(method);
		
		when(requestContext.getHeaderString(SecurityRequestFilter.AUTHORIZATION_HEADER))
			.thenReturn(BASIC_AUTH);
		
		securityRequestFilter.filter(requestContext);
	
		final ArgumentCaptor<SecurityContext> argCaptor = ArgumentCaptor.forClass(SecurityContext.class);
		verify(requestContext).setSecurityContext(argCaptor.capture());
		assertTrue(argCaptor.getValue() instanceof BasicSecurityContext);
	}
	
	@Test
	public void testFilterNullToken() throws IOException, NoSuchMethodException, SecurityException {
		final Method method = getClass().getMethod("registeredAnnotatedMethod", new Class<?>[] {});
		when(resourceInfo.getResourceMethod())
			.thenReturn(method);
		
		when(requestContext.getHeaderString(SecurityRequestFilter.AUTHORIZATION_HEADER))
			.thenReturn(null);
		
		securityRequestFilter.filter(requestContext);
		
		final ArgumentCaptor<SecurityContext> argCaptor = ArgumentCaptor.forClass(SecurityContext.class);
		verify(requestContext).setSecurityContext(argCaptor.capture());
		assertTrue(argCaptor.getValue() instanceof AnonSecurityContext);
	}
	
	@Test
	public void testFilterInvalidPrefix() throws IOException, NoSuchMethodException, SecurityException { 
		final Method method = getClass().getMethod("registeredAnnotatedMethod", new Class<?>[] {});
		when(resourceInfo.getResourceMethod())
			.thenReturn(method);
		
		when(requestContext.getHeaderString(SecurityRequestFilter.AUTHORIZATION_HEADER))
			.thenReturn("some_invalid_prefix aaaaaaaa");
		
		securityRequestFilter.filter(requestContext);
		
		final ArgumentCaptor<SecurityContext> argCaptor = ArgumentCaptor.forClass(SecurityContext.class);
		verify(requestContext).setSecurityContext(argCaptor.capture());
		assertTrue(argCaptor.getValue() instanceof AnonSecurityContext);
	}
	
	@Test
	public void testFilterOAuth() throws IOException, NoSuchMethodException, SecurityException {
		final Method method = getClass().getMethod("registeredAnnotatedMethod", new Class<?>[] {});
		when(resourceInfo.getResourceMethod())
			.thenReturn(method);
		
		when(requestContext.getHeaderString(SecurityRequestFilter.AUTHORIZATION_HEADER))
			.thenReturn(OAUTH_TOKEN);
		
		when(tokenManager.validateToken(Matchers.anyString()))
			.thenReturn(new OAuthSecurityContext(new OAuthUserInfo(USER_ID, OAUTH_TOKEN)));
		
		securityRequestFilter.filter(requestContext);
		
		verify(tokenManager).validateToken(OAUTH_TOKEN);
		final ArgumentCaptor<SecurityContext> argCaptor = ArgumentCaptor.forClass(SecurityContext.class);
		verify(requestContext).setSecurityContext(argCaptor.capture());
		assertTrue(argCaptor.getValue() instanceof OAuthSecurityContext);
	}

	@Test
	public void testNotAnnotatedMethod() throws IOException, NoSuchMethodException, SecurityException {
		final Method method = getClass().getMethod("notAnnotatedMethod", new Class<?>[] {});
		when(resourceInfo.getResourceMethod())
			.thenReturn(method);
		
		securityRequestFilter.filter(requestContext);
		
		final ArgumentCaptor<SecurityContext> argCaptor = ArgumentCaptor.forClass(SecurityContext.class);
		verify(requestContext).setSecurityContext(argCaptor.capture());
		assertTrue(argCaptor.getValue() instanceof AnonSecurityContext);
	}
}
