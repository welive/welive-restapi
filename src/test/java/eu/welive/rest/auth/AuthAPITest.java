/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.auth.filter.SecurityRequestFilter;
import eu.welive.rest.auth.securitycontext.AnonSecurityContext;
import eu.welive.rest.auth.securitycontext.OAuthSecurityContext;
import eu.welive.rest.auth.token.TokenManager;
import eu.welive.rest.auth.user.oauth.OAuthUserInfo;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ SecurityRequestFilter.class, TokenManager.class })
public class AuthAPITest extends JerseyTest {
	
	private static final String AUTHORIZATION = "Authorization";
	private static final String BASIC_AUTH = "Basic c29tZXVzZXI6c29tZXBhc3M="; 
	private static final String INVALID_BASIC_AUTH = "Basic invalid";
	private static final String BEARER_TOKEN = "Bearer 1234-1234-1234-1234";
	private static final String INVALID_BEARER_TOKEN = "Bearer 1111-1111-1111-1111";
	
	private static final String USER_ID = "5"; 
	
	private TokenManager tokenManager;
	
	@Override
	protected Application configure() {
		return new TestApplication();
	}
	
	@Before
	public void setUpChild() throws Exception {
		tokenManager = PowerMockito.mock(TokenManager.class);
		PowerMockito.whenNew(TokenManager.class)
			.withNoArguments()
			.thenReturn(tokenManager);
		
		when(tokenManager.validateToken(BEARER_TOKEN))
			.thenReturn(new OAuthSecurityContext(new OAuthUserInfo(USER_ID, BEARER_TOKEN)));	
		
		when(tokenManager.validateToken(INVALID_BEARER_TOKEN))
			.thenReturn(new AnonSecurityContext());
	}
	
	@Test(expected=ForbiddenException.class)
	public void testGetBasicAuthMissingHeader() {
		target("/auth/sayhello/basic")
			.request(MediaType.APPLICATION_JSON)
			.get(MessageResponse.class);
	}
		
	@Test
	public void testGetBasicAuth() {
		final MessageResponse message = target("/auth/sayhello/basic")
			.request(MediaType.APPLICATION_JSON)
			.header(AUTHORIZATION, BASIC_AUTH)
			.get(MessageResponse.class);
		
		assertEquals("Hello world!", message.getResponse());
	}
	
	@Test(expected=ForbiddenException.class)
	public void testGetBasicAuthInvalidHeader() {
		target("/auth/sayhello/basic")
			.request(MediaType.APPLICATION_JSON)
			.header(AUTHORIZATION, INVALID_BASIC_AUTH)
			.get(MessageResponse.class);
	}
	
	@Test
	public void testGetOAuth() {
		final MessageResponse message = target("/auth/sayhello/oauth")
			.request(MediaType.APPLICATION_JSON)
			.header(AUTHORIZATION, BEARER_TOKEN)
			.get(MessageResponse.class);
		
		assertEquals("Hello world!", message.getResponse());
	}
	
	@Test(expected=ForbiddenException.class)
	public void testGetOAuthMissingHeader() {
		target("/auth/sayhello/oauth")
			.request(MediaType.APPLICATION_JSON)
			.get(MessageResponse.class);
	}
	
	@Test(expected=ForbiddenException.class)
	public void testGetOAuthInvalidHeader() {
		target("/auth/sayhello/oauth")
			.request(MediaType.APPLICATION_JSON)
			.header(AUTHORIZATION, INVALID_BEARER_TOKEN)
			.get(MessageResponse.class);
	}
}
