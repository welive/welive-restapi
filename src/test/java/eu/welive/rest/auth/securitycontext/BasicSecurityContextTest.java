/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.securitycontext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.welive.rest.auth.roles.UserRoles;
import eu.welive.rest.config.Config;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Config.class)
public class BasicSecurityContextTest {
	
	private static final String AUTH_TOKEN = "YWRtaW46cGFzc3dvcmQ="; // admin:password
	private static final String INVALID_AUTH_TOKEN = "aW52YWxpZDppbnZhbGlk"; // invalid:invalid
	
	private static final String ADMIN_PASS = "password";
	
	@Before
	public void setUp() throws IOException {
		PowerMockito.mockStatic(Config.class);
		
		final Config config = Mockito.mock(Config.class);
		when(config.getAdminUser())
			.thenReturn(DataSourceSecurityManager.ADMIN_USER);
		
		when(config.getAdminPass())
			.thenReturn(ADMIN_PASS);
		
		when(Config.getInstance())
			.thenReturn(config);
	}

	@Test
	public void testGetUserPrincipal() throws IOException {
		final BasicSecurityContext context = new BasicSecurityContext("Basic " + AUTH_TOKEN);
		final Principal user = context.getUserPrincipal();
		
		assertEquals(DataSourceSecurityManager.ADMIN_USER, user.getName());
		
		final BasicSecurityContext otherContext = new BasicSecurityContext("basic " + AUTH_TOKEN);
		final Principal otherUser = otherContext.getUserPrincipal();
		
		assertEquals(DataSourceSecurityManager.ADMIN_USER, otherUser.getName());
	}
	
	@Test
	public void testGetUserPrincipalEmptyToken() throws IOException {
		final BasicSecurityContext context = new BasicSecurityContext("");
		final Principal user = context.getUserPrincipal();
		assertTrue(user.getName().isEmpty());
	}
	
	@Test
	public void testGetUserPrincipalIncorrectToken() throws IOException {
		final BasicSecurityContext context = new BasicSecurityContext("Basic some_incorrect_token");
		final Principal user = context.getUserPrincipal();
		assertTrue(user.getName().isEmpty());
	}
	
	@Test
	public void testGetUserPrincipalInvalidToken() throws IOException {
		final BasicSecurityContext context = new BasicSecurityContext("Basic " + INVALID_AUTH_TOKEN);
		final Principal user = context.getUserPrincipal();
		assertTrue(user.getName().isEmpty());
	}

	@Test
	public void testIsUserInRole() {
		final BasicSecurityContext context = new BasicSecurityContext("Basic " + AUTH_TOKEN);
		context.isUserInRole(UserRoles.BASIC_USER);
	}

	@Test
	public void testIsSecure() {
		final BasicSecurityContext context = new BasicSecurityContext("Basic " + AUTH_TOKEN);
		assertFalse(context.isSecure());
	}

	@Test
	public void testGetAuthenticationScheme() {
		final BasicSecurityContext context = new BasicSecurityContext("Basic " + AUTH_TOKEN);
		assertEquals(SecurityContext.BASIC_AUTH, context.getAuthenticationScheme());
	}
}
