/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.securitycontext;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import javax.ws.rs.core.SecurityContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.welive.rest.auth.roles.UserRoles;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.auth.user.oauth.OAuthInfo;

public class OAuthSecurityContextTest {

	private static final String USER_NAME = "user";
	private static final String USER_TOKEN = "token";
	
	private OAuthSecurityContext context;
	
	@Before
	public void setUp() {
		final OAuthInfo userInfo = Mockito.mock(OAuthInfo.class);
		when(userInfo.getRoles())
			.thenReturn(Arrays.asList(new String[] { UserRoles.REGISTERED_USER }));
		
		when (userInfo.getName())
			.thenReturn(USER_NAME);
		
		when (userInfo.getAuthToken())
			.thenReturn(USER_TOKEN);
		
		context = new OAuthSecurityContext(userInfo);
	}
	
	@Test
	public void testGetUserPrincipal() {
		assertEquals(USER_NAME, context.getUserPrincipal().getName());
		assertEquals(USER_TOKEN, ((UserInfo)context.getUserPrincipal()).getAuthToken());
	}

	@Test
	public void testIsUserInRole() {
		assertTrue(context.isUserInRole(UserRoles.REGISTERED_USER));
	}

	@Test
	public void testIsSecure() {
		assertFalse(context.isSecure());
	}

	@Test
	public void testGetAuthenticationScheme() { 
		assertEquals(SecurityContext.BASIC_AUTH, context.getAuthenticationScheme());
	}
}
