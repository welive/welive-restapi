/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.token;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import javax.ws.rs.core.SecurityContext;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import eu.welive.rest.auth.aac.AACManager;
import eu.welive.rest.auth.aac.ClientInfo;
import eu.welive.rest.auth.aac.TokenInfo;
import eu.welive.rest.auth.aac.profiles.BasicProfile;
import eu.welive.rest.auth.roles.UserRoles;
import eu.welive.rest.auth.securitycontext.AnonSecurityContext;
import eu.welive.rest.auth.securitycontext.OAuthSecurityContext;
import eu.welive.rest.auth.user.oauth.OAuthClientInfo;
import eu.welive.rest.auth.user.oauth.OAuthUserInfo;
import eu.welive.rest.exception.UnexpectedErrorException;

public class TokenManagerTest {
	
	private static String TOKEN = "Bearer 1234-1234-1234-1234";
	
	private static String USER_ID = "10";	
	
	private static String CLIENT_ID = "5";
	private static String CLIENT_NAME = "client-id";

	private TokenManager tokenManager;
	private AACManager aacManager;
	
	@Before
	public void setUp() {
		aacManager = Mockito.mock(AACManager.class);
		tokenManager = new TokenManager(aacManager);
	}
	
	public BasicProfile createBasicProfile() {
		final BasicProfile basicProfile = new BasicProfile();
		basicProfile.setUserId("10");
		return basicProfile;
	}
	
	@Test
	public void testValidateTokenBasicProfile() throws UnexpectedErrorException {
		final TokenInfo tokenInfo = new TokenInfo();
		tokenInfo.setAuthorities(Arrays.asList(AACManager.AACRole.ROLE_USER));
		when(aacManager.getTokenInfo(TOKEN))
			.thenReturn(tokenInfo);
		
		final BasicProfile basicProfile = createBasicProfile();
		when(aacManager.getBasicProfile(TOKEN))
			.thenReturn(basicProfile);
		
		final SecurityContext securityContext = tokenManager.validateToken(TOKEN);
		
		assertTrue(securityContext instanceof OAuthSecurityContext);
		final OAuthUserInfo userInfo = (OAuthUserInfo) securityContext.getUserPrincipal();
		
		assertEquals(USER_ID, userInfo.getName());
		assertEquals(TOKEN, userInfo.getAuthToken());
		
		assertEquals(1, userInfo.getRoles().size());
		assertTrue(userInfo.getRoles().contains(UserRoles.REGISTERED_USER));
	}
	
	@Test
	public void testValidateTokenExtendedProfileError() throws UnexpectedErrorException {
		final TokenInfo tokenInfo = new TokenInfo();
		tokenInfo.setAuthorities(Arrays.asList(AACManager.AACRole.ROLE_USER));
		when(aacManager.getTokenInfo(TOKEN))
			.thenReturn(tokenInfo);
		
		when(aacManager.getBasicProfile(TOKEN))
			.thenThrow(new UnexpectedErrorException("Some error ocurred"));
		
		final SecurityContext securityContext = tokenManager.validateToken(TOKEN);
		assertTrue(securityContext instanceof AnonSecurityContext);
	}
	
	private ClientInfo createClientInfo() {
		final ClientInfo clientInfo = new ClientInfo();
		clientInfo.setClientId(CLIENT_ID);
		clientInfo.setClientName(CLIENT_NAME);
		return clientInfo;
	}
	
	@Test
	public void testValidateTokenClient() throws UnexpectedErrorException {
		final TokenInfo tokenInfo = new TokenInfo();
		tokenInfo.setAuthorities(Arrays.asList(AACManager.AACRole.ROLE_CLIENT));
		when(aacManager.getTokenInfo(TOKEN))
			.thenReturn(tokenInfo);
		
		when(aacManager.getClientInfo(TOKEN))
			.thenReturn(createClientInfo());
		
		final SecurityContext securityContext = tokenManager.validateToken(TOKEN);
		
		assertTrue(securityContext instanceof OAuthSecurityContext);
		final OAuthClientInfo userInfo = (OAuthClientInfo) securityContext.getUserPrincipal();
		
		assertEquals(CLIENT_NAME, userInfo.getName());
		assertEquals(TOKEN, userInfo.getAuthToken());
		
		assertEquals(1, userInfo.getRoles().size());
		assertTrue(userInfo.getRoles().contains(UserRoles.REGISTERED_USER));
	}
}
