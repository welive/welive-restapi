/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.aac.providers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.json.Json;
import javax.json.JsonObject;

import org.junit.Test;

public class WeLiveProviderTest {
	
	private JsonObject createJSONAccount() {
		final JsonObject json = Json.createObjectBuilder()
				.add("username", "a@gmail.com")
			.build();
		
		return json;
	}
	
	private JsonObject createEmtpyJson() {
		final JsonObject json = Json.createObjectBuilder().build();
		return json;
	}
	
	@Test
	public void testLoadJson() {
		final WeLiveProvider provider = new WeLiveProvider();
		provider.loadJSON(createJSONAccount());
		
		assertEquals("a@gmail.com", provider.getUsername());
		assertEquals("a@gmail.com", provider.getEmail());
	}

	@Test
	public void testLoadJsonNoEmail() {
		final WeLiveProvider provider = new WeLiveProvider();
		provider.loadJSON(createEmtpyJson());
		
		assertTrue(provider.getEmail().isEmpty());
	}	
}
