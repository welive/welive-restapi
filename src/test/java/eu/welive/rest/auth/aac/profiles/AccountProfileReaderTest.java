/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.aac.profiles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;

import org.junit.Before;
import org.junit.Test;

public class AccountProfileReaderTest {

	private JsonObject googleAccount, weliveAccount, facebookAccount;
	private JsonObject emptyJson;
	
	private AccountProfileReader reader;
	
	@Before
	public void setUp() {
		googleAccount = Json.createObjectBuilder()
			.add("accounts", Json.createObjectBuilder()
				.add("google", Json.createObjectBuilder()
					.add("OIDC_CLAIM_email", "a@gmail.com")
				)
			)
		.build();
		
		weliveAccount = Json.createObjectBuilder()
			.add("accounts", Json.createObjectBuilder()
				.add("welive", Json.createObjectBuilder()
					.add("username", "a@welive.eu")
				)
			)
		.build();
		
		facebookAccount = Json.createObjectBuilder()
			.add("accounts", Json.createObjectBuilder()
				.add("facebook", Json.createObjectBuilder()
					.add("email", "a@gmail.com")
				)
			)
		.build();
		
		emptyJson = Json.createObjectBuilder().build();
		
		reader = new AccountProfileReader(); 
	}
	
	@Test
	public void testGoogleAccount() throws IOException {
		final AccountProfile accountProfile = reader.readJson(googleAccount);
		
		assertEquals(1, accountProfile.getProviders().size());
		assertEquals(1, accountProfile.getEmails().size());
		assertEquals("a@gmail.com", accountProfile.getEmail());
	}
	
	@Test
	public void testWeLiveAccount() throws IOException {
		final AccountProfile accountProfile = reader.readJson(weliveAccount);
		
		assertEquals(1, accountProfile.getProviders().size());
		assertEquals(1, accountProfile.getEmails().size());
		assertEquals("a@welive.eu", accountProfile.getEmail());
	}
	
	@Test
	public void testFacebookAccount() throws IOException {
		final AccountProfile accountProfile = reader.readJson(facebookAccount);
		
		assertEquals(1, accountProfile.getProviders().size());
		assertEquals(1, accountProfile.getEmails().size());
		assertEquals("a@gmail.com", accountProfile.getEmail());
	}
	
	@Test(expected=IOException.class)
	public void testEmtpyJSON() throws IOException {
		reader.readJson(emptyJson);
	}
	
	@Test
	public void testIsReadable() {
		assertTrue(reader.isReadable(AccountProfile.class, AccountProfile.class, new Annotation[] {}, MediaType.APPLICATION_JSON_TYPE));
		assertFalse(reader.isReadable(AccountProfile.class, AccountProfile.class, new Annotation[] {}, MediaType.TEXT_PLAIN_TYPE));
		assertFalse(reader.isReadable(Object.class, Object.class, new Annotation[] {}, MediaType.TEXT_PLAIN_TYPE));
		assertFalse(reader.isReadable(Object.class, Object.class, new Annotation[] {}, MediaType.APPLICATION_JSON_TYPE));
	}
	
	@Test
	public void testReadFrom() throws IOException {
		try(InputStream is = new ByteArrayInputStream(googleAccount.toString().getBytes())) {
			final AccountProfile accountProfile = reader.readFrom(AccountProfile.class, AccountProfile.class, new Annotation[] {}, MediaType.APPLICATION_JSON_TYPE, new MultivaluedHashMap<String, String>(), is);
		
					assertEquals(1, accountProfile.getProviders().size());
			assertEquals(1, accountProfile.getEmails().size());
			assertEquals("a@gmail.com", accountProfile.getEmail());
		}
	}

}
