/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.aac;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.rest.auth.aac.profiles.AccountProfile;
import eu.welive.rest.auth.aac.profiles.AccountProfileReader;
import eu.welive.rest.auth.aac.profiles.BasicProfile;
import eu.welive.rest.auth.aac.profiles.ExtendedProfile;
import eu.welive.rest.auth.aac.providers.FacebookProvider;
import eu.welive.rest.auth.aac.providers.GoogleProvider;
import eu.welive.rest.auth.aac.providers.WeLiveProvider;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.UnexpectedErrorException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientBuilder.class, Config.class})
public class AACManagerTest {
	
	private static final String TOKEN = "1234-1234-1234-1234";
	private static final String BEARER_TOKEN = "Bearer " + TOKEN;
	private static final String USER_ID = "some_user_id";
	
	private static final String EMAIL = "a@a.com";

	private WebTarget target;
	private Builder builder;
	private AACManager aacManager;
	
	@Before
	public void setUp() throws IOException {		
		prepareConfig();
		
		builder = Mockito.mock(Builder.class);
		when(builder.header(Matchers.anyString(), Matchers.anyString()))
			.thenReturn(builder);
				
		when(builder.header(Matchers.anyString(), Matchers.anyString()))
			.thenReturn(builder);
		
		target = Mockito.mock(WebTarget.class);
		when(target.path(Matchers.anyString()))
			.thenReturn(target);
		
		when(target.queryParam(Matchers.anyString(), Matchers.anyList()))
			.thenReturn(target);
		
		when(target.request(MediaType.APPLICATION_JSON))
			.thenReturn(builder);
		
		final Client client = Mockito.mock(Client.class);
		when(client.target(Matchers.anyString()))
			.thenReturn(target);
		
		PowerMockito.mockStatic(ClientBuilder.class);
		when(ClientBuilder.newClient())
			.thenReturn(client);
		
		when(ClientBuilder.newClient())
			.thenReturn(client);
		
		final ClientBuilder builder = Mockito.mock(ClientBuilder.class);
		when(ClientBuilder.newBuilder())
			.thenReturn(builder);
		
		when(builder.register(AccountProfileReader.class))
			.thenReturn(builder);
		
		when(builder.build())
			.thenReturn(client);
		
		aacManager = new AACManager();
	}
	
	private Config prepareConfig() throws IOException {
		final Config config = Mockito.mock(Config.class);
		when(config.getAacServiceURL())
			.thenReturn("http://someurl");
		
		PowerMockito.mockStatic(Config.class);
		when(Config.getInstance())
			.thenReturn(config);
		
		return config;
	} 
	
	private BasicProfile createBasicProfile() {
		final BasicProfile basicProfile = new BasicProfile();
		basicProfile.setUserId(USER_ID);
		return basicProfile;
	}
	
	private AccountProfile createAccountProfile() {
		final AccountProfile accountProfile = new AccountProfile();
			
		final GoogleProvider googleProvider = new GoogleProvider(EMAIL);
		accountProfile.addProvider(googleProvider);
		
		final WeLiveProvider weliveProvider = new WeLiveProvider("a@a.com", "a@a.com");
		accountProfile.addProvider(weliveProvider);
		
		final FacebookProvider facebookProfider = new FacebookProvider(EMAIL);
		accountProfile.addProvider(facebookProfider);
		
		return accountProfile;
	}
	
	@Test
	public void testGetBasicProfile() throws UnexpectedErrorException {
		when(builder.get(BasicProfile.class))
			.thenReturn(createBasicProfile());
		
		final BasicProfile basicProfile = aacManager.getBasicProfile(BEARER_TOKEN);
		
		verify(target).request(MediaType.APPLICATION_JSON);
		verify(builder).header("Authorization", BEARER_TOKEN);
		
		assertEquals(USER_ID, basicProfile.getUserId());
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testGetBasicProfileError() throws UnexpectedErrorException {
		when(builder.get(BasicProfile.class))
			.thenThrow(new ResponseProcessingException(Response.serverError().build(), "Some server error")); 
		
		aacManager.getBasicProfile(BEARER_TOKEN);
	}
	
	@Test
	public void testGetAccountProfile() throws UnexpectedErrorException {
		when(builder.get(AccountProfile.class))
			.thenReturn(createAccountProfile());
		
		final AccountProfile accountProfile = aacManager.getAccountProfile(BEARER_TOKEN);
		assertEquals(3, accountProfile.getProviders().size());
		
		verify(target).request(MediaType.APPLICATION_JSON);
		verify(builder).header("Authorization", BEARER_TOKEN);
	
		assertEquals(EMAIL, accountProfile.getEmail());
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testGetAccountProfileError() throws UnexpectedErrorException {
		when(builder.get(AccountProfile.class))
			.thenThrow(new ResponseProcessingException(Response.serverError().build(), "Some server error"));
		
		aacManager.getAccountProfile(BEARER_TOKEN);
	}
	
	@Test
	public void testGetExtendedProfile() throws UnexpectedErrorException {		
		when(builder.get(BasicProfile.class))
			.thenReturn(createBasicProfile());
		
		when(builder.get(AccountProfile.class))
			.thenReturn(createAccountProfile());
		
		final ExtendedProfile extendedProfile = aacManager.getExtendedProfile(BEARER_TOKEN);
		
		assertEquals(USER_ID, extendedProfile.getBasicProfile().getUserId());
		assertEquals(EMAIL, extendedProfile.getAccountProfile().getEmail());
	}
	
	private TokenInfo createUserRoleTokenInfo() {
		final TokenInfo tokenInfo = new TokenInfo();
		tokenInfo.setAuthorities(Arrays.asList(new AACManager.AACRole[] { AACManager.AACRole.ROLE_USER }));
		return tokenInfo;
	}
	
	@Test
	public void testGetTokenInfo() throws UnexpectedErrorException {
		when(builder.get(TokenInfo.class))
			.thenReturn(createUserRoleTokenInfo());
		
		final TokenInfo tokenInfo = aacManager.getTokenInfo(BEARER_TOKEN);
			
		verify(target).queryParam("token", TOKEN);
		verify(target).request(MediaType.APPLICATION_JSON);
		
		assertTrue(tokenInfo.isUserToken());
		assertFalse(tokenInfo.isClientToken());
		
		assertEquals(1, tokenInfo.getAuthorities().size());
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testGetTokenInfoError() throws UnexpectedErrorException {
		when(builder.get(TokenInfo.class))
			.thenThrow(new ResponseProcessingException(Response.serverError().build(), "Some server error"));
		
		aacManager.getTokenInfo(BEARER_TOKEN);
	}
	
	private ClientInfo createClientInfo() {
		final ClientInfo clientInfo = new ClientInfo();
		clientInfo.setClientId("5");
		clientInfo.setClientName("client-name");
		return clientInfo;
	}
	
	@Test
	public void testGetClientInfo() throws UnexpectedErrorException {
		when(builder.get(ClientInfo.class))
			.thenReturn(createClientInfo());
		
		final ClientInfo clientInfo = aacManager.getClientInfo(BEARER_TOKEN);
			
		verify(target).request(MediaType.APPLICATION_JSON);
		verify(builder).header("Authorization", BEARER_TOKEN);
		
		assertEquals("5", clientInfo.getClientId());
		assertEquals("client-name", clientInfo.getClientName());
	}
	
	@Test(expected=UnexpectedErrorException.class)
	public void testGetClientInfoError() throws UnexpectedErrorException {
		when(builder.get(ClientInfo.class))
			.thenThrow(new ResponseProcessingException(Response.serverError().build(), "Some server error"));
		
		aacManager.getClientInfo(BEARER_TOKEN);
	}
}
