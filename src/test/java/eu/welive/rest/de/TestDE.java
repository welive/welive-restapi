/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.de;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.de.pojo.AppRequest;
import eu.welive.rest.de.pojo.ArtifactRequest;
import eu.welive.rest.de.pojo.IdeaRequest;
import eu.welive.rest.exception.WeLiveException;

/**
 * Created by mikel on 1/06/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Entity.class, ClientBuilder.class, Config.class, DE.class})
@PowerMockIgnore("javax.net.ssl.*")
public class TestDE extends JerseyTest {

    private static String artifactID = "5555";
    private static String userID = "8";
    private static String ideaID = "5467";
    private static String token = "FAKE-TOKEN";
    private static String UDEUSTO_DE_URL;

    private DE de;

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Before
    public void setUp() throws URISyntaxException, IOException {
        de = Mockito.spy(DE.class);
        Mockito.doNothing().when(de).sendLog(Matchers.anyString(), Matchers.any(JSONObject.class));
        Mockito.doNothing().when(de).logApp(Matchers.anyString(), Matchers.anyDouble(), Matchers.anyDouble(),
                Matchers.anyDouble(), Matchers.any(JSONArray.class));
        Mockito.doNothing().when(de).logArtifact(Matchers.anyString(), Matchers.anyString(), Matchers.anyString(),
                Matchers.anyString());
        Mockito.doNothing().when(de).logRecommendedArtifacts(Matchers.anyString(), Matchers.any(JSONArray.class),
                Matchers.anyString(), Matchers.anyString());
        Mockito.doNothing().when(de).logIdea(Matchers.anyString());
        Mockito.doNothing().when(de).logIdeaRecommended(Matchers.anyString(), Matchers.any(JSONArray.class));
        
        UDEUSTO_DE_URL = Config.getInstance().getUdeustoDEURL();
    }

    @Test(expected = WeLiveException.class)
    public void testReadConfig() throws IOException, WeLiveException {
        PowerMockito.mockStatic(Config.class);
        PowerMockito.when(Config.getInstance()).thenThrow(IOException.class);

        Mockito.spy(new DE());
    }

    @Test
    public void testRecommendDatasetArtifact() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        JSONObject inputJSON = new JSONObject(String.format("{\"id\": \"%s\", \"types\": [\"dataset\", " +
                "\"buildingblock\", \"application\"]}", artifactID));

        String responseString = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(responseString);

        Mockito.doReturn(responseString).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callPOST(String.format("de/recommend_artifact/"), inputJSON.toString(), MediaType.APPLICATION_JSON, null);

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(200).when(mockedGetResponse).getStatus();
        Mockito.doReturn(responseString).when(mockedGetResponse).readEntity(String.class);
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));		
        
        de.recommendDatasetArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.DATASET.getName()), Matchers.eq(DE.ArtifactType.DATASET.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.DATASET.getName()), Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.DATASET.getName()), Matchers.eq(DE.ArtifactType.APP.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendDatasetArtifactException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));		
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendDatasetArtifact(artifactID);
    }

    @Test
    public void testRecommendDatasetArtifactEmpty() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[]";

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));		
        
        de.recommendDatasetArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
    }
    
    @Test
    public void testRecommendDatasetArtifactDatasetNotFound() throws WeLiveException {
    	Response mockedResponse = Mockito.mock(Response.class);
    	String stringResponse = "{\"detail\": \"Not Found\"}";
    	
    	Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
    	Mockito.doReturn(404).when(mockedResponse).getStatus();
    	Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));		
        
        Response response = de.recommendDatasetArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }
    
    @Test
    public void testRecommendDatasetArtifactBuildingBlockNotFound() throws WeLiveException {
    	Response mocked200Response = Mockito.mock(Response.class);
    	Response mocked404Response = Mockito.mock(Response.class);
    	String string200Response = "[]";
    	String string404Response = "{\"detail\": \"Not Found\"}";
    	
    	Mockito.doReturn(string200Response).when(mocked200Response).readEntity(String.class);
    	Mockito.doReturn(string404Response).when(mocked404Response).readEntity(String.class);
    	Mockito.doReturn(404).when(mocked404Response).getStatus();
    	Mockito.doReturn(200).when(mocked200Response).getStatus();
    	Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));		
        
        Response response = de.recommendDatasetArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }
    
    @Test
    public void testRecommendDatasetArtifactAppNotFound() throws WeLiveException {
    	Response mocked200Response = Mockito.mock(Response.class);
    	Response mocked404Response = Mockito.mock(Response.class);
    	String string200Response = "[]";
    	String string404Response = "{\"detail\": \"Not Found\"}";
    	
    	Mockito.doReturn(string200Response).when(mocked200Response).readEntity(String.class);
    	Mockito.doReturn(string404Response).when(mocked404Response).readEntity(String.class);
    	Mockito.doReturn(404).when(mocked404Response).getStatus();
    	Mockito.doReturn(200).when(mocked200Response).getStatus();
    	Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));		
        
        Response response = de.recommendDatasetArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }

    @Test
    public void testRecommendBuildingBlockArtifact() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        de.recommendBuildingBlockArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()), Matchers.eq(DE.ArtifactType.DATASET.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()), Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()), Matchers.eq(DE.ArtifactType.APP.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendBuildingBlockArtifactException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);


        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendBuildingBlockArtifact(artifactID);        
    }

    @Test
    public void testRecommendBuildingBlockArtifactEmpty() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn("[]").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
       
        de.recommendBuildingBlockArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
    }
    
    @Test
    public void testRecommendBuildingBlockArtifactDatasetNotFound() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn("{\"detail\": \"Not Found\"}").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(404).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
       
        Response response = de.recommendBuildingBlockArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }
    
    @Test
    public void testRecommendBuildingBlockArtifactBuildingBlockNotFound() throws WeLiveException {
        Response mocked200Response = Mockito.mock(Response.class);
        Response mocked404Response = Mockito.mock(Response.class);
        
        Mockito.doReturn("{\"detail\": \"Not Found\"}").when(mocked404Response).readEntity(String.class);
        Mockito.doReturn("[]").when(mocked200Response).readEntity(String.class);
        Mockito.doReturn(404).when(mocked404Response).getStatus();
        Mockito.doReturn(200).when(mocked200Response).getStatus();
        Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
       
        Response response = de.recommendBuildingBlockArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }
    
    @Test
    public void testRecommendBuildingBlockArtifactAppNotFound() throws WeLiveException {
        Response mocked200Response = Mockito.mock(Response.class);
        Response mocked404Response = Mockito.mock(Response.class);
        
        Mockito.doReturn("{\"detail\": \"Not Found\"}").when(mocked404Response).readEntity(String.class);
        Mockito.doReturn("[]").when(mocked200Response).readEntity(String.class);
        Mockito.doReturn(404).when(mocked404Response).getStatus();
        Mockito.doReturn(200).when(mocked200Response).getStatus();
        Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
       
        Response response = de.recommendBuildingBlockArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }

    @Test
    public void testRecommendAppArtifact() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        de.recommendAppArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.APP.getName()), Matchers.eq(DE.ArtifactType.DATASET.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.APP.getName()), Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.APP.getName()), Matchers.eq(DE.ArtifactType.APP.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendAppArtifactException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendAppArtifact(artifactID);
    }

    @Test
    public void testRecommendAppArtifactEmpty() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn("[]").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        de.recommendAppArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
    }
    
    @Test
    public void testRecommendAppArtifactDatasetNotFound() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn("{\"detail\": \"Not Found\"}").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(404).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        Response response = de.recommendAppArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }
    
    @Test
    public void testRecommendAppArtifactBuildingBlockNotFound() throws WeLiveException {
        Response mocked200Response = Mockito.mock(Response.class);
        Response mocked404Response = Mockito.mock(Response.class); 
        
        Mockito.doReturn("{\"detail\": \"Not Found\"}").when(mocked404Response).readEntity(String.class);
        Mockito.doReturn("[]").when(mocked200Response).readEntity(String.class);
        Mockito.doReturn(404).when(mocked404Response).getStatus();
        Mockito.doReturn(200).when(mocked200Response).getStatus();
        Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        Response response = de.recommendAppArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.never()).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }
    
    @Test
    public void testRecommendAppArtifactAppNotFound() throws WeLiveException {
        Response mocked200Response = Mockito.mock(Response.class);
        Response mocked404Response = Mockito.mock(Response.class); 
        
        Mockito.doReturn("{\"detail\": \"Not Found\"}").when(mocked404Response).readEntity(String.class);
        Mockito.doReturn("[]").when(mocked200Response).readEntity(String.class);
        Mockito.doReturn(404).when(mocked404Response).getStatus();
        Mockito.doReturn(200).when(mocked200Response).getStatus();
        Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked200Response).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.doReturn(mocked404Response).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        Response response = de.recommendAppArtifact(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        assertEquals(404, response.getStatus());
    }

    @Test
    public void testRecommendDatasetDataset() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        
        de.recommendDatasetDataset(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.DATASET.getName()), Matchers.eq(DE.ArtifactType.DATASET.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendDatasetDatasetException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendDatasetDataset(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
    }

    @Test
    public void testRecommendDatasetBuildingBlock() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        de.recommendDatasetBuildingBlock(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.DATASET.getName()), Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendDatasetBuildingBlockException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendDatasetBuildingBlock(artifactID);
    }

    @Test
    public void testRecommendDatasetApp() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        de.recommendDatasetApp(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.DATASET.getName()), Matchers.eq(DE.ArtifactType.APP.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendDatasetAppException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendDatasetApp(artifactID);
    }

    @Test
    public void testUpdateDataset() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        ArtifactRequest artifactRequest = new ArtifactRequest("Spanish", tagList);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"id\": \"%s\"}", new JSONArray(tagList), artifactID));
        
        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/dataset/", UDEUSTO_DE_URL)), Matchers.anyString(), 
        		Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));

        de.updateDataset(artifactID, artifactRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPOST(String.format("%s/dataset/", UDEUSTO_DE_URL), inputJSON.toString(), MediaType.APPLICATION_JSON, token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.DATASET.getName(),
                "Artifact Published", "ArtifactPublished");
    }

    @Test
    public void testUpdateExistingDataset() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        ArtifactRequest artifactRequest = new ArtifactRequest("Spanish", tagList);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"id\": \"%s\"}", new JSONArray(tagList), artifactID));

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(200).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID));

        Mockito.doReturn(mockedResponse).when(de).callPUT(Matchers.eq(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID)), Matchers.anyString(),
                Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));

        de.updateDataset(artifactID, artifactRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPUT(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID), inputJSON.toString(), MediaType.APPLICATION_JSON, token);
    }

    @Test(expected = WeLiveException.class)
    public void testUpdateDatasetEmptyBody() throws WeLiveException {
        de.updateDataset(artifactID, null, token);
    }

    @Test(expected = WeLiveException.class)
    public void testUpdateDatasetFail() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        ArtifactRequest artifactRequest = new ArtifactRequest("Spanish", tagList);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"id\": \"%s\"}", new JSONArray(tagList), artifactID));

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/dataset/", UDEUSTO_DE_URL)), Matchers.anyString(), 
        		Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logArtifact(artifactID, DE.ArtifactType.DATASET.getName(),
                "Artifact Published", "ArtifactPublished");

        de.updateDataset(artifactID, artifactRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPOST(String.format("%s/dataset/", UDEUSTO_DE_URL), inputJSON.toString(), MediaType.APPLICATION_JSON, token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.DATASET.getName(),
                "Artifact Published", "ArtifactPublished");
    }

    @Test
    public void testRemoveDataset() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(204).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callDelete(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID), token);

        de.removeDataset(artifactID, token);

        Mockito.verify(de, Mockito.times(1)).callDelete(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID), token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.DATASET.getName(), "Artifact Removed", "ArtifactRemoved");
    }

    @Test(expected = WeLiveException.class)
    public void testRemoveDatasetFail() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(204).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callDelete(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID), token);

        Mockito.doThrow(new URISyntaxException("", "")).when(de).logArtifact(artifactID,
                DE.ArtifactType.DATASET.getName(), "Artifact Removed", "ArtifactRemoved");

        de.removeDataset(artifactID, token);

        Mockito.verify(de, Mockito.times(1)).callDelete(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID), token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.DATASET.getName(), "Artifact Removed", "ArtifactRemoved");
    }

    @Test
    public void testRecommendBuildingBlockDataset() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        
        de.recommendBuildingBlockDataset(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()), Matchers.eq(DE.ArtifactType.DATASET.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendBuildingBlockDatasetException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendBuildingBlockDataset(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
    }

    @Test
    public void testRecommendBuildingBlockBuildingBlock() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        de.recommendBuildingBlockBuildingBlock(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()), Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendBuildingBlockBuildingBlockException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendBuildingBlockBuildingBlock(artifactID);
    }

    @Test
    public void testRecommendBuildingBlockApp() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        de.recommendBuildingBlockApp(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()), Matchers.eq(DE.ArtifactType.APP.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendBuildingBlockAppException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendBuildingBlockApp(artifactID);
    }

    @Test
    public void testUpdateBuildingBlock() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        ArtifactRequest artifactRequest = new ArtifactRequest("Spanish", tagList);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"id\": \"%s\"}", new JSONArray(tagList), artifactID));

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/buildingblock/%s/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/buildingblock/", UDEUSTO_DE_URL)), Matchers.anyString(), 
        		Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        de.updateBuildingBlock(artifactID, artifactRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPOST(String.format("%s/buildingblock/", UDEUSTO_DE_URL), inputJSON.toString(), MediaType.APPLICATION_JSON, token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.BUILDING_BLOCK.getName(),
                "Artifact Published", "ArtifactPublished");
    }

    @Test
    public void testUpdateExistingBuildingBlock() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        ArtifactRequest artifactRequest = new ArtifactRequest("Spanish", tagList);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"id\": \"%s\"}", new JSONArray(tagList), artifactID));

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(200).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/buildingblock/%s/", UDEUSTO_DE_URL, artifactID));

        Mockito.doReturn(mockedResponse).when(de).callPUT(Matchers.eq(String.format("%s/buildingblock/%s/", UDEUSTO_DE_URL, artifactID)), Matchers.anyString(),
                Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        de.updateBuildingBlock(artifactID, artifactRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPUT(String.format("%s/buildingblock/%s/", UDEUSTO_DE_URL, artifactID), inputJSON.toString(), MediaType.APPLICATION_JSON, token);
    }

    @Test(expected = WeLiveException.class)
    public void testUpdateBuildingBlockEmptyBody() throws WeLiveException {
        de.updateBuildingBlock(artifactID, null, token);
    }

    @Test(expected = WeLiveException.class)
    public void testUpdateBuildingBlockFail() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        ArtifactRequest artifactRequest = new ArtifactRequest("Spanish", tagList);

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/buildingblock/%s/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/buildingblock/", UDEUSTO_DE_URL)), Matchers.anyString(), 
        		Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logArtifact(artifactID, DE.ArtifactType.BUILDING_BLOCK.getName(),
                "Artifact Published", "ArtifactPublished");

        de.updateBuildingBlock(artifactID, artifactRequest, token);
    }

    @Test
    public void testRemoveBuildingBlock() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(204).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callDelete(String.format("%s/buildingblock/%s", UDEUSTO_DE_URL, artifactID), token);

        de.removeBuildingBlock(artifactID, token);

        Mockito.verify(de, Mockito.times(1)).callDelete(String.format("%s/buildingblock/%s", UDEUSTO_DE_URL, artifactID), token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.BUILDING_BLOCK.getName(), "Artifact Removed", "ArtifactRemoved");
    }

    @Test(expected = WeLiveException.class)
    public void testRemoveBuildingBlockFail() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(204).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callDelete(String.format("%s/buildingblock/%s", UDEUSTO_DE_URL, artifactID), token);

        Mockito.doThrow(new URISyntaxException("", "")).when(de).logArtifact(artifactID,
                DE.ArtifactType.BUILDING_BLOCK.getName(), "Artifact Removed", "ArtifactRemoved");

        de.removeBuildingBlock(artifactID, token);
    }

    @Test
    public void testRecommendAppDataset() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));

        de.recommendAppDataset(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.APP.getName()), Matchers.eq(DE.ArtifactType.DATASET.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendAppDatasetException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));

        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendAppDataset(artifactID);
    }

    @Test
    public void testRecommendAppBuildingBlock() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));

        de.recommendAppBuildingBlock(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.APP.getName()), Matchers.eq(DE.ArtifactType.BUILDING_BLOCK.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendAppBuildingBlockException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));

        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendAppBuildingBlock(artifactID);
    }

    @Test
    public void testRecommenAppApp() throws URISyntaxException, WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        String stringResponse = "[1, 2, 3, 4]";
        JSONArray responseJSON = new JSONArray(stringResponse);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        de.recommendAppApp(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));

        Mockito.verify(de, Mockito.times(1)).logRecommendedArtifacts(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture(),
                Matchers.eq(DE.ArtifactType.APP.getName()), Matchers.eq(DE.ArtifactType.APP.getName()));
        JSONAssert.assertEquals(responseJSON, jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommenAppAppException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        String stringResponse = "[1, 2, 3, 4]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logRecommendedArtifacts(Matchers.anyString(),
                Matchers.any(JSONArray.class), Matchers.anyString(), Matchers.anyString());

        de.recommendAppApp(artifactID);
    }

    @Test
    public void testUpdateApp() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        AppRequest appRequest = new AppRequest("Spanish", tagList, "Bilbao", 12);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"scope\": \"Bilbao\", \"min_age\": 12, \"id\": \"%s\"}", new JSONArray(tagList), artifactID));
        
        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/app/", UDEUSTO_DE_URL)), Matchers.anyString(), 
        		Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));

        de.updateApp(artifactID, appRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPOST(Matchers.eq(String.format("%s/app/", UDEUSTO_DE_URL)), 
        		stringArgumentCaptor.capture(), Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.APP.getName(),
                "Artifact Published", "ArtifactPublished");
        JSONAssert.assertEquals(inputJSON, new JSONObject(stringArgumentCaptor.getValue()), false);
    }

    @Test
    public void TestUpdateExistingApp() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        AppRequest appRequest = new AppRequest("Spanish", tagList, "Bilbao", 12);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"scope\": \"Bilbao\", \"min_age\": 12, \"id\": \"%s\"}", new JSONArray(tagList), artifactID));

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(200).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID));

        Mockito.doReturn(mockedResponse).when(de).callPUT(Matchers.eq(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID)), Matchers.anyString(),
                Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));

        de.updateApp(artifactID, appRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPUT(Matchers.eq(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID)),
                stringArgumentCaptor.capture(), Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        JSONAssert.assertEquals(inputJSON, new JSONObject(stringArgumentCaptor.getValue()), false);
    }

    @Test(expected = WeLiveException.class)
    public void TestUpdateAppEmptyBody() throws WeLiveException {
        de.updateApp(artifactID, null, token);
    }

    @Test(expected = WeLiveException.class)
    public void testUpdateAppException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        AppRequest appRequest = new AppRequest("Spanish", tagList, "Bilbao", 12);

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID));
        
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/app/", UDEUSTO_DE_URL)), Matchers.anyString(), 
        		Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logArtifact(artifactID, DE.ArtifactType.APP.getName(),
                "Artifact Published", "ArtifactPublished");

        de.updateApp(artifactID, appRequest, token);
    }

    @Test
    @Ignore
    public void testUpdateApp400() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tagList = new ArrayList<>();
        tagList.add("tag1");
        tagList.add("tag2");
        AppRequest appRequest = new AppRequest("Spanish", tagList, "Bilbao", 12);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", \"tags\": %s, " +
                "\"scope\": \"Bilbao\", \"minimum_age\": 12, \"id\": \"%s\"}", new JSONArray(tagList), artifactID));
        Mockito.doReturn(500).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callPOST(String.format("%s/app/", UDEUSTO_DE_URL), inputJSON.toString(), MediaType.APPLICATION_JSON, token);

        Response response = de.updateApp(artifactID, appRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPOST("de/update_artifact", inputJSON.toString(), MediaType.TEXT_PLAIN, token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.APP.getName(),
                "Artifact Published", "ArtifactPublished");

        assertEquals(500, response.getStatus());
    }

    @Test
    public void testRemoveApp() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(204).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callDelete(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID), token);

        de.removeApp(artifactID, token);

        Mockito.verify(de, Mockito.times(1)).callDelete(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID), token);
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.APP.getName(), "Artifact Removed", "ArtifactRemoved");
    }

    @Test(expected = WeLiveException.class)
    public void testRemoveAppException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(204).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callDelete(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID), token);
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logArtifact(artifactID,
                DE.ArtifactType.APP.getName(), "Artifact Removed", "ArtifactRemoved");

        de.removeApp(artifactID, token);
    }

    @Test
    @Ignore
    public void testRemoveApp400() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(500).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("de/remove_artifact/%s", artifactID));

        Response response = de.removeApp(artifactID, token);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("de/remove_artifact/%s", artifactID));
        Mockito.verify(de, Mockito.times(1)).logArtifact(artifactID, DE.ArtifactType.APP.getName(), "Artifact Removed", "ArtifactRemoved");

        assertEquals(500, response.getStatus());
    }

    @Test
    public void testAppRecommendation() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        double lat = 43.272139;
        double lon = -2.939277;
        double radius = 10.0;

        String responseString = "[1, 2, 3]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(responseString).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/user/%s/apps/?lat=%s&lon=%s&radius=%s", UDEUSTO_DE_URL, userID, lat, lon, radius));

        de.appRecommendation(userID, lat, lon, radius);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/user/%s/apps/?lat=%s&lon=%s&radius=%s", UDEUSTO_DE_URL, userID, lat, lon, radius));
        Mockito.verify(de, Mockito.times(1)).logApp(Matchers.eq(userID), Matchers.eq(lat), Matchers.eq(lon),
                Matchers.eq(radius), jsonArrayArgumentCaptor.capture());
        JSONAssert.assertEquals(new JSONArray("[1, 2, 3]"), jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testAppRecommendationException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        double lat = 43.272139;
        double lon = -2.939277;
        double radius = 10.0;

        String stringResponse = "[1, 2, 3]";

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(stringResponse).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/user/%s/apps/?lat=%s&lon=%s&radius=%s", UDEUSTO_DE_URL, userID, lat, lon, radius));

        Mockito.doThrow(new URISyntaxException("", "")).when(de).logApp(Matchers.eq(userID), Matchers.eq(lat), Matchers.eq(lon),
                Matchers.eq(radius), Matchers.any(JSONArray.class));

        de.appRecommendation(userID, lat, lon, radius);
    }

    @Test
    public void testUpdateIdea() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", " +
                "\"tags\": [\"tag1\", \"tag2\", \"tag3\"], \"id\": \"%s\"}", artifactID));

        List<String> tags = new ArrayList<String>();
        
        tags.add("tag1");
        tags.add("tag2");
        tags.add("tag3");
        
        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID));
        
        IdeaRequest ideaRequest = new IdeaRequest("Spanish", tags);
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/idea/", UDEUSTO_DE_URL)), 
        		Matchers.anyString(), Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));

        de.updateIdea(artifactID, ideaRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPOST(Matchers.eq(String.format("%s/idea/", UDEUSTO_DE_URL)), 
        		stringArgumentCaptor.capture(), Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        Mockito.verify(de, Mockito.times(1)).logIdea(artifactID);
        JSONAssert.assertEquals(inputJSON, new JSONObject(stringArgumentCaptor.getValue()), false);
    }

    @Test
    public void testUpdateExistingIdea() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

        JSONObject inputJSON = new JSONObject(String.format("{\"lang\": \"Spanish\", " +
                "\"tags\": [\"tag1\", \"tag2\", \"tag3\"], \"id\": \"%s\"}", artifactID));

        List<String> tags = new ArrayList<String>();

        tags.add("tag1");
        tags.add("tag2");
        tags.add("tag3");

        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(200).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID));

        IdeaRequest ideaRequest = new IdeaRequest("Spanish", tags);
        Mockito.doReturn(mockedResponse).when(de).callPUT(Matchers.eq(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID)),
                Matchers.anyString(), Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));

        de.updateIdea(artifactID, ideaRequest, token);

        Mockito.verify(de, Mockito.times(1)).callPUT(Matchers.eq(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID)),
                stringArgumentCaptor.capture(), Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));

        JSONAssert.assertEquals(inputJSON, new JSONObject(stringArgumentCaptor.getValue()), false);
    }

    @Test(expected = WeLiveException.class)
    public void testUpdateIdeaEmtpyBody() throws WeLiveException {
        de.updateIdea(artifactID, null, token);
    }

    @Test(expected = WeLiveException.class)
    public void testUpdateIdeaFail() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        List<String> tags = new ArrayList<String>();
        
        tags.add("tag1");
        tags.add("tag2");
        tags.add("tag3");
        
        Response mockedGetResponse = Mockito.mock(Response.class);
        Mockito.doReturn(404).when(mockedGetResponse).getStatus();
        Mockito.doReturn(mockedGetResponse).when(de).callGET(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID));
        
        IdeaRequest ideaRequest = new IdeaRequest("Spanish", tags);
        Mockito.doReturn(mockedResponse).when(de).callPOST(Matchers.eq(String.format("%s/idea/", UDEUSTO_DE_URL)), 
        		Matchers.anyString(), Matchers.eq(MediaType.APPLICATION_JSON), Matchers.eq(token));
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logIdea(artifactID);

        de.updateIdea(artifactID, ideaRequest, token);

    }

    @Test
    public void testRemoveIdea() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(mockedResponse).when(de).callDelete(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID), token);

        de.removeIdea(artifactID, token);

        Mockito.verify(de, Mockito.times(1)).callDelete(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID), token);
    }

    @Test
    public void testRecommendIdea() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);
        ArgumentCaptor<JSONArray> jsonArrayArgumentCaptor = ArgumentCaptor.forClass(JSONArray.class);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn("[1, 2, 3]").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/idea/%s/recommend/idea/", UDEUSTO_DE_URL, artifactID));

        de.recommendIdea(artifactID);

        Mockito.verify(de, Mockito.times(1)).callGET(String.format("%s/idea/%s/recommend/idea/", UDEUSTO_DE_URL, artifactID));
        Mockito.verify(de, Mockito.times(1)).logIdeaRecommended(Matchers.eq(artifactID), jsonArrayArgumentCaptor.capture());
        JSONAssert.assertEquals("[1, 2, 3]", jsonArrayArgumentCaptor.getValue(), true);
    }

    @Test(expected = WeLiveException.class)
    public void testRecommendIdeaException() throws WeLiveException, URISyntaxException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn("[1, 2, 3]").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(mockedResponse).when(de).callGET(String.format("%s/idea/%s/recommend/idea/", UDEUSTO_DE_URL, artifactID));
        Mockito.doThrow(new URISyntaxException("", "")).when(de).logIdeaRecommended(Matchers.eq(artifactID), Matchers.any(JSONArray.class));

        de.recommendIdea(artifactID);
    }

    @Test
    public void testCallGetSimple() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(mockedResponse).when(de).callGET("/any/subpath");

        de.callGET("/any/subpath");

        Mockito.verify(de, Mockito.times(1)).callGET("/any/subpath");

    }

    @Test
    public void testCallGetAnonymousAccess() throws WeLiveException, IOException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).get();
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        Response response = de.callGET("subpath");

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).get();

        assertEquals(mockedResponse, response);
    }

    @Test
    public void testCallGetNullPassword() throws WeLiveException, IOException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).get();
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        Response response = de.callGET("subpath");

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).get();

        assertEquals(mockedResponse, response);
    }

    @Test
    public void testCallGetNullUsername() throws WeLiveException, IOException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).get();
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        Response response = de.callGET("subpath");

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).get();

        assertEquals(mockedResponse, response);
    }

    @Test
    @Ignore
    public void testCallGetAuthAccess() throws IOException, WeLiveException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);
        String DE_URL = Config.getInstance().getDecisionEngineURL();

        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).get();
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        Response response = de.callGET("subpath");

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).get();
        String encoding = Base64.encodeAsString(String.format("%s:%s", "user", "password"));
        Mockito.verify(mockedRequest, Mockito.times(1)).header("Authorization", "Basic " + encoding);

        assertEquals(mockedResponse, response);
    }

    @Test(expected = WeLiveException.class)
    @Ignore
    public void testCallGet404() throws IOException, WeLiveException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);
        Response.StatusType mockedStatusType = Mockito.mock(Response.StatusType.class);


        String DE_URL = Config.getInstance().getDecisionEngineURL();

        Mockito.doReturn("").when(mockedStatusType).getReasonPhrase();
        Mockito.doReturn(mockedStatusType).when(mockedResponse).getStatusInfo();
        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(404).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).get();
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s/%s", DE_URL, "subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        de.callGET("subpath");

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s/%s", DE_URL, "subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).get();
    }

    @Test
    public void testCallPostSimple() throws WeLiveException {
        Response mockedResponse = Mockito.mock(Response.class);

        Mockito.doReturn(mockedResponse).when(de).callPOST("/any/subpath", "{}", MediaType.APPLICATION_JSON, null);

        de.callPOST("/any/subpath", "{}", MediaType.APPLICATION_JSON, null);

        Mockito.verify(de, Mockito.times(1)).callPOST("/any/subpath", "{}", MediaType.APPLICATION_JSON, null);
    }

    @Test
    @Ignore
    public void testCallPostAnonymousAccess() throws IOException, WeLiveException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);
        PowerMockito.mockStatic(Entity.class);
        Entity mockedEntity = Mockito.mock(Entity.class);

        String DE_URL = Config.getInstance().getDecisionEngineURL();

        PowerMockito.when(Entity.entity(Matchers.any(JSONObject.class), Matchers.eq(MediaType.APPLICATION_JSON))).thenReturn(mockedEntity);
        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s/%s", DE_URL, "subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        Response response = de.callPOST("subpath", "{}", MediaType.APPLICATION_JSON, null);

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s/%s", DE_URL, "subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);

        assertEquals(mockedResponse, response);
    }

    @Test
    @Ignore
    public void testCallPostNullPassword() throws WeLiveException, IOException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);
        PowerMockito.mockStatic(Entity.class);
        Entity mockedEntity = Mockito.mock(Entity.class);

        String DE_URL = Config.getInstance().getDecisionEngineURL();

        PowerMockito.when(Entity.entity(Matchers.any(JSONObject.class), Matchers.eq(MediaType.APPLICATION_JSON))).thenReturn(mockedEntity);
        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s/%s", DE_URL, "subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        Response response = de.callPOST("subpath", "{}", MediaType.APPLICATION_JSON, "user");

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s/%s", DE_URL, "subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);

        assertEquals(mockedResponse, response);
    }

    @Test
    @Ignore
    public void testCallPostNullUsername() throws IOException, WeLiveException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);
        PowerMockito.mockStatic(Entity.class);
        Entity mockedEntity = Mockito.mock(Entity.class);

        String DE_URL = Config.getInstance().getDecisionEngineURL();

        PowerMockito.when(Entity.entity(Matchers.any(JSONObject.class), Matchers.eq(MediaType.APPLICATION_JSON))).thenReturn(mockedEntity);
        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s/%s", DE_URL, "subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        Response response = de.callPOST("subpath", "{}", MediaType.APPLICATION_JSON, "password");

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s/%s", DE_URL, "subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);

        assertEquals(mockedResponse, response);
    }

    @Test
    @Ignore
    public void testCallPostAuthAccess() throws WeLiveException, IOException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);
        PowerMockito.mockStatic(Entity.class);
        Entity mockedEntity = Mockito.mock(Entity.class);

        String DE_URL = Config.getInstance().getDecisionEngineURL();

        PowerMockito.when(Entity.entity(Matchers.any(JSONObject.class), Matchers.eq(MediaType.APPLICATION_JSON))).thenReturn(mockedEntity);
        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(200).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s/%s", DE_URL, "subpath"));
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        String auth = String.format("Basic %s", Base64.encodeAsString("user:password"));
        
        Response response = de.callPOST("subpath", "{}", MediaType.APPLICATION_JSON, auth);

        Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s/%s", DE_URL, "subpath"));
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);
        String encoding = Base64.encodeAsString(String.format("%s:%s", "user", "password"));
        Mockito.verify(mockedRequest, Mockito.times(1)).header("Authorization", "Basic " + encoding);

        assertEquals(mockedResponse, response);
    }

    @Test(expected = WeLiveException.class)
    public void testCallPost404() throws WeLiveException, IOException {
        PowerMockito.mockStatic(ClientBuilder.class);
        Client mockedClient = Mockito.mock(Client.class);
        WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
        Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
        Response mockedResponse = Mockito.mock(Response.class);
        PowerMockito.mockStatic(Entity.class);
        Entity mockedEntity = Mockito.mock(Entity.class);
        Response.StatusType mockedStatusType = Mockito.mock(Response.StatusType.class);


        String DE_URL = Config.getInstance().getDecisionEngineURL();

        Mockito.doReturn("").when(mockedStatusType).getReasonPhrase();
        Mockito.doReturn(mockedStatusType).when(mockedResponse).getStatusInfo();
        PowerMockito.when(Entity.entity(Matchers.any(JSONObject.class), Matchers.eq(MediaType.APPLICATION_JSON))).thenReturn(mockedEntity);
        Mockito.doReturn("OK Response").when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(404).when(mockedResponse).getStatus();
        Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
        Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
        Mockito.doReturn(mockedWebTarget).when(mockedClient).target("path");
        PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

        de.callPOST("path", "{}", MediaType.APPLICATION_JSON, null);

        Mockito.verify(mockedClient, Mockito.times(1)).target("path");
        Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
        Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);
    }

    @Test
    public void testLogArtifact() throws Exception {
        DE de = Mockito.spy(DE.class);
        Date date = PowerMockito.mock(Date.class);

        Mockito.doReturn(new Long(100000)).when(date).getTime();
        PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(date);

        ArgumentCaptor<JSONObject> jsonObjectArgumentCaptor = ArgumentCaptor.forClass(JSONObject.class);

        Mockito.doNothing().when(de).sendLog(Matchers.anyString(), Matchers.any(JSONObject.class));
        JSONObject logJSON = new JSONObject(String.format("{\"msg\": \"msg\", \"appId\": \"de\", \"type\": \"type\", " +
                        "\"timestamp\": %s, \"custom_attr\": {\"artifactid\": \"%s\", \"artifacttype\": \"dataset\"}}",
                new Date().getTime() / 1000, artifactID));

        de.logArtifact(artifactID, "dataset", "msg", "type");

        Mockito.verify(de, Mockito.times(1)).sendLog(Matchers.eq("de"), jsonObjectArgumentCaptor.capture());
        JSONAssert.assertEquals(logJSON, jsonObjectArgumentCaptor.getValue(), false);
    }

    @Test
    public void testLogRecommendedArtifacts() throws Exception {
        DE de = Mockito.spy(DE.class);
        Date date = PowerMockito.mock(Date.class);

        Mockito.doReturn(new Long(100000)).when(date).getTime();
        PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(date);

        ArgumentCaptor<JSONObject> jsonObjectArgumentCaptor = ArgumentCaptor.forClass(JSONObject.class);

        Mockito.doNothing().when(de).sendLog(Matchers.anyString(), Matchers.any(JSONObject.class));
        JSONObject logJSON = new JSONObject(String.format("{\"msg\": \"Artifact Recommended\", \"appId\": \"de\", \"type\": \"ArtifactRecommended\", " +
                        "\"timestamp\": %s, \"custom_attr\": {\"sourceartifactid\": \"%s\", " +
                        "\"sourceartifacttype\": \"dataset\", \"recommendedartifacttype\": \"dataset\", " +
                        "\"recommendedartifactlist\": [1, 2, 3]}}",
                new Date().getTime() / 1000, artifactID));

        JSONArray resultJSON = new JSONArray("[1, 2, 3]");

        de.logRecommendedArtifacts(artifactID, resultJSON, "dataset", "dataset");

        Mockito.verify(de, Mockito.times(1)).sendLog(Matchers.eq("de"), jsonObjectArgumentCaptor.capture());
        JSONAssert.assertEquals(logJSON, jsonObjectArgumentCaptor.getValue(), false);
    }

    @Test
    public void testLogApp() throws Exception {
        DE de = Mockito.spy(DE.class);
        Date date = PowerMockito.mock(Date.class);

        Mockito.doReturn(new Long(100000)).when(date).getTime();
        PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(date);

        ArgumentCaptor<JSONObject> jsonObjectArgumentCaptor = ArgumentCaptor.forClass(JSONObject.class);

        Mockito.doNothing().when(de).sendLog(Matchers.anyString(), Matchers.any(JSONObject.class));
        JSONObject logJSON = new JSONObject(String.format("{\"msg\": \"User Recommendation\", \"appId\": \"de\", \"type\": \"UserRecommendation\", " +
                        "\"timestamp\": %s, \"custom_attr\": {\"userid\": \"%s\", " +
                        "\"lat\": 42.3, \"lon\": -2.3, " +
                        "\"radius\": 10.0, \"recommendedapplist\": [1, 2, 3]}}",
                new Date().getTime() / 1000, userID));

        JSONArray jsonResult = new JSONArray("[1, 2, 3]");

        de.logApp(userID, 42.3, -2.3, 10.0, jsonResult);

        Mockito.verify(de, Mockito.times(1)).sendLog(Matchers.eq("de"), jsonObjectArgumentCaptor.capture());
        JSONAssert.assertEquals(logJSON, jsonObjectArgumentCaptor.getValue(), false);
    }

    @Test
    public void testLogIdea() throws Exception {
        DE de = Mockito.spy(DE.class);
        Date date = PowerMockito.mock(Date.class);

        Mockito.doReturn(new Long(100000)).when(date).getTime();
        PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(date);

        ArgumentCaptor<JSONObject> jsonObjectArgumentCaptor = ArgumentCaptor.forClass(JSONObject.class);

        Mockito.doNothing().when(de).sendLog(Matchers.anyString(), Matchers.any(JSONObject.class));
        JSONObject logJSON = new JSONObject(String.format("{\"msg\": \"Idea Published\", \"appId\": \"de\", \"type\": \"IdeaPublished\", " +
                        "\"timestamp\": %s, \"custom_attr\": {\"ideaid\": \"%s\"}}",
                new Date().getTime() / 1000, ideaID));

        de.logIdea(ideaID);

        Mockito.verify(de, Mockito.times(1)).sendLog(Matchers.eq("de"), jsonObjectArgumentCaptor.capture());
        JSONAssert.assertEquals(logJSON, jsonObjectArgumentCaptor.getValue(), false);
    }

    @Test
    public void testLogIdeaRecommended() throws Exception {
        DE de = Mockito.spy(DE.class);
        Date date = PowerMockito.mock(Date.class);

        Mockito.doReturn(new Long(100000)).when(date).getTime();
        PowerMockito.whenNew(Date.class).withNoArguments().thenReturn(date);

        ArgumentCaptor<JSONObject> jsonObjectArgumentCaptor = ArgumentCaptor.forClass(JSONObject.class);

        Mockito.doNothing().when(de).sendLog(Matchers.anyString(), Matchers.any(JSONObject.class));
        JSONObject logJSON = new JSONObject(String.format("{\"msg\": \"Idea Recommended\", \"appId\": \"de\", \"type\": \"IdeaRecommended\", " +
                        "\"timestamp\": %s, \"custom_attr\": {\"sourceideaid\": \"%s\", \"recommendedidealist\": [1, 2, 3]}}",
                new Date().getTime() / 1000, ideaID));

        JSONArray jsonResult = new JSONArray("[1, 2, 3]");

        de.logIdeaRecommended(ideaID, jsonResult);

        Mockito.verify(de, Mockito.times(1)).sendLog(Matchers.eq("de"), jsonObjectArgumentCaptor.capture());
        JSONAssert.assertEquals(logJSON, jsonObjectArgumentCaptor.getValue(), false);
    }
}