/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.de.pojo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import eu.welive.rest.app.TestApplication;

/**
 * Created by mikel on 6/06/16.
 */
public class TestAppRequest extends JerseyTest {

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Test
    public void testConstructor() {
        AppRequest appRequest = new AppRequest();

        assertEquals("", appRequest.getLang());
        assertTrue(appRequest.getTags().equals(new ArrayList<String>()));
        assertEquals("", appRequest.getScope());
        assertEquals(0, appRequest.getMinimumAge());
    }

    @Test
    public void testConstructorWithArguments() {
        List<String> tags = new ArrayList<String>();
        tags.add("tag1");
        tags.add("tag2");

        AppRequest appRequest = new AppRequest("Spanish", tags, "Bilbao", 13);

        assertEquals("Spanish", appRequest.getLang());
        assertTrue(tags.equals(appRequest.getTags()));
        assertEquals("Bilbao", appRequest.getScope());
        assertEquals(13, appRequest.getMinimumAge());
    }

    @Test
    public void testSetters() {
        List<String> tags = new ArrayList<String>();
        tags.add("tag1");
        tags.add("tag2");

        AppRequest appRequest = new AppRequest();

        appRequest.setLang("Spanish");
        appRequest.setTags(tags);
        appRequest.setScope("Bilbao");
        appRequest.setMinimumAge(13);

        assertEquals("Spanish", appRequest.getLang());
        assertTrue(tags.equals(appRequest.getTags()));
        assertEquals("Bilbao", appRequest.getScope());
        assertEquals(13, appRequest.getMinimumAge());
    }
}