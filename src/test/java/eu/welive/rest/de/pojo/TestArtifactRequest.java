/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.de.pojo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import eu.welive.rest.app.TestApplication;

/**
 * Created by mikel on 6/06/16.
 */
public class TestArtifactRequest extends JerseyTest {
    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Test
    public void testConstructor() {
        ArtifactRequest artifactRequest = new ArtifactRequest();

        assertEquals("", artifactRequest.getLang());
        assertTrue(artifactRequest.getTags().equals(new ArrayList<String>()));
    }

    @Test
    public void testConstructorWithArguments() {
        List<String> tags = new ArrayList<String>();
        tags.add("tag1");
        tags.add("tag2");

        ArtifactRequest artifactRequest = new ArtifactRequest("Spanish", tags);

        assertEquals("Spanish", artifactRequest.getLang());
        assertTrue(tags.equals(artifactRequest.getTags()));
    }

    @Test
    public void testSetters() {
        ArtifactRequest artifactRequest = new ArtifactRequest();

        List<String> tags = new ArrayList<String>();
        tags.add("tag1");
        tags.add("tag2");

        artifactRequest.setLang("Spanish");
        artifactRequest.setTags(tags);

        assertEquals("Spanish", artifactRequest.getLang());
        assertTrue(tags.equals(artifactRequest.getTags()));
    }
}