/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.scheduler;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.quartz.Scheduler;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager.Status;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANPackage;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({CKANUtil.class, DatasetStatusManager.class, WeLiveMainJob.class})
public class WeLiveMainJobTest {
	
	private static final String CREATOR_ID = "10";
	private static final String ORGANIZATION_ID = "1";
	
	private final static String MAPPING = "{\r\n   \"mapping\": \"json\",\r\n   \"uri\": \"http://someuri/data.json\",\r\n   \"root\": \"results\",\r\n   \"key\": \"_id\",\r\n   \"table\": \"main\",\r\n   \"refresh\": 86400\r\n}";
	private final static String MAPPING_NOT_AVAILABLE = "{}";
	
	private final static String MAPPING_JSON_SCHEMA = "{\r\n   \"mapping\":\"json_schema\",\r\n   \"schema\":{\r\n      \"tables\":[\r\n         {\r\n            \"key\":\"key1\",\r\n            \"name\":\"table1\",\r\n            \"table1\":[\r\n               {\r\n                  \"key1\": \"value1\",\r\n                  \"key2\": 10,\r\n                  \"key3\": \"2015-01-01\"\r\n               }\r\n            ]\r\n         },\r\n         {\r\n            \"key\":\"key2\",\r\n            \"name\":\"table2\",\r\n            \"table2\":[\r\n               {\r\n                  \"key1\": \"value1\",\r\n                  \"key2\": \"value2\",\r\n                  \"key3\": 5\r\n               }\r\n            ]\r\n         }\r\n      ]\r\n   }\r\n}";
	
	private SchedulerContext schContext;
	private Scheduler sch;
	private WeLiveMainJob weliveMainJob;
	
	private DatasetStatusManager datasetStatusManager;
	private CKANUtil ckanUtil;
	
	@Before
	public void setUp() throws Exception {
		schContext = Mockito.mock(SchedulerContext.class);
		when(schContext.get(WeLiveMainJob.LAST_REVISION))
			.thenReturn(new DateTime(2016, 10, 24, 10, 20));
		
		sch = Mockito.mock(Scheduler.class);
		when(sch.getContext())
			.thenReturn(schContext);
		
		datasetStatusManager = PowerMockito.mock(DatasetStatusManager.class);
		PowerMockito.mockStatic(DatasetStatusManager.class);
		when(DatasetStatusManager.getInstance())
			.thenReturn(datasetStatusManager);
		
		ckanUtil = PowerMockito.mock(CKANUtil.class);
		PowerMockito.whenNew(CKANUtil.class)
			.withNoArguments()
			.thenReturn(ckanUtil);
		
		weliveMainJob = Mockito.spy(new WeLiveMainJob());
		
		Mockito.doNothing().when(weliveMainJob).createDatasourceJob(Matchers.anyString(), Matchers.any(DataSourceConnector.class), Matchers.any(Scheduler.class));
		Mockito.doNothing().when(weliveMainJob).initializeUserDataset(Matchers.any(JSONSchemaConnector.class), Matchers.anyString());
	}
	
	private Map<String, CKANPackage> createPackages(String mapping) {
		final Map<String, CKANPackage> packages = new HashMap<String, CKANPackage>();
		
		final List<CKANResource> resources = new ArrayList<CKANResource>();
		final CKANResource resourceInfo = new CKANResource("sample-resource-1", "sample-package-1", mapping, "{}");
		resources.add(resourceInfo);
	
		final CKANPackage packageInfo = new CKANPackage("sample-package-1", CREATOR_ID, ORGANIZATION_ID, resources);
		packages.put("sample-package-1", packageInfo);
		
		return packages;
	} 
	
	private List<String> createUpdatedPackages(Map<String, CKANPackage> packages) { 		
		return new ArrayList<String>(packages.keySet());
	}

	@Test
	public void testCheckJobs() throws CKANUtilException, DatasetStatusManagerException, IOException, SchedulerException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING);
		
		when(ckanUtil.getUpdatedPackages(Matchers.any(DateTime.class)))
			.thenReturn( createUpdatedPackages(packages));
		
		when(ckanUtil.getPackageInfo("sample-package-1"))
			.thenReturn(packages.get("sample-package-1"));
		
		weliveMainJob.checkJobs(sch);
		
		verify(schContext, times(1)).get(WeLiveMainJob.LAST_REVISION);
		
		verify(datasetStatusManager, times(1)).updateStatus("sample-resource-1", Status.PROCESSING, "Data download job created");
		
		verify(weliveMainJob, times(1)).createDatasourceJob(Matchers.anyString(), Matchers.any(DataSourceConnector.class), Matchers.any(Scheduler.class));
	}
	
	@Test
	public void testCheckJobsPackageInfoError() throws CKANUtilException, DatasetStatusManagerException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING);
		
		when(ckanUtil.getUpdatedPackages(Matchers.any(DateTime.class)))
			.thenReturn(createUpdatedPackages(packages));
		
		when(ckanUtil.getPackageInfo("sample-package-1"))
			.thenThrow(new CKANUtilException("Some problem occurred"));
		
		weliveMainJob.checkJobs(sch);
		
		verify(schContext, times(1)).get(WeLiveMainJob.LAST_REVISION);
		
		verify(datasetStatusManager, times(1)).updateStatus("sample-package-1", Status.ERROR, "Problem obtaining package information. Some problem occurred");
	}
	
	@Test
	public void testCheckJobsMappingInvalid() throws CKANUtilException, DatasetStatusManagerException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING_NOT_AVAILABLE);
		
		when(ckanUtil.getUpdatedPackages(Matchers.any(DateTime.class)))
			.thenReturn(createUpdatedPackages(packages));
		
		when(ckanUtil.getPackageInfo("sample-package-1"))
			.thenReturn(packages.get("sample-package-1"));
		
		weliveMainJob.checkJobs(sch);
		
		verify(schContext, times(1)).get(WeLiveMainJob.LAST_REVISION);
		
		verify(datasetStatusManager, times(1)).updateStatus("sample-resource-1", Status.ERROR, "Problem loading mapping. \"\": object has missing required properties ([\"mapping\"])\n");
	}
	
	@Test
	public void testCheckJobsJsonSchema() throws CKANUtilException, DatasetStatusManagerException, DatabaseCreationException, DataSourceManagementException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING_JSON_SCHEMA);
		
		when(ckanUtil.getUpdatedPackages(Matchers.any(DateTime.class)))
			.thenReturn(createUpdatedPackages(packages));
		
		when(ckanUtil.getPackageInfo("sample-package-1"))
			.thenReturn(packages.get("sample-package-1"));
		
		weliveMainJob.checkJobs(sch);
		
		verify(schContext, times(1)).get(WeLiveMainJob.LAST_REVISION);
		
		verify(datasetStatusManager, times(1)).updateStatus("sample-resource-1", Status.PROCESSING, "Started creation of user storage");
		
		verify(weliveMainJob, times(1)).initializeUserDataset(Matchers.any(JSONSchemaConnector.class), Matchers.eq("sample-resource-1"));
		
		verify(datasetStatusManager, times(1)).updateStatus("sample-resource-1", Status.OK, "User storage created");
	}
	
	@Test
	public void testCheckJobsJsonSchemaError() throws CKANUtilException, DatabaseCreationException, DataSourceManagementException, DatasetStatusManagerException {
		final Map<String, CKANPackage> packages = createPackages(MAPPING_JSON_SCHEMA);
		
		when(ckanUtil.getUpdatedPackages(Matchers.any(DateTime.class)))
			.thenReturn(createUpdatedPackages(packages));
		
		when(ckanUtil.getPackageInfo("sample-package-1"))
			.thenReturn(packages.get("sample-package-1"));
		
		Mockito.doThrow(new DataSourceManagementException("Some problem occurred")).when(weliveMainJob).initializeUserDataset(Matchers.any(JSONSchemaConnector.class), Matchers.anyString());
		
		weliveMainJob.checkJobs(sch);
		
		verify(schContext, times(1)).get(WeLiveMainJob.LAST_REVISION);
		
		verify(datasetStatusManager, times(1)).updateStatus("sample-resource-1", Status.PROCESSING, "Started creation of user storage");
		
		verify(datasetStatusManager, times(1)).updateStatus("sample-resource-1", Status.ERROR, "Problem creating user dataset");
	}
}
