/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.security;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import eu.iescities.server.querymapper.datasource.security.Permissions.AccessType;

public class WeLiveDefaultPermissionsTest {

	private WeLiveDefaultPermissions p;
	
	@Before
	public void setUp() {
		p = new WeLiveDefaultPermissions();
	}

	@Test
	public void testGetDeleteDefaultPermission() {
		assertEquals(Arrays.asList(AccessType.NONE), p.getDeleteDefaultPermission());
	}

	@Test
	public void testGetInsertDefaultPermission() {
		assertEquals(Arrays.asList(AccessType.NONE), p.getInsertDefaultPermission());
	}

	@Test
	public void testGetSelectDefaultPermission() {
		assertEquals(Arrays.asList(AccessType.ALL), p.getSelectDefaultPermission());
	}

	@Test
	public void testGetUpdateDefaultPermission() {
		assertEquals(Arrays.asList(AccessType.NONE), p.getUpdateDefaultPermission());
	}
}
