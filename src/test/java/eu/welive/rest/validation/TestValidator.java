/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.validation;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.mockito.Mockito;

import eu.welive.rest.app.TestApplication;

/**
 * Created by mikel on 21/12/16.
 */
public class TestValidator extends JerseyTest {

    private String mockCSV = "COD_MSC;COD_CCAA;COD_EUROST;DENOMINACI;EUSTAT;Municipio;MUN_PROV;TERRITORIO;url_ficha;X_UTM;Y_UTM\n" +
            "1.746;CPV01008C;ES21100008C01008C;EMBALSE ULLIBARRI GAMBOA-LANDA;1.008;Arratzua-Ubarrundia;1;ARABA;Landa.pdf;533.563;4.755.713\n" +
            "1.746;CPV01008C;ES21100008C01008C;EMBALSE ULLIBARRI GAMBOA-LANDA;1.008;Arratzua-Ubarrundia;1;ARABA;Landa.pdf;533.612;4.755.453\n" +
            "1.619;CPV01008B;ES21100008C01008B;EMBALSE ULLIBARRI GAMBOA-ISLA ZUHATZA IRLA;1.008;Arratzua-Ubarrundia;1;ARABA;zuatza-irla.pdf;533.257;4.753.213\n" +
            "1.748;CPV01013B;ES21100013C01013B;EMBALSE ULLIBARRI GAMBOA-GARAIO;1.013;Barrundia;1;ARABA;garaio.pdf;537.329;4.750.736\n" +
            "1.748;CPV01013B;ES21100013C01013B;EMBALSE ULLIBARRI GAMBOA-GARAIO;1.013;Barrundia;1;ARABA;garaio.pdf;537.039;4.750.091\n" +
            "530;MPV20061A;ES21200061M20061A;PLAYA DE ANTILLA;20.061;Orio;20;GIPUZKOA;Antilla.pdf;570.724;4.793.299\n" +
            "540;MPV48017A;ES21300017M48017A;PLAYA DE ARITZATXU;48.017;Bermeo;48;BIZKAIA;Aritzatxu.pdf;521.889;4.808.149\n" +
            "2.036;MPV48056A;ES21300056M48056A;PLAYA DE ARMINTZA;48.056;Lemoiz;48;BIZKAIA;Armintza.pdf;508.700;4.808.870\n" +
            "557;MPV48085B;ES21300085M48085B;PLAYA DE ARRIATERA-ATXABIRIBIL;48.085;Sopelana;48;BIZKAIA;Arriatera-Atxabiribil.pdf;500.281;4.803.977\n" +
            "552;MPV48073A;ES21300073M48073A;PLAYA DE ARRIGORRI;48.073;Ondarroa;48;BIZKAIA;Arrigorri.pdf;547.205;4.796.895\n" +
            "544;MPV48044C;ES21300044M48044C;PLAYA DE ARRIGUNAGA;48.044;Getxo;48;BIZKAIA;Arrigunaga.pdf;498.388;4.800.315\n" +
            "543;MPV48044B;ES21300044M48044B;PLAYA DE AZKORRI (GORRONDATXE);48.044;Getxo;48;BIZKAIA;Azkorri-Gorrondatxe.pdf;498.674;4.803.099\n";

    private String mockInvalidCSV = "COD_MSC;COD_CCAA;COD_EUROST;DENOMINACI;EUSTAT;Municipio;MUN_PROV;TERRITORIO;url_ficha;X_UTM;Y_UTM\n" +
            "1.746;CPV01008C;ES21100008C01008C;EMBALSE ULLIBARRI GAMBOA-LANDA;1.008;Arratzua-Ubarrundia;1;ARABA;Landa.pdf;533.563;4.755.713\n" +
            "1.746;CPV01008C;ES21100008C01008C;EMBALSE ULLIBARRI GAMBOA-LANDA;1.008;Arratzua-Ubarrundia;1;ARABA;Landa.pdf;533.612;4.755.453\n" +
            "1.619;CPV01008B;ES21100008C01008B;EMBALSE ULLIBARRI GAMBOA-ISLA ZUHATZA IRLA;1.008;Arratzua-Ubarrundia;1;ARABA;zuatza-irla.pdf;533.257;4.753.213\n" +
            "1.748;CPV01013B;ES21100013C01013B;EMBALSE ULLIBARRI GAMBOA-GARAIO;1.013;Barrundia;1;ARABA;garaio.pdf;537.329;4.750.736\n" +
            "1.748;CPV01013B;ES21100013C01013B;EMBALSE ULLIBARRI GAMBOA-GARAIO;1.013;Barrundia;1;ARABA;garaio.pdf;537.039;4.750.091\n" +
            "530;MPV20061A;ES21200061M20061A;PLAYA DE ANTILLA;20.061;Orio;20;GIPUZKOA;Antilla.pdf;570.724;4.793.299\n" +
            "540;MPV48017A;ES21300017M48017A;PLAYA DE ARITZATXU;48.017;Bermeo;48;BIZKAIA;521.889;4.808.149\n" +
            "2.036;MPV48056A;ES21300056M48056A;PLAYA DE ARMINTZA;48.056;Lemoiz;48;BIZKAIA;Armintza.pdf;508.700;4.808.870\n" +
            "557;MPV48085B;ES21300085M48085B;PLAYA DE ARRIATERA-ATXABIRIBIL;48.085;Sopelana;48;BIZKAIA;Arriatera-Atxabiribil.pdf;500.281;4.803.977\n" +
            "552;MPV48073A;ES21300073M48073A;PLAYA DE ARRIGORRI;48.073;Ondarroa;48;BIZKAIA;Arrigorri.pdf;547.205;4.796.895\n" +
            "544;MPV48044C;ES21300044M48044C;PLAYA DE ARRIGUNAGA;48.044;Getxo;48;BIZKAIA;Arrigunaga.pdf;498.388;4.800.315\n" +
            "543;MPV48044B;ES21300044M48044B;PLAYA DE AZKORRI (GORRONDATXE);48.044;Getxo;48;BIZKAIA;Azkorri-Gorrondatxe.pdf;498.674;4.803.099\n";

    private String mockCSVSchema = "version 1.0\n" +
            "@separator ';'\n" +
            "@totalColumns 11\n" +
            "COD_MSC: notEmpty\n" +
            "COD_CCAA: notEmpty\n" +
            "COD_EUROST: notEmpty\n" +
            "DENOMINACI: notEmpty\n" +
            "COD_EUSTAT: notEmpty\n" +
            "Municipio: notEmpty\n" +
            "MUN_PROV: notEmpty\n" +
            "TERRITORIO: is(\"ARABA\") or is(\"BIZKAIA\") or is(\"GIPUZKOA\")\n" +
            "url_ficha: notEmpty\n" +
            "X_UTM: notEmpty\n" +
            "Y_UTM: notEmpty\n";

    private String mockValidRDF = "<?xml version=\"1.0\"?>\n" +
            "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "  xmlns:dc=\"http://purl.org/dc/elements/1.1/\">\n" +
            "  <rdf:Description rdf:about=\"http://www.w3.org/\">\n" +
            "    <dc:title>World Wide Web Consortium</dc:title> \n" +
            "  </rdf:Description>\n" +
            "</rdf:RDF>";

    private String mockInvalidRDF = "<?xml version=\"1.0\"?>\n" +
            "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "  xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n" +
            "  xmlns:foaf=\"http://xmlns.com/foaf/0.1/\">\n" +
            "  <foaf:FakePerson rdf:about=\"http://foo/bar\">\n" +
            "    <dc:fakeTitle rdf:resource=\"http://www.jimmywales.com/aus_img_small.jpg\"/> \n" +
            "  </foaf:FakePerson>\n" +
            "</rdf:RDF>";

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Test
    public void testValidCsv() {
        Validator validator = new Validator();

        InputStream csvStream = new ByteArrayInputStream(mockCSV.getBytes(StandardCharsets.UTF_8));
        InputStream schemaStream = new ByteArrayInputStream(mockCSVSchema.getBytes(StandardCharsets.UTF_8));

        ValidatorResponse response = validator.csv(csvStream, schemaStream);

        assertEquals("true", response.getResult());
        assertNull(response.getErrors());
    }

    @Test
    public void testInvalidCsv() {
        Validator validator = new Validator();

        InputStream csvStream = new ByteArrayInputStream(mockInvalidCSV.getBytes(StandardCharsets.UTF_8));
        InputStream schemaStream = new ByteArrayInputStream(mockCSVSchema.getBytes(StandardCharsets.UTF_8));

        ValidatorResponse response = validator.csv(csvStream, schemaStream);

        assertEquals("false", response.getResult());
        assertEquals(2, response.getErrors().size());

    }

    @Test
    public void testRdf() {
        Validator mockedValidator = Mockito.spy(Validator.class);
        ValidatorResponse mockedValidatorResponse = new ValidatorResponse("true", null);
        
        InputStream rdfStream = new ByteArrayInputStream(mockValidRDF.getBytes(StandardCharsets.UTF_8));

        Mockito.doReturn(mockedValidatorResponse).when(mockedValidator).rdf(rdfStream);
        
        ValidatorResponse response = mockedValidator.rdf(rdfStream);

        assertEquals("true", response.getResult());
        assertNull(response.getErrors());
    }

    @Test
    public void testInvalidRdf() {
    	Validator mockedValidator = Mockito.spy(Validator.class);
    	List<String> errorList = new ArrayList<String>();
    	errorList.add("error 1");
    	errorList.add("error 2");
    	
        ValidatorResponse mockedValidatorResponse = new ValidatorResponse("false", errorList);

        InputStream rdfStream = new ByteArrayInputStream(mockInvalidRDF.getBytes(StandardCharsets.UTF_8));

        Mockito.doReturn(mockedValidatorResponse).when(mockedValidator).rdf(rdfStream);
        
        ValidatorResponse response = mockedValidator.rdf(rdfStream);
        
        assertEquals("false", response.getResult());
        assertEquals(2, response.getErrors().size());
    }
}
