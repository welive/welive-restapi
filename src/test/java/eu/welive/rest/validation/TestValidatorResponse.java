/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.validation;

import eu.welive.rest.app.TestApplication;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import javax.ws.rs.core.Application;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created by mikel on 21/12/16.
 */
public class TestValidatorResponse extends JerseyTest {

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Test
    public void testEmptyConstructor() {
        ValidatorResponse validatorResponse = new ValidatorResponse();

        assertNotNull(validatorResponse);
    }

    @Test
    public void testSetResult() {
        ValidatorResponse validatorResponse = new ValidatorResponse();

        validatorResponse.setResult("true");
        assertEquals("true", validatorResponse.getResult());
    }

    @Test
    public void testSetErrors() {
        ValidatorResponse validatorResponse = new ValidatorResponse();
        List<String> stringList = new ArrayList<>();
        stringList.add("Error 1");

        validatorResponse.setErrors(stringList);

        assertEquals(stringList, validatorResponse.getErrors());
    }
}
