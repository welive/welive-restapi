/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.vc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientBuilder.class, Entity.class, Config.class, VC.class})
@PowerMockIgnore("javax.net.ssl.*")

public class TestVC extends JerseyTest{
	
	HttpHeaders headers;
	IdeaWorkgroup ideaData;
	Member member;
	Members members;
	Service service;
	//public static String baseurl = "https://test.welive.eu/visualcomposer/webservice";
	public static enum AuthType {Basic, Oauth};
	public static Integer ccUserID = 657;
	public static Boolean cascade = true;
	public static String token = "FAKE-TOKEN";
	public static Integer ccOrganizationId = 54637;
	
	@Override
    protected javax.ws.rs.core.Application configure() {
        return new TestApplication();
    }
	
	

	
	@Test
    public void testIdeaWorkgroup() throws Exception{
    	
		
		//String baseurl = "https://test.welive.eu/visualcomposer/webservice";
		 member = new Member();
		 ideaData = new IdeaWorkgroup();
		 ideaData.setAuthority("authority");
		 ideaData.setIdeaid(234);
		 ideaData.setIdeaname("idea");
		 ideaData.setWgname("wg");
		 ideaData.getWgname();
		 member.setUsername("username");
		 member.getUsername();
		 List<Member> members = new ArrayList<Member>();
		 members.add(member);
		 ideaData.setMembers(members);
		 ideaData.getAuthority();
		 ideaData.getIdeaid();
		 ideaData.getIdeaname();
		 ideaData.getMembers();
		 WgResponse wg = new WgResponse();
		 wg.setMessage("message");
		 wg.setType(2);
		 wg.getMessage();
		 wg.getType();
		 VC vc = Mockito.spy(new VC());
	        Response mockedResponse = Mockito.mock(Response.class);
	        String idea = "idea";
		 String url =Config.getInstance().getVcURL() +"/ideaworkgroup";
    	String token = "FAKE-TOKEN";
		Mockito.doReturn(mockedResponse).when(vc).post(url, token, idea);
    		vc.createWorkgroupForIdea(null, token, idea);
    		Mockito.verify(vc,Mockito.times(1)).post(url, token, idea);
    	 
    	 }
	
	
	@Test
	public void testRegisterUser() throws WeLiveException, Exception {
		JSONObject json = new JSONObject(String.format("{\"error\": true,\"message\": \"Only basic authentication is allowed\",}"));
		//String baseurl = "https://test.welive.eu/visualcomposer/webservice";
		String url = Config.getInstance().getVcURL() + "/registeruser";
		Birthdate b = new Birthdate();
		b.setDay(3);
		b.setMonth(5);
		b.setYear(2000);
		b.getDay();
		b.getMonth();
		b.getYear();
		UserData userData = new UserData();
		userData.setCcUserID(5);
		userData.setEmail("mail");
		userData.setFirstName("name");
		userData.setLastName("last");
		userData.getCcUserID();
		userData.getEmail();
		userData.getFirstName();
		userData.getLastName();
		userData.setAddress("address");
		userData.setBirthDate(b);
		userData.setCity("city");
		userData.setCountry("country");
		userData.setEmployement("empl");
		userData.setId(21);
		userData.setIsCompany(true);
		userData.setIsDeveloper(false);
		userData.setIsMale(true);
		userData.setLanguages(null);
		userData.setLiferayScreenName("life");
		userData.setReferredPilot("trento");
		userData.setRole("role");
		userData.setSecret("secret");
		userData.setZipCode("zip");
		userData.getAddress();
		userData.getBirthDate();
		userData.getCity();
		userData.getCountry();
		userData.getEmployement();
		userData.getId();
		userData.getIsCompany();
		userData.getIsDeveloper();
		userData.getIsMale();
		userData.getLanguages();
		userData.getLiferayScreenName();
		userData.getReferredPilot();
		userData.getRole();
		userData.getSecret();
		userData.setUsersSkills(null);
		userData.setUserTags(null);
		userData.getUsersSkills();
		userData.getUserTags();
		userData.getZipCode();
		VC vc = Mockito.spy(new VC());
        Response mockedResponse = Mockito.mock(Response.class);
		Mockito.doReturn(mockedResponse).when(vc).post(url, token, userData);
    		vc.registerUser(null, token, userData);
    		Mockito.verify(vc,Mockito.times(1)).post(url, token, userData);
	}
	
	@Test
	public void testDeleteUser() throws WeLiveException, Exception {
		
		String url = Config.getInstance().getVcURL() + "/deleteuser/"
				+ ccUserID+ "?cascade=" +cascade; 
		VC vc = Mockito.spy(new VC());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(vc).delete(url, token);
        vc.deleteUser(token, ccUserID, cascade);
        Mockito.verify(vc,Mockito.times(1)).delete(url, token);
        UserDeleteBean b = new UserDeleteBean();
        b.setError(true);
        b.setMessage("message");
        b.setReason("reason");
        b.setStatus(200);
        b.getError();
        b.getMessage();
        b.getReason();
        b.getStatus();
		
	}
	
	@Test
	public void testUpsertOrganization() throws WeLiveException, Exception {
		
		Organization organization = new Organization();
		organization.setccOrganizationId(4);
		organization.setInfo("info");
		organization.setLeaderId(4);
		organization.setName("name");
		organization.getccOrganizationId();
		organization.getInfo();
		organization.getLeaderId();
		organization.getName();
		String url = Config.getInstance().getVcURL() + "/upsert-organization"; 
		VC vc = Mockito.spy(new VC());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(vc).post(url, token, organization);
        vc.upsertOrganization(null, token, organization);
        Mockito.verify(vc,Mockito.times(1)).post(url, token, organization);
        VcResponse r = new VcResponse();
        r.setError(false);
        r.setMessage("no");
        r.setStatus(200);
        r.getError();
        r.getMessage();
        r.getStatus();
	}
	
	@Test
	public void testDeleteOrganization() throws WeLiveException, Exception {
		
		
		String url = Config.getInstance().getVcURL() + "/delete-organization"
				+ "/ccOrganizationId/"+ccOrganizationId;  
		VC vc = Mockito.spy(new VC());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(vc).delete(url, token);
        vc.deleteOrganization(token, ccOrganizationId);
        Mockito.verify(vc,Mockito.times(1)).delete(url, token);
        
	}
	
	@Test
	public void testAddMembers() throws WeLiveException, Exception {
		
		members = new Members();
		members.setCcOrganizationId(ccOrganizationId);
		members.getCcOrganizationId();
		members.getMembers();
		members.setMembers(null);
		String url = Config.getInstance().getVcURL() + "/add-organization-members";  
		VC vc = Mockito.spy(new VC());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(vc).post(url, token, members);
        vc.addMembers(null, token, members);
        Mockito.verify(vc,Mockito.times(1)).post(url, token, members);
        
	}
	
	@Test
	public void testRemoveMembers() throws WeLiveException, Exception {
		
		members = new Members();
		members.setCcOrganizationId(ccOrganizationId);
		members.getCcOrganizationId();
		String url = Config.getInstance().getVcURL() + "/remove-organization-members";  
		VC vc = Mockito.spy(new VC());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(vc).post(url, token, members);
        vc.removeMembers(null, token, members);
        Mockito.verify(vc,Mockito.times(1)).post(url, token, members);
        
	}
	
	@Test
	public void testValidateService() throws WeLiveException, Exception {
		
		service = new Service();
		service.setType("type");
		service.setUrl("url");
		service.getType();
		service.getUrl();
		
		ServiceResponse s = new ServiceResponse();
		s.setValid(true);
		s.setCode("code");
		s.setMessage("messsage");
		s.getValid();
		s.getCode();
		s.getMessage();
		String url = Config.getInstance().getVcURL() + "/validateService";  
		VC vc = Mockito.spy(new VC());
        Response mockedResponse = Mockito.mock(Response.class);
        Mockito.doReturn(mockedResponse).when(vc).post(url, token, service);
        vc.validateService(null, token, service);
        Mockito.verify(vc,Mockito.times(1)).post(url, token, service);
        
	}
	
}
