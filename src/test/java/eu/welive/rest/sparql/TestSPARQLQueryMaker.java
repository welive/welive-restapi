/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.sparql;

import static junit.framework.TestCase.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilder;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilderException;
import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * Created by mikel on 20/12/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({VirtModel.class, SPARQLQueryMaker.class, ResultSetFormatter.class, VirtuosoQueryExecutionFactory.class, Config.class})
@PowerMockIgnore({"javax.xml.*", "org.xml.sax.*", "org.w3c.dom.*", "org.apache.log4j.*"})
public class TestSPARQLQueryMaker extends JerseyTest {

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Test
    public void testCreate() throws IOException, WeLiveException {
        SPARQLQueryMaker sparqlQueryMaker = Mockito.spy(SPARQLQueryMaker.class);
        VirtModel mockedModel = Mockito.mock(VirtModel.class);
        PowerMockito.mockStatic(VirtModel.class);
        ArgumentCaptor<InputStreamReader> argumentCaptor = ArgumentCaptor.forClass(InputStreamReader.class);

        String testRDF = "FAKE RDF";
        InputStream stream = new ByteArrayInputStream(testRDF.getBytes(StandardCharsets.UTF_8));

        PowerMockito.when(VirtModel.openDatabaseModel("mockGraph", "jdbc:virtuoso://localhost:1111", "dba", "dba")).thenReturn(mockedModel);

        SPARQLResponse sparqlResponse = sparqlQueryMaker.create(stream, "mockGraph");

        assertEquals("Triples inserted successfully", sparqlResponse.getResponse());
        Mockito.verify(mockedModel, Mockito.times(1)).read(argumentCaptor.capture(), (String) Matchers.isNull());

        InputStreamReader value = argumentCaptor.getValue();
        StringWriter writer = new StringWriter();
        IOUtils.copy(value, writer);
        String expectedString = writer.toString();

        assertEquals(testRDF, expectedString);
        Mockito.verify(mockedModel, Mockito.times(1)).close();
        PowerMockito.verifyStatic(Mockito.times(1));
    }

    @Test(expected = WeLiveException.class)
    public void testCreateException() throws WeLiveException {
        SPARQLQueryMaker sparqlQueryMaker = Mockito.spy(SPARQLQueryMaker.class);
        PowerMockito.mockStatic(VirtModel.class);

        String testRDF = "FAKE RDF";
        InputStream stream = new ByteArrayInputStream(testRDF.getBytes(StandardCharsets.UTF_8));

        PowerMockito.when(VirtModel.openDatabaseModel("mockGraph", "jdbc:virtuoso://localhost:1111", "dba", "dba")).thenThrow(IOException.class);

        sparqlQueryMaker.create(stream, "mockGraph");
    }

    @Test
    public void testQuery() throws Exception {
        SPARQLQueryMaker sparqlQueryMaker = Mockito.spy(SPARQLQueryMaker.class);
        VirtGraph virtGraph = Mockito.mock(VirtGraph.class);
        PowerMockito.mockStatic(VirtuosoQueryExecutionFactory.class);
        VirtuosoQueryExecution vqe = Mockito.mock(VirtuosoQueryExecution.class);
        ResultSet rs = Mockito.mock(ResultSet.class);
        PowerMockito.mockStatic(ResultSetFormatter.class);
        PowerMockito.mockStatic(Files.class);

        String queryResponse = "FAKE RESPONSE QUERY";
        byte[] bytes = queryResponse.getBytes();

        PowerMockito.whenNew(VirtGraph.class).withArguments("mockGraph", "jdbc:virtuoso://localhost:1111", "dba", "dba").thenReturn(virtGraph);
        Mockito.when(vqe.execSelect()).thenReturn(rs);
        PowerMockito.when(VirtuosoQueryExecutionFactory.create(Matchers.any(Query.class), Matchers.eq(virtGraph))).thenReturn(vqe);
        PowerMockito.when(Files.readAllBytes(Matchers.any(Path.class))).thenReturn(bytes);

        Response response = sparqlQueryMaker.query("SELECT * WHERE {?s ?p ?o} LIMIT 10", "mockGraph");

        assertEquals(queryResponse, response.getEntity().toString());
        assertEquals(200, response.getStatus());

        Mockito.verify(vqe, Mockito.times(1)).execSelect();

    }

    @Test(expected = WeLiveException.class)
    public void testQueryException() throws IOException, WeLiveException {
        SPARQLQueryMaker sparqlQueryMaker = Mockito.spy(SPARQLQueryMaker.class);
        PowerMockito.mockStatic(Config.class);

        PowerMockito.when(Config.getInstance()).thenThrow(IOException.class);

        sparqlQueryMaker.query("SELECT * WHERE {?s ?p ?o} LIMIT 10", "mockGraph");
    }

    @Test
    public void testGetMapping() throws Exception {
        SPARQLQueryMaker sparqlQueryMaker = Mockito.spy(SPARQLQueryMaker.class);
        SchemaBuilder schemaBuilder = Mockito.mock(SchemaBuilder.class);
        SPARQLConnector schema = Mockito.mock(SPARQLConnector.class);

        PowerMockito.whenNew(SchemaBuilder.class).withArguments("http://mockendpoint.com/sparql", "mockGraph", false).thenReturn(schemaBuilder);
        Mockito.when(schema.toString()).thenReturn("mockedSchema");
        Mockito.when(schemaBuilder.getSchema()).thenReturn(schema);

        Response response = sparqlQueryMaker.getMapping("http://mockendpoint.com/sparql", "mockGraph");

        assertEquals("mockedSchema", response.getEntity().toString());
        assertEquals(200, response.getStatus());

        Mockito.verify(schemaBuilder, Mockito.times(1)).getSchema();
    }

    @Test(expected = WeLiveException.class)
    public void testGetMappingIOException() throws Exception {
        SPARQLQueryMaker sparqlQueryMaker = Mockito.spy(SPARQLQueryMaker.class);

        PowerMockito.whenNew(SchemaBuilder.class).withArguments("http://mockendpoint.com/sparql", "mockGraph", false).thenThrow(IOException.class);

        sparqlQueryMaker.getMapping("http://mockendpoint.com/sparql", "mockGraph");
    }

    @Test(expected = WeLiveException.class)
    public void testGetMappingSchemaBuilderException() throws Exception {
        SPARQLQueryMaker sparqlQueryMaker = Mockito.spy(SPARQLQueryMaker.class);
        SchemaBuilder schemaBuilder = Mockito.mock(SchemaBuilder.class);

        PowerMockito.whenNew(SchemaBuilder.class).withArguments("http://mockendpoint.com/sparql", "mockGraph", false).thenReturn(schemaBuilder);
        Mockito.when(schemaBuilder.getSchema()).thenThrow(SchemaBuilderException.class);

        sparqlQueryMaker.getMapping("http://mockendpoint.com/sparql", "mockGraph");
    }
}
