/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.sparql;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import eu.welive.rest.app.TestApplication;

/**
 * Created by mikel on 21/12/16.
 */
public class TestSPARQLResponse extends JerseyTest {

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Test
    public void testConstructor() {
        SPARQLResponse response = new SPARQLResponse();
        assertNotNull(response);
    }

    @Test
    public void testSetter() {
        SPARQLResponse response = new SPARQLResponse();
        response.setResponse("mockResponse");

        assertEquals("mockResponse", response.getResponse());
    }
}
