/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ods.pojo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import eu.welive.rest.app.TestApplication;

public class TestOrganizationRequest extends JerseyTest {
	
	@Override
    protected Application configure() {
        return new TestApplication();
    }

	@Test
	public void testEmptyConstructor() {
		OrganizationRequest or = new OrganizationRequest();
		
		assertNotNull(or);
	}
	
	@Test
	public void testConstructor() {
		OrganizationRequest or = new OrganizationRequest("title", "description", 44564, 8);
		
		assertNotNull(or);
		assertEquals("title", or.getName());
		assertEquals("description", or.getInfo());
		assertEquals(44564, or.getCcOrganizationId());
		assertEquals(8, or.getLeaderId());
	}
	
	@Test
	public void testSetters() {
		OrganizationRequest or = new OrganizationRequest();
		
		or.setName("title");
		or.setInfo("description");
		or.setCcOrganizationId(44564);
		or.setLeaderId(8);
		
		assertEquals("title", or.getName());
		assertEquals("description", or.getInfo());
		assertEquals(44564, or.getCcOrganizationId());
		assertEquals(8, or.getLeaderId());
	}
}
