/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ods.pojo;

import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;

import eu.welive.rest.app.TestApplication;

/**
 * Created by mikel on 1/06/16.
 */
public class TestRatingRequest extends JerseyTest {

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    @Test
    public void testConstructor() {
        RatingRequest ratingRequest = new RatingRequest();
        float expectedRating = ratingRequest.getRating();
        assertEquals(0.0, expectedRating, 0.0);
    }

    @Test
    public void testIntConstructor() {
        RatingRequest ratingRequest = new RatingRequest(5);

        float expectedRating = ratingRequest.getRating();
        assertEquals(5.0, expectedRating, 0.0);
    }

    @Test
    public void testSet() {
        RatingRequest ratingRequest = new RatingRequest(5);
        assertEquals(5.0, ratingRequest.getRating(), 0.0);

        ratingRequest.setRating(3);
        assertEquals(3, ratingRequest.getRating(), 0.0);
    }


}
