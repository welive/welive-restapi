/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ods;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.skyscreamer.jsonassert.JSONAssert;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.ods.pojo.OrganizationRequest;
import eu.welive.rest.ods.pojo.RatingRequest;

/**
 * Created by mikel on 11/05/16.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ClientBuilder.class, Entity.class, ODS.class, Config.class, IOUtils.class })
@PowerMockIgnore("javax.net.ssl.*")
public class TestODS extends JerseyTest {

	private static String apiKey = "FAKE-API-KEY";
	private static String packageID = "test-package";
	private static String resourceID = "test-resource";
	private static String userID = "5";
	private static String mapping = "example-mapping";
	private static String validation = "example-validation";

	@Override
	protected Application configure() {
		return new TestApplication();
	}

	@Test(expected = WeLiveException.class)
	public void testReadConfig() throws WeLiveException, IOException {
		PowerMockito.mockStatic(Config.class);
		PowerMockito.when(Config.getInstance()).thenThrow(IOException.class);

		Mockito.spy(new ODS());
	}

	@Test
	public void testGetAllPublicDatasets() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_list", null, MediaType.APPLICATION_JSON,
				null);

		ods.all(null);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_list", null, MediaType.APPLICATION_JSON, null);
	}

	@Test
	public void testGetAllDatasets() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_list", null, MediaType.APPLICATION_JSON,
				apiKey);

		ods.all(apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_list", null, MediaType.APPLICATION_JSON, apiKey);
	}

	@Test
	public void testCreateDataset() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"name\": \"%s\", \"language\": \"es\"}", packageID);

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("package_create", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);

		ods.createDataset(packageDict, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("package_create", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testGetPublicDataset() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON,
				null);

		ods.getDataset(packageID, null);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, null);
	}

	@Test
	public void testGetPrivateDataset() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON,
				apiKey);

		ods.getDataset(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
	}

	@Test
	public void testUpdateDataset() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"name\": \"%s\"}", packageID);
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("package_update", packageDict, apiKey, params,
				MediaType.APPLICATION_JSON);

		ods.updateDataset(packageID, packageDict, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("package_update", packageDict, apiKey, params,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testDeleteDataset() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"id\": \"%s\"}", packageID);

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("package_delete", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);

		ods.removeDataset(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("package_delete", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testDeleteDatasetBasicAuth() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"id\": \"%s\", \"ccUserID\": \"%s\"}", packageID, userID);

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("package_delete", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);

		ods.removeDatasetBasic(packageID, userID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("package_delete", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testGetValidUSDL() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());

		Mockito.doReturn("validUSDL").when(ods).generateUSDL(packageID, apiKey);

		ods.getUSDL(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).generateUSDL(packageID, apiKey);
	}

	@Test
	public void testGetInvalidUSDL() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());

		Mockito.doReturn(null).when(ods).generateUSDL(packageID, apiKey);

		ods.getUSDL(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).generateUSDL(packageID, apiKey);
	}

	@Test
	public void testAddRating() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"rating\": %s}", 4.0);
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("package", packageID));

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("rating_create", packageDict, apiKey, params,
				MediaType.APPLICATION_JSON);
		RatingRequest ratingRequest = new RatingRequest(4);

		ods.addRating(packageID, ratingRequest, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("rating_create", packageDict, apiKey, params,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testAddRatingPublic() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"rating\": %s, \"ccUserID\": %s}", 4.0, userID);
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("package", packageID));

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("rating_create", packageDict, apiKey, params,
				MediaType.APPLICATION_JSON);
		RatingRequest ratingRequest = new RatingRequest(4);

		ods.addRatingBasic(packageID, userID, ratingRequest, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("rating_create", packageDict, apiKey, params,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testGetRating() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("rating_show", params, MediaType.APPLICATION_JSON,
				apiKey);

		ods.getRating(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("rating_show", params, MediaType.APPLICATION_JSON, apiKey);
	}

	@Test
	public void testGetRatingBasic() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));
		params.add(new BasicNameValuePair("ccUserID", userID));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("rating_show", params, MediaType.APPLICATION_JSON,
				apiKey);

		ods.getRatingBasic(packageID, userID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("rating_show", params, MediaType.APPLICATION_JSON, apiKey);
	}

	@Test
	public void testCreateResource() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"name\": \"%s\"}", resourceID);
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("package_id", packageID));
		InputStream inputStream = new ByteArrayInputStream("uploaded file content".getBytes(StandardCharsets.UTF_8));
		FormDataContentDisposition formDataContentDisposition = null;

		Mockito.doReturn(mockedResponse).when(ods).callPostFileAction("resource_create", params, packageDict,
				inputStream, formDataContentDisposition, apiKey, MediaType.APPLICATION_JSON);

		ods.createResource(packageID, packageDict, inputStream, formDataContentDisposition, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostFileAction("resource_create", params, packageDict, inputStream,
				formDataContentDisposition, apiKey, MediaType.APPLICATION_JSON);
	}

	@Test
	public void testGetPublicResource() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("resource_show", params, MediaType.APPLICATION_JSON,
				null);

		ods.getResource(packageID, resourceID);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
	}

	@Test
	public void testUpdateResource() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = "{\"title\": \"Resource Title\"}";
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		InputStream inputStream = new ByteArrayInputStream("uploaded file content".getBytes(StandardCharsets.UTF_8));
		FormDataContentDisposition formDataContentDisposition = null;

		Mockito.doReturn(mockedResponse).when(ods).callPostFileAction("resource_update", params, packageDict,
				inputStream, formDataContentDisposition, apiKey, MediaType.APPLICATION_JSON);

		ods.updateResource(packageID, resourceID, packageDict, inputStream, formDataContentDisposition, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostFileAction("resource_update", params, packageDict, inputStream,
				formDataContentDisposition, apiKey, MediaType.APPLICATION_JSON);
	}

	@Test
	public void testDeleteResource() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		String packageDict = String.format("{\"id\": \"%s\"}", resourceID);

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("resource_delete", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);

		ods.deleteResource(packageID, resourceID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("resource_delete", packageDict, apiKey, null,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testCreateMapping() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("mapping", mapping));

		Mockito.doReturn(mockedResponse).when(ods).updateResourceParam(params, apiKey);

		ods.createMapping(packageID, resourceID, mapping, apiKey);

		Mockito.verify(ods, Mockito.times(1)).updateResourceParam(params, apiKey);
	}

	@Test
	public void testGetMapping() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		String responseResult = String.format("{\"result\": {\"mapping\": \"%s\"}}", mapping);
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("resource_show", params, MediaType.APPLICATION_JSON,
				null);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		ods.getMapping(packageID, resourceID);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);

	}

	@Test
	public void testGetEmptyMapping() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		String responseResult = "{\"result\": {}}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("resource_show", params, MediaType.APPLICATION_JSON,
				null);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		ods.getMapping(packageID, resourceID);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
	}

	@Test
	public void testGetEmptyResultMapping() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		String responseResult = "{}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("resource_show", params, MediaType.APPLICATION_JSON,
				null);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		ods.getMapping(packageID, resourceID);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
	}

	@Test
	public void testUpdateMapping() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("mapping", mapping));

		Mockito.doReturn(mockedResponse).when(ods).updateResourceParam(params, apiKey);

		ods.updateMapping(packageID, resourceID, mapping, apiKey);

		Mockito.verify(ods, Mockito.times(1)).updateResourceParam(params, apiKey);
	}

	@Test
	public void testDeleteMapping() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("mapping", ""));

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("resource_update", null, apiKey, params,
				MediaType.APPLICATION_JSON);

		ods.deleteMapping(packageID, resourceID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("resource_update", null, apiKey, params,
				MediaType.APPLICATION_JSON);
	}

	@Test
	public void testCreateVerification() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("validation", validation));

		Mockito.doReturn(mockedResponse).when(ods).updateResourceParam(params, apiKey);

		ods.createVerification(packageID, resourceID, validation, apiKey);

		Mockito.verify(ods, Mockito.times(1)).updateResourceParam(params, apiKey);
	}

	@Test
	public void testUpdateVerification() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("validation", validation));

		Mockito.doReturn(mockedResponse).when(ods).updateResourceParam(params, apiKey);

		ods.updateVerification(packageID, resourceID, validation, apiKey);

		Mockito.verify(ods, Mockito.times(1)).updateResourceParam(params, apiKey);
	}

	@Test
	public void testDeleteVerification() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("validation", ""));

		Mockito.doReturn(mockedResponse).when(ods).updateResourceParam(params, apiKey);

		ods.deleteVerification(packageID, resourceID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).updateResourceParam(params, apiKey);
	}

	@Test
	public void testGetVerification() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		String responseResult = String.format("{\"result\": {\"validation\": \"%s\"}}", validation);
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("resource_show", params, MediaType.APPLICATION_JSON,
				null);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		ods.getVerification(packageID, resourceID);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
	}

	@Test
	public void testGetEmptyVerification() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		String responseResult = "{\"result\": {}}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("resource_show", params, MediaType.APPLICATION_JSON,
				null);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		ods.getVerification(packageID, resourceID);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
	}

	@Test
	public void testGetEmptyResultVerification() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		String responseResult = "{}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("resource_show", params, MediaType.APPLICATION_JSON,
				null);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		ods.getVerification(packageID, resourceID);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
	}

	@Test
	public void testUpdateResourceParam() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));

		Mockito.doReturn(mockedResponse).when(ods).callPostAction("resource_update", null, apiKey, params,
				MediaType.APPLICATION_JSON);

		ods.updateResourceParam(params, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callPostAction("resource_update", null, apiKey, params,
				MediaType.APPLICATION_JSON);
	}

	@Test(expected = WeLiveException.class)
	public void TestNoSuccessGenerateUSDL() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		String responseResult = "{}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON,
				apiKey);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		ods.generateUSDL(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
		Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);
	}

	@Test
	public void TestNoTypeGenerateUSDL() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		String responseResult = "{\"success\": true, \"result\": {}}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON,
				apiKey);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		String resultUSDL = ods.generateUSDL(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
		Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);

		assertNull(resultUSDL);
	}

	@Test
	public void TestErrorGenerateUSDL() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		String responseResult = "{\"error\": { \"message\": \"Not Found\"}}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON,
				apiKey);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		String resultUSDL = ods.generateUSDL(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
		Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);

		assertEquals("Not Found", resultUSDL);
	}

	@Test
	public void TestNotDatasetGenerateUSDL() throws WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));

		String responseResult = "{\"success\": true, \"result\": {\"type\": \"harvest\"}}";
		JSONObject jsonObject = new JSONObject(responseResult);

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON,
				apiKey);
		Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

		String resultUSDL = ods.generateUSDL(packageID, apiKey);

		Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
		Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
		Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);

		assertNull(resultUSDL);
	}

	@Test
	public void TestCallPublicGetAction() throws IOException, WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));
		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";

		Mockito.doReturn(mockedResponse).when(mockedRequest).get();
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "package_show"));
		Mockito.doReturn(mockedWebTarget).when(mockedWebTarget).queryParam(Matchers.anyString(), Matchers.anyString());
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

		Response response = ods.callGetAction("package_show", params, null, null);

		Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s%s", ODS_API, "package_show"));
		Mockito.verify(mockedWebTarget, Mockito.times(1)).queryParam("id", packageID);
		Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
		Mockito.verify(mockedRequest, Mockito.times(1)).get();

		assertEquals(mockedResponse, response);
	}

	@Test
	public void TestCallGetAction() throws IOException, WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", packageID));
		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";

		Mockito.doReturn(mockedResponse).when(mockedRequest).get();
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "package_show"));
		Mockito.doReturn(mockedWebTarget).when(mockedWebTarget).queryParam(Matchers.anyString(), Matchers.anyString());
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

		Response response = ods.callGetAction("package_show", params, null, apiKey);

		Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s%s", ODS_API, "package_show"));
		Mockito.verify(mockedWebTarget, Mockito.times(1)).queryParam("id", packageID);
		Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
		Mockito.verify(mockedRequest, Mockito.times(1)).get();
		Mockito.verify(mockedRequest, Mockito.times(1)).header("Authorization", apiKey);

		assertEquals(mockedResponse, response);

	}

	@Test
	public void TestCallGetActionMissingParams() throws IOException, WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";

		Mockito.doReturn(mockedResponse).when(mockedRequest).get();
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "package_show"));
		Mockito.doReturn(mockedWebTarget).when(mockedWebTarget).queryParam(Matchers.anyString(), Matchers.anyString());
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

		Response response = ods.callGetAction("package_show", null, null, null);

		Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s%s", ODS_API, "package_show"));
		Mockito.verify(mockedWebTarget, Mockito.times(0)).queryParam(Matchers.anyString(), Matchers.anyString());
		Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
		Mockito.verify(mockedRequest, Mockito.times(1)).get();

		assertEquals(mockedResponse, response);

	}

	@Test
	public void TestCallPostAction() throws IOException, WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);
		PowerMockito.mockStatic(Entity.class);
		Entity mockedEntity = Mockito.mock(Entity.class);

		ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("extraKey", "extraValue"));
		String packageDict = String.format("{\"id\": \"%s\"}", packageID);
		JSONObject jsonPackage = new JSONObject(
				String.format("{\"id\": \"%s\", \"extraKey\": \"extraValue\"}", packageID));

		// JSONObject does not implement equals method, so PowerMockito can't
		// know if the arguments match
		PowerMockito.when(Entity.json(Matchers.any(JSONObject.class))).thenReturn(mockedEntity);
		Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "package_update"));
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

		Response response = ods.callPostAction("package_update", packageDict, apiKey, params, null);

		Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s%s", ODS_API, "package_update"));
		Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
		Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);
		Mockito.verify(mockedRequest, Mockito.times(1)).header("Authorization", apiKey);
		PowerMockito.verifyStatic(Mockito.times(1));
		Entity.json(stringArgumentCaptor.capture());

		JSONAssert.assertEquals(jsonPackage, new JSONObject(stringArgumentCaptor.getValue()), true);

		assertEquals(mockedResponse, response);
	}

	@Test
	public void TestCallPostActionNullValues() throws IOException, WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);
		PowerMockito.mockStatic(Entity.class);
		Entity mockedEntity = Mockito.mock(Entity.class);

		ArgumentCaptor<String> stringArgumentCaptor = ArgumentCaptor.forClass(String.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
		JSONObject jsonPackage = new JSONObject();

		// JSONObject does not implement equals method, so PowerMockito can't
		// know if the arguments match
		PowerMockito.when(Entity.json(Matchers.any(JSONObject.class))).thenReturn(mockedEntity);
		Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "package_update"));
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

		Response response = ods.callPostAction("package_update", null, null, null, null);

		Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s%s", ODS_API, "package_update"));
		Mockito.verify(mockedWebTarget, Mockito.times(1)).request();
		Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);
		Mockito.verify(mockedRequest, Mockito.times(0)).header("Authorization", apiKey);
		PowerMockito.verifyStatic(Mockito.times(1));
		Entity.json(stringArgumentCaptor.capture());

		JSONAssert.assertEquals(jsonPackage, new JSONObject(stringArgumentCaptor.getValue()), true);

		assertEquals(mockedResponse, response);
	}

	@Test(expected = WeLiveException.class)
	public void TestFailCallPostAction() throws IOException, WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);
		PowerMockito.mockStatic(Entity.class);
		Entity mockedEntity = Mockito.mock(Entity.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("extraKey", "extraValue"));
		String packageDict = String.format("{\"id\": \"%s}", packageID);

		// JSONObject does not implement equals method, so PowerMockito can't
		// know if the arguments match
		PowerMockito.when(Entity.entity(Matchers.any(JSONObject.class), Matchers.eq(MediaType.APPLICATION_JSON)))
				.thenReturn(mockedEntity);
		Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request();
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "package_update"));
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);

		ods.callPostAction("package_update", packageDict, apiKey, params, null);
	}

	@Test
	public void TestCallPostFileAction() throws WeLiveException, IOException {
		ODS ods = Mockito.spy(new ODS());
		FormDataContentDisposition mockedFDCD = Mockito.mock(FormDataContentDisposition.class);
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);
		PowerMockito.mockStatic(Entity.class);
		Entity mockedEntity = Mockito.mock(Entity.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("extraKey", "extraValue"));
		String packageDict = String.format("{\"id\": \"%s\"}", resourceID);
		InputStream inputStream = new ByteArrayInputStream("uploaded file content".getBytes(StandardCharsets.UTF_8));

		PowerMockito.when(Entity.entity(Matchers.any(MultiPart.class), Matchers.any(MediaType.class)))
				.thenReturn(mockedEntity);
		Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
		Mockito.doReturn(mockedRequest).when(mockedRequest).header("Authorization", apiKey);
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request(MediaType.APPLICATION_JSON_TYPE);
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "resource_update"));
		Mockito.doReturn(mockedClient).when(mockedClient).register(MultiPartFeature.class);
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);
		Mockito.when(mockedFDCD.getFileName()).thenReturn("mocked_file.ext");

		Response response = ods.callPostFileAction("resource_update", params, packageDict, inputStream, mockedFDCD,
				apiKey, null);

		Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s%s", ODS_API, "resource_update"));
		Mockito.verify(mockedWebTarget, Mockito.times(1)).request(MediaType.APPLICATION_JSON_TYPE);
		Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);
		Mockito.verify(mockedRequest, Mockito.times(1)).header("Authorization", apiKey);

		assertEquals(mockedResponse, response);
	}

	@Test(expected = WeLiveException.class)
	public void TestCallPostFileActionFail() throws WeLiveException, IOException {
		ODS ods = Mockito.spy(new ODS());
		FormDataContentDisposition mockedFDCD = Mockito.mock(FormDataContentDisposition.class);
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		PowerMockito.mockStatic(IOUtils.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("extraKey", "extraValue"));
		String packageDict = String.format("{\"id\": \"%s\"}", resourceID);
		InputStream inputStream = new ByteArrayInputStream("uploaded file content".getBytes(StandardCharsets.UTF_8));

		PowerMockito.when(IOUtils.copy(Matchers.any(InputStream.class), Matchers.any(FileOutputStream.class)))
				.thenThrow(IOException.class);
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "resource_update"));
		Mockito.doReturn(mockedClient).when(mockedClient).register(MultiPartFeature.class);
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);
		Mockito.when(mockedFDCD.getFileName()).thenReturn("mocked_file.ext");

		ods.callPostFileAction("resource_update", params, packageDict, inputStream, mockedFDCD, apiKey, null);
	}

	@Test
	public void TestCallPostFileActionMissingURL() throws WeLiveException, IOException {
		ODS ods = Mockito.spy(new ODS());
		FormDataContentDisposition mockedFDCD = Mockito.mock(FormDataContentDisposition.class);
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);
		PowerMockito.mockStatic(Entity.class);
		Entity mockedEntity = Mockito.mock(Entity.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("extraKey", "extraValue"));
		String packageDict = String.format("{\"id\": \"%s\", \"url\": \"http://foo/bar\"}", resourceID);
		InputStream inputStream = new ByteArrayInputStream("uploaded file content".getBytes(StandardCharsets.UTF_8));

		PowerMockito.when(Entity.entity(Matchers.any(MultiPart.class), Matchers.any(MediaType.class)))
				.thenReturn(mockedEntity);
		Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
		Mockito.doReturn(mockedRequest).when(mockedRequest).header("Authorization", apiKey);
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request(MediaType.APPLICATION_JSON_TYPE);
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "resource_update"));
		Mockito.doReturn(mockedClient).when(mockedClient).register(MultiPartFeature.class);
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);
		Mockito.when(mockedFDCD.getFileName()).thenReturn("mocked_file.ext");

		Response response = ods.callPostFileAction("resource_update", params, packageDict, inputStream, mockedFDCD,
				apiKey, null);

		Mockito.verify(mockedClient, Mockito.times(1)).target(String.format("%s%s", ODS_API, "resource_update"));
		Mockito.verify(mockedWebTarget, Mockito.times(1)).request(MediaType.APPLICATION_JSON_TYPE);
		Mockito.verify(mockedRequest, Mockito.times(1)).post(mockedEntity);
		Mockito.verify(mockedRequest, Mockito.times(1)).header("Authorization", apiKey);

		assertEquals(mockedResponse, response);
	}

	@Test(expected = WeLiveException.class)
	public void TestFailCallPostFileAction() throws IOException, WeLiveException {
		ODS ods = Mockito.spy(new ODS());
		FormDataContentDisposition mockedFDCD = Mockito.mock(FormDataContentDisposition.class);
		PowerMockito.mockStatic(ClientBuilder.class);
		Client mockedClient = Mockito.mock(Client.class);
		WebTarget mockedWebTarget = Mockito.mock(WebTarget.class);
		Invocation.Builder mockedRequest = Mockito.mock(Invocation.Builder.class);
		Response mockedResponse = Mockito.mock(Response.class);
		PowerMockito.mockStatic(Entity.class);
		Entity mockedEntity = Mockito.mock(Entity.class);
		PowerMockito.mockStatic(System.class);

		String ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("extraKey", "extraValue"));
		String packageDict = String.format("{\"id\": \"%s\"}", resourceID);
		InputStream inputStream = new ByteArrayInputStream("uploaded file content".getBytes(StandardCharsets.UTF_8));

		PowerMockito.when(System.getProperty("java.io.tmpdir")).thenReturn("/faked/dir");
		PowerMockito.when(Entity.entity(Matchers.any(MultiPart.class), Matchers.any(MediaType.class)))
				.thenReturn(mockedEntity);
		Mockito.doReturn(mockedResponse).when(mockedRequest).post(mockedEntity);
		Mockito.doReturn(mockedRequest).when(mockedRequest).header("Authorization", apiKey);
		Mockito.doReturn(mockedRequest).when(mockedWebTarget).request(MediaType.APPLICATION_JSON_TYPE);
		Mockito.doReturn(mockedWebTarget).when(mockedClient).target(String.format("%s%s", ODS_API, "resource_update"));
		Mockito.doReturn(mockedClient).when(mockedClient).register(MultiPartFeature.class);
		PowerMockito.when(ClientBuilder.newClient()).thenReturn(mockedClient);
		Mockito.when(mockedFDCD.getFileName()).thenReturn("mocked_file.ext");

		ods.callPostFileAction("resource_update", params, packageDict, inputStream, mockedFDCD, apiKey, null);

		PowerMockito.verifyStatic();
		System.getProperty("java.io.tmpdir");

	}

	@Test
	public void testGetJsonObject() throws WeLiveException {
		ODS ods = new ODS();

		String actionResult = "{\"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_show\", \"success\": true, \"result\": {\"license_title\": null, \"maintainer\": null, \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": null, \"num_tags\": 2, \"id\": \"b4ddd504-e1f2-4111-a950-0ae58de8e817\", \"metadata_created\": \"2016-02-08T12:10:00.082514\", \"ratings\": null, \"metadata_modified\": \"2016-02-08T12:10:00.103245\", \"author\": null, \"author_email\": null, \"isopen\": false, \"state\": \"active\", \"version\": null, \"archiver\": {\"status\": \"Archived successfully\", \"is_broken\": false, \"reason\": \"\", \"status_id\": 0}, \"creator_user_id\": \"f785a681-d339-47d1-a609-9a0d811684f3\", \"type\": \"dataset\", \"resources\": [{\"cache_last_updated\": null, \"package_id\": \"b4ddd504-e1f2-4111-a950-0ae58de8e817\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"b9bcbe8c-8249-4105-b873-ec6981556f08\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-04-04T16:49:28.278376\", \"cache_filepath\": \"/home/deusto/ckan/archiver/b9/b9bcbe8c-8249-4105-b873-ec6981556f08/od_dataset.jsp\", \"last_success\": \"2016-04-04T16:49:28.278376\", \"size\": \"10511265\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": \"https://www.bilbao.net/aytoonline/jsp/od_dataset.jsp?idioma=&formato=csv&dataset=VehiculosBarrio\", \"hash\": \"5ec53dd0c7835f86ccdec6b110374b4c36e505ea\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-02-08T12:10:00.082514\", \"mimetype\": \"application/csv\", \"cache_url\": \"/cache/b9/b9bcbe8c-8249-4105-b873-ec6981556f08/od_dataset.jsp\", \"created\": \"2016-04-04T16:49:28.287773\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"\", \"format\": \"CSV\", \"mapping\": \"{\\\"delimiter\\\": \\\";\\\", \\\"refresh\\\": 1000, \\\"mapping\\\": \\\"csv\\\", \\\"key\\\": \\\"_id\\\", \\\"uri\\\": \\\"http://www.bilbao.net/aytoonline/jsp/od_dataset.jsp?idioma=&formato=csv&dataset=VehiculosBarrio\\\"}\", \"mimetype_inner\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"CSV\", \"created\": \"2016-02-08T13:10:00.108668\", \"url\": \"http://www.bilbao.net/aytoonline/jsp/od_dataset.jsp?idioma=&formato=csv&dataset=VehiculosBarrio\", \"uri\": \"None\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-04-05T13:37:23.708193\", \"openness_score\": 0, \"archival_timestamp\": \"2016-04-04T16:49:28.278376\", \"format\": \"CSV\", \"created\": \"2016-04-04T16:51:41.532690\", \"resource_timestamp\": null, \"openness_score_reason\": \"License not open\"}, \"last_modified\": null, \"position\": 0, \"revision_id\": \"8cb103fc-e0a5-4837-a9b7-9530b72dbc4d\", \"resource_type\": null}], \"num_resources\": 1, \"tags\": [{\"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"barrio\", \"id\": \"2039dd00-a819-462c-a17e-6b4491c34395\", \"name\": \"barrio\"}, {\"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"veh\\u00edculo\", \"id\": \"e3555d86-68bb-4490-8082-1d01b8b42c83\", \"name\": \"veh\\u00edculo\"}], \"groups\": [], \"license_id\": null, \"relationships_as_subject\": [], \"organization\": {\"description\": \"Datasets from Bilbao City Council.\", \"created\": \"2016-01-25T10:02:45.855432\", \"title\": \"Bilbao City Council\", \"name\": \"bilbao-city-council\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"2016-02-08-112113.4809672015-09-30-123527.952561bilbao.jpg\", \"revision_id\": \"d7b53497-c05c-4c7d-983f-dcf75507e437\", \"type\": \"organization\", \"id\": \"9a8ec148-a714-4db0-ad18-c411b5971d00\", \"approval_status\": \"approved\"}, \"name\": \"vehiculos-por-barrio-detalle\", \"language\": \"es\", \"url\": null, \"notes\": \"Relaci&#243;n detallada por barrio de los veh&#237;culos particulares en el municipio de Bilbao.\", \"owner_org\": \"9a8ec148-a714-4db0-ad18-c411b5971d00\", \"qa\": {\"openness_score_reason\": \"License not open\", \"updated\": \"2016-04-05T13:37:23.708193\", \"openness_score\": 0}, \"extras\": [{\"key\": \"frequency\", \"value\": \"Nd4d67a631feb43dfae8b762a03f15897\"}, {\"key\": \"guid\", \"value\": \"http://www.bilbao.net/opendata/catalogo/dato-vehiculos-barrio\"}, {\"key\": \"identifier\", \"value\": \"http://www.bilbao.net/opendata/catalogo/dato-vehiculos-barrio\"}, {\"key\": \"issued\", \"value\": \"2014-09-16\"}, {\"key\": \"modified\", \"value\": \"2015-12-16\"}, {\"key\": \"publisher_uri\", \"value\": \"http://datos.gob.es/recurso/sector-publico/org/Organismo/L01480209\"}, {\"key\": \"spatial_uri\", \"value\": \"http://datos.gob.es/recurso/sector-publico/territorio/provincia/Vizcaya\"}, {\"key\": \"theme\", \"value\": \"[\\\"http://datos.gob.es/kos/sector-publico/sector/transporte\\\"]\"}, {\"key\": \"uri\", \"value\": \"http://www.bilbao.net/opendata/catalogo/dato-vehiculos-barrio\"}], \"title\": \"Veh\\u00edculos por Barrio (detalle)\", \"revision_id\": \"8cb103fc-e0a5-4837-a9b7-9530b72dbc4d\"}}";
		String expectedResult = "{\"result\": {\"license_title\": null, \"maintainer\": null, \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": null, \"num_tags\": 2, \"id\": \"b4ddd504-e1f2-4111-a950-0ae58de8e817\", \"metadata_created\": \"2016-02-08T12:10:00.082514\", \"ratings\": null, \"metadata_modified\": \"2016-02-08T12:10:00.103245\", \"author\": null, \"author_email\": null, \"isopen\": false, \"state\": \"active\", \"version\": null, \"archiver\": {\"status\": \"Archived successfully\", \"is_broken\": false, \"reason\": \"\", \"status_id\": 0}, \"creator_user_id\": \"f785a681-d339-47d1-a609-9a0d811684f3\", \"type\": \"dataset\", \"resources\": [{\"cache_last_updated\": null, \"package_id\": \"b4ddd504-e1f2-4111-a950-0ae58de8e817\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"b9bcbe8c-8249-4105-b873-ec6981556f08\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-04-04T16:49:28.278376\", \"cache_filepath\": \"/home/deusto/ckan/archiver/b9/b9bcbe8c-8249-4105-b873-ec6981556f08/od_dataset.jsp\", \"last_success\": \"2016-04-04T16:49:28.278376\", \"size\": \"10511265\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": \"https://www.bilbao.net/aytoonline/jsp/od_dataset.jsp?idioma=&formato=csv&dataset=VehiculosBarrio\", \"hash\": \"5ec53dd0c7835f86ccdec6b110374b4c36e505ea\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-02-08T12:10:00.082514\", \"mimetype\": \"application/csv\", \"cache_url\": \"/cache/b9/b9bcbe8c-8249-4105-b873-ec6981556f08/od_dataset.jsp\", \"created\": \"2016-04-04T16:49:28.287773\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"\", \"format\": \"CSV\", \"mapping\": \"{\\\"delimiter\\\": \\\";\\\", \\\"refresh\\\": 1000, \\\"mapping\\\": \\\"csv\\\", \\\"key\\\": \\\"_id\\\", \\\"uri\\\": \\\"http://www.bilbao.net/aytoonline/jsp/od_dataset.jsp?idioma=&formato=csv&dataset=VehiculosBarrio\\\"}\", \"mimetype_inner\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"CSV\", \"created\": \"2016-02-08T13:10:00.108668\", \"url\": \"http://www.bilbao.net/aytoonline/jsp/od_dataset.jsp?idioma=&formato=csv&dataset=VehiculosBarrio\", \"uri\": \"None\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-04-05T13:37:23.708193\", \"openness_score\": 0, \"archival_timestamp\": \"2016-04-04T16:49:28.278376\", \"format\": \"CSV\", \"created\": \"2016-04-04T16:51:41.532690\", \"resource_timestamp\": null, \"openness_score_reason\": \"License not open\"}, \"last_modified\": null, \"position\": 0, \"revision_id\": \"8cb103fc-e0a5-4837-a9b7-9530b72dbc4d\", \"resource_type\": null}], \"num_resources\": 1, \"tags\": [{\"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"barrio\", \"id\": \"2039dd00-a819-462c-a17e-6b4491c34395\", \"name\": \"barrio\"}, {\"vocabulary_id\": null, \"state\": \"active\", \"display_name\": \"veh\\u00edculo\", \"id\": \"e3555d86-68bb-4490-8082-1d01b8b42c83\", \"name\": \"veh\\u00edculo\"}], \"groups\": [], \"license_id\": null, \"relationships_as_subject\": [], \"organization\": {\"description\": \"Datasets from Bilbao City Council.\", \"created\": \"2016-01-25T10:02:45.855432\", \"title\": \"Bilbao City Council\", \"name\": \"bilbao-city-council\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"2016-02-08-112113.4809672015-09-30-123527.952561bilbao.jpg\", \"revision_id\": \"d7b53497-c05c-4c7d-983f-dcf75507e437\", \"type\": \"organization\", \"id\": \"9a8ec148-a714-4db0-ad18-c411b5971d00\", \"approval_status\": \"approved\"}, \"name\": \"vehiculos-por-barrio-detalle\", \"language\": \"es\", \"url\": null, \"notes\": \"Relaci&#243;n detallada por barrio de los veh&#237;culos particulares en el municipio de Bilbao.\", \"owner_org\": \"9a8ec148-a714-4db0-ad18-c411b5971d00\", \"qa\": {\"openness_score_reason\": \"License not open\", \"updated\": \"2016-04-05T13:37:23.708193\", \"openness_score\": 0}, \"extras\": [{\"key\": \"frequency\", \"value\": \"Nd4d67a631feb43dfae8b762a03f15897\"}, {\"key\": \"guid\", \"value\": \"http://www.bilbao.net/opendata/catalogo/dato-vehiculos-barrio\"}, {\"key\": \"identifier\", \"value\": \"http://www.bilbao.net/opendata/catalogo/dato-vehiculos-barrio\"}, {\"key\": \"issued\", \"value\": \"2014-09-16\"}, {\"key\": \"modified\", \"value\": \"2015-12-16\"}, {\"key\": \"publisher_uri\", \"value\": \"http://datos.gob.es/recurso/sector-publico/org/Organismo/L01480209\"}, {\"key\": \"spatial_uri\", \"value\": \"http://datos.gob.es/recurso/sector-publico/territorio/provincia/Vizcaya\"}, {\"key\": \"theme\", \"value\": \"[\\\"http://datos.gob.es/kos/sector-publico/sector/transporte\\\"]\"}, {\"key\": \"uri\", \"value\": \"http://www.bilbao.net/opendata/catalogo/dato-vehiculos-barrio\"}], \"title\": \"Veh\\u00edculos por Barrio (detalle)\", \"revision_id\": \"8cb103fc-e0a5-4837-a9b7-9530b72dbc4d\"}}";
		JSONObject expectedJSON = new JSONObject(expectedResult);

		JSONObject jsonObject = ods.getJsonObject(actionResult);

		JSONAssert.assertEquals(expectedJSON, jsonObject, true);
	}

	@Test
	public void testGetJsonObjectArray() throws WeLiveException {
		ODS ods = new ODS();

		String actionResult = "{\n"
				+ "  \"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_list\",\n"
				+ "  \"success\": true,\n" + "  \"result\": [\n" + "    \"abbonamenti-alla-rai\",\n"
				+ "    \"abitazioni\",\n" + "    \"abitazioni-di-proprieta-di-enti-pubblici\",\n"
				+ "    \"abitazioni-di-proprieta-di-persona-fisica\",\n"
				+ "    \"abitazioni-di-proprieta-in-usufrutto-o-riscatto\",\n"
				+ "    \"abitazioni-di-recente-costruzione\",\n" + "    \"abitazioni-in-affitto\",\n"
				+ "    \"abitazioni-non-occupate\",\n" + "    \"abitazioni-non-occupate-utilizzabili-per-vacanza\"\n"
				+ "  ]}";

		String expectedResult = "{\n" + "  \"result\": [\n" + "    \"abbonamenti-alla-rai\",\n"
				+ "    \"abitazioni\",\n" + "    \"abitazioni-di-proprieta-di-enti-pubblici\",\n"
				+ "    \"abitazioni-di-proprieta-di-persona-fisica\",\n"
				+ "    \"abitazioni-di-proprieta-in-usufrutto-o-riscatto\",\n"
				+ "    \"abitazioni-di-recente-costruzione\",\n" + "    \"abitazioni-in-affitto\",\n"
				+ "    \"abitazioni-non-occupate\",\n" + "    \"abitazioni-non-occupate-utilizzabili-per-vacanza\"\n"
				+ "  ]}";

		JSONObject expectedJSON = new JSONObject(expectedResult);

		JSONObject jsonObject = ods.getJsonObject(actionResult);

		JSONAssert.assertEquals(expectedJSON, jsonObject, true);
	}

	@Test
	public void testGetJsonObjectNull() throws WeLiveException {
		ODS ods = new ODS();

		String actionResult = "{\n"
				+ "  \"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_list\",\n"
				+ "  \"success\": true,\n" + "  \"result\": null}";

		String expectedResult = "{}";

		JSONObject expectedJSON = new JSONObject(expectedResult);

		JSONObject jsonObject = ods.getJsonObject(actionResult);

		JSONAssert.assertEquals(expectedJSON, jsonObject, true);
	}

	@Test
	public void testGetJsonObjectFail() throws WeLiveException {
		ODS ods = new ODS();

		String actionResult = "{\n"
				+ "  \"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_show\",\n"
				+ "  \"success\": false,\n" + "  \"error\": {\n" + "    \"message\": \"Not found\",\n"
				+ "    \"__type\": \"Not Found Error\"\n" + "  }\n" + "}";

		String expectedResult = "{\"error\": {\n" + "    \"message\": \"Not found\",\n"
				+ "    \"__type\": \"Not Found Error\"\n" + "  }}";

		JSONObject expectedJSON = new JSONObject(expectedResult);

		JSONObject jsonObject = ods.getJsonObject(actionResult);

		JSONAssert.assertEquals(expectedJSON, jsonObject, true);
	}

	@Test
	public void testDeleteUser() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("cascade", "false"));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");

		ods.deleteUser("FAKE_KEY", "false");

		Mockito.verify(ods, Mockito.times(1)).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");
	}

	@Test
	public void testDeleteUserCascade() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("cascade", "true"));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");

		ods.deleteUser("FAKE_KEY", "true");

		Mockito.verify(ods, Mockito.times(1)).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");
	}

	@Test
	public void testDeleteUserBasic() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("ccUserID", "8"));
		params.add(new BasicNameValuePair("cascade", "false"));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");

		ods.deleteUserBasic("8", "FAKE_KEY", "false");

		Mockito.verify(ods, Mockito.times(1)).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");
	}

	@Test
	public void testDeleteUserBasicCascade() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("ccUserID", "8"));
		params.add(new BasicNameValuePair("cascade", "true"));

		Mockito.doReturn(mockedResponse).when(ods).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");

		ods.deleteUserBasic("8", "FAKE_KEY", "true");

		Mockito.verify(ods, Mockito.times(1)).callGetAction("user_delete", params, MediaType.APPLICATION_JSON,
				"FAKE_KEY");
	}
	
	@Test
	public void testCreateOrganization() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);
		JSONObject mockedJSONResponse = new JSONObject();
		mockedJSONResponse.put("success", true);
		
		JSONObject organization = new JSONObject();
		organization.put("error", true);
		
		String dataDict = "{\"name\": \"test-org\", \"title\": \"Test Org\", \"description\": \"Org Description\", \"extras\": [{\"key\": \"orgID\", \"value\": 44564}], \"ccUserID\": 8}";

		Mockito.doReturn(organization).when(ods).getOrganizationID("44564");
		Mockito.doReturn(mockedJSONResponse.toString()).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(mockedResponse).when(ods).callPostAction("organization_create", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);

		OrganizationRequest organizationRequest = new OrganizationRequest("Test Org", "Org Description", 44564, 8);
		Response response = ods.createOrganization(organizationRequest, "FAKE_KEY");

		Mockito.verify(ods, Mockito.times(1)).getOrganizationID("44564");
		Mockito.verify(ods, Mockito.times(1)).callPostAction("organization_create", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);
		
		JSONObject jsonResponse = new JSONObject((String) response.getEntity());
		assertEquals(false, jsonResponse.get("error"));
		assertEquals(200, jsonResponse.get("status"));
		assertEquals("Organization created/updated successfully", jsonResponse.get("message"));
	}
	
	@Test
	public void testUpsertOrganizationError() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);
		JSONObject mockedJSONResponse = new JSONObject();
		mockedJSONResponse.put("success", false);
		JSONObject mockedError = new JSONObject();
		mockedError.put("message", "Server Error");
		mockedJSONResponse.put("error", mockedError);
		
		JSONObject organization = new JSONObject();
		organization.put("error", true);
		
		String dataDict = "{\"name\": \"test-org\", \"title\": \"Test Org\", \"description\": \"Org Description\", \"extras\": [{\"key\": \"orgID\", \"value\": 44564}], \"ccUserID\": 8}";

		Mockito.doReturn(organization).when(ods).getOrganizationID("44564");
		Mockito.doReturn(mockedJSONResponse.toString()).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(mockedResponse).when(ods).callPostAction("organization_create", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);

		OrganizationRequest organizationRequest = new OrganizationRequest("Test Org", "Org Description", 44564, 8);
		Response response = ods.createOrganization(organizationRequest, "FAKE_KEY");

		Mockito.verify(ods, Mockito.times(1)).getOrganizationID("44564");
		Mockito.verify(ods, Mockito.times(1)).callPostAction("organization_create", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);
		
		JSONObject jsonResponse = new JSONObject((String) response.getEntity());
		assertEquals(true, jsonResponse.get("error"));
		assertEquals(500, jsonResponse.get("status"));
		assertEquals("Server Error", jsonResponse.get("message"));
	}
	
	@Test
	public void testUpserOrganizationServerError() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = null;
		
		JSONObject organization = new JSONObject();
		organization.put("error", true);
		
		String dataDict = "{\"name\": \"test-org\", \"title\": \"Test Org\", \"description\": \"Org Description\", \"extras\": [{\"key\": \"orgID\", \"value\": 44564}], \"ccUserID\": 8}";

		Mockito.doReturn(organization).when(ods).getOrganizationID("44564");
		Mockito.doReturn(mockedResponse).when(ods).callPostAction("organization_create", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);

		OrganizationRequest organizationRequest = new OrganizationRequest("Test Org", "Org Description", 44564, 8);
		Response response = ods.createOrganization(organizationRequest, "FAKE_KEY");

		Mockito.verify(ods, Mockito.times(1)).getOrganizationID("44564");
		Mockito.verify(ods, Mockito.times(1)).callPostAction("organization_create", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);
		assertEquals(500, response.getStatus());
	}

	@Test
	public void testUpdateOrganization() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);
		JSONObject mockedJSONResponse = new JSONObject();
		mockedJSONResponse.put("success", true);
		
		JSONObject organization = new JSONObject();
		JSONObject result = new JSONObject();
		result.put("id", "test-org");
		JSONArray users = new JSONArray();
		result.put("users", users);
		organization.put("result", result);
		
		String dataDict = "{\"id\": \"test-org\", \"title\": \"Test Org\", \"description\": \"Org Description\", \"extras\": [{\"key\": \"orgID\", \"value\": 44564}], \"ccUserID\": 8, \"users\": []}";

		Mockito.doReturn(organization).when(ods).getOrganizationID("44564");
		Mockito.doReturn(mockedJSONResponse.toString()).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(mockedResponse).when(ods).callPostAction("organization_update", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);

		OrganizationRequest organizationRequest = new OrganizationRequest("Test Org", "Org Description", 44564, 8);
		Response response = ods.createOrganization(organizationRequest, "FAKE_KEY");

		Mockito.verify(ods, Mockito.times(1)).getOrganizationID("44564");
		Mockito.verify(ods, Mockito.times(1)).callPostAction("organization_update", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);
		
		JSONObject jsonResponse = new JSONObject((String) response.getEntity());
		assertEquals(false, jsonResponse.get("error"));
		assertEquals(200, jsonResponse.get("status"));
		assertEquals("Organization created/updated successfully", jsonResponse.get("message"));
	}

	@Test
	public void testDeleteOrganization() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);
		JSONObject mockedJSONResponse = new JSONObject();
		mockedJSONResponse.put("success", true);
		

		JSONObject organization = new JSONObject();
		JSONObject result = new JSONObject();
		result.put("id", "organization-ckan-id");
		JSONArray users = new JSONArray();
		result.put("users", users);
		organization.put("result", result);

		String dataDict = "{\"id\": \"organization-ckan-id\"}";

		Mockito.doReturn(organization).when(ods).getOrganizationID("44564");
		Mockito.doReturn(mockedJSONResponse.toString()).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(mockedResponse).when(ods).callPostAction("organization_delete", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);

		Response response = ods.deleteOrganization("44564", "FAKE_KEY");

		Mockito.verify(ods, Mockito.times(1)).getOrganizationID("44564");
		Mockito.verify(ods, Mockito.times(1)).callPostAction("organization_delete", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);
		
		JSONObject jsonResponse = new JSONObject((String) response.getEntity());
		assertEquals(false, jsonResponse.get("error"));
		assertEquals(200, jsonResponse.get("status"));
		assertEquals("Organization deleted successfully", jsonResponse.get("message"));
	}
	
	@Test
	public void testDeleteOrganization404() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);
		JSONObject mockedJSONResponse = new JSONObject();
		mockedJSONResponse.put("success", true);
		

		JSONObject organization = new JSONObject();
		JSONObject mockedError = new JSONObject();
		mockedError.put("message", "Organization not found");
		organization.put("error", mockedError);
		organization.put("success", false);
		
		Mockito.doReturn(organization).when(ods).getOrganizationID("44564");
		Mockito.doReturn(mockedJSONResponse.toString()).when(mockedResponse).readEntity(String.class);

		Response response = ods.deleteOrganization("44564", "FAKE_KEY");

		Mockito.verify(ods, Mockito.times(1)).getOrganizationID("44564");
		
		JSONObject jsonResponse = new JSONObject((String) response.getEntity());
		assertEquals(true, jsonResponse.get("error"));
		assertEquals(404, jsonResponse.get("status"));
		assertEquals("Organization not found", jsonResponse.get("message"));
	}
	
	@Test
	public void testDeleteOrganization500() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);
		
		JSONObject mockedJSONResponse = new JSONObject();
		mockedJSONResponse.put("success", false);
		JSONObject mockedError = new JSONObject();
		mockedError.put("message", "Organization not found");
		mockedJSONResponse.put("error", mockedError);
		

		JSONObject organization = new JSONObject();
		JSONObject result = new JSONObject();
		result.put("id", "organization-ckan-id");
		JSONArray users = new JSONArray();
		result.put("users", users);
		organization.put("result", result);

		String dataDict = "{\"id\": \"organization-ckan-id\"}";

		Mockito.doReturn(organization).when(ods).getOrganizationID("44564");
		Mockito.doReturn(mockedJSONResponse.toString()).when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(mockedResponse).when(ods).callPostAction("organization_delete", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);

		Response response = ods.deleteOrganization("44564", "FAKE_KEY");

		Mockito.verify(ods, Mockito.times(1)).getOrganizationID("44564");
		Mockito.verify(ods, Mockito.times(1)).callPostAction("organization_delete", dataDict, "FAKE_KEY", null,
				MediaType.APPLICATION_JSON);
		
		JSONObject jsonResponse = new JSONObject((String) response.getEntity());
		assertEquals(true, jsonResponse.get("error"));
		assertEquals(500, jsonResponse.get("status"));
		assertEquals("Organization not found", jsonResponse.get("message"));
	}

	@Test
	public void testGetOrganizationID() throws WeLiveException {
		ODS ods = Mockito.spy(ODS.class);
		Response mockedResponse = Mockito.mock(Response.class);

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", "44564"));

		Mockito.doReturn("{}").when(mockedResponse).readEntity(String.class);
		Mockito.doReturn(mockedResponse).when(ods).callGetAction("get_organization_id", params,
				MediaType.APPLICATION_JSON, null);

		ods.getOrganizationID("44564");

		Mockito.verify(ods, Mockito.times(1)).callGetAction("get_organization_id", params, MediaType.APPLICATION_JSON,
				null);
	}
}
