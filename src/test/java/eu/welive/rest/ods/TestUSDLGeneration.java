/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ods;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

import eu.welive.rest.app.TestApplication;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;

public class TestUSDLGeneration extends JerseyTest {

    @Override
    protected Application configure() {
        return new TestApplication();
    }

    private static String packageID = "test-package";
    private static String apiKey = "FAKE-API-KEY";

    @Test
    public void testGenerateUSDL() throws WeLiveException, IOException {
        ODS ods = Mockito.spy(new ODS());
        Response mockedResponse = Mockito.mock(Response.class);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("id", packageID));

        String responseResult = "{\"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_show\", \"success\": true, \"result\": {\"license_title\": \"Creative Commons Attribution\", \"maintainer\": \"Servizio Statistica\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"serv.statistica@provincia.tn.it\", \"num_tags\": 2, \"id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"metadata_created\": \"2016-05-06T15:06:16.779406\", \"ratings\": null, \"metadata_modified\": \"2016-05-09T12:33:48.530481\", \"author\": \"Servizio Statistica\", \"author_email\": \"serv.statistica@provincia.tn.it\", \"isopen\": true, \"state\": \"active\", \"version\": null, \"archiver\": {\"status\": \"Archived successfully\", \"is_broken\": false, \"reason\": \"\", \"status_id\": 0}, \"creator_user_id\": \"8e52130f-602f-4843-8971-fdd4429b9a7b\", \"type\": \"dataset\", \"resources\": [{\"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"63e9e456-b235-4ced-a52a-9000ac2fbfa7\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:48.152149\", \"cache_filepath\": \"/home/deusto/ckan/archiver/63/63e9e456-b235-4ced-a52a-9000ac2fbfa7/exp.aspx\", \"last_success\": \"2016-05-09T14:46:48.152149\", \"size\": \"44552\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"14d508a86ee3be66685c946bce1aa3bfaf7d0372\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/html\", \"cache_url\": \"/cache/63/63e9e456-b235-4ced-a52a-9000ac2fbfa7/exp.aspx\", \"created\": \"2016-04-15T19:01:42.447075\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"format\": \"JSON\", \"mapping\": \"{\\\"refresh\\\": 1000, \\\"root\\\": \\\"Abitazioni occupate con riscaldamento a combustibile gassoso\\\", \\\"mapping\\\": \\\"json\\\", \\\"key\\\": \\\"_id\\\", \\\"uri\\\": \\\"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=json\\\"}\", \"mimetype_inner\": null, \"url_type\": null, \"permissions\": \"\", \"mimetype\": \"application/json\", \"cache_url\": null, \"name\": null, \"created\": \"2016-03-16T10:58:23.474193\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=json\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.911450\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:48.152149\", \"format\": \"JSON\", \"created\": \"2016-04-15T19:16:20.926723\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"JSON\\\" which receives openness score: 3.\"}, \"last_modified\": null, \"position\": 0, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": true, \"id\": \"4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:49.102262\", \"cache_filepath\": \"/home/deusto/ckan/archiver/41/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a/exp.aspx\", \"last_success\": \"2016-05-09T14:46:49.102262\", \"size\": \"12332\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"00e7686753252ecd0711b876dabf4e09f782dbe6\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/plain\", \"cache_url\": \"/cache/41/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a/exp.aspx\", \"created\": \"2016-04-15T19:01:43.430454\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"format\": \"CSV\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474230\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=csv\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.935901\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:49.102262\", \"format\": \"CSV\", \"created\": \"2016-04-15T19:16:20.946693\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 1, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"924e4b77-84c2-4394-b623-9f6dcfb70e4b\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:49.786498\", \"cache_filepath\": \"/home/deusto/ckan/archiver/92/924e4b77-84c2-4394-b623-9f6dcfb70e4b/exp.aspx\", \"last_success\": \"2016-05-09T14:46:49.786498\", \"size\": \"360\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"2d1ca8f1d695ee9d8f3b9fd80f84d253a3f562b2\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/html\", \"cache_url\": \"/cache/92/924e4b77-84c2-4394-b623-9f6dcfb70e4b/exp.aspx\", \"created\": \"2016-04-15T19:01:43.940682\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\", \"format\": \"JSON\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"application/json\", \"cache_url\": null, \"name\": \"numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474240\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?ntab=Sub_Abitazioni_Gassoso&info=md&fmt=json\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.950680\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:49.786498\", \"format\": \"JSON\", \"created\": \"2016-04-15T19:16:20.964330\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"JSON\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 2, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": true, \"id\": \"fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:50.477284\", \"cache_filepath\": \"/home/deusto/ckan/archiver/fd/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57/exp.aspx\", \"last_success\": \"2016-05-09T14:46:50.477284\", \"size\": \"265\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"97af73d223f01c795f0bde068fc517f2b45e0d5d\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/plain\", \"cache_url\": \"/cache/fd/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57/exp.aspx\", \"created\": \"2016-04-15T19:01:44.468343\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\", \"format\": \"CSV\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474247\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?ntab=Sub_Abitazioni_Gassoso&info=md&fmt=csv\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.972649\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:50.477284\", \"format\": \"CSV\", \"created\": \"2016-04-15T19:16:20.979125\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 3, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}], \"num_resources\": 4, \"tags\": [{\n" +
                "        \"vocabulary_id\": null,\n" +
                "        \"state\": \"active\",\n" +
                "        \"display_name\": \"barrio\",\n" +
                "        \"id\": \"2039dd00-a819-462c-a17e-6b4491c34395\",\n" +
                "        \"name\": \"barrio\"\n" +
                "      },\n" +
                "      ], \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"organization\": {\"description\": \"Datasets from Trento City Council.\", \"created\": \"2016-02-08T12:40:00.548842\", \"title\": \"Trento\", \"name\": \"trento\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"2016-02-08-114000.4994622015-09-30-123748.307226comuneditrento.jpg\", \"revision_id\": \"5e957fdd-2bc3-4939-a6b6-5d08c2109912\", \"type\": \"organization\", \"id\": \"34d0a42f-5b01-4177-88d7-d68bbd705dc3\", \"approval_status\": \"approved\"}, \"name\": \"abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso\", \"language\": \"it\", \"url\": \"http://www.statweb.provincia.tn.it/INDICATORISTRUTTURALISubPro/\", \"notes\": \"**Settore:** Edilizia e opere pubbliche\\n\\n**Algoritmo:** Numero di abitazioni con riscaldamento a combustibile gassoso\\n\\n**Tipo di indicatore:** rapporto\\n\\n**Livello geografico minimo:** Comune\", \"owner_org\": \"34d0a42f-5b01-4177-88d7-d68bbd705dc3\", \"qa\": {\"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\", \"updated\": \"2016-05-09T14:46:50.972649\", \"openness_score\": 3}, \"extras\": [{\"key\": \"Aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Algoritmo\", \"value\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\"}, {\"key\": \"Anno di inizio\", \"value\": \"1971\"}, {\"key\": \"Codifica Caratteri\", \"value\": \"UTF-8\"}, {\"key\": \"Copertura Geografica\", \"value\": \"Provincia di Trento\"}, {\"key\": \"Copertura Temporale (Data di inizio)\", \"value\": \"{1971-01-01T00:00:00}\"}, {\"key\": \"Fonte\", \"value\": \"Ispat\"}, {\"key\": \"Frequenza di aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Livello Geografico Minimo\", \"value\": \"Comune\"}, {\"key\": \"Settore\", \"value\": \"Edilizia e opere pubbliche\"}, {\"key\": \"Tipo di Fenomeno\", \"value\": \"Stock\"}, {\"key\": \"Tipo di Indicatore\", \"value\": \"R\"}, {\"key\": \"Titolare\", \"value\": \"Provincia Autonoma di Trento\"}, {\"key\": \"Ultimo aggiornamento\", \"value\": \"19/11/2014\"}, {\"key\": \"Unit\\u00e0 di misura\", \"value\": \"Unit\\u00e0\"}, {\"key\": \"_harvest_source\", \"value\": \"statistica_subpro:541\"}], \"license_url\": \"http://www.opendefinition.org/licenses/cc-by\", \"title\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"revision_id\": \"00c7486a-24fe-40b1-8f86-4e7289637108\"}}";
        JSONObject jsonObject = new JSONObject(responseResult);

        String API_URL = Config.getInstance().getApiURL();
        String CKAN_URL = Config.getInstance().getCkanURL();

        String expectedUSDL = String.format("<rdf:RDF\n" +
                "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
                "    xmlns:j.0=\"http://xmlns.com/foaf/0.1/\"\n" +
                "    xmlns:j.2=\"http://www.linked-usdl.org/ns/usdl-core#\"\n" +
                "    xmlns:j.1=\"http://www.welive.eu/ns/welive-core#\"\n" +
                "    xmlns:j.3=\"http://purl.org/dc/terms/\"\n" +
                "    xmlns:j.4=\"http://schema.org/\"\n" +
                "    xmlns:j.5=\"http://welive.eu/ns#\"\n" +
                "    xmlns:j.6=\"http://www.holygoat.co.uk/owl/redwood/0.1/tags#\">\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package\">\n" +
                "    <j.3:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Dataset\"/>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/924e4b77-84c2-4394-b623-9f6dcfb70e4b\"/>\n" +
                "    <j.1:hasBusinessRole rdf:resource=\"%2$s/organization/trento\"/>\n" +
                "    <j.3:title>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Service\"/>\n" +
                "    <j.6:tag>barrio</j.6:tag>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/63e9e456-b235-4ced-a52a-9000ac2fbfa7\"/>\n" +
                "    <j.5:hasLegalCondition rdf:resource=\"%1$s/ods/dataset/test-package/license\"/>\n" +
                "    <j.3:created rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2016-05-06</j.3:created>\n" +
                "    <j.0:page rdf:resource=\"%2$s/dataset/test-package\"/>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\"/>\n" +
                "    <j.3:description>**Settore:** Edilizia e opere pubbliche\n" +
                "\n" +
                "**Algoritmo:** Numero di abitazioni con riscaldamento a combustibile gassoso\n" +
                "\n" +
                "**Tipo di indicatore:** rapporto\n" +
                "\n" +
                "**Livello geografico minimo:** Comune</j.3:description>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/license\">\n" +
                "    <j.4:url>http://www.opendefinition.org/licenses/cc-by</j.4:url>\n" +
                "    <j.3:description rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">Creative Commons Attribution</j.3:description>\n" +
                "    <j.3:title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">cc-by</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.linked-usdl.org/ns/usdl-core#LegalCondition\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a</j.4:url>\n" +
                "    <j.3:description>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/63e9e456-b235-4ced-a52a-9000ac2fbfa7\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/63e9e456-b235-4ced-a52a-9000ac2fbfa7</j.4:url>\n" +
                "    <j.3:description>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>Unnamed resource</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%2$s/organization/trento\">\n" +
                "    <j.1:businessRole rdf:resource=\"http://www.welive.eu/ns/welive-core#Owner\"/>\n" +
                "    <j.3:title>Trento</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Entity\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/924e4b77-84c2-4394-b623-9f6dcfb70e4b\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/924e4b77-84c2-4394-b623-9f6dcfb70e4b</j.4:url>\n" +
                "    <j.3:description>Numero di abitazioni con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57</j.4:url>\n" +
                "    <j.3:description>Numero di abitazioni con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "</rdf:RDF>\n", API_URL, CKAN_URL);


        Model expectedModel = ModelFactory.createDefaultModel();
        expectedModel.read(new ByteArrayInputStream(expectedUSDL.getBytes()), null);

        Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

        String resultUSDL = ods.generateUSDL(packageID, apiKey);
        Model resultModel = ModelFactory.createDefaultModel();
        resultModel.read(new ByteArrayInputStream(resultUSDL.getBytes()), null);

        Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
        Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);
        
        assertTrue(expectedModel.isIsomorphicWith(resultModel));
    }

    @Test
    public void testGenerateUSDLMissingParams() throws WeLiveException, IOException {
        ODS ods = Mockito.spy(new ODS());
        Response mockedResponse = Mockito.mock(Response.class);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("id", packageID));

        String responseResult = "{\"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_show\", \"success\": true, \"result\": {\"maintainer\": \"Servizio Statistica\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"serv.statistica@provincia.tn.it\", \"num_tags\": 2, \"id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"ratings\": null, \"metadata_modified\": \"2016-05-09T12:33:48.530481\", \"author\": \"Servizio Statistica\", \"author_email\": \"serv.statistica@provincia.tn.it\", \"isopen\": true, \"state\": \"active\", \"version\": null, \"archiver\": {\"status\": \"Archived successfully\", \"is_broken\": false, \"reason\": \"\", \"status_id\": 0}, \"creator_user_id\": \"8e52130f-602f-4843-8971-fdd4429b9a7b\", \"type\": \"dataset\", \"num_resources\": 4," +
                "      \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"name\": \"abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso\", \"language\": \"it\", \"url\": \"http://www.statweb.provincia.tn.it/INDICATORISTRUTTURALISubPro/\", \"owner_org\": \"34d0a42f-5b01-4177-88d7-d68bbd705dc3\", \"qa\": {\"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\", \"updated\": \"2016-05-09T14:46:50.972649\", \"openness_score\": 3}, \"extras\": [{\"key\": \"Aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Algoritmo\", \"value\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\"}, {\"key\": \"Anno di inizio\", \"value\": \"1971\"}, {\"key\": \"Codifica Caratteri\", \"value\": \"UTF-8\"}, {\"key\": \"Copertura Geografica\", \"value\": \"Provincia di Trento\"}, {\"key\": \"Copertura Temporale (Data di inizio)\", \"value\": \"{1971-01-01T00:00:00}\"}, {\"key\": \"Fonte\", \"value\": \"Ispat\"}, {\"key\": \"Frequenza di aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Livello Geografico Minimo\", \"value\": \"Comune\"}, {\"key\": \"Settore\", \"value\": \"Edilizia e opere pubbliche\"}, {\"key\": \"Tipo di Fenomeno\", \"value\": \"Stock\"}, {\"key\": \"Tipo di Indicatore\", \"value\": \"R\"}, {\"key\": \"Titolare\", \"value\": \"Provincia Autonoma di Trento\"}, {\"key\": \"Ultimo aggiornamento\", \"value\": \"19/11/2014\"}, {\"key\": \"Unit\\u00e0 di misura\", \"value\": \"Unit\\u00e0\"}, {\"key\": \"_harvest_source\", \"value\": \"statistica_subpro:541\"}], \"license_url\": \"http://www.opendefinition.org/licenses/cc-by\", \"revision_id\": \"00c7486a-24fe-40b1-8f86-4e7289637108\"}}";
        JSONObject jsonObject = new JSONObject(responseResult);

        String API_URL = Config.getInstance().getApiURL();
        String CKAN_URL = Config.getInstance().getCkanURL();

        String expectedUSDL = String.format("<rdf:RDF\n" +
                "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
                "    xmlns:j.0=\"http://xmlns.com/foaf/0.1/\"\n" +
                "    xmlns:j.2=\"http://www.linked-usdl.org/ns/usdl-core#\"\n" +
                "    xmlns:j.1=\"http://www.welive.eu/ns/welive-core#\"\n" +
                "    xmlns:j.3=\"http://purl.org/dc/terms/\"\n" +
                "    xmlns:j.4=\"http://schema.org/\"\n" +
                "    xmlns:j.5=\"http://welive.eu/ns#\"\n" +
                "    xmlns:j.6=\"http://www.holygoat.co.uk/owl/redwood/0.1/tags#\">\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package\">\n" +
                "    <j.3:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Dataset\"/>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Service\"/>\n" +
                "    <j.0:page rdf:resource=\"%2$s/dataset/test-package\"/>\n" +
                "  </rdf:Description>\n" +
                "</rdf:RDF>\n", API_URL, CKAN_URL);


        Model expectedModel = ModelFactory.createDefaultModel();
        expectedModel.read(new ByteArrayInputStream(expectedUSDL.getBytes()), null);

        Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

        String resultUSDL = ods.generateUSDL(packageID, apiKey);
        Model resultModel = ModelFactory.createDefaultModel();
        resultModel.read(new ByteArrayInputStream(resultUSDL.getBytes()), null);

        Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
        Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);

        assertTrue(expectedModel.isIsomorphicWith(resultModel));
    }

    @Test
    public void testGenerateUSDLNullLicenseTitle() throws WeLiveException, IOException {
        ODS ods = Mockito.spy(new ODS());
        Response mockedResponse = Mockito.mock(Response.class);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("id", packageID));

        String responseResult = "{\"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_show\", \"success\": true, \"result\": {\"license_title\": null, \"maintainer\": \"Servizio Statistica\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"serv.statistica@provincia.tn.it\", \"num_tags\": 2, \"id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"metadata_created\": \"2016-05-06T15:06:16.779406\", \"ratings\": null, \"metadata_modified\": \"2016-05-09T12:33:48.530481\", \"author\": \"Servizio Statistica\", \"author_email\": \"serv.statistica@provincia.tn.it\", \"isopen\": true, \"state\": \"active\", \"version\": null, \"archiver\": {\"status\": \"Archived successfully\", \"is_broken\": false, \"reason\": \"\", \"status_id\": 0}, \"creator_user_id\": \"8e52130f-602f-4843-8971-fdd4429b9a7b\", \"type\": \"dataset\", \"resources\": [{\"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"63e9e456-b235-4ced-a52a-9000ac2fbfa7\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:48.152149\", \"cache_filepath\": \"/home/deusto/ckan/archiver/63/63e9e456-b235-4ced-a52a-9000ac2fbfa7/exp.aspx\", \"last_success\": \"2016-05-09T14:46:48.152149\", \"size\": \"44552\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"14d508a86ee3be66685c946bce1aa3bfaf7d0372\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/html\", \"cache_url\": \"/cache/63/63e9e456-b235-4ced-a52a-9000ac2fbfa7/exp.aspx\", \"created\": \"2016-04-15T19:01:42.447075\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"format\": \"JSON\", \"mapping\": \"{\\\"refresh\\\": 1000, \\\"root\\\": \\\"Abitazioni occupate con riscaldamento a combustibile gassoso\\\", \\\"mapping\\\": \\\"json\\\", \\\"key\\\": \\\"_id\\\", \\\"uri\\\": \\\"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=json\\\"}\", \"mimetype_inner\": null, \"url_type\": null, \"permissions\": \"\", \"mimetype\": \"application/json\", \"cache_url\": null, \"name\": null, \"created\": \"2016-03-16T10:58:23.474193\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=json\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.911450\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:48.152149\", \"format\": \"JSON\", \"created\": \"2016-04-15T19:16:20.926723\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"JSON\\\" which receives openness score: 3.\"}, \"last_modified\": null, \"position\": 0, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": true, \"id\": \"4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:49.102262\", \"cache_filepath\": \"/home/deusto/ckan/archiver/41/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a/exp.aspx\", \"last_success\": \"2016-05-09T14:46:49.102262\", \"size\": \"12332\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"00e7686753252ecd0711b876dabf4e09f782dbe6\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/plain\", \"cache_url\": \"/cache/41/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a/exp.aspx\", \"created\": \"2016-04-15T19:01:43.430454\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"format\": \"CSV\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474230\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=csv\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.935901\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:49.102262\", \"format\": \"CSV\", \"created\": \"2016-04-15T19:16:20.946693\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 1, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"924e4b77-84c2-4394-b623-9f6dcfb70e4b\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:49.786498\", \"cache_filepath\": \"/home/deusto/ckan/archiver/92/924e4b77-84c2-4394-b623-9f6dcfb70e4b/exp.aspx\", \"last_success\": \"2016-05-09T14:46:49.786498\", \"size\": \"360\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"2d1ca8f1d695ee9d8f3b9fd80f84d253a3f562b2\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/html\", \"cache_url\": \"/cache/92/924e4b77-84c2-4394-b623-9f6dcfb70e4b/exp.aspx\", \"created\": \"2016-04-15T19:01:43.940682\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\", \"format\": \"JSON\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"application/json\", \"cache_url\": null, \"name\": \"numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474240\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?ntab=Sub_Abitazioni_Gassoso&info=md&fmt=json\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.950680\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:49.786498\", \"format\": \"JSON\", \"created\": \"2016-04-15T19:16:20.964330\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"JSON\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 2, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": true, \"id\": \"fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:50.477284\", \"cache_filepath\": \"/home/deusto/ckan/archiver/fd/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57/exp.aspx\", \"last_success\": \"2016-05-09T14:46:50.477284\", \"size\": \"265\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"97af73d223f01c795f0bde068fc517f2b45e0d5d\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/plain\", \"cache_url\": \"/cache/fd/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57/exp.aspx\", \"created\": \"2016-04-15T19:01:44.468343\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\", \"format\": \"CSV\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474247\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?ntab=Sub_Abitazioni_Gassoso&info=md&fmt=csv\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.972649\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:50.477284\", \"format\": \"CSV\", \"created\": \"2016-04-15T19:16:20.979125\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 3, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}], \"num_resources\": 4, \"tags\": [{\n" +
                "        \"vocabulary_id\": null,\n" +
                "        \"state\": \"active\",\n" +
                "        \"display_name\": \"barrio\",\n" +
                "        \"id\": \"2039dd00-a819-462c-a17e-6b4491c34395\",\n" +
                "        \"name\": \"barrio\"\n" +
                "      },\n" +
                "      ], \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"organization\": {\"description\": \"Datasets from Trento City Council.\", \"created\": \"2016-02-08T12:40:00.548842\", \"title\": \"Trento\", \"name\": \"trento\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"2016-02-08-114000.4994622015-09-30-123748.307226comuneditrento.jpg\", \"revision_id\": \"5e957fdd-2bc3-4939-a6b6-5d08c2109912\", \"type\": \"organization\", \"id\": \"34d0a42f-5b01-4177-88d7-d68bbd705dc3\", \"approval_status\": \"approved\"}, \"name\": \"abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso\", \"language\": \"it\", \"url\": \"http://www.statweb.provincia.tn.it/INDICATORISTRUTTURALISubPro/\", \"notes\": \"**Settore:** Edilizia e opere pubbliche\\n\\n**Algoritmo:** Numero di abitazioni con riscaldamento a combustibile gassoso\\n\\n**Tipo di indicatore:** rapporto\\n\\n**Livello geografico minimo:** Comune\", \"owner_org\": \"34d0a42f-5b01-4177-88d7-d68bbd705dc3\", \"qa\": {\"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\", \"updated\": \"2016-05-09T14:46:50.972649\", \"openness_score\": 3}, \"extras\": [{\"key\": \"Aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Algoritmo\", \"value\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\"}, {\"key\": \"Anno di inizio\", \"value\": \"1971\"}, {\"key\": \"Codifica Caratteri\", \"value\": \"UTF-8\"}, {\"key\": \"Copertura Geografica\", \"value\": \"Provincia di Trento\"}, {\"key\": \"Copertura Temporale (Data di inizio)\", \"value\": \"{1971-01-01T00:00:00}\"}, {\"key\": \"Fonte\", \"value\": \"Ispat\"}, {\"key\": \"Frequenza di aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Livello Geografico Minimo\", \"value\": \"Comune\"}, {\"key\": \"Settore\", \"value\": \"Edilizia e opere pubbliche\"}, {\"key\": \"Tipo di Fenomeno\", \"value\": \"Stock\"}, {\"key\": \"Tipo di Indicatore\", \"value\": \"R\"}, {\"key\": \"Titolare\", \"value\": \"Provincia Autonoma di Trento\"}, {\"key\": \"Ultimo aggiornamento\", \"value\": \"19/11/2014\"}, {\"key\": \"Unit\\u00e0 di misura\", \"value\": \"Unit\\u00e0\"}, {\"key\": \"_harvest_source\", \"value\": \"statistica_subpro:541\"}], \"license_url\": \"http://www.opendefinition.org/licenses/cc-by\", \"title\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"revision_id\": \"00c7486a-24fe-40b1-8f86-4e7289637108\"}}";
        JSONObject jsonObject = new JSONObject(responseResult);

        String API_URL = Config.getInstance().getApiURL();
        String CKAN_URL = Config.getInstance().getCkanURL();

        String expectedUSDL = String.format("<rdf:RDF\n" +
                "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
                "    xmlns:j.0=\"http://xmlns.com/foaf/0.1/\"\n" +
                "    xmlns:j.2=\"http://www.linked-usdl.org/ns/usdl-core#\"\n" +
                "    xmlns:j.1=\"http://www.welive.eu/ns/welive-core#\"\n" +
                "    xmlns:j.3=\"http://purl.org/dc/terms/\"\n" +
                "    xmlns:j.4=\"http://schema.org/\"\n" +
                "    xmlns:j.5=\"http://welive.eu/ns#\"\n" +
                "    xmlns:j.6=\"http://www.holygoat.co.uk/owl/redwood/0.1/tags#\">\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package\">\n" +
                "    <j.3:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Dataset\"/>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/924e4b77-84c2-4394-b623-9f6dcfb70e4b\"/>\n" +
                "    <j.1:hasBusinessRole rdf:resource=\"%2$s/organization/trento\"/>\n" +
                "    <j.3:title>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Service\"/>\n" +
                "    <j.6:tag>barrio</j.6:tag>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/63e9e456-b235-4ced-a52a-9000ac2fbfa7\"/>\n" +
                "    <j.3:created rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2016-05-06</j.3:created>\n" +
                "    <j.0:page rdf:resource=\"%2$s/dataset/test-package\"/>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\"/>\n" +
                "    <j.3:description>**Settore:** Edilizia e opere pubbliche\n" +
                "\n" +
                "**Algoritmo:** Numero di abitazioni con riscaldamento a combustibile gassoso\n" +
                "\n" +
                "**Tipo di indicatore:** rapporto\n" +
                "\n" +
                "**Livello geografico minimo:** Comune</j.3:description>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a</j.4:url>\n" +
                "    <j.3:description>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/63e9e456-b235-4ced-a52a-9000ac2fbfa7\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/63e9e456-b235-4ced-a52a-9000ac2fbfa7</j.4:url>\n" +
                "    <j.3:description>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>Unnamed resource</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%2$s/organization/trento\">\n" +
                "    <j.1:businessRole rdf:resource=\"http://www.welive.eu/ns/welive-core#Owner\"/>\n" +
                "    <j.3:title>Trento</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Entity\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/924e4b77-84c2-4394-b623-9f6dcfb70e4b\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/924e4b77-84c2-4394-b623-9f6dcfb70e4b</j.4:url>\n" +
                "    <j.3:description>Numero di abitazioni con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57</j.4:url>\n" +
                "    <j.3:description>Numero di abitazioni con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "</rdf:RDF>\n", API_URL, CKAN_URL);


        Model expectedModel = ModelFactory.createDefaultModel();
        expectedModel.read(new ByteArrayInputStream(expectedUSDL.getBytes()), null);

        Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

        String resultUSDL = ods.generateUSDL(packageID, apiKey);
        Model resultModel = ModelFactory.createDefaultModel();
        resultModel.read(new ByteArrayInputStream(resultUSDL.getBytes()), null);

        Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
        Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);

        assertTrue(expectedModel.isIsomorphicWith(resultModel));
    }

    @Test
    public void testGenerateUSDLMissingLicenseURLAndResourceParams() throws WeLiveException, IOException {
        ODS ods = Mockito.spy(new ODS());
        Response mockedResponse = Mockito.mock(Response.class);

        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("id", packageID));

        String responseResult = "{\"help\": \"https://dev.welive.eu/ods/api/3/action/help_show?name=package_show\", \"success\": true, \"result\": {\"license_title\": \"Creative Commons Attribution\", \"maintainer\": \"Servizio Statistica\", \"relationships_as_object\": [], \"private\": false, \"maintainer_email\": \"serv.statistica@provincia.tn.it\", \"num_tags\": 2, \"id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"metadata_created\": \"2016-05-06T15:06:16.779406\", \"ratings\": null, \"metadata_modified\": \"2016-05-09T12:33:48.530481\", \"author\": \"Servizio Statistica\", \"author_email\": \"serv.statistica@provincia.tn.it\", \"isopen\": true, \"state\": \"active\", \"version\": null, \"archiver\": {\"status\": \"Archived successfully\", \"is_broken\": false, \"reason\": \"\", \"status_id\": 0}, \"creator_user_id\": \"8e52130f-602f-4843-8971-fdd4429b9a7b\", \"type\": \"dataset\", \"resources\": [{\"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"63e9e456-b235-4ced-a52a-9000ac2fbfa7\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:48.152149\", \"cache_filepath\": \"/home/deusto/ckan/archiver/63/63e9e456-b235-4ced-a52a-9000ac2fbfa7/exp.aspx\", \"last_success\": \"2016-05-09T14:46:48.152149\", \"size\": \"44552\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"14d508a86ee3be66685c946bce1aa3bfaf7d0372\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/html\", \"cache_url\": \"/cache/63/63e9e456-b235-4ced-a52a-9000ac2fbfa7/exp.aspx\", \"created\": \"2016-04-15T19:01:42.447075\", \"first_failure\": null}, \"hash\": \"\", \"format\": \"JSON\", \"mapping\": \"{\\\"refresh\\\": 1000, \\\"root\\\": \\\"Abitazioni occupate con riscaldamento a combustibile gassoso\\\", \\\"mapping\\\": \\\"json\\\", \\\"key\\\": \\\"_id\\\", \\\"uri\\\": \\\"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=json\\\"}\", \"mimetype_inner\": null, \"url_type\": null, \"permissions\": \"\", \"mimetype\": \"application/json\", \"cache_url\": null, \"created\": \"2016-03-16T10:58:23.474193\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.911450\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:48.152149\", \"format\": \"JSON\", \"created\": \"2016-04-15T19:16:20.926723\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"JSON\\\" which receives openness score: 3.\"}, \"last_modified\": null, \"position\": 0, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": true, \"id\": \"4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:49.102262\", \"cache_filepath\": \"/home/deusto/ckan/archiver/41/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a/exp.aspx\", \"last_success\": \"2016-05-09T14:46:49.102262\", \"size\": \"12332\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"00e7686753252ecd0711b876dabf4e09f782dbe6\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/plain\", \"cache_url\": \"/cache/41/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a/exp.aspx\", \"created\": \"2016-04-15T19:01:43.430454\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"format\": \"CSV\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474230\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?idind=541&info=d&fmt=csv\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.935901\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:49.102262\", \"format\": \"CSV\", \"created\": \"2016-04-15T19:16:20.946693\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 1, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": false, \"id\": \"924e4b77-84c2-4394-b623-9f6dcfb70e4b\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:49.786498\", \"cache_filepath\": \"/home/deusto/ckan/archiver/92/924e4b77-84c2-4394-b623-9f6dcfb70e4b/exp.aspx\", \"last_success\": \"2016-05-09T14:46:49.786498\", \"size\": \"360\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"2d1ca8f1d695ee9d8f3b9fd80f84d253a3f562b2\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/html\", \"cache_url\": \"/cache/92/924e4b77-84c2-4394-b623-9f6dcfb70e4b/exp.aspx\", \"created\": \"2016-04-15T19:01:43.940682\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\", \"format\": \"JSON\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"application/json\", \"cache_url\": null, \"name\": \"numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474240\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?ntab=Sub_Abitazioni_Gassoso&info=md&fmt=json\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.950680\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:49.786498\", \"format\": \"JSON\", \"created\": \"2016-04-15T19:16:20.964330\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"JSON\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 2, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}, {\"resource_group_id\": \"803eb9bf-3d7c-4623-a47c-55dd497d48e3\", \"cache_last_updated\": null, \"package_id\": \"037208ab-8b07-4532-8b4d-d1eba25bd795\", \"webstore_last_updated\": null, \"datastore_active\": true, \"id\": \"fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\", \"size\": null, \"state\": \"active\", \"archiver\": {\"is_broken_printable\": \"Downloaded OK\", \"updated\": \"2016-05-09T14:46:50.477284\", \"cache_filepath\": \"/home/deusto/ckan/archiver/fd/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57/exp.aspx\", \"last_success\": \"2016-05-09T14:46:50.477284\", \"size\": \"265\", \"is_broken\": false, \"failure_count\": 0, \"etag\": null, \"status\": \"Archived successfully\", \"url_redirected_to\": null, \"hash\": \"97af73d223f01c795f0bde068fc517f2b45e0d5d\", \"status_id\": 0, \"reason\": \"\", \"last_modified\": null, \"resource_timestamp\": \"2016-05-09T12:33:48.526883\", \"mimetype\": \"text/plain\", \"cache_url\": \"/cache/fd/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57/exp.aspx\", \"created\": \"2016-04-15T19:01:44.468343\", \"first_failure\": null}, \"hash\": \"\", \"description\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\", \"format\": \"CSV\", \"last_modified\": null, \"url_type\": null, \"mimetype\": \"text/csv\", \"cache_url\": null, \"name\": \"numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso\", \"created\": \"2016-03-16T10:58:23.474247\", \"url\": \"http://www.statweb.provincia.tn.it/indicatoristrutturalisubpro/exp.aspx?ntab=Sub_Abitazioni_Gassoso&info=md&fmt=csv\", \"webstore_url\": null, \"qa\": {\"updated\": \"2016-05-09T14:46:50.972649\", \"openness_score\": 3, \"archival_timestamp\": \"2016-05-09T14:46:50.477284\", \"format\": \"CSV\", \"created\": \"2016-04-15T19:16:20.979125\", \"resource_timestamp\": null, \"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\"}, \"mimetype_inner\": null, \"position\": 3, \"revision_id\": \"88ae37ab-e84c-4aad-882b-48c657a9a1e4\", \"resource_type\": \"\"}], \"num_resources\": 4, \"tags\": [{\n" +
                "        \"vocabulary_id\": null,\n" +
                "        \"state\": \"active\",\n" +
                "        \"display_name\": \"barrio\",\n" +
                "        \"id\": \"2039dd00-a819-462c-a17e-6b4491c34395\",\n" +
                "        \"name\": \"barrio\"\n" +
                "      },\n" +
                "      ], \"groups\": [], \"license_id\": \"cc-by\", \"relationships_as_subject\": [], \"organization\": {\"description\": \"Datasets from Trento City Council.\", \"created\": \"2016-02-08T12:40:00.548842\", \"title\": \"Trento\", \"name\": \"trento\", \"is_organization\": true, \"state\": \"active\", \"image_url\": \"2016-02-08-114000.4994622015-09-30-123748.307226comuneditrento.jpg\", \"revision_id\": \"5e957fdd-2bc3-4939-a6b6-5d08c2109912\", \"type\": \"organization\", \"id\": \"34d0a42f-5b01-4177-88d7-d68bbd705dc3\", \"approval_status\": \"approved\"}, \"name\": \"abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso\", \"language\": \"it\", \"url\": \"http://www.statweb.provincia.tn.it/INDICATORISTRUTTURALISubPro/\", \"notes\": \"**Settore:** Edilizia e opere pubbliche\\n\\n**Algoritmo:** Numero di abitazioni con riscaldamento a combustibile gassoso\\n\\n**Tipo di indicatore:** rapporto\\n\\n**Livello geografico minimo:** Comune\", \"owner_org\": \"34d0a42f-5b01-4177-88d7-d68bbd705dc3\", \"qa\": {\"openness_score_reason\": \"Content of file appeared to be format \\\"CSV\\\" which receives openness score: 3.\", \"updated\": \"2016-05-09T14:46:50.972649\", \"openness_score\": 3}, \"extras\": [{\"key\": \"Aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Algoritmo\", \"value\": \"Numero di abitazioni con riscaldamento a combustibile gassoso\"}, {\"key\": \"Anno di inizio\", \"value\": \"1971\"}, {\"key\": \"Codifica Caratteri\", \"value\": \"UTF-8\"}, {\"key\": \"Copertura Geografica\", \"value\": \"Provincia di Trento\"}, {\"key\": \"Copertura Temporale (Data di inizio)\", \"value\": \"{1971-01-01T00:00:00}\"}, {\"key\": \"Fonte\", \"value\": \"Ispat\"}, {\"key\": \"Frequenza di aggiornamento\", \"value\": \"Decennale\"}, {\"key\": \"Livello Geografico Minimo\", \"value\": \"Comune\"}, {\"key\": \"Settore\", \"value\": \"Edilizia e opere pubbliche\"}, {\"key\": \"Tipo di Fenomeno\", \"value\": \"Stock\"}, {\"key\": \"Tipo di Indicatore\", \"value\": \"R\"}, {\"key\": \"Titolare\", \"value\": \"Provincia Autonoma di Trento\"}, {\"key\": \"Ultimo aggiornamento\", \"value\": \"19/11/2014\"}, {\"key\": \"Unit\\u00e0 di misura\", \"value\": \"Unit\\u00e0\"}, {\"key\": \"_harvest_source\", \"value\": \"statistica_subpro:541\"}], \"title\": \"Abitazioni occupate con riscaldamento a combustibile gassoso\", \"revision_id\": \"00c7486a-24fe-40b1-8f86-4e7289637108\"}}";
        JSONObject jsonObject = new JSONObject(responseResult);

        String API_URL = Config.getInstance().getApiURL();
        String CKAN_URL = Config.getInstance().getCkanURL();

        String expectedUSDL = String.format("<rdf:RDF\n" +
                "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
                "    xmlns:j.0=\"http://xmlns.com/foaf/0.1/\"\n" +
                "    xmlns:j.2=\"http://www.linked-usdl.org/ns/usdl-core#\"\n" +
                "    xmlns:j.1=\"http://www.welive.eu/ns/welive-core#\"\n" +
                "    xmlns:j.3=\"http://purl.org/dc/terms/\"\n" +
                "    xmlns:j.4=\"http://schema.org/\"\n" +
                "    xmlns:j.5=\"http://welive.eu/ns#\"\n" +
                "    xmlns:j.6=\"http://www.holygoat.co.uk/owl/redwood/0.1/tags#\">\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package\">\n" +
                "    <j.3:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Dataset\"/>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/924e4b77-84c2-4394-b623-9f6dcfb70e4b\"/>\n" +
                "    <j.1:hasBusinessRole rdf:resource=\"%2$s/organization/trento\"/>\n" +
                "    <j.3:title>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Service\"/>\n" +
                "    <j.6:tag>barrio</j.6:tag>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/63e9e456-b235-4ced-a52a-9000ac2fbfa7\"/>\n" +
                "    <j.5:hasLegalCondition rdf:resource=\"%1$s/ods/dataset/test-package/license\"/>\n" +
                "    <j.3:created rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2016-05-06</j.3:created>\n" +
                "    <j.0:page rdf:resource=\"%2$s/dataset/test-package\"/>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\"/>\n" +
                "    <j.3:description>**Settore:** Edilizia e opere pubbliche\n" +
                "\n" +
                "**Algoritmo:** Numero di abitazioni con riscaldamento a combustibile gassoso\n" +
                "\n" +
                "**Tipo di indicatore:** rapporto\n" +
                "\n" +
                "**Livello geografico minimo:** Comune</j.3:description>\n" +
                "    <j.1:hasInteractionPoint rdf:resource=\"%1$s/ods/dataset/test-package/interaction-point/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/license\">\n" +
                "    <j.3:description rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">Creative Commons Attribution</j.3:description>\n" +
                "    <j.3:title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">cc-by</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.linked-usdl.org/ns/usdl-core#LegalCondition\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/4131c8a9-69b6-45c9-9e6d-ee7fcbb7124a</j.4:url>\n" +
                "    <j.3:description>Abitazioni occupate con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>abitazioni-occupate-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/63e9e456-b235-4ced-a52a-9000ac2fbfa7\">\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%2$s/organization/trento\">\n" +
                "    <j.1:businessRole rdf:resource=\"http://www.welive.eu/ns/welive-core#Owner\"/>\n" +
                "    <j.3:title>Trento</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#Entity\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/924e4b77-84c2-4394-b623-9f6dcfb70e4b\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/924e4b77-84c2-4394-b623-9f6dcfb70e4b</j.4:url>\n" +
                "    <j.3:description>Numero di abitazioni con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "  <rdf:Description rdf:about=\"%1$s/ods/dataset/test-package/interaction-point/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57\">\n" +
                "    <j.4:url>%1$s/ods/dataset/test-package/resource/fd02d3b9-c595-42b9-a1a2-ab7f78b8ea57</j.4:url>\n" +
                "    <j.3:description>Numero di abitazioni con riscaldamento a combustibile gassoso</j.3:description>\n" +
                "    <j.3:title>numero-di-abitazioni-con-riscaldamento-a-combustibile-gassoso</j.3:title>\n" +
                "    <rdf:type rdf:resource=\"http://www.welive.eu/ns/welive-core#InteractionPoint\"/>\n" +
                "  </rdf:Description>\n" +
                "</rdf:RDF>\n", API_URL, CKAN_URL);


        Model expectedModel = ModelFactory.createDefaultModel();
        expectedModel.read(new ByteArrayInputStream(expectedUSDL.getBytes()), null);

        Mockito.doReturn(mockedResponse).when(ods).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.doReturn(responseResult).when(mockedResponse).readEntity(String.class);
        Mockito.doReturn(jsonObject).when(ods).getJsonObject(responseResult);

        String resultUSDL = ods.generateUSDL(packageID, apiKey);
        Model resultModel = ModelFactory.createDefaultModel();
        resultModel.read(new ByteArrayInputStream(resultUSDL.getBytes()), null);

        Mockito.verify(ods, Mockito.times(1)).callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);
        Mockito.verify(ods, Mockito.times(1)).getJsonObject(responseResult);
        Mockito.verify(mockedResponse, Mockito.times(1)).readEntity(String.class);

        assertTrue(expectedModel.isIsomorphicWith(resultModel));
    }
}
