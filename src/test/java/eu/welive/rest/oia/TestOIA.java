/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.oia;
import eu.welive.rest.exception.WeLiveException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.test.JerseyTest;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


import eu.welive.rest.cdv.CDV.AuthType;
import eu.welive.rest.config.Config;
import eu.welive.rest.mkp.MKP;
import eu.welive.rest.mkp.NewDataset;
import eu.welive.rest.mkp.Pilot;



@RunWith(PowerMockRunner.class)
@PrepareForTest({ClientBuilder.class, Entity.class, Config.class, OIA.class, IOUtils.class})
@PowerMockIgnore("javax.net.ssl.*")

public class TestOIA extends JerseyTest{
	
	private static String token = "FAKE-TOKEN";
	Pilot pilotID;
	AuthType cdvType;
	
	public static enum AuthType {Basic, Oauth};
	
	//private static String url = "url";
	//private String baseurl = "https://test.welive.eu/api/jsonws/Challenge62-portlet.clsidea";
	
	
	@Override
	protected Application configure() {
		return new Application();
	}
	
	/*@Test(expected = WeLiveException.class)
	public void testReadConfig() throws WeLiveException{
		PowerMockito.mockStatic(Config.class);
		

		Mockito.spy(new CDV());
	}*/
	
	//@Override
    //protected javax.ws.rs.core.Application configure() {
      //  return new TestApplication();
    //}
	
	
	@Test
	public void testPublishScreen() throws Exception {
		
		JSONObject json = new JSONObject(String.format("{\"error\": true, \"message\": \"Authentication is required\"}"));
		JSONObject json2 = new JSONObject(String.format("{\"error\": true, \"message\": \"IdeaId is mandatory\"}"));
		String ideaId = "345266";
		NewScreenshot screenshot = new NewScreenshot();
		screenshot.setDescription("description");
		screenshot.setEncodedScreenShot(null);
		screenshot.setImgMimeType(null);
		screenshot.setUsername("username");
		screenshot.getDescription();
		screenshot.getEncodedScreenShot();
		screenshot.getImgMimeType();
		screenshot.getUsername();
		screenshot.setIdea_id(ideaId);
		screenshot.getIdea_id();
		
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getOiapath() +"/publish-screen-shot";
		 OIA oia = Mockito.spy(new OIA());
		 Response mockedResponse = Mockito.mock (Response.class);
	   	    String responseString = "{\"error\": true, \"message\": \"Authentication is required\"}";
	    	Mockito.doReturn(responseString).when(mockedResponse).readEntity(String.class);
	    	HttpHeaders headers = null;
			Mockito.doReturn(mockedResponse).when(oia).post(url, token, screenshot);
	    	 
			oia.publishScreenShot(headers, token, screenshot);
	    	
	    	 Mockito.verify(oia,Mockito.times(1)).post(url, token, screenshot);
	    	// oia.publishScreenShotAction(ideaId, screenshot, token);
	    	 
		
	}
	
	@Test
	public void testUpdateProject() throws Exception {
		
		AckResponse resp = new AckResponse();
		resp.setError(true);
		resp.setMessage("message");
		resp.getError();
		resp.getMessage();
		String ideaId = "345266";
		JSONObject json = new JSONObject(String.format("{\"error\": true, \"message\": \"Authentication is required\"}"));
		OIA oia = Mockito.spy(new OIA());
		Response mockedResponse = Mockito.mock (Response.class);
		String responseString = "{\"error\": true, \"message\": \"Authentication is required\"}";
    	Mockito.doReturn(responseString).when(mockedResponse).readEntity(String.class);
    	HttpHeaders headers = null;
    	VCProjectAssociation vcprj = new VCProjectAssociation();
    	vcprj.setIdeaId(ideaId);
    	vcprj.setMockup(true);
    	vcprj.setVcProjectId("id");
    	vcprj.setVcProjectName("name");
    	//vcprj.getIdeaId();
    	//vcprj.getVcProjectId();
    	//vcprj.getVcProjectName();
    	String url = Config.getInstance().getLiferayURL() + Config.getInstance().getOiapath() +"/vc-project-update"
				+ "/vmeprojectid/"+vcprj.getVcProjectId()
				+ "/vmeprojectname/"+vcprj.getVcProjectName()
				+ "/ismockup/"+vcprj.isMockup()
				+ "/ideaid/"+vcprj.getIdeaId();
   
    	Mockito.doReturn(mockedResponse).when(oia).invoke(url, token);
    	oia.vcProjectUpdate(headers, token, vcprj);
    	
    	
   	 Mockito.verify(oia,Mockito.times(1)).invoke(url, token);
   	 
   	 
   	OIA oia2 = Mockito.spy(new OIA());
  	 
	   //JSONObject json2 = new JSONObject(String.format("{\"message\": \"IdeaId is mandatory\",\"error\": true}"));
	//String responseString2 = "{\"message\": \"IdeaId is mandatory\",\"error\": true}";
	Mockito.doReturn(responseString).when(mockedResponse).readEntity(JSONObject.class);
	JSONObject mock = Mockito.mock(JSONObject.class);
 mock = new JSONObject (String.format("{\"message\": \"IdeaId is mandatory\",\"error\": true}"));
	Mockito.doReturn(mock).when(oia2).isValidVCProjectAssociation(vcprj); 
	oia2.isValidVCProjectAssociation(vcprj);
	Mockito.verify(oia2,Mockito.times(1)).isValidVCProjectAssociation(vcprj);
   	 
		
	}
	
	@Test
	public void testValiddScreenResquest () throws Exception{
		OIA oia = Mockito.spy(new OIA());
	 	VCProjectAssociation vcprj = new VCProjectAssociation();
    
	Long ideaId =(long) 548778;
    	vcprj.setMockup(true);
    	//vcprj.setVcProjectId("id");
    	vcprj.setVcProjectName("name");
    	vcprj.getIdeaId();
    	vcprj.getVcProjectId();
    	vcprj.getVcProjectName();
				
		JSONObject json = new JSONObject(String.format("{\"error\": true, \"message\": \"IdeaId is mandatory\"}"));
		NewScreenshot screenshot = new NewScreenshot();
		oia.isValidNewScreenshotRequest(ideaId, screenshot);
	}

  
	@Test
	public void testPublishScreenAction() throws Exception {
		OIA oia = Mockito.spy(new OIA());
		NewScreenshot s = new NewScreenshot();
		
		String ideaId = "154789";
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getOiapath() + "/publish-screen-shot";
		//+"/idea_id/"+ideaId.toString(); 
		//+"/encoded-screen-shot/"+s.getEncodedScreenShot()+"/-description/"+"/img-type/"+s.getImgMimeType()+"/username/"+s.getUsername();
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(oia).post(url, token, s);
		
		oia.publishScreenShotAction(ideaId, s, token);
		Mockito.verify(oia,Mockito.timeout(1)).post(url, token, s);
	
	}
	
	@Test
	public void testValiddProjectAssociation () throws Exception{
		OIA oia = Mockito.spy(new OIA());
	 	VCProjectAssociation vcprj = new VCProjectAssociation();
    
	//Long ideaId =(long) 548778;
    	//vcprj.setMockup(true);
    	//vcprj.setVcProjectId("id");
    	//vcprj.setVcProjectName("name");
    	vcprj.getIdeaId();
    	vcprj.getVcProjectId();
    	vcprj.getVcProjectName();
				
		JSONObject json = new JSONObject(String.format("{\"error\": true, \"message\": \"IdeaId is mandatory\"}"));
		//NewScreenshot screenshot = new NewScreenshot();
		oia.isValidVCProjectAssociation(vcprj);
	}
	
	@Test
	public void testProjectUpdateAction() throws Exception {
		OIA oia = Mockito.spy(new OIA());
		//NewScreenshot s = new NewScreenshot();
		VCProjectAssociation v = new VCProjectAssociation();
		v.setVcProjectId("25dj");
		v.setVcProjectName("name");
		v.setMockup(true);
		v.setIdeaId("fhdy23");
		
		//Long ideaId = (long) 154789;
		String url = Config.getInstance().getLiferayURL() + Config.getInstance().getOiapath() + "/vc-project-update"+"/vmeprojectid/"+v.getVcProjectId()+"/vmeprojectname/"+v.getVcProjectName()+"/ismockup/"+v.isMockup()+"/ideaid/"+v.getIdeaId();
		Response mockedResponse = Mockito.mock (Response.class);
		Mockito.doReturn(mockedResponse).when(oia).invoke(url, token);
		
		oia.vcProjectUpdateAction(v, token);
		Mockito.verify(oia,Mockito.timeout(1)).invoke(url, token);
	
	}
		
}
