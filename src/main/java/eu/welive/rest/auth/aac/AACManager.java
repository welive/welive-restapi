/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.aac;

import java.io.IOException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ResponseProcessingException;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.welive.rest.auth.aac.profiles.AccountProfile;
import eu.welive.rest.auth.aac.profiles.AccountProfileReader;
import eu.welive.rest.auth.aac.profiles.BasicProfile;
import eu.welive.rest.auth.aac.profiles.ExtendedProfile;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.UnexpectedErrorException;

public class AACManager {
	
	public enum AACRole { ROLE_CLIENT, ROLE_USER };
	
	private static final Logger logger = LogManager.getLogger(AACManager.class);
	
	private static final String BASIC_PROFILE_ME = "basicprofile/me";
	private static final String ACCOUNT_PROFILE_ME = "accountprofile/me";
	private static final String CHECK_TOKEN = "eauth/check_token";
	private static final String CLIENT_INFO = "resources/clientinfo";
	
	private static final String AUTHORIZATION_HEADER = "Authorization";
	
	private static Config config;
	
	static {
		try {
			config = Config.getInstance();
		} catch (IOException e) {
			logger.error("Problem loading config information.", e);
		}
	}

	public BasicProfile getBasicProfile(String token) throws UnexpectedErrorException {
		try {
			final Client client = ClientBuilder.newClient();
			final WebTarget target = client.target(config.getAacServiceURL());
			
			final BasicProfile basicProfile = target.path(BASIC_PROFILE_ME)
				.request(MediaType.APPLICATION_JSON)
				.header(AUTHORIZATION_HEADER, token)
				.get(BasicProfile.class);
			
			return basicProfile;
		} catch (ResponseProcessingException e) {
			throw new UnexpectedErrorException("Could not read basic profile from AAC", e); 
		}
	}
	
	public AccountProfile getAccountProfile(String token) throws UnexpectedErrorException {
		try {
			final Client client = ClientBuilder.newBuilder()
						.register(AccountProfileReader.class)
						.build();
			
			final WebTarget target = client.target(config.getAacServiceURL());
			
			final AccountProfile accountProfile = target.path(ACCOUNT_PROFILE_ME)
				.request(MediaType.APPLICATION_JSON)
				.header(AUTHORIZATION_HEADER, token)
				.get(AccountProfile.class);
								
			return accountProfile;
		} catch (ResponseProcessingException e) {
			throw new UnexpectedErrorException("Could not read account profile from AAC", e);
		}
	}
	
	public ExtendedProfile getExtendedProfile(String token) throws UnexpectedErrorException {
		final BasicProfile basicProfile = getBasicProfile(token);
		final AccountProfile accountProfile = getAccountProfile(token);
		return new ExtendedProfile(basicProfile, accountProfile);
	}
	
	public TokenInfo getTokenInfo(String token) throws UnexpectedErrorException {
		try {
			final Client client = ClientBuilder.newClient();
			final WebTarget target = client.target(config.getAacServiceURL());
			
			final TokenInfo tokenInfo = target.path(CHECK_TOKEN)
				.queryParam("token", token.split(" ")[1])
				.request(MediaType.APPLICATION_JSON)
				.get(TokenInfo.class);
			
			return tokenInfo;
		} catch (ResponseProcessingException e) {
			throw new UnexpectedErrorException("Could not read account profile from AAC", e);
		}
	}
	
	public ClientInfo getClientInfo(String token) throws UnexpectedErrorException {
		try {
			final Client client = ClientBuilder.newClient();
			final WebTarget target = client.target(config.getAacServiceURL());
			
			final ClientInfo clientInfo = target.path(CLIENT_INFO)
				.request(MediaType.APPLICATION_JSON)
				.header(AUTHORIZATION_HEADER, token)
				.get(ClientInfo.class);
			
			return clientInfo;
		} catch (ResponseProcessingException e) {
			throw new UnexpectedErrorException("Could not read account profile from AAC", e);
		}
	}
}
