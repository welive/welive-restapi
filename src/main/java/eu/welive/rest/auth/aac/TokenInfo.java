/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.aac;

import java.util.ArrayList;
import java.util.List;

import eu.welive.rest.auth.aac.AACManager.AACRole;

public class TokenInfo {

	private List<AACRole> authorities;
	
	public TokenInfo() {
		this.authorities = new ArrayList<AACRole>();
	}

	public List<AACRole> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<AACRole> authorities) {
		this.authorities = authorities;
	}
	
	public boolean isClientToken() {
		return authorities.contains(AACRole.ROLE_CLIENT);
	}
	
	public boolean isUserToken() {
		return authorities.contains(AACRole.ROLE_USER);
	}

}
