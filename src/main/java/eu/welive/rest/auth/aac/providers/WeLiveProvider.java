/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.aac.providers;

import javax.json.JsonObject;

public class WeLiveProvider extends Provider {

	public static final String ACCOUNT_TYPE = "welive";
	public static final String USERNAME = "username";
	public static final String EMAIL = "email";
	
	private String username;
	private String email;
	
	public WeLiveProvider() {
		this.username = "";
		this.email = "";
	}
	
	public WeLiveProvider(String username, String email) {
		this.username = username;
		this.email = email;
	}
	
	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public String getAccountType() {
		return ACCOUNT_TYPE;
	}

	@Override
	public void loadJSON(JsonObject json) {
		if (json.containsKey(USERNAME)) {
			username = json.getString(USERNAME);
			email = username;
		}
	}

}
