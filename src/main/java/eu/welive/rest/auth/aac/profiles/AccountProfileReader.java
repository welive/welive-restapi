/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.aac.profiles;

import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;

import eu.welive.rest.auth.aac.providers.FacebookProvider;
import eu.welive.rest.auth.aac.providers.GoogleProvider;
import eu.welive.rest.auth.aac.providers.WeLiveProvider;

@Provider
@Consumes(MediaType.APPLICATION_JSON)
public class AccountProfileReader implements MessageBodyReader<AccountProfile> {
	
	private static final String ACCOUNTS = "accounts";

	@Override
	public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
		return mediaType.isCompatible(MediaType.APPLICATION_JSON_TYPE)
			&& genericType == AccountProfile.class;
	}

	@Override
	public AccountProfile readFrom(Class<AccountProfile> type, Type genericType, Annotation[] annotations,
			MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream)
			throws IOException, WebApplicationException {
		
		final JsonObject json = Json.createReader(entityStream).readObject();
		return readJson(json);
	}

	public AccountProfile readJson(final JsonObject json) throws IOException {
		final AccountProfile accountProfile = new AccountProfile();
		
		if (json.containsKey(ACCOUNTS)) {			
			final JsonObject accounts = json.getJsonObject(ACCOUNTS);
			for (Object key : accounts.keySet()) {
				final String accountType = (String)key;
				final JsonObject account = accounts.getJsonObject(accountType);
				
				if (accountType.equals(GoogleProvider.ACCOUNT_TYPE)) {
					final GoogleProvider provider = new GoogleProvider();
					provider.loadJSON(account);
					accountProfile.addProvider(provider);
				}
				
				if (accountType.equals(WeLiveProvider.ACCOUNT_TYPE)) {
					final WeLiveProvider provider = new WeLiveProvider();
					provider.loadJSON(account);
					accountProfile.addProvider(provider);
				}
				
				if (accountType.equals(FacebookProvider.ACCOUNT_TYPE)) {
					final FacebookProvider provider = new FacebookProvider();
					provider.loadJSON(account);
					accountProfile.addProvider(provider);
				}
			}
			
			return accountProfile;
		}
		
		throw new IOException("Cannot read AccountProfile. 'accounts' property not found");
	}
	
}
