/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.token;

import javax.ws.rs.core.SecurityContext;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.welive.rest.auth.aac.AACManager;
import eu.welive.rest.auth.aac.ClientInfo;
import eu.welive.rest.auth.aac.TokenInfo;
import eu.welive.rest.auth.aac.profiles.BasicProfile;
import eu.welive.rest.auth.securitycontext.AnonSecurityContext;
import eu.welive.rest.auth.securitycontext.OAuthSecurityContext;
import eu.welive.rest.auth.user.oauth.OAuthClientInfo;
import eu.welive.rest.auth.user.oauth.OAuthUserInfo;
import eu.welive.rest.exception.WeLiveException;

public class TokenManager {
	
	private static final Logger logger = LogManager.getLogger(TokenManager.class);
	
	private final AACManager aacManager;
	
	public TokenManager() {
		aacManager = new AACManager();
	}
	
	protected TokenManager(AACManager aacManager) {
		this.aacManager = aacManager;
	}
	
	public SecurityContext validateToken(String token) {
		try {
			final TokenInfo tokenInfo = aacManager.getTokenInfo(token);
			if (tokenInfo.isUserToken()) {
				final BasicProfile basicProfile = aacManager.getBasicProfile(token);
				final String userId = basicProfile.getUserId();
				final OAuthUserInfo oauthInfo = new OAuthUserInfo(userId, token);
				return new OAuthSecurityContext(oauthInfo);
			} else if (tokenInfo.isClientToken()) {
				final ClientInfo clientInfo = aacManager.getClientInfo(token);
				final OAuthClientInfo oauthInfo = new OAuthClientInfo(clientInfo.getClientName(), token);
				return new OAuthSecurityContext(oauthInfo);
			}
			
			return new AnonSecurityContext();
		} catch (WeLiveException e) {
			logger.error("Problem obtaining information from AAC. ", e);
			return new AnonSecurityContext();
		} 
	}
}
