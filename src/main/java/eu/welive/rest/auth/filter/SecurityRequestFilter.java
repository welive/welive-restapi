/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.filter;

import java.io.IOException;
import java.lang.reflect.Method;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import eu.welive.rest.auth.annotation.SecurityFilterCheck;
import eu.welive.rest.auth.securitycontext.AnonSecurityContext;
import eu.welive.rest.auth.securitycontext.BasicSecurityContext;
import eu.welive.rest.auth.token.TokenManager;
import eu.welive.rest.exception.ErrorMessage;

@Provider
@SecurityFilterCheck
public class SecurityRequestFilter implements ContainerRequestFilter {

	public static final String AUTHORIZATION_HEADER = "Authorization";
	private static final String BEARER_PREFIX = "Bearer ";
	private static final String BASIC_PREFIX = "Basic ";
	
	private @Context ResourceInfo resourceInfo;
	
	public SecurityRequestFilter() {
		
	}
	
	public SecurityRequestFilter(ResourceInfo resourceInfo) {
		this.resourceInfo = resourceInfo;
	}
	
	@Override
	public void filter(final ContainerRequestContext requestContext) throws IOException {
		final String authHeader = requestContext.getHeaderString(AUTHORIZATION_HEADER);
		if (authHeader != null) {
			if (authHeader.toLowerCase().startsWith(BEARER_PREFIX.toLowerCase())) {
				final TokenManager tokenManager = new TokenManager();
				requestContext.setSecurityContext(tokenManager.validateToken(authHeader));
			} else if (authHeader.toLowerCase().startsWith(BASIC_PREFIX.toLowerCase())) {
				requestContext.setSecurityContext(new BasicSecurityContext(authHeader));
			} else {
				requestContext.setSecurityContext(new AnonSecurityContext());
			}
		} else {
			requestContext.setSecurityContext(new AnonSecurityContext());
		}
		
		final Method method = resourceInfo.getResourceMethod();
		if (method.isAnnotationPresent(RolesAllowed.class)) {
			final RolesAllowed rolesAllowed = method.getAnnotation(RolesAllowed.class);
			
			if (requestContext.getSecurityContext() != null) {
				boolean userInRole = false;
				for (String role : rolesAllowed.value()) {
					if (requestContext.getSecurityContext().isUserInRole(role)) {
						userInRole = true;
					}
				}
				
				if (!userInRole) {
					final ErrorMessage errorMessage = new ErrorMessage(Status.FORBIDDEN, "Method Forbidden", "Method Forbidden");
					final Response response = Response.status(Status.FORBIDDEN)
						.entity(errorMessage)
						.type(MediaType.APPLICATION_JSON)
						.build();
					requestContext.abortWith(response);
				}
			}
		}
	}

}
