/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.user;

import java.util.ArrayList;
import java.util.List;

import javax.management.remote.JMXPrincipal;

@SuppressWarnings("serial")
public abstract class UserInfo extends JMXPrincipal {
	
	private final List<String> userRoles;
	private final String token;
	
	public UserInfo() {
		super("");
		this.userRoles = new ArrayList<String>();
		this.token = "";
	}
	
	public UserInfo(String userId) {
		super(userId);
		this.userRoles = new ArrayList<String>();
		this.token = "";
	}
	
	public UserInfo(String userId, String token) {
		super(userId);
		this.userRoles = new ArrayList<String>();
		this.token = token;
	} 
	
	public List<String> getRoles() {
		return userRoles;
	}

	public void addRole(String role) {
		userRoles.add(role);
	}
	
	public String getAuthToken() {
		return token;
	}
}