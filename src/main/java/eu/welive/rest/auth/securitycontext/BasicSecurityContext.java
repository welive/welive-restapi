/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.securitycontext;

import java.nio.charset.StandardCharsets;
import java.security.Principal;

import javax.ws.rs.core.SecurityContext;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.auth.user.anon.AnonUserInfo;
import eu.welive.rest.auth.user.basic.BasicUserAdminInfo;
import eu.welive.rest.config.Config;

public class BasicSecurityContext implements SecurityContext {
	
	private static final Logger logger = LogManager.getLogger(BasicSecurityContext.class);

	private static final String BASIC_PREFIX = "[B|b]asic";
	private static final String TOKEN_SEPARATOR = ":";
	
	private UserInfo userInfo = new AnonUserInfo();

	public BasicSecurityContext(String auth) {
		try {
			auth = auth.replaceFirst(BASIC_PREFIX, "").trim();
			String decodedAuth = new String(DatatypeConverter.parseBase64Binary(auth), StandardCharsets.UTF_8);
			String user = decodedAuth.split(TOKEN_SEPARATOR)[0];
			String password = decodedAuth.split(TOKEN_SEPARATOR)[1];
			
			if (user.equals(Config.getInstance().getAdminUser())
					&& password.equals(Config.getInstance().getAdminPass())) {
				userInfo = new BasicUserAdminInfo();
			}
		} catch (Exception e) {
			logger.error("Problem validating user using Basic Auth", e);
		}
	}

	@Override
	public Principal getUserPrincipal() {
		return userInfo;
	}

	@Override
	public boolean isUserInRole(String s) {
		return userInfo.getRoles().contains(s);
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public String getAuthenticationScheme() {
		return BASIC_AUTH;
	}
}
