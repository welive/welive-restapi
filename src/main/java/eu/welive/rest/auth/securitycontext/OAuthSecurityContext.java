/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.auth.securitycontext;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import eu.welive.rest.auth.user.oauth.OAuthInfo;

public class OAuthSecurityContext implements SecurityContext {

	private OAuthInfo userInfo;

	public OAuthSecurityContext(OAuthInfo userInfo) {
		this.userInfo = userInfo;
	} 
	
	@Override
	public Principal getUserPrincipal() {
		return userInfo;
	} 

	@Override
	public boolean isUserInRole(final String role) {
		return userInfo.getRoles().contains(role);
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public String getAuthenticationScheme() {
		return BASIC_AUTH;
	}
}
