/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.security;

import java.util.Arrays;
import java.util.List;

import eu.iescities.server.querymapper.datasource.security.DefaultPermissions;
import eu.iescities.server.querymapper.datasource.security.Permissions.AccessType;

public class WeLiveDefaultPermissions extends DefaultPermissions {

	public WeLiveDefaultPermissions() {
		
	}

	@Override
	public List<AccessType> getDeleteDefaultPermission() {
		return Arrays.asList(AccessType.NONE);
	}

	@Override
	public List<AccessType> getInsertDefaultPermission() {
		return Arrays.asList(AccessType.NONE);
	}

	@Override
	public List<AccessType> getSelectDefaultPermission() {
		return Arrays.asList(AccessType.ALL);
	}

	@Override
	public List<AccessType> getUpdateDefaultPermission() {
		return Arrays.asList(AccessType.NONE);
	}	
}
