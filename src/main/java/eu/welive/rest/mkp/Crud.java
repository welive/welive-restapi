/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


/**
 * http://openrdf.callimachus.net/sesame/2.7/docs/users.docbook?view#Parsing_and_Writing_RDF_with_Rio
 * 
 */
package eu.welive.rest.mkp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

import org.openrdf.model.Resource;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.repository.http.HTTPRepository;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.BasicParserSettings;
import org.openrdf.rio.rdfxml.RDFXMLWriter;

public class Crud {

	//private static Logger _log = Logger.getLogger(Crud.class.getName());
	public static String sesameServerURI = "http://dev.welive.eu/openrdf-sesame/repositories/";
	public static String repositoryID = "WeLiveJavaStore1";
	public Repository repo;
	
	/**
	 * @throws RepositoryException 
	 * @throws IOException 
	 */
	public Repository getRepository( ) throws Exception  {
		return getRepository(sesameServerURI+repositoryID);
	}

	/**
	 * Apre e chiude una connessione con il repository per verificare se il repository � raggiungibile.
	 * @return true se il repository � raggiungibile, altrimenti false. 
	 */ 
	public boolean isRepositoryOn(){
		
		boolean opened=false;
		boolean closed=false;
		
		RepositoryConnection con = null;
		try { 
			 con = getRepository().getConnection();
			 opened=true;
		} 
		catch (Exception e) { e.printStackTrace(); }
		finally { 
			if(con!=null){
				try{ 
					con.close();
					closed = true;
				}
				catch(Exception e){ e.printStackTrace(); }
			}
		}
		
		return opened && closed;
		
	}
	
	/**
	 * @param repo url del repository (es. http://localhost:8080/openrdf-sesame/repositories/Repo1
	 * @throws Exception 
	 */
	public Repository getRepository(String repoURL) throws Exception{
		
		if (this.repo==null){
			this.repo = new HTTPRepository(repoURL);
			repo.initialize();
		}
		
		return  repo;
	}

	public static String getRepositoryURL() {
		return sesameServerURI+repositoryID;
	} 
	
	public ByteArrayOutputStream exportRDFOutputStream(String resourceName) throws Exception{
		
		RepositoryConnection con = getRepository().getConnection();
		Resource resource=new URIImpl(resourceName);
		
		if (!resourceName.contains("file://")){ 
			resourceName=("file://")+resourceName;
		}
		
		ByteArrayOutputStream output = new ByteArrayOutputStream(); 
		     
			//il seguente setting disabilita il check della validit� dei campi.
			//soluzione al problema:Caused by: org.openrdf.rio.RDFParseException:  "was not recognised, and could not be verified, with datatype" 
			con.getParserConfig().set(BasicParserSettings.VERIFY_DATATYPE_VALUES, false);
			
			//UTF-8
			Charset	charset = Charset.forName("UTF-8"); //$NON-NLS-1$
			CharsetEncoder encoder = charset.newEncoder();
			RDFXMLWriter writerUTF = (RDFXMLWriter) Rio.createWriter(RDFFormat.RDFXML, new OutputStreamWriter(output, encoder));
			
			con.export(writerUTF, resource);
			
			output.close();
			con.close();
		
		return output;
	}

}
