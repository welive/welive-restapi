/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PublicServiceApplication extends Artefact
{ 
	private String url;
	private String type;
	
	@JsonProperty("uses")
	private List<Dependency> usedBBs;

	public PublicServiceApplication() {
		super();
		this.url = "";
		this.type = "";
		this.usedBBs = new ArrayList<Dependency>();
	}

	public PublicServiceApplication(String title, String description,
			String abstractDescription, String pilot, String created,
			String page, String language, List<String> tags, List<Entity> businessRoles,
			List<License> legalConditions,
			List<ServiceOffering> serviceOfferings,
			String url, String type, List<Dependency> usedBBs) {
		super(title, description, abstractDescription, pilot, created, page, language, tags,
				businessRoles, legalConditions, serviceOfferings);

		this.url = url;
		this.type = type;
		this.usedBBs = usedBBs;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Dependency> getUsedBBs() {
		return usedBBs;
	}

	public void setUsedBBs(List<Dependency> usedBBs) {
		this.usedBBs = usedBBs;
	}
}
