/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONObject;

import com.hp.hpl.jena.rdf.model.Resource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import eu.welive.rest.config.Config;
import eu.welive.rest.lum.Language;
import eu.welive.rest.mkp.exception.MissingRequiredResourceException;
import eu.welive.rest.mkp.exception.RDFValidationWarning;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Created by asirchia (antonino.sirchia at eng dot it) on 29/01/16.
 */

@Path("mkp/")
@Api(value = "/mkp")
@Produces({MediaType.APPLICATION_JSON})
public class MKP {
	
	public static enum AuthType {Basic, Oauth};
	private static String baseurl;
	
	
	
	public AuthType authType(String auth){
		
		if(auth.startsWith("Basic ")) return AuthType.Basic;
		else if(auth.startsWith("Bearer ")) return AuthType.Oauth;
		else return null;
	}
	
	
	
	public Response invoke(String url, String token){
		
		Client client = Client.create();
		WebResource webResource;
		
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}
	
	public Response post(String url, String token, Object data){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}

	public Response delete(String url, String token) {
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).delete(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}
	
	public MKP() throws Exception {
		baseurl = Config.getInstance().getLiferayURL() + Config.getInstance().getMkppath();
    }
	
	private static String myURLEncode(String s, boolean base64Encoded) throws UnsupportedEncodingException{
		if(base64Encoded){ 
			String step1 = new String(Base64.encode(s.getBytes())); 
			return step1.replaceAll("/", "--fs--");
		}
		else{ 
			String step1 = s.replaceAll("/", "--fs--"); 
			return URLEncoder.encode(step1, "UTF-8"); 
		}
	}
	
	
	@GET
	@Path("/get-all-artefacts/pilot-id/{pilot-id}/artefact-types/{artefact-types}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get all building blocks, datasets and apps published in the Marketplace filtered by pilot and/or by type", 
					notes = "Multiple types can be provided with comma seperated strings",
					response = ArtefactResponse.class)
	public Response getAllArtefacts(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
									@ApiParam(value = "The unique id of the pilot") @PathParam("pilot-id") eu.welive.rest.mkp.Pilot pilotId,
			   						@ApiParam(value = "The types of artefacts you are looking for") @PathParam("artefact-types") ArtefactType artefactTypes) throws UnsupportedEncodingException {
		
		
			String url = baseurl+"/get-all-artefacts/pilot-id/"+URLEncoder.encode(pilotId.toString(), "UTF-8")+"/artefact-types/"+URLEncoder.encode(artefactTypes.toString(), "UTF-8");
			return invoke(url,token);	
	}

	
	@GET
	@Path("/get-all-artefacts/pilot-id/{pilot-id}/artefact-types/{artefact-types}/level/{level}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get all building blocks, datasets and apps published in the Marketplace filtered by pilot and/or by type", 
					notes = "Multiple types can be provided with comma seperated strings",
					response = ArtefactResponse.class)
	public Response getAllArtefacts(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
									@ApiParam(value = "The unique id of the pilot") @PathParam("pilot-id") eu.welive.rest.mkp.Pilot pilotId,
			   						@ApiParam(value = "The types of artefacts you are looking for") @PathParam("artefact-types") ArtefactType artefactTypes, 
		                            @ApiParam(value = "The level of the artefacts you are looking for") @PathParam ("level") Integer level) throws UnsupportedEncodingException {
		
		
			String url = baseurl+"/get-all-artefacts/pilot-id/"+URLEncoder.encode(pilotId.toString(), "UTF-8")+"/artefact-types/"+URLEncoder.encode(artefactTypes.toString(), "UTF-8") + "/level/" + URLEncoder.encode(level.toString(), "UTF-8");
			return invoke(url,token);	
	}
	
public JSONObject isValidNewDatasetRequest(NewDataset dataset){
		
		JSONObject json = new JSONObject();
		json.put("error", true);
		
		if(dataset.getDatasetId()==null || dataset.getDatasetId().equals("")){
			json.put("message", "datasetId is mandatory");
			return json;
		}
		if(dataset.getCcUserId()==null || dataset.getCcUserId() <= 0){
			json.put("message", "ccUserId is mandatory");
			return json;
		}
		if(dataset.getResourceRdf()==null || dataset.getResourceRdf().equals("")){
			json.put("message", "resourceRdf is mandatory");
			return json;
		}
		if(dataset.getTitle()==null || dataset.getTitle().equals("")){
			json.put("message", "title is mandatory");
			return json;				
		}
		
		json.put("error", false);
		return json;
	}
	
	public Response newDatasetAction(NewDataset dataset, String token) 
			throws UnsupportedEncodingException{
		
		String url = "/create-dataset"
				+"/dataset-id/"+MKP.myURLEncode(dataset.getDatasetId(),false)
				+"/title/"+MKP.myURLEncode(dataset.getTitle(), false);
		
		String description = dataset.getDescription();
		if(description!=null  && !description.equals("")) 
			url+="/description/"+MKP.myURLEncode(description, false);
		else
			url+="/-description";
		
		url += "/resource-rdf/"+MKP.myURLEncode(dataset.getResourceRdf(), true);
		
		String webpage = dataset.getWebpage();
		if(webpage!=null && !webpage.equals("")){
			url+="/webpage/"+MKP.myURLEncode(webpage, true);
		}
		else url+="/-webpage";
		
		url+="/cc-user-id/"+MKP.myURLEncode(dataset.getCcUserId().toString(), false);
		
		String providername = dataset.getProviderName();
		if(providername!=null && !providername.equals("")) url+="/provider-name/"+MKP.myURLEncode(providername, false);
		else url+="/-provider-name";
		
		Language lang = dataset.getLanguage();
		if(lang!=null) url+="/language/"+MKP.myURLEncode(lang.toString(),false);
		else url+="/-language";
		
		String path = baseurl + url;
		
		return post(path,token, null);
	}


@GET
	@Path("/get-artefacts-by-keywords/keywords/{keywords}/artefact-types/{artefact-types}/pilot/{pilot-id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get artefacts published in the Marketplace which contains a set of specific keywords",
				notes = "Multiple keywords can be provided with comma(,) seperated strings",
				response = ArtefactResponse.class)
	public Response getArtefactsByKeywords( @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
											@ApiParam(value = "Keywords you are looking for") @PathParam("keywords") String keywords,
											@ApiParam(value = "The unique id of the pilot") @PathParam("pilot-id") eu.welive.rest.mkp.Pilot pilotId,
											@ApiParam(value = "The types of artefacts you are looking for") @PathParam("artefact-types") ArtefactType artefactTypes) throws UnsupportedEncodingException {
		
			String url = baseurl+"/get-artefacts-by-keywords/keywords/"+ keywords +"/artefact-types/"+ artefactTypes.toString() +  "/pilot/" + pilotId.toString();
			return invoke(url,token);
	}

	@GET
	@Path("/get-artefacts-by-keywords/keywords/{keywords}/artefact-types/{artefact-types}/pilot/{pilot-id}/level/{level}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get artefacts published in the Marketplace which contains a set of specific keywords",
				notes = "Multiple keywords can be provided with comma(,) seperated strings",
				response = ArtefactResponse.class)
	public Response getArtefactsByKeywords( @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
											@ApiParam(value = "Keywords you are looking for") @PathParam("keywords") String keywords,
											@ApiParam(value = "The unique id of the pilot") @PathParam("pilot-id") eu.welive.rest.mkp.Pilot pilotId,
											@ApiParam(value = "The types of artefacts you are looking for") @PathParam("artefact-types") ArtefactType artefactTypes, 
											@ApiParam(value = "The level of artefacts you are looking for") @PathParam("level") Integer level) throws UnsupportedEncodingException {
		
			String url = baseurl+"/get-artefacts-by-keywords/keywords/"+ keywords +"/artefact-types/"+ artefactTypes.toString() +  "/pilot/" + pilotId.toString() + "/level" + URLEncoder.encode(level.toString(), "UTF-8");
			System.out.print("\nurl dell'API" + url);
			return invoke(url,token);
	}
	
	@POST
	@Path("/create-dataset")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Publishes a dataset into the Welive Marketplace showcase", response = ArtefactCreationResponse.class)
	public Response createDataset(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								  @ApiParam(value = "Dataset information") NewDataset dataset) throws UnsupportedEncodingException {

		JSONObject json = isValidNewDatasetRequest(dataset);
		if(json.getBoolean("error")){
			return Response.status(500).entity(json.toString()).build();
		}
		
	
		switch(authType(token)){
			case Basic:
				return newDatasetAction(dataset, token);
			case Oauth:
			default:
				json.put("error", true);
				json.put("message", "Basic authentication required");
				return Response.status(500).entity(json.toString()).build();
		}
		
	}
	
	public JSONObject isValidNewServiceRequest(NewService service){
		
		JSONObject json = new JSONObject();
		json.put("error", true);
		
		if(service.getCcUserId()==null || service.getCcUserId().equals("")){
			json.put("message", "ccUid is mandatory");
			return json;
		}
		if(service.getResourceRdf()==null || service.getResourceRdf().equals("")){
			json.put("message", "resourceRdf is mandatory");
			return json;
		}
		if(service.getTitle()==null || service.getTitle().equals("")){
			json.put("message", "title is mandatory");
			return json;		
		}
		if(service.getCategory()==null || service.getCategory().equals("")){
			json.put("message", "category is mandatory");
			return json;
		}
		if(service.getEndpoint()==null || service.getEndpoint().equals("")){
			json.put("message", "endpoint is mandatory");
			return json;
		}
		
		json.put("error", false);
		return json;
	}

	
	@POST
	@Path("/create-service")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Publishes a service into the Welive Marketplace showcase", response = ArtefactCreationResponse.class)
	public Response createService(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
									@ApiParam(value = "Service information") NewService service) throws UnsupportedEncodingException {
		
		
		JSONObject json = isValidNewServiceRequest(service);
		if(json.getBoolean("error")){
			return Response.status(500).entity(json.toString()).build();
		}
		
		switch(authType(token)){
			case Basic:
				return newServiceAction(service, token);
			case Oauth:
			default:
				json.put("error", true);
				json.put("message", "Basic authentication required");
				return Response.status(500).entity(json.toString()).build();
		}
		
	}

	
	public Response newServiceAction(NewService service, String token) 
			throws UnsupportedEncodingException{
		
		String url = "/create-service"
				+"/title/"+MKP.myURLEncode(service.getTitle(),false);
		
		String description = service.getDescription();
		if(description==null || description.equals("")) url+="/-description/";
		else url+="/description/"+MKP.myURLEncode(description, false);
		
		url+="/category/"+MKP.myURLEncode(service.getCategory().toString(), false);
		
		url+= "/resource-rdf/"+MKP.myURLEncode(service.getResourceRdf(), true);
		
		url+="/endpoint/"+MKP.myURLEncode(service.getEndpoint(), true);
		
		String webpage = service.getWebpage();
		if(webpage==null || webpage.equals("")) url+="/-webpage/";
		else url+="/webpage/"+MKP.myURLEncode(webpage, true);;
		
		url+="/cc-user-id/"+MKP.myURLEncode(service.getCcUserId().toString(), false);
		
		String providername = service.getProviderName();
		if(providername==null || providername.equals("")) url+="/-provider-name/";
		else url+="/provider-name/"+MKP.myURLEncode(providername, false);
		
		Language lang = service.getLanguage();
		if(lang!=null) url+="/language/"+MKP.myURLEncode(lang.toString(),false);
		else url+="/-language";
		
		String path = baseurl + url;
		
		return post(path, token, null);
	}

	@GET
	@Path("/get-artefact/artefactid/{artefact-id}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a single artefact by its unique identifier", response = ArtefactResponse.class)
	public Response getArtefact(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "The unique id of the artefact you are looking for") @PathParam("artefact-id") Long artefactid) throws UnsupportedEncodingException {
		
			String url = baseurl+"/get-artefact/artefactid/"+URLEncoder.encode(artefactid.toString(), "UTF-8");
			return invoke(url,token);
	}
	
	public boolean isValidValidationRequest(ValidationRequest request){
		return request==null || request.getResourcerdf()==null || request.getResourcerdf().equals("");
	}
	
	public Response validateRDFResourceAction(ValidationRequest request) throws Exception{
		
		ValidationResponse resp = new ValidationResponse();
		
		String resourcerdf = request.getResourcerdf();
		java.io.ByteArrayOutputStream os = null;
		
		Pattern sesameresource = Pattern.compile("^file://.+");
		Matcher sesameMatcher = sesameresource.matcher(resourcerdf);

		Pattern urlresource = Pattern.compile("^https?://.+");
		Matcher urlMatcher = urlresource.matcher(resourcerdf);
		
		if(sesameMatcher.matches()){
			Crud c = new Crud();
			os = c.exportRDFOutputStream(resourcerdf); 
			
		}
		else if(urlMatcher.matches()){
			
				URL url = new URL(resourcerdf);
				URLConnection urlConnection = url.openConnection();
				os = new ByteArrayOutputStream();
				IOUtils.copy(urlConnection.getInputStream(), os);
			
			
		}

		RDFValidationWarning warning = null;
		
			ReaderRDF reader = new ReaderRDF(os);
			Resource artefact = SesameValidationUtils.getArtefact(reader);
			if(artefact==null)
				throw new MissingRequiredResourceException("The required resource WeLiveArtefact is missing");
			
			SesameValidationUtils.checkArtefactMandatoryProperties(artefact, reader);
			
			try{ SesameValidationUtils.checkArtefactRecommendedProperties(artefact, reader); }
			catch(RDFValidationWarning w){ warning = w; }
			
			try{ SesameValidationUtils.checkBusinessRoles(reader); }
			catch(RDFValidationWarning w){ warning = w; }
			
			try{ SesameValidationUtils.checkInteractionPoints(reader); }
			catch(RDFValidationWarning w){ warning = w; }

			resp.setIsValid(true);
			if(warning!=null){
				resp.setCode(warning.getCode().getCode());
				resp.setType(warning.getClass().getSimpleName());
				resp.setMessage(warning.getMessage());
			}
			else resp.setCode(SesameValidationCode.OK.getCode());
			
		
		
		return Response.status(resp.getCode()).entity(resp).build();
	}

	
	@GET
	@Path("/get-artefacts-by-ids/ids/{ids-list}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get a list of artefacts by their unique identifiers", 
				notes = "Multiple ids can be provided with comma seperated strings",
				response = ArtefactResponse.class)
	public Response getArtefactsByIds(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
									  @ApiParam(value = "The ids you are looking for") @PathParam("ids-list") String ids) throws UnsupportedEncodingException {
		
			String url = baseurl+"/get-artefacts-by-ids/ids/"+URLEncoder.encode(ids.toString(), "UTF-8");
			return invoke(url,token);
	}
	
	@POST
	@Path("/validate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Receive in input the url or the sesame context identifier of the RDF resource to be validated", 
				response = ValidationResponse.class)
	public Response validateRDFResource(
			@ApiParam(value = "The url or the sesame context identifier of the RDF resource to be validated") ValidationRequest request) throws Exception {
		
ValidationResponse resp = new ValidationResponse();
		
		if(isValidValidationRequest(request)){
			resp.setCode(SesameValidationCode.INVALID_INPUT.getCode());
			resp.setIsValid(false);
			resp.setMessage("The input rdf resource is invalid");
			
			return Response.status(resp.getCode()).entity(resp).build();
		}
		
		return validateRDFResourceAction(request);
		
	}

	
	@POST
	@Path("/validateService")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(response = ValidationServiceResponse.class, value = "Validates WADL and/or WSDL to ensure service compliance with WeLive Platform")
	public Response validateService(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
			                        @ApiParam (value= "The url and the type of the service to be validate")   ValidationServiceRequest request) {
	
			String url = baseurl+"/validateService";
			return post(url,token, request);
	}
	
	@POST
	@Path("/update-dataset")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Receive in input a json descriptor of the dataset. The dataset is fully overwritten with the provided data.",
					response = ConfirmResponse.class)
	public Response updateDataset( @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
			                       @ApiParam(value = "Confirmation response message") UpdateDatasetRequest body) {
		
			String url = baseurl+"/update-dataset";
			return post(url,token, body);

	}
	
	@POST
	@Path("/setup-artefact")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Receive in input a json descriptor of the artefact in order to publish it into the Marketplace.",
					response = SetupResponse.class)
	public Response setupArtefact( @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
			                       @ApiParam(value = "Response message") SetupArtefactRequest body) {
		
			String url = baseurl+"/setup-artefact";
			return post(url,token, body);

	}
	
}

