/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import java.io.IOException;
import java.util.List;
 

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;

public abstract class AuthorizationUtils {
	
	private static enum AuthType {Basic, Oauth}
	
	private static boolean isValidToken(String authHeader, String scope) 
			throws WeLiveException{
		
		String aacURL;
		try { 
		 aacURL = Config.getInstance().getApiURL() + "/aac";
		}
		catch (IOException e) {
		       throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error reading welive.properties", e);
		}
		
		String aacValidatorUrl = aacURL+"/resources/access?scope="+scope;
		
		Client client = Client.create(); 
        Builder webResource = client.resource(aacValidatorUrl).header("Authorization", authHeader);
        
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        try{
	        String resp = clientResponse.getEntity(String.class);
	        JSONObject json = new JSONObject(resp);
	        return json.getBoolean("error");
        }
        catch(Exception e){ 
        	return false;
        }
		
	}
	
	private static AuthType authType(String auth){
		
		if(auth.startsWith("Basic ")) return AuthType.Basic;
		else if(auth.startsWith("Bearer ")) return AuthType.Oauth;
		else return null;
	}
	
	private static boolean checkAuth(HttpHeaders headers) throws WeLiveException{
		
		List<String> auths = headers.getRequestHeaders().get("authorization");
		if(auths==null || auths.isEmpty()){
			return false;
		}

		String auth = auths.get(0).trim();
		
		AuthType authType = authType(auth);
		if(authType.equals(AuthType.Basic))
			return true;
		else if(authType.equals(AuthType.Oauth))
			return isValidToken(auth, "");
		else
			return false;
		
	}
	
	private static String invoke(String url, String token){
		
		Client client = Client.create();
		WebResource webResource;
		
		AuthType authtype = authType(token);
		switch(authtype){
			case Basic:
				String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
				String[] credentials = decoded.split(":");
				client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
				webResource = client.resource(url);
				break;
			case Oauth:
				webResource = client.resource(url);
				webResource.header("Authorization", token);
				break;
			default:
				webResource = client.resource(url);
				break;
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return resp;
	}
	
	private static String post(String url, String token, Object data){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		switch(authtype){
			case Basic:
				String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
				String[] credentials = decoded.split(":");
				client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
				webResource = client.resource(url);
				break;
			case Oauth:
				webResource = client.resource(url);
				webResource.header("Authorization", token);
				break;
			default:
				webResource = client.resource(url);
				break;
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return resp;
	}

	private static Object delete(String url, String token) {
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		switch(authtype){
			case Basic:
				String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
				String[] credentials = decoded.split(":");
				client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
				webResource = client.resource(url);
				break;
			case Oauth:
				webResource = client.resource(url);
				webResource.header("Authorization", token);
				break;
			default:
				webResource = client.resource(url);
				break;
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).delete(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return resp;
	}
	
}
