/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import javax.xml.bind.annotation.XmlElement;

public class UpdateDatasetRequest {
	
	@XmlElement(required=true)
	private String datasetid;
	@XmlElement(required=true)
	private Boolean hasmapping;
	@XmlElement(required=false)
	private Dataset model;
	@XmlElement(required=false)
	private Boolean isPrivate;
	
	public String getDatasetid() {
		return datasetid;
	}
	public void setDatasetid(String datasetid) {
		this.datasetid = datasetid;
	} 
	public Boolean getHasmapping() {
		return hasmapping;
	}
	public void setHasmapping(Boolean hasmapping) {
		this.hasmapping = hasmapping;
	}
	public Dataset getModel() {
		return model;
	}
	public void setModel(Dataset model) {
		this.model = model;
	}
	public Boolean getIsPrivate() {
		return isPrivate;
	}
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}
	
}
