/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
 
@JsonIgnoreProperties(ignoreUnknown = true)
public class Entity 
{
	private String title;
	private String page;
	private String mbox;
	private String businessRole;
	
	public Entity() {
		super();		
		this.title = "";
		this.page = "";
		this.mbox = "";
		this.businessRole = "";
	}
	
	public Entity(String title, String page, String mbox, String businessRole) {
		super();
		this.title = title;
		this.page = page;
		this.mbox = mbox;
		this.businessRole = businessRole;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getPage() {
		return page;
	}
	public void setPage(String page) {
		this.page = page;
	}
	public String getMbox() {
		return mbox;
	}
	public void setMbox(String mbox) {
		this.mbox = mbox;
	}
	public String getBusinessRole() {
		return businessRole;
	}
	public void setBusinessRole(String businessRole) {
		this.businessRole = businessRole;
	}
	
	
}
