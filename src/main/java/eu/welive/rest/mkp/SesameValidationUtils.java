/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import java.util.HashSet;
import java.util.Set;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

import eu.welive.rest.mkp.exception.MissingRecommendedPropertyException;
import eu.welive.rest.mkp.exception.MissingRecommendedResourceException;
import eu.welive.rest.mkp.exception.MissingRequiredPropertyException;
import eu.welive.rest.mkp.exception.MissingRequiredResourceException;
import eu.welive.rest.mkp.exception.RDFValidationException;
import eu.welive.rest.mkp.exception.ResourceUniquenessViolationException;

public abstract class SesameValidationUtils {
	
	private static final String mainResourceName =  "Artifact";
	private static final String mainResourceNameBB =  "BuildingBlock";
	private static final String mainResourceNameDS =  "Dataset";
	private static final String mainResourceNamePSA =  "PublicServiceApplication";
	
	private static final String mainResourceNamespace =  Namespace.WELIVE_CORE;

	public static Resource getArtefact(ReaderRDF reader) throws MissingRequiredResourceException, ResourceUniquenessViolationException{
		
		StmtIterator stmtIt = reader.getResourcesOfType(mainResourceNamespace+mainResourceName);
		StmtIterator stmtItBB = reader.getResourcesOfType(mainResourceNamespace+mainResourceNameBB);
		StmtIterator stmtItPSA = reader.getResourcesOfType(mainResourceNamespace+mainResourceNamePSA);
		StmtIterator stmtItDS = reader.getResourcesOfType(mainResourceNamespace+mainResourceNameDS);
		int nServices = 0;
		Resource artefact = null;
		Set<Statement> services = new HashSet<Statement>();
		if(stmtIt!=null){
			services.addAll(stmtIt.toSet());
		}
		
		if(stmtItBB!=null){
			services.addAll(stmtItBB.toSet());
		}
		
		if(stmtItPSA!=null){
			services.addAll(stmtItPSA.toSet());
		}
		
		if(stmtItDS!=null){
			services.addAll(stmtItDS.toSet());
		}
		
		if(!services.isEmpty()){
			nServices = services.size();
			artefact = services.iterator().next().getSubject();
		}
		
		if(nServices <= 0)
			throw new MissingRequiredResourceException("The required resource of type "+mainResourceNamespace+mainResourceName+" is missing");
		
		if(nServices > 1)
			throw new ResourceUniquenessViolationException("The resource of type "+mainResourceNamespace+mainResourceName+" must be unique");
		
		return artefact;
	}
	
	public static void checkArtefactMandatoryProperties(Resource artefact, ReaderRDF reader) throws MissingRequiredPropertyException{
		
		Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
		Property propertyAbstract = reader.getModel().createProperty(Namespace.DCTERMS + "abstract");
		Property propertyDescription = reader.getModel().createProperty(Namespace.DCTERMS + "description");
		Property propertyCreated = reader.getModel().createProperty(Namespace.DCTERMS + "created");
		Property propertyType = reader.getModel().createProperty(Namespace.DCTERMS + "type");
		Property propertyPilot = reader.getModel().createProperty(Namespace.WELIVE_CORE + "pilot");
		
		reader.getMandatoryPropertyValue(artefact, propertyTitle);
		
		boolean missingabs = false;
		
		try{ reader.getMandatoryPropertyValue(artefact, propertyAbstract); }
		catch(MissingRequiredPropertyException e){ missingabs = true; }
		
		try{ reader.getMandatoryPropertyValue(artefact, propertyDescription); }
		catch(MissingRequiredPropertyException e){ if(missingabs) throw new MissingRequiredPropertyException("At least one of "+propertyAbstract+" and "+propertyDescription+" must be present"); }

		reader.getMandatoryPropertyValue(artefact, propertyCreated);
		reader.getMandatoryPropertyValue(artefact, propertyType);
		reader.getMandatoryPropertyValue(artefact, propertyPilot);
		
		return;
	}
	
	public static void checkArtefactRecommendedProperties(Resource artefact, ReaderRDF reader) throws MissingRecommendedPropertyException{
		Property propertyTag = reader.getModel().createProperty(Namespace.TAGS + "tag");
		reader.getRecommendedMultiplePropertyValues(artefact, propertyTag);
	}
	
	public static void checkBusinessRoles(ReaderRDF reader) throws RDFValidationException{
		
		boolean hasAuthor = false;
		boolean hasProvider = false;
		RDFValidationException e = null;
		
		StmtIterator itauthor = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Author");
		if(itauthor!=null && itauthor.hasNext()){
			Resource author = itauthor.next().getSubject();
			if(author != null){
				hasAuthor = true;
				Property propertyName = reader.getModel().createProperty(Namespace.DCTERMS + "title");
				Property propertyEmail = reader.getModel().createProperty(Namespace.FOAF + "mbox");
				try{ 
					reader.getMandatoryPropertyValue(author, propertyName);
					reader.getRecommendedPropertyValue(author, propertyEmail);
				}
				catch(MissingRequiredPropertyException | MissingRecommendedPropertyException mrpe){ e = mrpe; }
			}
		}
		if(!hasAuthor){
			e = new MissingRecommendedResourceException("The recommended resource "+Namespace.WELIVE_CORE + "Author"+" is missing");
		}
		
		StmtIterator itprovider = reader.getResourcesOfType(Namespace.WELIVE_CORE + "Provider");
		if(itprovider!=null && itprovider.hasNext()){
			Resource provider = itprovider.next().getSubject();
			if(provider != null){
				hasProvider = true;
				Property propertyName = reader.getModel().createProperty(Namespace.DCTERMS + "title");
				Property propertyPage = reader.getModel().createProperty(Namespace.FOAF + "page");
				try{ 
					reader.getMandatoryPropertyValue(provider, propertyName);
					reader.getRecommendedPropertyValue(provider, propertyPage);
				}
				catch(MissingRequiredPropertyException | MissingRecommendedPropertyException mrpe){
					e = mrpe;
				}
			}
		}
		if(!hasProvider){
			e = new MissingRecommendedResourceException("The recommended resource "+Namespace.WELIVE_CORE + "Provider"+" is missing");
		}
	
		if(!hasAuthor && !hasProvider){
			throw new MissingRequiredResourceException("No resource of type "+Namespace.WELIVE_CORE + "Provider nor "+Namespace.WELIVE_CORE + "Author is specified");
		}
		else if(e!=null){
			throw e;
		}
		
	}

	public static void checkInteractionPoints(ReaderRDF reader) throws MissingRequiredResourceException, MissingRequiredPropertyException, MissingRecommendedPropertyException{
		StmtIterator itrest = reader.getResourcesOfType(Namespace.WELIVE_CORE+"RESTWebServiceIP");
		StmtIterator itsoap = reader.getResourcesOfType(Namespace.WELIVE_CORE+"SOAPWebServiceIP");
		StmtIterator it = reader.getResourcesOfType(Namespace.WELIVE_CORE+"InteractionPoint");
		
		if((itrest==null || !itrest.hasNext()) && (itsoap==null || !itsoap.hasNext()) && (it==null || !it.hasNext())){
			throw new MissingRequiredResourceException("No InteractionPoint has been provided");
		}
		
		Set<Statement> ip = itrest.toSet();
		ip.addAll(itsoap.toSet());
		ip.addAll(it.toSet());
		
		Property propertyTitle = reader.getModel().createProperty(Namespace.DCTERMS + "title");
		Property propertyInterfaceOperation = reader.getModel().createProperty(Namespace.WELIVE_CORE + "wadl");
		Property propertySoapInterfaceOperation = reader.getModel().createProperty(Namespace.WELIVE_CORE + "wsdl");
		
		MissingRequiredPropertyException mrpe = null;
		MissingRecommendedPropertyException mrecpe = null;
		
		for(Statement stmt : ip){
			boolean hasEndpoint = false;
			Resource res = stmt.getSubject();
			try{ 
				reader.getRecommendedPropertyValue(res, propertyInterfaceOperation);
				hasEndpoint = true;
			}
			catch(Exception e){ /*Do nothing */ }
			try{ 
				reader.getRecommendedPropertyValue(res, propertySoapInterfaceOperation);
				hasEndpoint = true;
			}
			catch(Exception e){ /*Do nothing */ }
			
			if(!hasEndpoint){
				mrpe = new MissingRequiredPropertyException("No WADL nor WSDL has been specified for Interaction Point "+res.getLocalName());
			}
			
			try{ reader.getRecommendedPropertyValue(res, propertyTitle); }
			catch(MissingRecommendedPropertyException e){ mrecpe = e; }
			
		}
		
		if(mrpe!=null)
			throw mrpe;
		else if(mrecpe!=null)
			throw mrecpe;
		
		return;
	}
}
