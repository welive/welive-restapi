/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
 
@JsonIgnoreProperties(ignoreUnknown = true)
public class Operation 
{
	private String title;
	private String description;
	private String endpoint;
	private String method;
	private String produces;
	private String consumes;
	
	@JsonProperty("hasInput")
	private List<Parameter> parameters;
	@JsonProperty("hasOutput")
	private List<Response> responses;
	@JsonProperty("hasSecurityMeasure")
	private AuthenticationMeasure securityMeasure;
	
	public Operation() {
		super();
		this.title = "";
		this.description = "";
		//this.path = "";
		this.endpoint = "";
		this.method = "";
		this.produces = "";
		this.consumes = "";
		this.parameters = new ArrayList<Parameter>();
		this.responses = new ArrayList<Response>();
	}
	
	public Operation(String title, String description, String endpoint,
			String method, String produces, String consumes,
			List<Parameter> parameters, List<Response> responses,
			AuthenticationMeasure securityMeasure) {
		super();
		this.title = title;
		this.description = description;
		//this.path = path;
		this.endpoint = endpoint;
		this.method = method;
		this.produces = produces;
		this.consumes = consumes;
		this.parameters = parameters;
		this.responses = responses;
		this.securityMeasure = securityMeasure;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	/*
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	*/

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getProduces() {
		return produces;
	}

	public void setProduces(String produces) {
		this.produces = produces;
	}

	public String getConsumes() {
		return consumes;
	}

	public void setConsumes(String consumes) {
		this.consumes = consumes;
	}

	public List<Parameter> getParameters() {
		return parameters;
	}

	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}

	public List<Response> getResponses() {
		return responses;
	}

	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}

	public SecurityMeasure getSecurityMeasure() {
		return securityMeasure;
	}

	public void setSecurityMeasure(AuthenticationMeasure securityMeasure) {
		this.securityMeasure = securityMeasure;
	}
}
