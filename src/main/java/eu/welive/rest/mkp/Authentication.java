/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;


import java.util.List;

public class Authentication {

	
	Boolean protecte; 
	List<String> securities; 
	
	
	public Boolean getProtecte() {
		return protecte;
	}
	public void setProtectd(Boolean protecte) {
		this.protecte = protecte;
	}
	public List<String> getSecurities() {
		return securities;
	}
	public void setSecurities(List<String> securities) {
		this.securities = securities;
}
	
}
