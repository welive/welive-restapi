/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class ArtefactObj{
	
	@XmlElement(required = true)
	Long artefactId;
	@XmlElement(required = true)
	String name;
	String description;
	String interfaceOperation;
	String type;
	Long typeId;
	Double rating;
	String url;
	Long eId;
	List<String> tags;
	List<Comment> comments;
	Authentication authentication;
	String linkImage;
	Boolean hasmapping;
	
	
	public Long getArtefactId() {
		return artefactId;
	}
	public void setArtefactId(Long artefactId) {
		this.artefactId = artefactId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getInterfaceOperation() {
		return interfaceOperation;
	}
	public void setInterfaceOperation(String interfaceOperation) {
		this.interfaceOperation = interfaceOperation;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getTypeId() {
		return typeId;
	}
	public void setTypeId(Long typeId) {
		this.typeId = typeId;
	}
	public Double getRating() {
		return rating;
	}
	public void setRating(Double rating) {
		this.rating = rating;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Long getEId() {
		return eId;
	}
	public void setEId(Long eId) {
		this.eId = eId;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	
	public Authentication getAuthentication() {
		
		return authentication;
	}
	
	public void setAuthentication (Authentication authentication){
		
		this.authentication = authentication;
	}
	
	public String getLinkImage() {
		return linkImage;
	}
	public void setLinkImage(String linkImage) {
		this.linkImage = linkImage;
}

	 public Boolean geHasmapping() {
			return hasmapping;
		}
		public void setHasmapping(Boolean hasmapping) {
			this.hasmapping = hasmapping;
		}
}
