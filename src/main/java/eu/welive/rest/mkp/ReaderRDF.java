/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import com.hp.hpl.jena.rdf.model.LiteralRequiredException;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.shared.PropertyNotFoundException;
import com.hp.hpl.jena.util.FileManager;

import eu.welive.rest.mkp.exception.MissingRecommendedPropertyException;
import eu.welive.rest.mkp.exception.MissingRequiredPropertyException;

public class ReaderRDF {

	public final static Logger _log = Logger.getLogger(ReaderRDF.class.getName());
	
	private Model  model;
	private Property propertyType; 
	
	public ReaderRDF(String inputFileName){
		model = this.getModel(inputFileName);
		if(model == null) return;
		
		propertyType = model.createProperty(Namespace.RDF + "type");
	}
	
	public ReaderRDF(File file){
		InputStream inputStream;
		try { inputStream = new FileInputStream(file); } 
		catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		
		model = this.getModel(inputStream);
	}
	
	public ReaderRDF(ByteArrayOutputStream inputStream){
		model = this.getModel(inputStream);
		if(model == null) return;
	}
	
	public ReaderRDF(InputStream inputStream){
		model = this.getModel(inputStream);
		if(model == null) return;
	}
	
	public Model getModel(String inputFileName) {

		InputStream in = FileManager.get().open( inputFileName );
		if (in == null) { throw new IllegalArgumentException( "File: " + inputFileName + " not found"); }
		
		return this.getModel(in);
	}
	
	public Model getModel(ByteArrayOutputStream inputStream) {
		
		InputStream in = new ByteArrayInputStream(inputStream.toByteArray());
		return this.getModel(in);
		
	}
	
	public Model getModel(InputStream in) {
		
		//create an empty model
		Model model = ModelFactory.createDefaultModel();
		
		try{ 
			// read the RDF/XML file
			model.read(in, "");
			return model;
		}
		catch(Exception e){
			e.printStackTrace();
			return model; 
		}
	}
	
	public Model getModel(){
		return this.model;
	}
	
	public StmtIterator getResourcesOfType(String type){
		return model.listStatements(null, propertyType, ResourceFactory.createResource(type));
	}
	
	public Resource getResourceOfName(String name){
		return model.getResource(name);
	}
	
	public HashSet<String> getMandatoryMultiplePropertyValues(Resource resource,Property property) throws MissingRequiredPropertyException{
		try{ return getMultiplePropertyValues(resource,property, true); }
		catch(MissingRecommendedPropertyException e){
			e.printStackTrace();
			return new HashSet<String>();
		}
	}
	
	public HashSet<String> getRecommendedMultiplePropertyValues(Resource resource,Property property) throws MissingRecommendedPropertyException{
		try{ return getMultiplePropertyValues(resource,property, false); }
		catch(MissingRequiredPropertyException e){
			e.printStackTrace();
			return new HashSet<String>();
		}
	}
	
	public HashSet<String>  getMultiplePropertyValues(Resource resource,Property property, boolean isMandatory) throws MissingRequiredPropertyException, MissingRecommendedPropertyException{
		HashSet<String> outSet = new HashSet<String>();
		StmtIterator stmtit = resource.listProperties(property);
		List<Statement> list = stmtit.toList();
		
		for(Statement stmt : list){
			String toBeAdded = null;
			try{
				RDFNode object = stmt.getLiteral();
				if (object instanceof Resource) { toBeAdded = object.asNode().toString().split("\\^\\^")[0]; }
				else{ toBeAdded = object.toString().split("\\^\\^")[0]; }
			}
			catch(LiteralRequiredException e){
				_log.warning("Exception:" + e.getMessage());
				toBeAdded = stmt.getLiteral().getValue().toString().split("\\^\\^")[0];
			}
			catch(PropertyNotFoundException e){
				if(isMandatory){
					String msg = "The mandatory property "+property.getNameSpace()+property.getLocalName()+" is missing in "+resource.getNameSpace()+resource.getLocalName();
					throw new MissingRequiredPropertyException(msg);
				}
				else{
					String msg = "The recommended property "+property.getNameSpace()+property.getLocalName()+" is missing in "+resource.getNameSpace()+resource.getLocalName();
					throw new MissingRecommendedPropertyException(msg);
				}
			}
			
			if(toBeAdded!=null)
				outSet.add(toBeAdded);
		}
		
		return outSet;
	}
	
	public String getMandatoryPropertyValue(Resource resource,Property property) throws MissingRequiredPropertyException{
		System.out.println("getMandatoryPropertyValue("+resource+", "+property+")");
		try{ return this.getPropertyValue(resource, property, true); }
		catch(MissingRecommendedPropertyException mrpe){
			mrpe.printStackTrace();
			return null;
		}
	}
	
	public String getRecommendedPropertyValue(Resource resource,Property property) throws MissingRecommendedPropertyException{
		try{ return this.getPropertyValue(resource, property, false); }
		catch(MissingRequiredPropertyException mrpe){
			mrpe.printStackTrace();
			return null;
		}
	}
	
	public String  getPropertyValue(Resource resource,Property property, boolean isMandatory) throws MissingRequiredPropertyException, MissingRecommendedPropertyException{
		String out=null;
		Statement stm =null;
		try{
			stm = resource.getRequiredProperty(property);
			RDFNode object = stm.getObject();    // get the object

			if (object instanceof Resource) {
				out=object.asNode().toString();
			}
			else{
				out=object.toString();
			}
		}catch(LiteralRequiredException e){
			out=resource.getRequiredProperty(property).getLiteral().getValue().toString();
		}catch(PropertyNotFoundException e){
			if(isMandatory){
				String msg = "The mandatory property "+property.getNameSpace()+property.getLocalName()+" is missing in "+resource.getNameSpace()+resource.getLocalName();
				throw new MissingRequiredPropertyException(msg);
			}
			else{
				String msg = "The recommended property "+property.getNameSpace()+property.getLocalName()+" is missing in "+resource.getNameSpace()+resource.getLocalName();
				throw new MissingRecommendedPropertyException(msg);
			}
		}
		
		if(out!=null)
			out = out.split("\\^\\^")[0];
		
		return out;
	}
	
}
