/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import javax.xml.bind.annotation.XmlElement;

import eu.welive.rest.lum.Language;

public abstract class NewArtefact {

	@XmlElement(required = true)
	String title;
	
	String description;
	@XmlElement(required = true)
	String resourceRdf;
	String webpage;
	@XmlElement(required = true)
	Integer ccUserId;
	String providerName;
	
	Language language;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getResourceRdf() {
		return resourceRdf;
	}
	public void setResourceRDF(String resourceRDF) {
		this.resourceRdf = resourceRDF;
	}
	public String getWebpage() {
		return webpage;
	}
	public void setWebpage(String webpage) {
		this.webpage = webpage;
	}
	public Integer getCcUserId() {
		return ccUserId;
	}
	public void setCcUserId(Integer ccUserId) {
		this.ccUserId = ccUserId;
	}
	public String getProviderName() {
		return providerName;
	}
	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}
	public Language getLanguage() {
		return language;
	}
	public void setLanguage(Language language) {
		this.language = language;
	}
	
}
