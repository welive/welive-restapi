/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.mkp;

import javax.xml.bind.annotation.XmlElement;

public class SetupArtefactRequest {



    @XmlElement(required=true)
	private Long ccuserid;
    @XmlElement(required=true)
	private Long ideaid;
	@XmlElement(required=true)
	private Long mashupid;
	@XmlElement(required=true)
	private Long mockupid;
	
	
	@XmlElement(required=true)
	private SetupType type;
	
	@XmlElement(required=true)
	private Object lusdl;
	
	public Long getCcuserid() {
		return ccuserid;
	}
	public void setCcuserid(Long ccuserid) {
		this.ccuserid = ccuserid;
	} 
	
	public Long getIdeaid() {
		return ideaid;
	}
	public void setIdeaid(Long ideaid) {
		this.ideaid = ideaid;
	} 
	
	public Long getMashupid() {
		return mashupid;
	}
	public void setMashupid(Long mashupid) {
		this.mashupid = mashupid;
	} 
	
	public Long getMockupid() {
		return mockupid;
	}
	public void setMockupid(Long mockupid) {
		this.mockupid = mockupid;
	} 
	
	public SetupType getType () {
		return type;
	}
	
	public void setType (SetupType type) {
		this.type = type;
	}
	
	public Object getLusdl () {
		return lusdl;
	}
	
	public void setLusdl (Object lusdl) {
		
		this.lusdl=lusdl;
	}
}

