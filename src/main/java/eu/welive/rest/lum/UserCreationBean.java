/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.lum;

import javax.xml.bind.annotation.XmlElement;

public class UserCreationBean {
	
	@XmlElement(required = true)
	Integer ccUserId;
	@XmlElement(required = true)
	Pilot pilot;
	@XmlElement(required = true)
	String firstName;
	@XmlElement(required = false)
	String surname;
	@XmlElement(required = true)
	String email;
	@XmlElement(required = false)
	Boolean isMale;
	@XmlElement(required = false)
	Integer birthdayDay;
	@XmlElement(required = false)
	Integer BirthdayMonth;
	@XmlElement(required = false)
	Integer birthdayYear;
	@XmlElement(required = false, defaultValue="false")
	Boolean developer;
	@XmlElement(required = false)
	String address;
	@XmlElement(required = false)
	String zipCode;
	@XmlElement(required = false)
	String city;
	@XmlElement(required = false)
	String country;
	@XmlElement(required = false)
	Language[] languages;
	@XmlElement(required = true)
	Role role;
	@XmlElement(required = false)
	Employement employement;
	@XmlElement(required = false)
	String[] tags;
	
	public Integer getCcUserId() {
		return ccUserId;
	}
	public void setCcUserId(Integer ccUserId) {
		this.ccUserId = ccUserId;
	}
	public Pilot getPilot() {
		return pilot;
	}
	public void setPilot(Pilot pilot) {
		this.pilot = pilot;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getIsMale() {
		return isMale;
	}
	public void setIsMale(Boolean isMale) {
		this.isMale = isMale;
	}
	public Integer getBirthdayDay() {
		return birthdayDay;
	}
	public void setBirthdayDay(Integer birthdayDay) {
		this.birthdayDay = birthdayDay;
	}
	public Integer getBirthdayMonth() {
		return BirthdayMonth;
	}
	public void setBirthdayMonth(Integer birthdayMonth) {
		BirthdayMonth = birthdayMonth;
	}
	public Integer getBirthdayYear() {
		return birthdayYear;
	}
	public void setBirthdayYear(Integer birthdayYear) {
		this.birthdayYear = birthdayYear;
	}
	public Boolean getDeveloper() {
		return developer;
	}
	public void setDeveloper(Boolean developer) {
		this.developer = developer;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Language[] getLanguages() {
		return languages;
	}
	public void setLanguages(Language[] languages) {
		this.languages = languages;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public Employement getEmployement(){
		return employement;
	}
	public void setEmployement(Employement e){
		employement = e;
	}
	public String[] getTags() {
		return tags;
	}
	public void setTags(String[] tags) {
		this.tags = tags;
	}
	
}
