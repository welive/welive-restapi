/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.lum;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import eu.welive.rest.config.Config;
import eu.welive.rest.mkp.CreationResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("lum/")
@Api(value = "/lum")
@Produces({"application/json"})
public class LUM {

	public static enum AuthType {Basic, Oauth};
	private static String baseurl;
	
	public LUM() throws Exception {
        baseurl = Config.getInstance().getLiferayURL() + Config.getInstance().getLumpath();
    }
	
	public AuthType authType(String auth){
		
		if(auth.startsWith("Basic ")) return AuthType.Basic;
		else if(auth.startsWith("Bearer ")) return AuthType.Oauth;
		else return null;
	}
	
	
	public Response invoke(String url, String token){
		
		Client client = Client.create();
		WebResource webResource;
		
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}
	
	public Response post(String url, String token, Object data){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}

	
	@POST
	@Path("/add-new-user/")
	@Produces("application/json")
	@ApiOperation(value = "Create a new User", 
			notes = "Multiple languages can be provided with comma(,) seperated strings",
			response = CreationResponse.class)
	public Response addNewUser(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "New User's data") UserCreationBean userData){
		
		if(userData.getCcUserId()==null){
			JSONObject error = new JSONObject();
			error.put("error", true);
			error.put("message", "'ccUserId' field is missing");
			return Response.status(400).entity(error.toString()).build();
		}
		else if(userData.getPilot()==null){
			JSONObject error = new JSONObject();
			error.put("error", true);
			error.put("message", "'pilot' field is missing");
			return Response.status(400).entity(error.toString()).build();
		}
		else if(userData.getFirstName()==null || userData.getFirstName().equalsIgnoreCase("")){
			JSONObject error = new JSONObject();
			error.put("error", true);
			error.put("message", "'firstName' field is missing or empty");
			return Response.status(400).entity(error.toString()).build();
		}
		else if(userData.getEmail()==null || userData.getEmail().equalsIgnoreCase("")){
			JSONObject error = new JSONObject();
			error.put("error", true);
			error.put("message", "'email' field is missing or empty");
			return Response.status(400).entity(error.toString()).build();
		}
		else if(userData.getRole()==null){
			JSONObject error = new JSONObject();
			error.put("error", true);
			error.put("message", "'role' field is missing");
			return Response.status(400).entity(error.toString()).build();
		}
		
		String url = baseurl+"/add-new-user";
		try{
			url = url + "/ccuser/"+URLEncoder.encode(userData.getCcUserId().toString(), "UTF-8")
					+ "/pilot/"+URLEncoder.encode(userData.getPilot().toString(), "UTF-8")
					+ "/firstname/"+URLEncoder.encode(userData.getFirstName(), "UTF-8");
			
			String surname = userData.getSurname();
			if(surname!=null && !surname.equals("")){ url += "/surname/"+URLEncoder.encode(surname, "UTF-8"); }
			else{ url += "/-surname"; }
			
			url = url + "/email/"+URLEncoder.encode(userData.getEmail(), "UTF-8");
			
			if(userData.getIsMale()==null){ url = url + "/-ismale"; }
			else{ url = url + "/ismale/"+URLEncoder.encode(userData.getIsMale().toString(), "UTF-8"); }
			
			Integer day = userData.getBirthdayDay();
			if(day!=null){ url += "/birthdayday/"+URLEncoder.encode(day.toString(), "UTF-8"); }
			else{ url += "/-birthdayday"; }
			
			Integer month = userData.getBirthdayMonth();
			if(month!=null){ url += "/birthdaymonth/"+URLEncoder.encode(month.toString(), "UTF-8"); }
			else{ url += "/-birthdaymonth"; }
			
			Integer year = userData.getBirthdayYear();
			if(year!=null){ url += "/birthdayyear/"+URLEncoder.encode(year.toString(), "UTF-8"); }
			else{ url += "/-birthdayyear"; }
			
			if(userData.getDeveloper()==null) userData.setDeveloper(false);
			url = url + "/isdeveloper/"+URLEncoder.encode(userData.getDeveloper().toString(), "UTF-8");
					
			String address = userData.getAddress();
			if(address!=null && !address.equals("")){ url += "/address/"+URLEncoder.encode(address, "UTF-8"); }
			else{ url += "/-address"; }
			
			String zipcode = userData.getZipCode();
			if(zipcode!=null && !zipcode.equals("")){ url += "/zipcode/"+URLEncoder.encode(zipcode, "UTF-8"); }
			else{ url += "/-zipcode"; }
			
			String city = userData.getCity();
			if(city!=null && !city.equals("")){ url += "/city/"+URLEncoder.encode(city, "UTF-8"); }
			else{ url += "/-city"; }
			
			String country = userData.getCountry();
			if(country!=null && !country.equals("")){ url += "/country/"+URLEncoder.encode(city, "UTF-8"); }
			else{ url += "/-country"; }

			Language[] langs = userData.getLanguages();
			if(langs!=null && langs.length>0){ url += "/languages/"+URLEncoder.encode(StringUtils.join(langs, ","), "UTF-8"); }
			else{ url += "/-languages"; }
			
			url = url + "/role/"+URLEncoder.encode(userData.getRole().toString(), "UTF-8");
			
			Employement emp = userData.getEmployement();
			if(emp!=null){ url += "/employement/"+URLEncoder.encode(emp.toString(), "UTF-8"); }
			else{ url += "/-employement"; }
			
			String [] tags = userData.getTags();
			if(tags!=null && tags.length>0){ url += "/tags/"+URLEncoder.encode(StringUtils.join(tags, ","), "UTF-8"); }
			else{ url += "/-tags"; }
			
		}
		catch(Exception e){ 
			e.printStackTrace(); 
		}
		return post(url, token, null);
	}
	
	@POST
	@Path("/get-user-roles/{ccuserid}")
	@Produces("application/json")
	@ApiOperation(value = "Retrieve the role", 
				response = UserRoleResponse.class)
	public Response getUserRoles(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "The ccuserid id of user you are looking for") @PathParam("ccuserid") Long ccuserid){
		
			String url = baseurl+"/get-user-roles"
					+ "/ccuserid/"+ccuserid; 
			
			return invoke(url,token);
	}
	
	@POST
	@Path("/check-login/email/{email}/pwd/{password}")
	@Produces("application/json")
	@ApiOperation(value = "Verify the credentials provided", 
				response = CheckLoginResponse.class)
	public Response checklogin(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "The Email you are going to check") @PathParam("email") String email,
								@ApiParam(value = "The Password you are going to check") @PathParam("password") String password) throws UnsupportedEncodingException{
		
	
			String url = baseurl+"/check-login/email/"+URLEncoder.encode(email, "UTF-8")+"/pwd/"+URLEncoder.encode(password, "UTF-8");
			return invoke(url,token);
		
	}
	
	@POST
	@Path("/get-leader-ccuserid-by-organizationid/organization-id/{organizationId}")
	@Produces("application/json")
	@ApiOperation(value = "Retrieve the ccUserId of the company leader", 
				response = CompanyLeader.class)
	public Response getCompanyLeader(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "The organizationId of the company of the user you want information about") @PathParam("organizationId") String companyId) throws UnsupportedEncodingException{
	
		
		    
			String url = baseurl+"/get-leader-ccuserid-by-organizationid/organization-id/"+URLEncoder.encode(companyId, "UTF-8");
			return invoke(url,token);
			
		
}
	
	@POST
	@Path("/get-user-company-details/ccuserid/{ccuserid}")
	@Produces("application/json")
	@ApiOperation(value = "Retrieve information about the organization the user belongs to", 
				response = GetOrganizationDetailsResponse.class)
	public Response getCompanyDetails(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "The ccuserid of the user you want information about") @PathParam("ccuserid") String ccuserid) throws UnsupportedEncodingException{
	
			String url = baseurl+"/get-user-company-details/ccuserid/"+URLEncoder.encode(ccuserid, "UTF-8");
			return invoke(url,token);
			
		
}
	
	@POST
	@Path("/get-all-users-data")
	@Produces("application/json")
	@ApiOperation(value = "Retrieve information about all the users", 
				response = GetUsersData.class)
	public Response getAllUsersData(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token)  throws UnsupportedEncodingException{
	
			String url = baseurl+"/get-all-users-data";
			return invoke(url,token);
			
		
}
	
	@POST
	@Path("/get-user-data/cc-user-id/{ccuserid}")
	@Produces("application/json")
	@ApiOperation(value = "Get all the data related to an user", 
			
			response = GetUserData.class)
	public Response getuserdata(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "The ccuserid of the user you want information about") @PathParam("ccuserid") String ccuserid) throws UnsupportedEncodingException{
		
		if(ccuserid==null){
			JSONObject error = new JSONObject();
			error.put("error", true);
			error.put("message", "'KO - User does not exist");
			return Response.status(400).entity(error.toString()).build();
		}
		
		String url = baseurl+"/get-user-data/cc-user-id/"+URLEncoder.encode(ccuserid, "UTF-8");;
		return invoke(url,token);
	}
}
