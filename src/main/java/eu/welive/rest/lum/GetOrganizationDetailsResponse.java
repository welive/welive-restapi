/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.lum;

import javax.xml.bind.annotation.XmlElement;

public class GetOrganizationDetailsResponse {
	
	@XmlElement(required = false)
	String message;
	
	@XmlElement(required = true)
	Boolean error;
	
	@XmlElement(required = false)
	Boolean isCompanyLeader;
	
	@XmlElement(required = false)
	Long companyId;
	
	@XmlElement(required = false)
	String companyName;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getError() {
		return error;
	}

	public void setError(Boolean error) {
		this.error = error;
	}

	public Boolean getIsCompanyLeader() {
		return isCompanyLeader;
	}

	public void setIsCompanyLeader(Boolean isCompanyLeader) {
		this.isCompanyLeader = isCompanyLeader;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	
	
	

}
