/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.lum;

import java.util.List;

public class User {

	String lastName;
	Integer ccUserID;
	String referredPilot;
	String avatar;
	Boolean isMale;
	String country;
	String city;
	String employment;
	Integer id;
	List <String> languages;
	Boolean isDeveloper;
	String liferayScreenName;
	String email;
	String address;
	String zipCode;
	String role;
	BirthDate birthDate;
	String firstName;
	
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public Integer getCcUserID()
	{
		return ccUserID;
	}
	
	public void setCcUserID(Integer ccUserID)
	{
		this.ccUserID = ccUserID;
	}
	
	public String getReferredPilot()
	{
		return referredPilot;
	}
	
	public void setReferredPilot(String referredPilot)
	{
		this.referredPilot = referredPilot;
	}
	
	public String getAvatar()
	{
		return avatar;
	}
	
	public void setAvatar(String avatar)
	{
		this.avatar  = avatar;
	}
	
	public Boolean getIsMale()
	{
		return isMale;
	}
	
	public void setIsMale(Boolean isMale)
	{
		this.isMale = isMale;
	}
	
	public String getCountry()
	{
		return country;
	}
	
	public void setCountry(String country)
	{
		this.country = country;
	}
	
	public String getCity()
	{
		return city;
	}
	
	public void setCity(String city)
	{
		this.city = city;
	}
	
	public String getEmployment()
	{
		return employment;
	}
	
	public void setEmployment(String employment)
	{
		this.employment = employment;
	}

	public Integer getId()
	{
		return id;
	}
	
	public void setId(Integer id)
	{
		this.id = id;
	}
	
	public List<String> getLanguages()
	{
		return languages;
	}
	
	public void setLanguages (List <String> languages) 
	{
		this.languages = languages;
	}
	
	public Boolean getIsDeveloper ()
	{
		return isDeveloper;
	}
	public void setIsDeveloper (Boolean isDeveloper)
	{
		this.isDeveloper = isDeveloper;
	}
	public String getLiferayScreenName()
	{
		return liferayScreenName;
	}
	
	public void setLiferayScreenName(String liferayScreenName)
	{
		this.liferayScreenName = liferayScreenName;
	}
	
	public String getEmail ()
	{
		return email;
	}
	
	public void setEmail(String email)
	{
		this.email=email;
	}
	
	public String getAddress ()
	{
		return address;
	}
	
	public void setAddress (String address)
	{
		this.address = address;
	}
	
	public String getZipCode()
	{
		return zipCode;
	}
	
	public void setZipCode (String zipCode)
	{
		this.zipCode = zipCode;
	}
	
	public String getRole ()
	{
		return role;
	}
	
	public void setRole (String role) 
	{
		this.role = role; 
	}
	
	public BirthDate getBirthDate()
	{
		return birthDate;
	}
	
	public void setBirthDate (BirthDate birthDate)
	{
		this.birthDate = birthDate;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
}
