/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.StatusType;

public class ErrorMessage {

	private boolean error;
	private int status;
	private String message;
	private String reason;
	
	public ErrorMessage() {
		this.error = true;
	}
	
	public ErrorMessage(StatusType status, String message, String reason) {
		this.status = status.getStatusCode();
		this.message = message;
		this.reason = reason;
		this.error = true;
	}
	
	public ErrorMessage(WeLiveException e) {
		this.status = e.getStatus().getStatusCode();
		this.message = e.getMessage();
		this.reason = e.getCause() != null?e.getCause().getMessage():null;
		this.error = true;
	}
	
	public ErrorMessage(StatusType status, RuntimeException e) {
		this.status = status.getStatusCode();
		this.message = e.toString();
		
		final StringWriter stringWriter = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		
		this.reason = stringWriter.toString();
		this.error = true;
	}
	
	public ErrorMessage(StatusType status, WebApplicationException e) {
		this.status = status.getStatusCode();
		this.message = e.getMessage();
		this.reason = e.getMessage();
		this.error = true;
	}

	public int getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}
}
