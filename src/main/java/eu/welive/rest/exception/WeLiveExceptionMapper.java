/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.exception;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

@Provider
@Singleton
public class WeLiveExceptionMapper implements ExceptionMapper<WeLiveException> {
	
	private static final Logger logger = LogManager.getLogger(WeLiveExceptionMapper.class);
	
	public Response toResponse(WeLiveException e) {
		if (e.getStatus() == Status.INTERNAL_SERVER_ERROR) {
			logger.error(e, e);
		} else {
			logger.warn(e, e);
		}
		return Response.status(e.getStatus()).entity(new ErrorMessage(e))
				.type(MediaType.APPLICATION_JSON).build();
	}
}
