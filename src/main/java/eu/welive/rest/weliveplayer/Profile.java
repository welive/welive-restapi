/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.weliveplayer;

import java.util.ArrayList;
import java.util.List;

public class Profile {

	private String ccUserID;
	private String referredPilot;
	private String address;
	private String city;
	private String country;
	private String zipCode;
	private boolean developer = false;
	private List<String> languages = new ArrayList<String>();
	private List<String> userTags = new ArrayList<String>();
	private List<String> skills = new ArrayList<String>();
	private String birthdate;

	public Profile() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCcUserID() {
		return ccUserID;
	}

	public void setCcUserID(String ccUserID) {
		this.ccUserID = ccUserID;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public List<String> getLanguages() {
		return languages;
	}

	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

	public boolean isDeveloper() {
		return developer;
	}

	public void setDeveloper(boolean isDeveloper) {
		this.developer = isDeveloper;
	}

	public List<String> getSkills() {
		return skills;
	}

	public void setSkills(List<String> skills) {
		this.skills = skills;
	}

	public String getReferredPilot() {
		return referredPilot;
	}

	public void setReferredPilot(String referredPilot) {
		this.referredPilot = referredPilot;
	}

	public List<String> getUserTags() {
		return userTags;
	}

	public void setUserTags(List<String> userTags) {
		this.userTags = userTags;
	}

	public String getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}

}