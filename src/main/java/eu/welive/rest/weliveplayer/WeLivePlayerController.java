/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.weliveplayer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.wadl.WadlApplicationContext;
import org.glassfish.jersey.server.wadl.internal.ApplicationDescription;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.glassfish.jersey.server.wadl.internal.WadlUtils;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;

import eu.welive.rest.exception.WeLiveException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "/weliveplayer")
@Path("/weliveplayer/")
public class WeLivePlayerController {

	private WeLivePlayerService welivePlayerService;

	
	@Context
	protected UriInfo uriInfo;

	@Context
	protected WadlApplicationContext wadlContext;

	@GET
	@Path("/wadl")
	@Produces({ "application/vnd.sun.wadl+xml", MediaType.APPLICATION_XML })
	public Response wadl() {

		try {
			boolean detailedWadl = WadlUtils.isDetailedWadlRequested(uriInfo);
			String lastModified = new SimpleDateFormat(WadlResource.HTTPDATEFORMAT).format(new Date());
			ApplicationDescription applicationDescription = wadlContext.getApplication(uriInfo, detailedWadl);
			com.sun.research.ws.wadl.Application application = applicationDescription.getApplication();
			List<com.sun.research.ws.wadl.Resource> extResources = new ArrayList<com.sun.research.ws.wadl.Resource>();
			for (com.sun.research.ws.wadl.Resource temp : application.getResources().get(0).getResource()) {
				if (!temp.getPath().equalsIgnoreCase("/weliveplayer/")) {
					extResources.add(temp);
				}
			}
			application.getResources().get(0).getResource().removeAll(extResources);
			return Response.ok(application).header("Last-modified", lastModified).build();
		} catch (Exception e) {
			throw new ProcessingException("Error generating WADL", e);
		}
	}
	
	@ApiOperation(value = "Get Applications By City and Type.")
	@GET
	@Path("/api/apps/{pilotId}/{appType}")
	@Timed
	@ExceptionMetered
	public Response getArtifacts(@ApiParam(value = "pilotId", required = true) @PathParam("pilotId") String pilotId,
			@ApiParam(value = "appType", required = true) @PathParam("appType") String appType,
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "start", required = false) @QueryParam("start") Integer start,
			@ApiParam(value = "count", required = false) @QueryParam("count") Integer count) throws WeLiveException {

		String result = null;
		try {
			result = getWeLivePlayerService().findApplications(pilotId, appType, token, (start == null ? 0 : start),
					(count == null ? 20 : count));
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get Single Application Comments.")
	@GET
	@Path("/api/appComments/{artifactId}")
	@Timed
	@ExceptionMetered
	public Response getAppComments(
			@ApiParam(value = "artifactId", required = true) @PathParam("artifactId") String artifactId,
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "start", required = false) @QueryParam("start") Integer start,
			@ApiParam(value = "count", required = false) @QueryParam("count") Integer count) throws WeLiveException {

		String result = null;
		try {
			result = getWeLivePlayerService().findApplicationComments(artifactId, token, (start == null ? 0 : start),
					(count == null ? 20 : count));
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Read User Profile Using ACCESS_TOKEN.")
	@GET
	@Path("/api/userProfile")
	@Timed
	@ExceptionMetered
	public Response readUserProfile(
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getWeLivePlayerService().findUserProfile(token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Update User Profile Using ACCESS_TOKEN.")
	@POST
	@Path("/api/update/userProfile")
	@Timed
	@ExceptionMetered
	public Response updateUserProfile(
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "Profile") Profile profile) throws WeLiveException {

		String result = null;
		try {
			result = getWeLivePlayerService().updateUserProfile(token, profile);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	private WeLivePlayerService getWeLivePlayerService() {

		if (welivePlayerService == null)
			welivePlayerService = WeLivePlayerService.getInstance();

		return welivePlayerService;
	}

	public void setWeLivePlayerService(WeLivePlayerService wlPlayerService) {
		this.welivePlayerService = wlPlayerService;

	}

}