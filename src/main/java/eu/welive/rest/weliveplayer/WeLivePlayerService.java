/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.weliveplayer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;

public class WeLivePlayerService {

	private static WeLivePlayerService instance = null;
	private static String weLivePlayerService = null;
	private static CloseableHttpClient httpClient = null;
	private static String birthdayFormat = "yyyy-MM-dd";
	ObjectMapper mapper = new ObjectMapper();

	protected WeLivePlayerService() {
		try {
			weLivePlayerService = Config.getInstance().getWeLivePlayerServiceURL();
		} catch (IOException e) {
			e.printStackTrace();
		}
		SSLContextBuilder builder = new SSLContextBuilder();
		try {
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	

	public static void setHttpClient(CloseableHttpClient httpClient) {
		WeLivePlayerService.httpClient = httpClient;
	}



	public static WeLivePlayerService getInstance() {
		if (instance == null) {
			instance = new WeLivePlayerService();
		}

		return instance;
	}
	
	public WeLivePlayerService(String url) {
		weLivePlayerService = url;
	}

	protected String findApplications(String pilotId, String type, String token, Integer page, Integer count)
			throws Exception {
		return doGet("/api/apps/" + pilotId + "/" + type + "?start=" + page + "&count=" + count, null, token);
	}

	protected String findApplicationComments(String appId, String token, Integer page, Integer count) throws Exception {
		return doGet("/api/appComments/" + appId + "?start=" + page + "&count=" + count, null, token);
	}

	protected String findUserProfile(String token) throws Exception {
		return doGet("/api/userProfile/", null, token);
	}

	protected String updateUserProfile(String token, Profile profile) throws Exception {
		return doPost("/api/update/userProfile/", getProfileJson(profile), token, true);
	}

	protected String doGet(String operation, String query, String token) throws Exception {
		URIBuilder uriBuilder = new URIBuilder(weLivePlayerService + operation + (query != null ? ("?" + query) : ""));
		HttpGet httpGet = new HttpGet(uriBuilder.build());
		httpGet.addHeader("Content-Type", "application/json");
		httpGet.addHeader("Accept", "application/json");
		httpGet.addHeader("Authorization", token);

		CloseableHttpResponse response = httpClient.execute(httpGet);

		InputStream content = response.getEntity().getContent();
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
		StringBuilder responseStrBuilder = new StringBuilder();
		String inputStr;
		while ((inputStr = streamReader.readLine()) != null) {
			responseStrBuilder.append(inputStr);
		}
		content.close();

		if (response.getStatusLine().getStatusCode() >= 400) {
			throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
					responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
		}

		return responseStrBuilder.toString();
	}

	protected String doPost(String operation, String body, String token, boolean getResponse) throws Exception {
		URIBuilder uriBuilder = new URIBuilder(weLivePlayerService + operation);

		System.out.println(body);

		HttpPost httpPost = new HttpPost(uriBuilder.build());
		httpPost.setEntity(new StringEntity(body));

		httpPost.addHeader("Content-Type", "application/json");
		httpPost.addHeader("Accept", "application/json");
		httpPost.addHeader("Authorization", token);
		CloseableHttpResponse response = httpClient.execute(httpPost);

		if (getResponse) {
			InputStream content = response.getEntity().getContent();
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			content.close();

			if (response.getStatusLine().getStatusCode() >= 400) {
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
						responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
			}

			return responseStrBuilder.toString();
		} else {
			return null;
		}
	}

	public String getProfileJson(Profile profile)
			throws JsonGenerationException, JsonMappingException, IOException, ParseException {

		String body = null;
		Map<String, Integer> dob = new HashMap<>();
		Map<String, Object> map = new HashMap<>();

		map.put("ccUserID", profile.getCcUserID());
		map.put("referredPilot", profile.getReferredPilot());
		map.put("address", profile.getAddress());
		map.put("city", profile.getCity());
		map.put("country", profile.getCountry());
		map.put("zipCode", profile.getZipCode());
		map.put("developer", profile.isDeveloper());
		map.put("languages", profile.getLanguages());
		map.put("userTags", profile.getUserTags());
		map.put("skills", profile.getSkills());

		try {

			Date bday = parseDate(profile.getBirthdate(), birthdayFormat);

			dob.put("day", getDayOfMonth(bday));
			dob.put("month", getMonthOfYear(bday));
			dob.put("year", getYear(bday));

			map.put("birthdate", mapper.writeValueAsString(dob));

		} catch (Exception e) {

		}

		body = mapper.writeValueAsString(map);

		return body;
	}

	public int getDayOfMonth(Date reqDate) {
		int day = -1;
		Calendar cal = Calendar.getInstance();
		cal.setTime(reqDate);
		day = cal.get(Calendar.DAY_OF_MONTH);
		return day;
	}

	public int getMonthOfYear(Date reqDate) {
		int month = -1;
		Calendar cal = Calendar.getInstance();
		cal.setTime(reqDate);
		month = cal.get(Calendar.MONTH) + 1;
		return month;
	}

	public int getYear(Date reqDate) {
		int year = -1;
		Calendar cal = Calendar.getInstance();
		cal.setTime(reqDate);
		year = cal.get(Calendar.YEAR);
		return year;
	}

	private Date parseDate(String date, String format) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.parse(date);
	}

}