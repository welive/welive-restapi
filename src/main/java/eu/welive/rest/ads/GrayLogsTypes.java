/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads;

public class GrayLogsTypes {
	public static final String ATRIBUTE_PILOT = "custom_pilot";
	/*
	 * ODS  
	 */	
	public static final String TYPE_DATASHET_PUBLISHED = "DatasetPublished";
	public static final String TYPE_DATASHET_REMOVED = "DatasetRemoved";
	public static final String TYPE_DATASHET_METADATA_ACCESSED = "DatasetMetadataAccessed";
	public static final String TYPE_DATASHET_METADATA_UPDATED = "DatasetMetadataUpdated";
	
	public static final String ATRIBUTE_USERID = "custom_userid";
	public static final String ATRIBUTE_DATASET_TYPE = "custom_datasettype";
	public static final String ATRIBUTE_DATASET_NAME = "custom_datasetname";
	//public static final String ATRIBUTE_TYPE = "custom_Type";	
	
	////////////////////////////////
	
	/*
	 * MKT Artefact 
	 */	
	public static final String TYPE_ARTEFACT_PUBLISHED = "ArtefactPublished";
	public static final String TYPE_ARTEFACT_REMOVED = "ArtefactRemoved";
	public static final String ATRIBUTE_ARTEFACT_TYPE = "custom_artefacttype";
	
	public static final String PUBLIC_SERVICE_APPLICATION = "Public Service";
	public static final String BUILDING_BLOCK = "Building Block";
	public static final String DATASET = "Dataset";
	
	public static final String ATRIBUTE_USED_ARTEFACTS = "custom_usedartefacts";
	public static final String ATRIBUTE_OWNER_ID = "custom_ownerid";
	
	
	public static final String TYPE_ARTEFACT_RATED = "ArtefactRated";
	public static final String ATRIBUTE_RATE = "custom_rate";
	public static final String ATRIBUTE_ARTEFACT_ID = "custom_artefattoid";
	public static final String ATRIBUTE_ARTEFACT_NAME = "custom_artefactname";
	
	/*
	 * OIA  
	 */		
	public static final String TYPE_IDEA_IMPLEMENTATION = "IdeaInImplementation";
	public static final String TYPE_IDEA_PUBLISHED = "IdeaPublished";
	public static final String TYPE_IDEA_REMOVED = "IdeaRemoved";
	public static final String TYPE_NEEDPUBLISHED = "NeedPublished";
	public static final String TYPE_NEEDREMOVED = "NeedRemoved";
	public static final String TYPE_CHALLENGE_PUBLISHED = "ChallengePublished";
	public static final String TYPE_CHALLENGE_REMOVED = "ChallengeRemoved";
	public static final String TYPE_IDEA_RATED = "IdeaRated";
	public static final String TYPE_NEED_RATED = "NeedRated";
	public static final String ATRIBUTE_AUTHORID = "custom_authorid";
	public static final String ATRIBUTE_AUTHORITY_NAME = "custom_authorityname";
	public static final String ATRIBUTE_APP_ID = "appId";
	public static final String ATRIBUTE_IDEA_ID = "custom_ideaid";
	public static final String ATRIBUTE_CHALLENGE_ID = "custom_challengeid";
	public static final String ATRIBUTE_IDEA_TITLE = "custom_ideatitle";
	public static final String ATRIBUTE_NEED_TITLE = "custom_needtitle";
	
	/////////////////////////////
	
	/**
	 * Player
	 */
	public static final String TYPE_APP_DOWNLOAD = "AppDownload";
	
	public static final String TYPE_APP_OPEN = "AppOpen";
	public static final String ATRIBUTE_CUSTOM_APP_ID = "custom_AppID";
	public static final String ATRIBUTE_CUSTOM_APP_NAME = "custom_appname";
	
	/////////////////////////////////
	/*
	 * Controler
	 */
	public static final String TYPE_CITY_SELECTED = "CitySelected";
	
	// Use atribute ATRIBUTE_CITY_NAME
	public static final String TYPE_COMPONENT_SELECTED = "ComponentSelected";
	public static final String ATRIBUTE_COMPONENT_NAME = "custom_componentname";
	
	public static final String TYPE_CONTACT_FORM = "ContactForm";
	public static final String ATRIBUTE_YES = "custom_yes";
	
	/////////////////////////////////
	/*
	 * Common appKPI
	 */
	public static final String TYPE_QUESTIONNAIRE = "QuestionnaireFilled";
	public static final String ATRIBUTE_CUSTOM_APP = "custom_appname";
	public static final String ATRIBUTE_CUSTOM_VALUE = "custom_value";
	public static final String ATRIBUTE_CUSTOM_PILOT = "custom_pilot";    
	public static final String APPNAME_FILTER = " AND (appId:\"sportit\" OR appId:\"helsinkicitymuseums\" OR appId:\"helsinkicitybikes\" OR custom_appname:\"My Feelings-app\" OR custom_appname:\"EntrepreneurshipKlub\" OR custom_appname:\"BilbaoEvents\" OR custom_appname:\"TrentoInformer\" OR custom_appname:\"TrentoRoomBooking\" OR custom_appname:\"TrentoPAGuide\" OR custom_appname:\"localcommunity\" OR custom_appname:\"culturekey\" OR custom_appname:\"PublicProcurement\" OR custom_appname:\"SafeCityTrip\" OR custom_appname:\"RellocationAdvisor\" OR custom_appname:\"Trento Orari Trasporti\" OR custom_appname:\"Trento Bike Sharing\" OR custom_appname:\"Trento Street Cleaning\" OR custom_appname:\"MyCityMood\" OR custom_appname:\"MyNeighbourhood\" OR custom_appname:\"MyOpinion\" OR custom_appname:\"MyPolls\" OR custom_appname:\"Auzonet\" OR custom_appname:\"Bilbon\" OR custom_appname:\"Bilbozkatu\")";
	
	public static final String PILOT_FILTER =" AND (custom_pilot:\"Bilbao\" OR custom_pilot:\"Trento\" OR custom_pilot:\"Uusimaa\" OR custom_pilot:\"Novisad\")";
	
}
