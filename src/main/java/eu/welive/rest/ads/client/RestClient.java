/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads.client;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import eu.welive.rest.ads.client.json.Query;
import eu.welive.rest.config.Config;

public class RestClient {
	
	private String _baseUrl ="";
	private String _user ="";
	private String _pass ="";
	
	private final static Logger logger = Logger.getLogger(RestClient.class.getName());
	
	public RestClient() throws IOException{
		
		_baseUrl = Config.getInstance().getBASE_URL();
		_user = Config.getInstance().getAdminUser();
		_pass = Config.getInstance().getAdminPass();
		logger.info("_baseUrl: "+_baseUrl+" _user:"+_user+" _pass:"+_pass);
		 //System.out.println("_baseUrl: "+_baseUrl);
	}
	public RestClient(String user, String pass) {
		try{
		_baseUrl = Config.getInstance().getBASE_URL();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		this._user= user;
		this._pass =pass;
		
		logger.info("_baseUrl: "+_baseUrl+" _user:"+_user+" _pass:"+_pass);
		//System.out.println("_baseUrl: "+_baseUrl);
	}
	
	public Client initClient(Configuration config)  {
		SSLContext ctx;
		try {
			ctx = SSLContext.getInstance("SSL");
	        
			ctx.init(null, certs, new SecureRandom());

		    return ClientBuilder.newBuilder()
		                .withConfig(config)
		                .hostnameVerifier(new TrustAllHostNameVerifier())
		                .sslContext(ctx)
		                .build();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
		return null;
	    
	}

	TrustManager[] certs = new TrustManager[]{
            new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType)
                        throws CertificateException {
                }
            }
    };

    public static class TrustAllHostNameVerifier implements HostnameVerifier {

        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }	
	public Response Get(String sUrl){
		Client client = null;
		logger.info("Get: "+_baseUrl+sUrl);
		System.out.println("Get: "+_baseUrl+sUrl);
		if (_baseUrl.startsWith("https")){
			client =initClient(new ClientConfig());				 
		}
		else{
			client = ClientBuilder.newClient();
		}
		
		if (_user!=null && !_user.equals("") && _pass!=null && !_pass.equals("")){
			System.out.println("user:"+_user+" Pass:"+_pass);
			client.register(HttpAuthenticationFeature.basic(_user, _pass));
		}
		WebTarget target = client.target(_baseUrl+sUrl);
		Response r = target.request(MediaType.APPLICATION_JSON_TYPE).get();


		//System.out.println(r.getStatus());
		return r;
		/*
		if (r.getStatus() != 200){
			return "";
		}
		return r.readEntity(String.class);	
		*/
	}

	public Response Post(String sUrl, String hData) {
		
		Client client = null;
		//logger.info("Post: "+sUrl+" Data: "+aux);
		System.out.println("Post: "+_baseUrl+sUrl+" Data: "+hData);
		if (_baseUrl.startsWith("https")){
			client =initClient(new ClientConfig());				 
		}
		else{
			client = ClientBuilder.newClient();
		}
		
		if (_user!=null && !_user.equals("") && _pass!=null && !_pass.equals("")){
			System.out.println("user:"+_user+" Pass:"+_pass);
			client.register(HttpAuthenticationFeature.basic(_user, _pass));//new Authenticator(user, pass));				
		}
		WebTarget target = client.target(_baseUrl);
		Response response = target.path(sUrl).request(MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")).post(
				Entity.entity(hData, MediaType.APPLICATION_JSON_TYPE.withCharset("utf-8")));
	
		return response;
		
	}
	public Response getCdvKPI(String pilotid,String datefrom, String dateto, String path) throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String to ="";
		String from="1970-01-01 00:00:00.000";
		if (dateto != null && !dateto.equals("")){
			Date dateTo;
			try {
				dateTo = dateFormat.parse(dateto);
				to = dateFormat.format(dateTo);
			} catch (ParseException e) {
				System.out.println("getCdvKPI dateto parse error: "+e.getMessage());
				Date now = new Date();
				to = dateFormat.format(now);
			}		
			
		}
		else{
			Date now = new Date();
			to = dateFormat.format(now);
		}
		
		if (datefrom != null && !datefrom.equals("")){
			Date dateFrom;
			try {
				dateFrom = dateFormat.parse(datefrom);
				from = dateFormat.format(dateFrom);
			} catch (ParseException e) {
				System.out.println("getCdvKPI datefrom parse error: "+e.getMessage());
				//Date now = new Date();
				try {
					dateFrom = dateFormat.parse("1970-01-01 00:00:00.000");
					from = dateFormat.format(dateFrom);
				} catch (ParseException e1) {
					
					System.out.println("getCdvKPI datefrom parse error: "+e.getMessage());
				}
				//from = dateFormat.format(dateFrom);
			}		
			
		}
		else{
			Date dateFrom;
			try {
				dateFrom = dateFormat.parse("1970-01-01 00:00:00.000");
				from = dateFormat.format(dateFrom);
			} catch (ParseException e) {
				
				System.out.println("getCdvKPI datefrom parse error: "+e.getMessage());
			}
			
		}
		if (from == null || to == null || from.equals("") || to.equals("")) {
			return Get(path+pilotid);
		}
		else{
			return Get(path+pilotid+"/from/"+from+"/to/"+to);
		}
	}

	public Response getKPI1_1(String pilotid) throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		String kpi1_1= RestClientDefs.KPI1_1;
		
		return Get(kpi1_1+pilotid);
	}
	public Response getKPI4_3(String pilotid)throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		String kpi4_3= RestClientDefs.KPI4_3;

		return Get(kpi4_3+pilotid);
	}
	public Response getKPI11_1(String pilotid)throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		String kpi11_1= RestClientDefs.KPI11_1;
		
		return Get(kpi11_1+pilotid);
	}
	public Response getKPI11_2(String pilotid)throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		String kpi11_2= RestClientDefs.KPI11_2;

		return Get(kpi11_2+pilotid);
	}

	public Response getKPI11_3(String pilotid)throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		String kpi11_3= RestClientDefs.KPI11_3;

		return Get(kpi11_3+pilotid);
	}

	public Response getKPI11_4(String pilotid)throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		String kpi11_4= RestClientDefs.KPI11_4;

		return Get(kpi11_4+pilotid);
	}

	public Response getKPI11_5(String pilotid)throws IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		String kpi11_5= RestClientDefs.KPI11_5;

		return Get(kpi11_5+pilotid);
	}

	public Response getQueryFromLoggin(String strquery, String datefrom, String dateto) throws ParseException, IOException{
		_baseUrl = Config.getInstance().getBASE_URL();
		System.out.println("datefrom "+ datefrom );
		System.out.println("dateto "+ dateto );
		
		String weliveLogginQuery=null;
		try{
			weliveLogginQuery = Config.getInstance().getWELIVE_LOGGIN_QUERY();
		}catch(Exception e){
			e.printStackTrace();
		}

		
		if ((datefrom == null && dateto==null ) || (datefrom.equals("") && dateto.equals("") )){
			Query query= new Query(strquery);
			return Post(weliveLogginQuery,query.toString());
		}
		else if ((datefrom != null && dateto != null ) && (!datefrom.equals("") && !dateto.equals("") )){
			Query query= new Query(strquery,datefrom,dateto);
			return Post(weliveLogginQuery,query.toString());
			
		}
		else if (datefrom != null && !datefrom.equals("") ) {
			Query query= new Query(strquery,datefrom);
			return Post(weliveLogginQuery,query.toString());
		}
		else {
			Query query= new Query(strquery,datefrom,dateto);
			return Post(weliveLogginQuery,query.toString());
			
		}
		//return null;
		
	}
	/*
	public Response insertLog(String strquery, String appid) throws ParseException{
		try{
			_baseUrl = Config.getInstance().getBASE_URL();
		}catch(Exception e){
			e.printStackTrace();
		}
		String weliveLogInsert=null;
		try{
			weliveLogInsert = Config.getInstance().getWELIVE_LOG_INSERT();
		}catch(Exception e){
			e.printStackTrace();
		}
		
			Query query= new Query(strquery);
			return Post(weliveLogInsert+"/"+appid,strquery);
		
	}

	public void login(String username, String password){
		Hashtable<String, String> hData = new Hashtable<String, String>();
		hData.put("username", username);
		hData.put("password", password);
	
		String urlWeliveLogin=null;
		try{
			urlWeliveLogin = Config.getInstance().getURL_WELIVE_LOGIN();
		}catch(Exception e){
			e.printStackTrace();
		}		
		this.Post(urlWeliveLogin, hData);
	}
	*/
}

