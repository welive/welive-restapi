/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads.client;

public class RestClientDefs {

	// URL definitions.
	/*
	public static final String URL_WELIVE_LOGIN = "welive.logging";//https://dev.welive.eu/dev/api/cdv/getKPI_1.1/All
	public static final String BASE_URL = "https://dev.welive.eu";///https://dev.welive.eu/cdv/api/statistics/operation/kpi_1.1/all
*/
	public static final String KPI1_1 = "/dev/api/cdv/getKPI_1.1/";//{pilotID}";
	public static final String KPI4_3 = "/dev/api/cdv/getKPI_4.3/";//{pilotID}
	public static final String KPI11_1 = "/dev/api/cdv/getKPI_11.1/";//{referredPilot}
	public static final String KPI11_2 = "/dev/api/cdv/getKPI_11.2/";//{referredPilot}
	public static final String KPI11_3 = "/dev/api/cdv/getKPI_11.3/";//{referredPilot}
	public static final String KPI11_4 = "/dev/api/cdv/getKPI_11.4/";//{referredPilot}
	public static final String KPI11_5 = "/dev/api/cdv/getKPI_11.5/";//{referredPilot}
/*	
	public static final String WELIVE_LOGGIN_QUERY = "/welive.logging/log/aggregate";
	public static final String WELIVE_LOG_INSERT = "/welive.logging/log";
	
	public static void loadData(){
		
	}
*/
}
