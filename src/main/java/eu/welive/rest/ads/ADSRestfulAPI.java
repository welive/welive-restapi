/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;

import eu.welive.rest.ads.client.RestClient;
import eu.welive.rest.ads.client.RestClientDefs;
import eu.welive.rest.exception.WeLiveException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


/**
 * Root resource
 */
@Path("/analytics")
@Api(value = "/analytics")
@Produces({ MediaType.APPLICATION_JSON })
public class ADSRestfulAPI {

	public JSONObject callQueryFromLoggin(String call, String from, String to)	throws ParseException, IOException {
		RestClient rc = new RestClient();
		Response resultlogquery = rc.getQueryFromLoggin(call, from, to);
		JSONObject resultquery = new JSONObject(resultlogquery.readEntity(String.class));
		return resultquery;
	}
	public JSONObject callCDV(String pilotid,  String path)	throws ParseException, JSONException, IOException {
		RestClient rc = new RestClient();
		return new JSONObject(rc.getCdvKPI(pilotid,"","", path).readEntity(String.class));
	}
	
	public JSONObject callCDV(String pilotid, String from, String to, String path)	throws ParseException, JSONException, IOException {
		RestClient rc = new RestClient();
		return new JSONObject(rc.getCdvKPI(pilotid,from, to, path).readEntity(String.class));
	}
	

	@GET
	@Timed 
	@ExceptionMetered
	@Path("/oia/ideainimplementation/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.2 - Number of public services apps, building blocks and dataset ideas (per pilot)", response = Response.class)
	public Response getKPI1_2(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_IDEA_IMPLEMENTATION + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_2 ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/ideainimplementation/{userid}/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI 1.2.1 Number of ideas in 'implementation stage' published in the OIA by an specific user. The number of new ideas published should be aggregated by month", response = Response.class)
	public Response getKPI1_2_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_AUTHORID + ":\"" + userid
							+ "\" AND  type:\""
							+ GrayLogsTypes.TYPE_IDEA_IMPLEMENTATION + "\"",
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_2_1 ", e);
		}
	}
	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Number of Users registered in the OIA (AAC or CDV profiles)
	 * who created or collaborated any need/idea (per city)
	 * 
	 * @return String in JSON Format with the stakeholders per city,
	 *         {"bilbao":0,"helsinki":0,"novisad":0,"trento":1}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/stakeholders")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1_1 Number of Users registered in the OIA (AAC or CDV profiles) who created or collaborated any need/idea (per city)", response = Response.class)
	public Response getKPI1_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to
			
			) throws WeLiveException {
		try {

			JSONObject resultquery = callCDV("All", from, to, RestClientDefs.KPI1_1);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_1 ", e);
		}
	}


	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Number of Users registered in the OIA (AAC or CDV profiles)
	 * who created or collaborated any need/idea (per city)
	 * 
	 * @param pilotid
	 *            : all, trento, bilbao, novisad, helsinki
	 * @return String in JSON Format with the stakeholders per city,
	 *         {"bilbao":0}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/stakeholders/{pilotid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1_1 Number of Users registered in the OIA (AAC or CDV profiles) who created or collaborated any need/idea (per city)", response = Response.class)
	public Response getKPI1_1(
			@ApiParam(value = "The pilotID: all, trento, bilbao, novisad, helsinki") @PathParam("pilotid") String pilotid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to

			)
			throws WeLiveException {
		try {
			JSONObject resultquery = callCDV(pilotid, from, to, RestClientDefs.KPI1_1);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_1 ", e);
		}
	}

	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Number of users (registered) (per city)
	 * 
	 * @return String in JSON Format
	 *         {"bilbao":20,"helsinki":0,"novisad":0,"trento":5}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/user/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI4_3 Number of users (registered) (per city)", response = Response.class)
	public Response getKPI4_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to
			
			) throws WeLiveException {
		try {
			JSONObject resultquery = callCDV("All", from, to, RestClientDefs.KPI4_3);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();

		} catch (Exception e) {
			e.printStackTrace();
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI4_3 ", e);
		}

		// return Response.serverError().build();

	}

	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Classification of user per Age range (per city). This
	 * information comes from CDV
	 * 
	 * @return String in JSON Format
	 *         {"bilbao":{"<20":0,"20-40":1,"40-60":19,">60"
	 *         :0},"helsinki":{"<20":0,"20-40":0,"40-60":0,">60":0},....}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/user/ages/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI11_1 Classification of user per Age range (per city). This information comes from CDV", response = Response.class)
	public Response getKPI11_1() throws WeLiveException {
		try {
			JSONObject resultquery = callCDV("All",  RestClientDefs.KPI11_1);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();

		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI11_1 ");
		}

	}

	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Classification of user per gender (per city). This
	 * information comes from CDV
	 * 
	 * @return String in JSON Format
	 *         {"bilbao":{"male":20,"female":0},"helsinki":
	 *         {"male":0,"female":0},
	 *         "novisad":{"male":0,"female":0},"trento":{"male":5,"female":0}}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/user/gender/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI11_2 Classification of user per gender (per city). This information comes from CDV", response = Response.class)
	public Response getKPI11_2() throws WeLiveException {
		try {
			JSONObject resultquery = callCDV("All",  RestClientDefs.KPI11_2);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API ");
		}

	}

	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Classification of user per gender (per city). This
	 * information comes from CDV
	 * 
	 * @return String in JSON Format
	 *         {"bilbao":{"male":20,"female":0},"helsinki":
	 *         {"male":0,"female":0},
	 *         "novisad":{"male":0,"female":0},"trento":{"male":5,"female":0}}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/user/work/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI11_3 Related to the work status of the people registered in the platform. This information comes from CDV", response = Response.class)
	public Response getKPI11_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to
			
			) throws WeLiveException {
		try {
			JSONObject resultquery = callCDV("All", from,to, RestClientDefs.KPI11_3);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();

		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API ");
		}
	}

	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Classification of user per gender (per city). This
	 * information comes from CDV
	 * 
	 * @return String in JSON Format
	 *         {"bilbao":{"male":20,"female":0},"helsinki":
	 *         {"male":0,"female":0},
	 *         "novisad":{"male":0,"female":0},"trento":{"male":5,"female":0}}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/user/role/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI11_4 Related to the users registered role in the platform. This information comes from CDV", response = Response.class)
	public Response getKPI11_4(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to
			
			) throws WeLiveException {
		try {
			JSONObject resultquery = callCDV("All",from, to,  RestClientDefs.KPI11_4);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();

		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API ");
		}
	}

	/**
	 * Really for this KPI we only need to call to the CDV service that has the
	 * information. Classification of user per gender (per city). This
	 * information comes from CDV
	 * 
	 * @return String in JSON Format
	 *         {"bilbao":{"male":20,"female":0},"helsinki":
	 *         {"male":0,"female":0},
	 *         "novisad":{"male":0,"female":0},"trento":{"male":5,"female":0}}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/cdv/user/developer/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI11_5 Related to the Number of users registered as developers in the platform. This information comes from CDV", response = Response.class)
	public Response getKPI11_5(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to
			
			) throws WeLiveException {
		try {
			JSONObject resultquery = callCDV("All",from,to,  RestClientDefs.KPI11_5);
			return Response.ok(resultquery.toString(),
					MediaType.APPLICATION_JSON).build();

		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API ");
		}
	}

	/**
	 * Total number of datasets processed by the ODS (Total and also per city)
	 * 
	 * Make a query to logging interface and parse the result in oder to get the
	 * data in the following format.
	 * {"Bilbao":6,"Test organization":2,"hits":210
	 * ,"Trento":13,"Helsinki-Uusimaa":18,"Bilbao City Council":171}
	 * 
	 * @return String in JSON Format
	 *         {"Bilbao":6,"Test organization":2,"hits":210
	 *         ,"Trento":13,"Helsinki-Uusimaa":18,"Bilbao City Council":171}
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/ods/dataset/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI2_1 Total number of datasets processed by the ODS (Total and also per city)", response = Response.class)
	public Response getKPI2_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_DATASHET_PUBLISHED + "\"", from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""

			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);

			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI2_1 ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/ods/dataset/all/delta")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI2_1 Total number of datasets processed by the ODS (Total and also per city)", response = Response.class)
	public Response getKPI2_1_delta(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_DATASHET_REMOVED + "\" OR type:\""
					+ GrayLogsTypes.TYPE_DATASHET_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils
					.getHitsPerField(jarrayhits, GrayLogsTypes.ATRIBUTE_PILOT,
							"", GrayLogsTypes.TYPE_DATASHET_REMOVED,
							GrayLogsTypes.TYPE_DATASHET_PUBLISHED, withmonths,
							from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI2_1 ", e);
		}
	}

	/**
	 * Number of types of broad datasets processed by the ODS (Total per type of
	 * dataset and also per city)
	 * 
	 * Make a query to logging interface and parse the result in oder to get the
	 * data in the following format. { "Bilbao": { "Private Data": 1,
	 * "Open Data": 2 }, "Test organization": { "Open Data": 2 }, "Trento": {
	 * "Private Data": 13 }, "Helsinki-Uusimaa": { "Private Data": 15,
	 * "Open Data": 3 }, "Novi Sad": { "Private Data": 1 },
	 * "Bilbao City Council": { "Private Data": 171 } }
	 * 
	 * @return String in JSON Format
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/ods/dataset/type/all/count")
	// /ads/ods/dataset/all/count
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI2_2 Number of types of broad datasets processed by the ODS (Total per type of dataset and also per city)", response = Response.class)
	public Response getKPI2_2(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_DATASHET_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.get2DDataset(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT,
					GrayLogsTypes.ATRIBUTE_DATASET_TYPE);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI2_2 ", e);
		}
	}

	/**
	 * Number of times a Dataset is used (Usage of datasets). Most used datasets
	 * Make a query to logging interface and parse the result in oder to get the
	 * data in the following format. { "api-test": 1, "Test validation": 45,
	 * "Helsinki Area Address Catalogue": 3,
	 * "Presupuesto de ingresos del Ayuntamiento de Bilbao de 2015": 93,
	 * "Population projection in the Metropolitan Area in 2015-2024": 1,
	 * "bilbao-twitter-topics": 14, .... }
	 * 
	 * @return String in JSON Format
	 * @throws WeLiveException
	 */
	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/ods/dataset/usage/all/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI2_3 Number of times a Dataset is used (Usage of datasets). Most used datasets", response = Response.class)
	public Response getKPI2_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {

			if (!pilot.equals("")) {
				pilot = " AND " + GrayLogsTypes.ATRIBUTE_PILOT + ":\"" + pilot
						+ "\"";
			}

			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_DATASHET_METADATA_ACCESSED
					+ "\" OR type:\""
					+ GrayLogsTypes.TYPE_DATASHET_METADATA_UPDATED + "\""
					+ pilot, from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");

			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_DATASET_NAME, "", withmonths, from,
					to);
			// Key is the custom_datasetName, value a hasmap with key the month
			// and value the uses of that datasets in that month .

			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (ParseException e) {
			e.printStackTrace();
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI2_3 ", e);

		}

		catch (Exception e) {
			e.printStackTrace();

		}

		return Response.serverError().build();

	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/ods/dataset/usage/{userid}/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI2_3_1 Most used datasets per month for an specific user", response = Response.class)
	public Response getKPI2_3_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(GrayLogsTypes.ATRIBUTE_USERID+":\""
					+ userid + "\" AND  (type:\""
					+ GrayLogsTypes.TYPE_DATASHET_METADATA_ACCESSED
					+ "\" OR type:\""
					+ GrayLogsTypes.TYPE_DATASHET_METADATA_UPDATED + "\")",
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_DATASET_NAME, "", withmonths, from,
					to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI2_3_1 ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/all/materialized")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3 - Number of public services apps, building blocks and dataset transformed into real assets within WeLive MarketPlace (per pilot)", response = Response.class)
	public Response getKPI1_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			if (!pilot.equals("")) {
				pilot = " AND " + GrayLogsTypes.ATRIBUTE_PILOT + ":\"" + pilot
						+ "\"";
			}
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\"" + pilot,
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE, "", withmonths, from,
					to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_3 ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/all/materializedcity")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3 - Number of public services apps, building blocks and dataset transformed into real assets within WeLive MarketPlace (per pilot)", response = Response.class)
	public Response getKPI1_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.get2DDataset(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_3 ", e);
		}

	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/all/materializedcitydelta")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3 - Number of public services apps, building blocks and dataset transformed into real assets within WeLive MarketPlace (per pilot)", response = Response.class)
	public Response getKPI1_3_delta(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_REMOVED + "\" OR type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.get2DDataset(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE,
					GrayLogsTypes.TYPE_ARTEFACT_REMOVED,
					GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_3 ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/all/materializedrem")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3 - Number of public services apps, building blocks and dataset transformed into real assets within WeLive MarketPlace (per pilot)", response = Response.class)
	public Response getKPI1_3_2(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			if (!pilot.equals("")) {
				pilot = " AND " + GrayLogsTypes.ATRIBUTE_PILOT + ":\"" + pilot
						+ "\"";
			}
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_REMOVED + "\"" + pilot, from,
					to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE, "", withmonths, from,
					to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_3 ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/all/materializeddelta")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3 - Number of public services apps, building blocks and dataset transformed into real assets within WeLive MarketPlace (per pilot)", response = Response.class)
	public Response getKPI1_3_delta(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			if (!pilot.equals("")) {
				pilot = " AND " + GrayLogsTypes.ATRIBUTE_PILOT + ":\"" + pilot
						+ "\"";
			}
			JSONObject resultquery = callQueryFromLoggin("(type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_REMOVED + "\" OR type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\")" + pilot,
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils
					.getHitsPerField(jarrayhits,
							GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE, "",
							GrayLogsTypes.TYPE_ARTEFACT_REMOVED,
							GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED, withmonths,
							from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling getKPI1_3 ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/all/materializedcityrem")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3_2 - Number of public services apps, building blocks and dataset transformed into real assets within WeLive MarketPlace (per pilot)", response = Response.class)
	public Response getKPI1_3_2(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_REMOVED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.get2DDataset(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API ", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/{userid}/materializedcity")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3.1 - Number of public services apps, building blocks and dataset transformed into real assets for an specific user", response = Response.class)
	public Response getKPI1_3_1_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_OWNER_ID + ":\"" + userid
							+ "\" AND  type:\""
							+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\"",
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.get2DDataset(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/search/{userid}/materialized")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.3.1 - Number of public services apps, building blocks and dataset transformed into real assets for an specific user", response = Response.class)
	public Response getKPI1_3_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_OWNER_ID + ":\"" + userid
							+ "\" AND  type:\""
							+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\"",
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE, "", withmonths, from,
					to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/published")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = " KPI1.4 - Number of ideas published  in the WeLive framework", response = Response.class)
	public Response getKPI1_4(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_IDEA_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/removed")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = " KPI1.4 - Number of ideas removed  in the WeLive framework", response = Response.class)
	public Response getKPI1_4_rem(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_IDEA_REMOVED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/delta")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = " KPI1.4 - Number of actual ideas  in the WeLive framework", response = Response.class)
	public Response getKPI1_4_delta(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_IDEA_REMOVED + "\" OR type:\""
					+ GrayLogsTypes.TYPE_IDEA_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "",
					GrayLogsTypes.TYPE_IDEA_REMOVED,
					GrayLogsTypes.TYPE_IDEA_PUBLISHED, withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/idea/{userid}/published")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.4.1 - Number of ideas published in the OIA for a specific user", response = Response.class)
	public Response getKPI1_4_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_AUTHORID + ":\"" + userid
							+ "\" AND type:\""
							+ GrayLogsTypes.TYPE_IDEA_PUBLISHED + "\"", from,
					to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/challengue/published")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = " KPI1.5 - Number of challengues published in the WeLive framework", response = Response.class)
	public Response getKPI1_5(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_CHALLENGE_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/challengue/delta")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = " KPI1.5 - Number of challengues published in the WeLive framework", response = Response.class)
	public Response getKPI1_5_delta(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_CHALLENGE_REMOVED + "\" OR type:\""
					+ GrayLogsTypes.TYPE_CHALLENGE_PUBLISHED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "",
					GrayLogsTypes.TYPE_CHALLENGE_REMOVED,
					GrayLogsTypes.TYPE_CHALLENGE_PUBLISHED, withmonths, from,
					to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/challengue/{userid}/published")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.5.1 - Number of challengues published in the WeLive framework for a specific user", response = Response.class)
	public Response getKPI1_5_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_AUTHORID + ":\"" + userid
							+ "\" AND type:\""
							+ GrayLogsTypes.TYPE_IDEA_PUBLISHED + "\" AND  _exists_: "+GrayLogsTypes.ATRIBUTE_CHALLENGE_ID,
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					"type", "", true, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/needpublished/delta")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.7 -Number of NEEDS (per pilot)", response = Response.class)
	public Response getKPI1_7_delta(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_NEEDPUBLISHED + "\" OR type:\""
					+ GrayLogsTypes.TYPE_NEEDREMOVED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "",
					GrayLogsTypes.TYPE_NEEDREMOVED,
					GrayLogsTypes.TYPE_NEEDPUBLISHED, withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/needpublished/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.7 -Number of NEEDS (per pilot)", response = Response.class)
	public Response getKPI1_7(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_NEEDPUBLISHED + "\"", from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);

			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/oia/needpublished/{userid}/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI1.7 -Number of NEEDS (per pilot)", response = Response.class)
	public Response getKPI1_7_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_AUTHORID + ":\"" + userid
							+ "\" AND type:\""
							+ GrayLogsTypes.TYPE_NEEDPUBLISHED + "\"", from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkt/bb_app/{userid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI3.2.1 - Number of BBs used per each app published by an specific user. The number should be agreggated by App published by a specific user.", response = Response.class)
	public Response getKPI3_2_1(
			@ApiParam(value = "The userID", required = true) @PathParam("userid") String userid,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_OWNER_ID + ":\"" + userid
							+ "\" AND  type:\""
							+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\" AND "
							+ GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
							+ GrayLogsTypes.PUBLIC_SERVICE_APPLICATION + "\"",
					from, to);
			JSONObject result = new JSONObject();
			Map<String, Integer> artefacttypes = new HashMap<String, Integer>();
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			for (int h = 0; h < jarrayhits.length(); h++) {
				JSONObject source = (JSONObject) ((JSONObject) jarrayhits
						.get(h)).get("_source");
				String appId = Utils.getString(source,
						GrayLogsTypes.ATRIBUTE_APP_ID);
				if (appId == null)
					continue;
				if (artefacttypes.containsKey(appId)) {
					Integer value = artefacttypes.get(appId);
					value = value + 1;
					artefacttypes.put(appId, value);
				} else {
					artefacttypes.put(appId, 1);
				}
			}
			for (String key : artefacttypes.keySet()) {
				result.put(key, artefacttypes.get(key));
			}
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/buildingblock/all")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI3.1 - Number of building blocks created with the help of the Open Service framework and Open Data Toolset (for all pilots)", response = Response.class)
	public Response getKPI3_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\" AND "
					+ GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
					+ GrayLogsTypes.BUILDING_BLOCK + "\"", from, to);
			JSONObject result = new JSONObject();

			Map<String, Integer> artefacttypes = new HashMap<String, Integer>();
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			for (int h = 0; h < jarrayhits.length(); h++) {
				JSONObject source = (JSONObject) ((JSONObject) jarrayhits
						.get(h)).get("_source");
				String appId = Utils.getString(source,
						GrayLogsTypes.ATRIBUTE_PILOT);
				if (appId == null)
					continue;
				if (artefacttypes.containsKey(appId)) {
					Integer value = artefacttypes.get(appId);
					value = value + 1;
					artefacttypes.put(appId, value);
				} else {
					artefacttypes.put(appId, 1);
				}
			}
			for (String key : artefacttypes.keySet()) {
				result.put(key, artefacttypes.get(key));
			}
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/buildingblock/delta")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI3.1delta - Number of Actial building blocks created with the help of the Open Service framework and Open Data Toolset (for all pilots)", response = Response.class)
	public Response getKPI3_1delta(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("(type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\" OR type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_REMOVED + "\") AND "
					+ GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
					+ GrayLogsTypes.BUILDING_BLOCK + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "",
					GrayLogsTypes.TYPE_ARTEFACT_REMOVED,
					GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED, false, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/app/personalizable/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI4.1 - Number of apps personalizable to the user context and profile (for all pilots)", response = Response.class)
	public Response getKPI4_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
							+ GrayLogsTypes.PUBLIC_SERVICE_APPLICATION + "\"  "
							+ "AND type:\""
							+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\"",
					from, to);

			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_APP_ID, "", false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/resource/download/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI5.3 - Average downloads of newly generated public service apps ", response = Response.class)
	public Response getKPI5_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			if (!pilot.equals("")) {
				pilot = " AND " + GrayLogsTypes.ATRIBUTE_PILOT + ":\"" + pilot
						+ "\"";
			}
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_APP_DOWNLOAD + "\"" + pilot, from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_CUSTOM_APP_NAME, "", withmonths,
					from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/app/open")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI5.4 - Apps usage (apps launching through the WeLive Player and marketplace) ", response = Response.class)
	public Response getKPI5_4(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			if (!pilot.equals("")) {
				pilot = " AND " + GrayLogsTypes.ATRIBUTE_PILOT + ":\"" + pilot
						+ "\"";
			}
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_APP_OPEN + "\"" + pilot, from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_CUSTOM_APP_NAME, "", withmonths,
					from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/app/commercial/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI6.1 - Number of commercial apps created and promoted through WeLive infrastructure (for all pilots) ", response = Response.class)
	public Response getKPI6_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin(
					GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
							+ GrayLogsTypes.PUBLIC_SERVICE_APPLICATION
							+ "\" AND " + "type:\""
							+ GrayLogsTypes.TYPE_ARTEFACT_PUBLISHED + "\"",
					from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_APP_ID, "", withmonths, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/app/public/ranking")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI12.1 - Ranking of apps taking into account votes received", response = Response.class)
	public Response getKPI12_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_RATED + "\" AND "
					+ GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
					+ GrayLogsTypes.PUBLIC_SERVICE_APPLICATION + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_NAME,
					GrayLogsTypes.ATRIBUTE_RATE, false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/app/buildingbock/ranking")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI12.3 - Ranking of apps taking into account votes received", response = Response.class)
	public Response getKPI12_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_RATED + "\" AND "
					+ GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
					+ GrayLogsTypes.BUILDING_BLOCK + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_NAME,
					GrayLogsTypes.ATRIBUTE_RATE, false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/app/dataset/ranking")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI12.4 - Ranking of apps taking into account votes received", response = Response.class)
	public Response getKPI12_4(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_RATED + "\" AND "
					+ GrayLogsTypes.ATRIBUTE_ARTEFACT_TYPE + ":\""
					+ GrayLogsTypes.DATASET + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_NAME,
					GrayLogsTypes.ATRIBUTE_RATE, false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/app/ranking")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI12.1 - Ranking of apps taking into account votes received", response = Response.class)
	public Response getKPI12_1_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_ARTEFACT_RATED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_ARTEFACT_NAME,
					GrayLogsTypes.ATRIBUTE_RATE, false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/idea/ranking")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI12.2 - Ranking of Ideas taking into account votes received", response = Response.class)
	public Response getKPI12_2(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_IDEA_RATED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_IDEA_TITLE,
					GrayLogsTypes.ATRIBUTE_RATE, false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/mkp/need/ranking")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI12.6 - Ranking of Ideas taking into account votes received", response = Response.class)
	public Response getKPI12_6(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_NEED_RATED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_NEED_TITLE,
					GrayLogsTypes.ATRIBUTE_RATE, false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/controler/component/usage")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI13.1 - Usage of WeLive framework components", response = Response.class)
	public Response getKPI13_1(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_COMPONENT_SELECTED + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_COMPONENT_NAME, "", false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/controler/cities/usage")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI13.2 - Cities Selection in WeLive Controller", response = Response.class)
	public Response getKPI13_2(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_CITY_SELECTED + "\""
					+ GrayLogsTypes.PILOT_FILTER, from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_PILOT, "", false, "", "");
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/controler/user/contact")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "KPI13.3 - User contact through teh contact form", response = Response.class)
	public Response getKPI13_3(
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to)
			throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_CONTACT_FORM + "\"", from, to);
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils.getHitsPerField(jarrayhits,
					GrayLogsTypes.ATRIBUTE_YES, "", true, from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/common/appkpi/{kpi}/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "CommonKPI DATA", response = Response.class)
	public Response getKPIcommon(
			@ApiParam(value = "The Kpi", required = true) @PathParam("kpi") String kpi,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_QUESTIONNAIRE + "\" AND custom_kpi:\""
					+ kpi + "\"" + " " + GrayLogsTypes.APPNAME_FILTER, from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils
					.getHitsPerField(jarrayhits,
							GrayLogsTypes.ATRIBUTE_CUSTOM_APP, "", withmonths,
							from, to);
			JSONObject resultappid = null;
			try {
				resultappid =Utils.getHitsPerField(jarrayhits,
								GrayLogsTypes.ATRIBUTE_APP_ID, "", withmonths,
								from, to);			
				Iterator itkeys = resultappid.keys();
				while(itkeys.hasNext()) {
					String key = (String)itkeys.next();
					//appId:\"sportit\" OR appId:\"helsinkicitymuseums\" OR appId:\"helsinkicitybikes\"
					if (key.equals("sportit") || key.equals("helsinkicitymuseums") || key.equals("helsinkicitybikes")){
						result.append(key, resultappid.getInt(key));
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/common/appkpi2d/{kpi}/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "CommonKPI DATA", response = Response.class)
	public Response getKPIcommon2d(
			@ApiParam(value = "The Kpi", required = true) @PathParam("kpi") String kpi,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_QUESTIONNAIRE + "\" AND custom_kpi:\""
					+ kpi + "\"" + " " + GrayLogsTypes.APPNAME_FILTER, from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			
			JSONObject result = Utils.get2DDataset(jarrayhits,
					GrayLogsTypes.ATRIBUTE_CUSTOM_APP,
					GrayLogsTypes.ATRIBUTE_CUSTOM_VALUE);

			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/common/appkpi/{kpi}/pilot")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "CommonKPI DATA", response = Response.class)
	public Response getKPIcommonPilot(
			@ApiParam(value = "The Kpi", required = true) @PathParam("kpi") String kpi,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			if (pilot != null) {
				pilot = " AND custom_pilot: " + pilot;
			} else {
				pilot = "";
			}
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_QUESTIONNAIRE + "\" AND custom_kpi:\""
					+ kpi + "\"" + pilot + " " + GrayLogsTypes.APPNAME_FILTER,
					from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils
					.getHitsPerField(jarrayhits,
							GrayLogsTypes.ATRIBUTE_CUSTOM_APP, "", withmonths,
							from, to);
			JSONObject resultappid = null;
			try {
				resultappid =Utils.getHitsPerField(jarrayhits,
								GrayLogsTypes.ATRIBUTE_APP_ID, "", withmonths,
								from, to);			
				Iterator itkeys = resultappid.keys();
				while(itkeys.hasNext()) {
					String key = (String)itkeys.next();
					//appId:\"sportit\" OR appId:\"helsinkicitymuseums\" OR appId:\"helsinkicitybikes\"
					if (key.equals("sportit") || key.equals("helsinkicitymuseums") || key.equals("helsinkicitybikes")){
						result.append(key, resultappid.getInt(key));
					}
				}
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/common/appkpi2d/{kpi}/pilot")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "CommonKPI DATA", response = Response.class)
	public Response getKPIcommon2dPilot(
			@ApiParam(value = "The Kpi", required = true) @PathParam("kpi") String kpi,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Pilot name", required = false) @DefaultValue("") @QueryParam(value = "pilot") String pilot,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			if (pilot != null) {
				pilot = " AND custom_pilot: " + pilot;
			} else {
				pilot = "";
			}
			JSONObject resultquery = callQueryFromLoggin("type:\""
					+ GrayLogsTypes.TYPE_QUESTIONNAIRE + "\" AND custom_kpi:\""
					+ kpi + "\"" + pilot + " " + GrayLogsTypes.APPNAME_FILTER,
					from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			//System.out.println(jarrayhits.toString());
			JSONObject result = Utils.get2DDataset(jarrayhits,
					GrayLogsTypes.ATRIBUTE_CUSTOM_APP,
					GrayLogsTypes.ATRIBUTE_CUSTOM_VALUE);
			//System.out.println(result.toString());
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}

	@GET
	@Timed 
	@ExceptionMetered	
	@Path("/apps/appkpi/count")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "App KPI DATA", response = Response.class)
	public Response getKPIApp(
			@ApiParam(value = "kpi type", required = true) @QueryParam("type") String type,
			@ApiParam(value = "app name", required = true) @QueryParam("app") String app,
			@ApiParam(value = "custom query", required = false) @QueryParam("custom") String custom,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "from") String from,
			@ApiParam(value = "Date to in yyyy-MM-dd HH:mm:ss.SSS format", required = false) @DefaultValue("") @QueryParam(value = "to") String to,
			@ApiParam(value = "Get the data split by month", required = false) @DefaultValue("false") @QueryParam(value = "withmonths") boolean withmonths

	) throws WeLiveException {
		try {
			String greylogtype= GrayLogsTypes.ATRIBUTE_CUSTOM_APP;
			if (app.equals("helsinkicitybikes") || app.equals("helsinkicitymuseums") || app.equals("sportit") ){
				greylogtype =GrayLogsTypes.ATRIBUTE_APP_ID;
			}
			String query = "type:\"" + type + "\" AND "
					+ greylogtype + ":\"" + app + "\" "
					+ custom;
			JSONObject resultquery = callQueryFromLoggin(query, from, to);// "DatasetID:\"*\" AND CityName:\"*\" AND type:\"DatasetPublished\""
			JSONObject jsonhits = (JSONObject) resultquery.get("hits");
			JSONArray jarrayhits = jsonhits.getJSONArray("hits");
			JSONObject result = Utils
					.getHitsPerField(jarrayhits,
							greylogtype, "", withmonths,
							from, to);
			return Response.ok(result.toString(), MediaType.APPLICATION_JSON)
					.build();
		} catch (Exception e) {
			throw new WeLiveException(Status.BAD_REQUEST,"Error calling ADS-API", e);
		}
	}
}
