/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ads;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
//import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class Utils {
	public static enum Months {
		JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC;
		public static int getMontIndex(String month) {
			switch (month) {
			case "JAN":
				return 0;
			case "FEB":
				return 1;
			case "MAR":
				return 2;
			case "APR":
				return 3;
			case "MAY":
				return 4;
			case "JUN":
				return 5;
			case "JUL":
				return 6;
			case "AUG":
				return 7;
			case "SEP":
				return 8;
			case "OCT":
				return 9;
			case "NOV":
				return 10;
			case "DEC":
				return 11;
			default:
				return 0;

			}
		}
	}

	/*
	 * APR-2016 comparador para que el SortedMap devuelva las fechas ordenadas.
	 * 0 --> iguales a.compareTo(b) < 0 // a < b
	 * 
	 * a.compareTo(b) > 0 // a > b
	 * 
	 * a.compareTo(b) == 0 // a == b
	 */
	private static Comparator<String> dateComparator = new Comparator<String>() {
		@Override
		public int compare(String s1, String s2) {
			if (s1.equals(s2))
				return 0;
			String syears1 = s1.substring(s1.indexOf("-") + 1);
			int years1 = Integer.parseInt(syears1);
			String syears2 = s2.substring(s2.indexOf("-") + 1);
			int years2 = Integer.parseInt(syears2);

			if (years1 < years2)
				return -1;
			if (years1 > years2)
				return 1;

			String smonthyear1 = s1.substring(0, s1.indexOf("-"));
			int monthyears1 = Months.getMontIndex(smonthyear1);
			String smonthyear2 = s2.substring(0, s2.indexOf("-"));
			int monthyears2 = Months.getMontIndex(smonthyear2);
			if (monthyears1 < monthyears2)
				return -1;
			if (monthyears1 > monthyears2)
				return 1;

			return s1.compareTo(s2);
		}
	};
	/**
	 * Chapu por el tema de las apps nuevas de Helsinki
	 * @param object
	 * @param key
	 * @return
	 */
	public static String getStringTwoKeys(JSONObject object, String key1, String key2) {
		if (object.has(key1)) {
			Object obj = object.get(key1);
			return obj.toString();

			// return object.getStrin//g(key);
		} else if (object.has(key2)) {
			Object obj = object.get(key2);
			return obj.toString();

		} else {
			System.out.println("Key:" + key1 + " - "+key2+" Not found in object:"
					+ object.toString());
			return null;
		}
	}
	public static String getString(JSONObject object, String key) {
		if (object.has(key)) {
			Object obj = object.get(key);
			return obj.toString();

			// return object.getStrin//g(key);
		} else {
			System.out.println("Key:" + key + " Not found in object:"
					+ object.toString());
			return null;
		}
	}

	public static String getString(JSONObject object, String key, String value) {
		if (object.has(key)) {
			Object obj = object.get(key);
			if (obj.toString().equals(value)) {
				return obj.toString();
			} else {
				return null;
			}

			// return object.getStrin//g(key);
		} else {
			System.out.println("Key:" + key + " Not found in object:"
					+ object.toString());
			return null;
		}
	}

	public static int getInt(JSONObject object, String key) {
		try {
			if (object.has(key)) {
				return object.getInt(key);
			} else {
				System.out.println("Key:" + key + " Not found in object:"
						+ object.toString());
				return -1;
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return -1;
	}

	private static SortedMap<String, Integer> fillMonths(String from, String to)
			throws ParseException {
		SortedMap<String, Integer> retorno = new TreeMap<String, Integer>(
				dateComparator);
		if (from == null)
			from = "2016-01-01 00:00:00.000";
		if (to == null)
			to = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.SSS");
		Calendar cal = Calendar.getInstance();
		int startYear = 0;
		int endYear = 0;
		Date startDate = new Date();
		Date endDate = new Date();

		if (from.equals("") == false) {
			startDate = dateFormat.parse(from);

		}
		if (to.equals("") == false) {
			endDate = dateFormat.parse(to);

		}
		cal.setTime(startDate);
		startYear = cal.get(Calendar.YEAR);
		cal.setTime(endDate);
		endYear = cal.get(Calendar.YEAR);
		System.out.println("startY: " + startYear + " EndY: " + endYear);
		for (int currYear = startYear; startYear < endYear; currYear++) {
			for (int month = 0; month < Utils.Months.values().length; month++) {
				String key = Utils.Months.values()[month] + "-" + currYear;
				System.out.println("nuevo mes: " + key);
				retorno.put(key, 0);
			}
		}
		return retorno;
	}
/*
	public static JSONObject getStackedHitsPerField(JSONArray jarrayhits,
			String field, String fieldtostack) throws ParseException {
		Map<String, Map<String, Integer>> fieldstacked = new HashMap<String, Map<String, Integer>>();
		JSONObject result = new JSONObject();
		for (int h = 0; h < jarrayhits.length(); h++) {
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(h))
					.get("_source");
			String fieldkey = Utils.getString(source, field);
			if (fieldkey == null)
				continue;
			String fieldstack = Utils.getString(source, fieldtostack);
			// if (custom_Type == null)
			// Utils.getString(source,GrayLogsTypes.ATRIBUTE_TYPE);
			if (fieldstack == null)
				continue;
			if (fieldstacked.containsKey(fieldkey)) {
				Map<String, Integer> citydatasettypes = fieldstacked
						.get(fieldkey);
				boolean added = false;
				for (String key : citydatasettypes.keySet()) {
					if (key.equals(fieldstack)) {
						Integer value = citydatasettypes.get(key);
						value = value + 1;
						citydatasettypes.put(key, value);
						added = true;
					}
				}
				if (!added) {
					citydatasettypes.put(fieldstack, 1);
				}

			} else {
				Map<String, Integer> citydatasettypes = new HashMap<String, Integer>();
				citydatasettypes.put(fieldstack, 1);
				fieldstacked.put(fieldkey, citydatasettypes);

			}
		}
		for (String key : fieldstacked.keySet()) {
			Map<String, Integer> citydatasettypes = fieldstacked.get(key);
			JSONObject jsondatsetcity = new JSONObject();
			for (String dataset : citydatasettypes.keySet()) {

				jsondatsetcity.put(dataset, citydatasettypes.get(dataset));

			}
			result.put(key, jsondatsetcity);

		}
		return result;
	}
*/
	public static JSONObject get2DDataset(JSONArray jarrayhits,
			String firstDim, String secondDim) throws ParseException {
		JSONObject result = new JSONObject();
		JSONArray labels = new JSONArray();
		JSONArray datasets = new JSONArray();
		Map<String, String> searcher = new HashMap<String, String>();
		for (int i = 0; i < jarrayhits.length(); i++) {// buscar los labels
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(i))
					.get("_source");
			String fieldkey = null;// Utils.getString(source, firstDim);
			if (firstDim != null && firstDim.equals(GrayLogsTypes.ATRIBUTE_CUSTOM_APP)){
				fieldkey = Utils.getStringTwoKeys(source, firstDim,GrayLogsTypes.ATRIBUTE_APP_ID);
			}
			else{
				fieldkey = Utils.getString(source, firstDim);
			}
			//String fieldkey = Utils.getString(source, firstDim);
			if (fieldkey != null) {
				if (searcher.get(fieldkey) == null) {
					searcher.put(fieldkey, fieldkey);
					labels.put(fieldkey);
				}
			}

		}
		searcher.clear();
		for (int i = 0; i < jarrayhits.length(); i++) {// buscar los datasets
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(i))
					.get("_source");
			String fieldkey = Utils.getString(source, secondDim);
			if (fieldkey != null) {
				if (searcher.get(fieldkey) == null) {
					searcher.put(fieldkey, fieldkey);
					JSONObject dataset = new JSONObject();
					JSONArray values = new JSONArray();
					for (int j = 0; j < labels.length(); j++) {
						values.put(0);
					}
					dataset.put("label", fieldkey);
					dataset.put("values", values);
					datasets.put(dataset);
				}
			}

		}

		for (int i = 0; i < jarrayhits.length(); i++) {// llenar los datasets
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(i))
					.get("_source");
			
			String fieldkey1 = null;// Utils.getString(source, firstDim);
			if (firstDim != null && firstDim.equals(GrayLogsTypes.ATRIBUTE_CUSTOM_APP)){
				fieldkey1 = Utils.getStringTwoKeys(source, firstDim,GrayLogsTypes.ATRIBUTE_APP_ID);
			}
			else{
				fieldkey1 = Utils.getString(source, firstDim);
			}
				
			//String fieldkey1 = Utils.getString(source, firstDim);
			String fieldkey2 = Utils.getString(source, secondDim);
			if (fieldkey1 != null && fieldkey2 != null) {
				JSONObject tempobj = (JSONObject) datasets.get(getArrPosition(
						datasets, "label", fieldkey2));
				JSONArray temparr = (JSONArray) tempobj.getJSONArray("values");
				int tempvalue = temparr
						.getInt(getArrPosition(labels, fieldkey1));
				temparr.put(getArrPosition(labels, fieldkey1), tempvalue + 1);
			}

		}

		result.put("labels", labels);
		result.put("datasets", datasets);
		return result;
	}

	public static JSONObject get2DDataset(JSONArray jarrayhits,
			String firstDim, String secondDim, String fieldtosubstract,
			String fieldtoadd) throws ParseException {
		JSONObject result = new JSONObject();
		JSONArray labels = new JSONArray();
		JSONArray datasets = new JSONArray();
		Map<String, String> searcher = new HashMap<String, String>();
		for (int i = 0; i < jarrayhits.length(); i++) {// buscar los labels
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(i))
					.get("_source");
			String fieldkey = Utils.getString(source, firstDim);
			if (fieldkey != null) {
				if (searcher.get(fieldkey) == null) {
					searcher.put(fieldkey, fieldkey);
					labels.put(fieldkey);
				}
			}

		}
		searcher.clear();
		for (int i = 0; i < jarrayhits.length(); i++) {// buscar los datasets
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(i))
					.get("_source");
			String fieldkey = Utils.getString(source, secondDim);
			if (fieldkey != null) {
				if (searcher.get(fieldkey) == null) {
					searcher.put(fieldkey, fieldkey);
					JSONObject dataset = new JSONObject();
					JSONArray values = new JSONArray();
					for (int j = 0; j < labels.length(); j++) {
						values.put(0);
					}
					dataset.put("label", fieldkey);
					dataset.put("values", values);
					datasets.put(dataset);
				}
			}

		}

		for (int i = 0; i < jarrayhits.length(); i++) {// llenar los datasets
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(i))
					.get("_source");
			String fieldkey1 = Utils.getString(source, firstDim);
			String fieldkey2 = Utils.getString(source, secondDim);
			if (fieldkey1 != null && fieldkey2 != null) {
				JSONObject tempobj = (JSONObject) datasets.get(getArrPosition(
						datasets, "label", fieldkey2));
				JSONArray temparr = (JSONArray) tempobj.getJSONArray("values");
				int tempvalue = temparr
						.getInt(getArrPosition(labels, fieldkey1));
				if (Utils.getString(source, "type", fieldtosubstract) != null) {
					temparr.put(getArrPosition(labels, fieldkey1),
							tempvalue - 1);
				}
				if (Utils.getString(source, "type", fieldtoadd) != null) {
					temparr.put(getArrPosition(labels, fieldkey1),
							tempvalue + 1);
				}
			}

		}

		result.put("labels", labels);
		result.put("datasets", datasets);
		return result;
	}

	private static int getArrPosition(JSONArray arr, String obj) {
		for (int i = 0; i < arr.length(); i++) {
			if (arr.getString(i).equals(obj))
				return i;
		}

		return -1;
	}

	private static int getArrPosition(JSONArray arr, String field, String obj) {
		for (int i = 0; i < arr.length(); i++) {
			JSONObject temp = (JSONObject) arr.get(i);
			if (temp.getString(field).equals(obj))
				return i;
		}

		return -1;
	}

	/**
	 * Esta funcion agrupa en un json los diferentes valores que toma el campo
	 * field dentro de jarrayhits, si el campo fieldtoaccumulate es diferente de
	 * "" en lugar de sumar 1 suma el valos de ese campo se usa para sumar el
	 * rating.
	 * 
	 * @param jarrayhits
	 * @param field
	 * @param fieldtoaccumulate
	 * @param withmonths
	 * @param from
	 * @param to
	 * @return
	 * @throws ParseException
	 */
	public static JSONObject getHitsPerField(JSONArray jarrayhits,
			String field, String fieldtoaccumulate, boolean withmonths,
			String from, String to) throws ParseException {

		SortedMap<String, Integer> fielduses = new TreeMap<String, Integer>();
		JSONObject result = new JSONObject();

		SortedMap<String, SortedMap<String, Integer>> usespermonth = new TreeMap<String, SortedMap<String, Integer>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.SSS");
		Calendar cal = Calendar.getInstance();

		for (int h = 0; h < jarrayhits.length(); h++) {
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(h))
					.get("_source");
			// El valor de field dentro de esta iteracion.
			String fieldkey = Utils.getString(source, field);
			if (fieldkey == null)
				continue;
			int fieldacumulate = -1;
			if (fieldtoaccumulate != null && !fieldtoaccumulate.equals("")) {
				fieldacumulate = Utils.getInt(source, fieldtoaccumulate);
			}
			// Si el campo que queremos agrupar ya ha aparecido antes.
			if (fielduses.containsKey(fieldkey)) {
				Integer value = fielduses.get(fieldkey);
				if (fieldacumulate != -1) {
					value = value + fieldacumulate;
				} else {
					value = value + 1;
				}
				fielduses.put(fieldkey, value);
				if (withmonths) {
					// Si ademas de que ya aparecido , ha aparecido tambien en
					// este mes.
					if (usespermonth.containsKey(fieldkey)) {
						SortedMap<String, Integer> artpermonthuses = usespermonth
								.get(fieldkey);
						String timestamp = Utils.getString(source, "timestamp");
						if (timestamp == null)
							continue;

						Date dateObj = dateFormat.parse(timestamp);
						cal.setTime(dateObj);
						String month = Utils.Months.values()[cal
								.get(Calendar.MONTH)]
								+ "-"
								+ cal.get(Calendar.YEAR);
						if (artpermonthuses.containsKey(month)) {
							Integer monthvalue = artpermonthuses.get(month);
							if (fieldacumulate != -1) {
								monthvalue = monthvalue + fieldacumulate;
							} else {
								monthvalue = monthvalue + 1;
							}
							artpermonthuses.put(month, monthvalue);

						} else {
							if (fieldacumulate != -1) {
								artpermonthuses.put(month, fieldacumulate);
							} else {
								artpermonthuses.put(month, 1);
							}

						}

					}
					// Ha aparecido pero por primera vez en este mes.
					else {

						SortedMap<String, Integer> artpermonthuses = new TreeMap<String, Integer>(
								dateComparator);

						String timestamp = Utils.getString(source, "timestamp");
						if (timestamp == null)
							continue;
						Date dateObj = dateFormat.parse(timestamp);
						cal.setTime(dateObj);
						String month = Utils.Months.values()[cal
								.get(Calendar.MONTH)]
								+ "-"
								+ cal.get(Calendar.YEAR);
						int monthvalue = 1;
						if (fieldacumulate != -1) {
							monthvalue = fieldacumulate;
						}

						artpermonthuses.put(month, monthvalue);
						usespermonth.put(fieldkey, artpermonthuses);

					}

				}

			}
			// No ha aparecido antes.
			else {
				if (fieldacumulate != -1) {
					fielduses.put(fieldkey, fieldacumulate);
					;
				} else {
					fielduses.put(fieldkey, 1);
				}
				if (withmonths) {
					// Si no ha aparecido antes,...pues es la primera vez que
					// aparece en este mes.
					// TODO rellenar de alguna manera los meses entre from y to.
					// TODO Cual es el from si viene en blanco? No puedo mapear
					// desde 1970!!
					SortedMap<String, Integer> artpermonthuses = fillMonths(
							from, to);

					String timestamp = Utils.getString(source, "timestamp");
					if (timestamp == null)
						continue;
					Date dateObj = dateFormat.parse(timestamp);
					cal.setTime(dateObj);
					String month = Utils.Months.values()[cal
							.get(Calendar.MONTH)]
							+ "-"
							+ cal.get(Calendar.YEAR);
					if (fieldacumulate != -1) {
						artpermonthuses.put(month, fieldacumulate);
						;
					} else {
						artpermonthuses.put(month, 1);
					}
					usespermonth.put(fieldkey, artpermonthuses);

				}

			}
		}
		for (String key : fielduses.keySet()) {

			result.put(key, fielduses.get(key));

		}

		if (withmonths) {
			JSONObject monthscity = new JSONObject();
			for (String key : usespermonth.keySet()) {
				SortedMap<String, Integer> datasetspermonthcities = usespermonth
						.get(key);
				JSONArray jsonmonthscity = new JSONArray();
				System.out.println(datasetspermonthcities.toString());
				while (!datasetspermonthcities.isEmpty()) {
					String keymonth = datasetspermonthcities.firstKey();
					System.out.println(keymonth);
					jsonmonthscity.put(new JSONObject().put(keymonth,
							datasetspermonthcities.get(keymonth)));
					datasetspermonthcities.remove(keymonth);
				}
				System.out.println(jsonmonthscity.toString());
				monthscity.put(key, jsonmonthscity);
				System.out.println(monthscity.toString());
			}
			result.put("date", monthscity);
		}
		return result;
	}

	public static JSONObject getHitsPerField(JSONArray jarrayhits,
			String field, String fieldtoaccumulate, String fieldtosubstract,
			String fieldtoadd, boolean withmonths, String from, String to)
			throws ParseException {

		SortedMap<String, Integer> fielduses = new TreeMap<String, Integer>();
		JSONObject result = new JSONObject();

		SortedMap<String, SortedMap<String, Integer>> usespermonth = new TreeMap<String, SortedMap<String, Integer>>();
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss.SSS");
		Calendar cal = Calendar.getInstance();

		for (int h = 0; h < jarrayhits.length(); h++) {
			JSONObject source = (JSONObject) ((JSONObject) jarrayhits.get(h))
					.get("_source");
			// El valor de field dentro de esta iteracion.
			String fieldkey = Utils.getString(source, field);
			if (fieldkey == null)
				continue;
			int fieldacumulate = -1;
			if (fieldtoaccumulate != null && !fieldtoaccumulate.equals("")) {
				fieldacumulate = Utils.getInt(source, fieldtoaccumulate);
			}
			// Si el campo que queremos agrupar ya ha aparecido antes.
			if (fielduses.containsKey(fieldkey)) {
				Integer value = fielduses.get(fieldkey);
				if (fieldacumulate != -1) {
					if (Utils.getString(source, "type", fieldtoadd) != null) {
						value = value + fieldacumulate;
					}
					if (Utils.getString(source, "type", fieldtosubstract) != null) {
						value = value - fieldacumulate;
					}

				} else {
					if (Utils.getString(source, "type", fieldtoadd) != null) {
						value = value + 1;
					}
					if (Utils.getString(source, "type", fieldtosubstract) != null) {
						value = value - 1;
					}
				}
				fielduses.put(fieldkey, value);
				if (withmonths) {
					// Si ademas de que ya aparecido , ha aparecido tambien en
					// este mes.
					if (usespermonth.containsKey(fieldkey)) {
						SortedMap<String, Integer> artpermonthuses = usespermonth
								.get(fieldkey);
						String timestamp = Utils.getString(source, "timestamp");
						if (timestamp == null)
							continue;

						Date dateObj = dateFormat.parse(timestamp);
						cal.setTime(dateObj);
						String month = Utils.Months.values()[cal
								.get(Calendar.MONTH)]
								+ "-"
								+ cal.get(Calendar.YEAR);
						if (artpermonthuses.containsKey(month)) {
							Integer monthvalue = artpermonthuses.get(month);
							if (fieldacumulate != -1) {
								monthvalue = monthvalue + fieldacumulate;
							} else {
								monthvalue = monthvalue + 1;
							}
							artpermonthuses.put(month, monthvalue);

						} else {
							if (fieldacumulate != -1) {
								artpermonthuses.put(month, fieldacumulate);
							} else {
								artpermonthuses.put(month, 1);
							}

						}

					}
					// Ha aparecido pero por primera vez en este mes.
					else {

						SortedMap<String, Integer> artpermonthuses = new TreeMap<String, Integer>(
								dateComparator);

						String timestamp = Utils.getString(source, "timestamp");
						if (timestamp == null)
							continue;
						Date dateObj = dateFormat.parse(timestamp);
						cal.setTime(dateObj);
						String month = Utils.Months.values()[cal
								.get(Calendar.MONTH)]
								+ "-"
								+ cal.get(Calendar.YEAR);
						int monthvalue = 1;
						if (fieldacumulate != -1) {
							monthvalue = fieldacumulate;
						}

						artpermonthuses.put(month, monthvalue);
						usespermonth.put(fieldkey, artpermonthuses);

					}

				}

			}
			// No ha aparecido antes.
			else {
				if (fieldacumulate != -1) {
					if (Utils.getString(source, "type", fieldtoadd) != null) {
						fielduses.put(fieldkey, fieldacumulate);
					}
					if (Utils.getString(source, "type", fieldtosubstract) != null) {
						fielduses.put(fieldkey, fieldacumulate);
					}
				} else {
					if (Utils.getString(source, "type", fieldtoadd) != null) {
						fielduses.put(fieldkey, 1);
					}
					if (Utils.getString(source, "type", fieldtosubstract) != null) {
						fielduses.put(fieldkey, -1);
					}
				}
				if (withmonths) {
					// Si no ha aparecido antes,...pues es la primera vez que
					// aparece en este mes.
					// TODO rellenar de alguna manera los meses entre from y to.
					// TODO Cual es el from si viene en blanco? No puedo mapear
					// desde 1970!!
					SortedMap<String, Integer> artpermonthuses = fillMonths(
							from, to);

					String timestamp = Utils.getString(source, "timestamp");
					if (timestamp == null)
						continue;
					Date dateObj = dateFormat.parse(timestamp);
					cal.setTime(dateObj);
					String month = Utils.Months.values()[cal
							.get(Calendar.MONTH)]
							+ "-"
							+ cal.get(Calendar.YEAR);
					if (fieldacumulate != -1) {
						artpermonthuses.put(month, fieldacumulate);
						;
					} else {
						artpermonthuses.put(month, 1);
					}
					usespermonth.put(fieldkey, artpermonthuses);

				}

			}
		}
		for (String key : fielduses.keySet()) {

			result.put(key, fielduses.get(key));

		}

		if (withmonths) {
			JSONObject monthscity = new JSONObject();
			for (String key : usespermonth.keySet()) {
				SortedMap<String, Integer> datasetspermonthcities = usespermonth
						.get(key);
				JSONArray jsonmonthscity = new JSONArray();
				System.out.println(datasetspermonthcities.toString());
				while (!datasetspermonthcities.isEmpty()) {
					String keymonth = datasetspermonthcities.firstKey();
					System.out.println(keymonth);
					jsonmonthscity.put(new JSONObject().put(keymonth,
							datasetspermonthcities.get(keymonth)));
					datasetspermonthcities.remove(keymonth);
				}
				System.out.println(jsonmonthscity.toString());
				monthscity.put(key, jsonmonthscity);
				System.out.println(monthscity.toString());
			}
			result.put("date", monthscity);
		}
		return result;
	}

}
