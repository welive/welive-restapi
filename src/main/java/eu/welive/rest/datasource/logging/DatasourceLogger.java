/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.logging;

import java.net.URISyntaxException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import eu.welive.rest.logging.LoggingService;

public class DatasourceLogger {
	
	private static final Logger logger = LogManager.getLogger(DatasourceLogger.class);
	
	public static final String LOGGING_SERVICE_NAME = "ods";
	
	public static final String RESOURCE_ID_KEY = "resourceid";
	public static final String QUERY_KEY = "query";
	public static final String AUTHOR_ID_KEY = "authorid";
	
	private final LoggingService loggingService;
	
	public DatasourceLogger() {
		loggingService = LoggingService.getInstance();
	}
	
	protected DatasourceLogger(LoggingService loggingService) {
		this.loggingService = loggingService;
	}

	public void logQuery(String resource, String sql, String ccUserId) {
		final JSONObject payload = loggingService.createMainPayload("Resource queried", LOGGING_SERVICE_NAME, "ResourceQueried");
		
		final JSONObject customAttributes = new JSONObject();
		customAttributes.put(RESOURCE_ID_KEY, resource);
		customAttributes.put(QUERY_KEY, sql);
		customAttributes.put(AUTHOR_ID_KEY, ccUserId);
		payload.put(LoggingService.CUSTOM_ATTR_KEY, customAttributes);
		
		try {
			loggingService.sendLog(LOGGING_SERVICE_NAME, payload);
		} catch (URISyntaxException e) {
			logger.error("Could not send log to WeLive logging component", e);
		}
	}
	
	public void logUpdate(String resource, String ccUserId) {
		final JSONObject payload = loggingService.createMainPayload("Resource update", LOGGING_SERVICE_NAME, "ResourceUpdated");
		
		final JSONObject customAttributes = new JSONObject();
		customAttributes.put(RESOURCE_ID_KEY, resource);
		customAttributes.put(AUTHOR_ID_KEY, ccUserId);
		payload.put(LoggingService.CUSTOM_ATTR_KEY, customAttributes);
		
		try {
			loggingService.sendLog(LOGGING_SERVICE_NAME, payload);
		} catch (URISyntaxException e) {
			logger.error("Could not send log to WeLive logging component", e);
		}
	}
}
