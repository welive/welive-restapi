/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.remover;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import eu.iescities.server.querymapper.config.Config;

public class UserRemover {
	
	private static final String PENDING_REMOVAL_FILE = "pending_removal.txt";
	
	private static UserRemover instance = null;
	
	private final Set<PendingUser> pendingUsers = new HashSet<PendingUser>();
	
	private String dataFilePath;

	private UserRemover() throws UserRemoverException {
		setDataDir(Config.getInstance().getDataDir());
		loadData();
	}
	
	public synchronized static UserRemover getInstance() throws UserRemoverException {
		if (instance == null) {
			instance = new UserRemover();
		}
		return instance; 
	}
	
	public synchronized boolean clearData() {
		boolean removed = false;
		final File dataFile = new File(dataFilePath);
		if (dataFile.exists()) {
			removed = dataFile.delete();
		}
		
		if (removed) {
			pendingUsers.clear();
		}
		
		return removed;
	}
	
	public synchronized void addPendingUser(String userId, boolean removeData) throws UserRemoverException {
		pendingUsers.add(new PendingUser(userId, removeData));
		saveData();
	}
	
	public synchronized void removePendingUser(String userId) throws UserRemoverException {
		pendingUsers.remove(new PendingUser(userId, false));
		saveData();
	}
	
	public synchronized Set<PendingUser> getPendingUsers() {
		return Collections.unmodifiableSet(pendingUsers);
	}
	
	public synchronized Set<PendingUser> getAnonymizedUsers() {
		final Set<PendingUser> anonUsers = new HashSet<PendingUser>();
		for (PendingUser user : pendingUsers) {
			if (!user.isDataRemoved()) {
				anonUsers.add(user);
			}
		}
		
		return anonUsers;
	}
	
	public synchronized Set<PendingUser> getErasedUsers() {
		final Set<PendingUser> erased = new HashSet<PendingUser>();
		for (PendingUser user : pendingUsers) {
			if (user.isDataRemoved()) {
				erased.add(user);
			}
		}
		
		return erased;
	}
	
	private void loadData() throws UserRemoverException {
		final File dataFile = new File(dataFilePath);
		if (dataFile.exists()) {			
			try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(dataFilePath), StandardCharsets.UTF_8))) {
				final String line = br.readLine();
				if (line != null) {
					final String[] users = line.split(",");
					for (String userStr : users) {
						final PendingUser pendingUser = new PendingUser();
						pendingUser.loadString(userStr);
						pendingUsers.add(pendingUser);
					}
				}
			} catch (IOException e) {
				throw new UserRemoverException(e);
			}
		}
	}
	
	private void saveData() throws UserRemoverException {
		final String data = StringUtils.join(pendingUsers, ",");
		
		try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(dataFilePath), StandardCharsets.UTF_8)) {
			writer.write(data);
		} catch (IOException e) {
			throw new UserRemoverException(e);
		}
	}

	public void setDataDir(String dir) {
		this.dataFilePath = dir + File.separatorChar + PENDING_REMOVAL_FILE;
	}
}
