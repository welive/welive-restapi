/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.remover;

public class PendingUser {

	private String userId;
	private boolean removeData;
	
	public PendingUser() {
		
	}
	
	public PendingUser(String user, boolean removeData) {
		this.userId = user;
		this.removeData = removeData;
	}
	
	public void loadString(String strValue) {
		final String[] data = strValue.split(":");
		this.userId = data[0];
		this.removeData = Boolean.parseBoolean(data[1]);
	}

	public String getUserId() {
		return userId;
	}

	public boolean isDataRemoved() {
		return removeData;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof PendingUser)) {
			return false;
		} else {
			final PendingUser u = (PendingUser)o;
			return u.getUserId().equals(this.getUserId());
		}
	}
	
	@Override
	public String toString() {
		return userId + ":" + removeData;
	}

	@Override
	public int hashCode() {
		return userId.hashCode();
	}
}
