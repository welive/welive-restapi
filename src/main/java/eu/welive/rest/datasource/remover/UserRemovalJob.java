/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.remover;

import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceInfo.TableInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactory;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactoryException;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANPackage;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANConnectionException;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

public class UserRemovalJob implements Job {
	
	private static final Logger logger = LogManager.getLogger(UserRemovalJob.class);

	@Override
	public void execute(JobExecutionContext jobContext) throws JobExecutionException {
		try {
			final String deleteClause = getDeleteClause();
			final String updateClause = getUpdateClause();
			
			if (!updateClause.isEmpty() || !deleteClause.isEmpty()) {
				final CKANUtil ckanUtil = new CKANUtil();
				for (String packageName : ckanUtil.getAllPackages()) {
					final CKANPackage packageInfo = ckanUtil.getPackageInfo(packageName);
					for (CKANResource resource : packageInfo.getResources()) {
						processResource(deleteClause, updateClause, resource);
					}
				}
			}
		} catch (CKANUtilException | CKANConnectionException e) {
			logger.error("Problem processing packages.", e);
			throw new JobExecutionException(e);
		} catch (UserRemoverException e) {
			logger.error("Problem obtaining user removals from file.", e);
			throw new JobExecutionException(e);
		}
	}

	private void processResource(String deleteClause, String updateClause, CKANResource resource) {
		if (resource.hasMapping()) {
			final String resourceID = resource.getID();
			try {
				final DataSourceConnector connector = resource.getConnector();
				switch (connector.getType()) {
					case json:
					case csv:			
					case json_schema:	final SQLUpdateable updateable = UpdateableFactory.createUpdateable(connector, resourceID, false);
										processTables(deleteClause, updateClause, updateable);
										logger.debug("Clearing pending user removal data");
										UserRemover.getInstance().clearData();
										break;
										
					default:			break;
				}
			} catch (DataSourceConnectorException | DatabaseCreationException | DataSourceSecurityManagerException | DataSourceManagementException | UpdateableFactoryException | UserRemoverException e) {
				logger.error(String.format("Problem processing resource %s for user removal. ", resourceID), e);
			}
		}
	}
	
	private String getUpdateSQL(String table, String setClause) throws UserRemoverException {		
		return String.format("UPDATE %s " + setClause, table);
	}
	
	private String getDeleteSQL(String table, String eraseClause) throws UserRemoverException {		
		return String.format("DELETE FROM %s " + eraseClause, table);
	}
	
	private String getUpdateClause() throws UserRemoverException {
		final Set<PendingUser> anonymizedUsers = UserRemover.getInstance().getAnonymizedUsers();
		if (!anonymizedUsers.isEmpty()) {
			final StringBuffer sb = new StringBuffer();
			sb.append("SET _row_user_ = '-1' WHERE ");
			
			boolean first = true;
			for (PendingUser user : anonymizedUsers) {
				if (first) {
					sb.append(String.format("_row_user_='%s'", user.getUserId()));
					first = false;
				} else {
					sb.append(String.format("OR _row_user_='%s'", user.getUserId()));
				}
			}
			
			return sb.toString();
		}
		
		return "";
	}
	
	private String getDeleteClause() throws UserRemoverException {
		final Set<PendingUser> anonymizedUsers = UserRemover.getInstance().getErasedUsers();
		if (!anonymizedUsers.isEmpty()) {
			final StringBuffer sb = new StringBuffer();
			sb.append("WHERE ");
			
			boolean first = true;
			for (PendingUser user : anonymizedUsers) {
				if (first) {
					sb.append(String.format("_row_user_='%s'", user.getUserId()));
					first = false;
				} else {
					sb.append(String.format("OR _row_user_='%s'", user.getUserId()));
				}
			}
			
			return sb.toString();
		}
		
		return "";
	}
	
	private void processTables(String deleteClause, String updateClause, SQLUpdateable updateable) throws DataSourceManagementException, UserRemoverException, DataSourceSecurityManagerException {		
		for (TableInfo tableInfo : updateable.getInfo().getTables()) {
			if (!deleteClause.isEmpty()) {
				final String deleteSQL = getDeleteSQL(tableInfo.getName(), deleteClause);
				updateable.executeSQLUpdate(deleteSQL, DataSourceSecurityManager.ADMIN_USER);
			}
			
			if (!updateClause.isEmpty()) {
				final String updateSQL = getUpdateSQL(tableInfo.getName(), updateClause);
				updateable.executeSQLUpdate(updateSQL, DataSourceSecurityManager.ADMIN_USER);
			}
		}
	}
}
