/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.util.status.DatasetStatus;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;
import eu.welive.rest.auth.MessageResponse;
import eu.welive.rest.auth.annotation.SecurityFilterCheck;
import eu.welive.rest.auth.roles.UserRoles;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.datasource.manager.DatasourceManager;
import eu.welive.rest.datasource.manager.MappingStatus;
import eu.welive.rest.datasource.manager.MappingValidator;
import eu.welive.rest.datasource.manager.UpdateResult;
import eu.welive.rest.datasource.remover.UserRemover;
import eu.welive.rest.datasource.remover.UserRemoverException;
import eu.welive.rest.exception.UnexpectedErrorException;
import eu.welive.rest.exception.WeLiveException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("ods/")
@Api(value = "/ods")
@SecurityFilterCheck
public class DatasourceAPI {
	
	@GET
	@Path("/{dataset}/resource/{resource}/mapping")
	@Produces(MediaType.TEXT_PLAIN)
	@ApiOperation(value = "Obtains the mapping associated with this resource",
		notes = "The dataset and the resource must be registered in CKAN",
		response = String.class)
	public String getMapping(@Context SecurityContext sc, @PathParam("dataset") String packageId,
			@PathParam("resource") String resourceId) throws WeLiveException {
 		final DatasourceManager datasourceManager = new DatasourceManager((UserInfo)sc.getUserPrincipal(), packageId, resourceId);
		return datasourceManager.getMapping();
	}
		
	@POST
	@Path("/mapping/validate")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Validates the received mapping description",
		response = MappingStatus.class)
	public MappingStatus validateMapping(String mapping) {
		final MappingValidator validator = new MappingValidator();
		return validator.validateMapping(mapping);
	}
	
	@GET
	@Path("/{dataset}/resource/{resource}/mapping/info")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Obtains the schema info provided by this resources mapping",
		notes = "The dataset and the resource must be registered in CKAN",
		response = DataSourceInfo.class)
	public DataSourceInfo getInfo(@Context SecurityContext sc, @PathParam("dataset") String packageId,
			@PathParam("resource") String resourceId) throws WeLiveException {
		
		final DatasourceManager datasourceManager = new DatasourceManager((UserInfo)sc.getUserPrincipal(), packageId, resourceId);
		return datasourceManager.getInfo();
	}
	
	@POST
	@Path("/{dataset}/resource/{resource}/query")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes a query on the specified dataset",
			notes = "The dataset and the resource must be registered in CKAN",
    	response = ResultSet.class)
	public ResultSet executeSQLQuery(@Context SecurityContext sc,
			@PathParam("dataset") String packageId,
			@PathParam("resource") String resourceId,
			@ApiParam(value = "Data origin selection: any, original, user", required = false)
			@QueryParam("origin") @DefaultValue("ANY") DataOrigin origin,
			String sql) throws WeLiveException, URISyntaxException {				
		
		final DatasourceManager datasourceManager = new DatasourceManager((UserInfo)sc.getUserPrincipal(), packageId, resourceId);
		return datasourceManager.executeSQLQuery(sql, origin);
	}
	
	@POST
	@Path("/{dataset}/resource/{resource}/update")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Executes a SQL update on the specified data source",
		response = UpdateResult.class)
	public UpdateResult executeSQLUpdate(@Context SecurityContext sc,
			@PathParam("dataset") String packageId,
			@PathParam("resource") String resourceId,
			@ApiParam(value = "Enable transaction mode for multiple update execution", required = false)
			@DefaultValue("false") @QueryParam("transaction") boolean transaction,
			String sql) throws WeLiveException, IOException, URISyntaxException {
		
		final DatasourceManager datasourceManager = new DatasourceManager((UserInfo)sc.getUserPrincipal(), packageId, resourceId);
		return datasourceManager.executeSQLUpdate(sql, transaction);
	}
	
	@GET
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/{dataset}/resource/{resource}/reset")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Resets the data source using the latest mapping from the ODS (deletes all stored data)",
			response = MessageResponse.class)
	public MessageResponse resetDataSource(@Context SecurityContext sc,
		@PathParam("dataset") String packageId,
		@PathParam("resource") String resourceId) throws WeLiveException {
		
		final DatasourceManager datasourceManager = new DatasourceManager((UserInfo)sc.getUserPrincipal(), packageId, resourceId);
		return datasourceManager.resetDataSource();
	}
	
	@GET
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/{dataset}/resource/{resource}/delete-stored-data")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes all the datasource's associated data (dump and user data).",
			response = MessageResponse.class)
	public MessageResponse deleteStoredData(@Context SecurityContext sc,
		@PathParam("dataset") String packageId,
		@PathParam("resource") String resourceId) throws WeLiveException {
		
		final DatasourceManager datasourceManager = new DatasourceManager((UserInfo)sc.getUserPrincipal(), packageId, resourceId);
		return datasourceManager.deleteStoredData();
	}
	
	@GET
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/{dataset}/resource/{resource}/stop-associated-job")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Stops the associated data downloading job",
			response = MessageResponse.class)
	public MessageResponse stopAssociatedJob(@Context SecurityContext sc,
		@PathParam("dataset") String packageId,
		@PathParam("resource") String resourceId) throws WeLiveException {
		
		final DatasourceManager datasourceManager = new DatasourceManager((UserInfo)sc.getUserPrincipal(), packageId, resourceId);
		return datasourceManager.stopAssociatedJob();
	}
	
	@GET
	@Path("/get-status")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the status of current dataset jobs")
	public List<DatasetStatus> getAllStatus() throws WeLiveException {
		try {
			final List<DatasetStatus> statusList = DatasetStatusManager.getInstance().getAll();
			return statusList;
		} catch (DatasetStatusManagerException e) {
			throw new UnexpectedErrorException("Could not obtain dataset status list", e);
		}
	}

	@GET
	@Path("/get-status/{resource}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets the status of current dataset job for resource identified by resource path param")
	public DatasetStatus getResourceStatus(@PathParam("resource") String resourceId) throws WeLiveException {
		try {
			final DatasetStatus status = DatasetStatusManager.getInstance().getStatus(resourceId);
			return status;
		} catch (DatasetStatusManagerException e) {
			throw new UnexpectedErrorException(String.format("Could not obtain dataset status for resource %s", resourceId), e);
		}
	}
	
	@DELETE
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Path("/dataset/user/me")
	@Produces(MediaType.APPLICATION_JSON)
	public MessageResponse deleteCurrentUser(@Context SecurityContext sc, @QueryParam("cascade") @DefaultValue("False") boolean cascade) throws WeLiveException {
		final String userId = ((UserInfo)sc.getUserPrincipal()).getName();
		try {
			UserRemover.getInstance().addPendingUser(userId, cascade);
			return new MessageResponse(String.format("User %s removal scheduled", userId));
		} catch (UserRemoverException e) {
			throw new UnexpectedErrorException("Could not schedule user " + userId + " for removal", e);
		}
	}
	
	@DELETE
	@RolesAllowed(UserRoles.BASIC_USER)
	@Path("/dataset/user/{ccUserID}")
	@Produces(MediaType.APPLICATION_JSON)
	public MessageResponse deleteUser(@PathParam("ccUserID") String userId, @QueryParam("cascade") @DefaultValue("False") boolean cascade) throws WeLiveException {
		try {
			UserRemover.getInstance().addPendingUser(userId, cascade);
			return new MessageResponse(String.format("User %s removal scheduled", userId));
		} catch (UserRemoverException e) {
			throw new UnexpectedErrorException("Could not schedule user " + userId + " for removal", e);
		}
	}
}