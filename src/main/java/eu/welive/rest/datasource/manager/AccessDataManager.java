/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.exception.EntityNotFoundException;
import eu.welive.rest.exception.UnexpectedErrorException;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANUtilException;
import eu.welive.rest.util.ckan.exception.CKANUtilNotFoundException;

public class AccessDataManager {
	
	public AccessData getAccessData(CKANUtil ckanUtil, UserInfo userInfo, String packageId, String resource) throws EntityNotFoundException, UnexpectedErrorException {
		try {
			final CKANResource resourceInfo = ckanUtil.getResourceInfo(resource);
			final DataSourceConnector connector = resourceInfo.getConnector();
			return new AccessData(connector, resourceInfo, userInfo, packageId);
		} catch (DataSourceConnectorException e) {
			throw new UnexpectedErrorException("Could not load datasource mapping. ", e);
		} catch (CKANUtilNotFoundException e) {
			throw new EntityNotFoundException(e.getMessage(), e);
		} catch (CKANUtilException e) {
			throw new UnexpectedErrorException("Unexpected error", e);
		}
	}
	
}
