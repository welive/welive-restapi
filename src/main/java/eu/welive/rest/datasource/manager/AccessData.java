/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.util.ckan.data.CKANResource;

public class AccessData {
	
	public enum WeLiveRole { ADMIN, EDITOR, CREATOR, NONE };
	
	private DataSourceConnector connector;
	private CKANResource resourceInfo;
	private UserInfo userInfo;
	
	private String packageId;
	
	public AccessData(DataSourceConnector connector, CKANResource resourceInfo, UserInfo userInfo, String packageId) {
		this.connector = connector;
		this.resourceInfo = resourceInfo;
		this.userInfo = userInfo;		
		this.packageId = packageId;
	}
	
	public DataSourceConnector getConnector() {
		return connector;
	}
	
	public CKANResource getResourceInfo() {
		return resourceInfo;
	}
	
	public UserInfo getUserInfo() {
		return userInfo;
	}
	
	public String getPackageId() {
		return packageId;
	}
}
