/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.welive.rest.datasource.manager.MappingStatus.Status;

public class MappingValidator {

	public MappingStatus validateMapping(String mapping) {
		try {
			ConnectorFactory.load(mapping);
			return new MappingStatus(Status.VALID, "Connector description valid");
		} catch (DataSourceConnectorException e) {
			return new MappingStatus(Status.ERROR, e.getMessage());
		}
	}
}
