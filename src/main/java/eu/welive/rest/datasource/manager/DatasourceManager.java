/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.datasource.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import eu.iescities.server.querymapper.datasource.DataSourceInfo;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.JSONDumper;
import eu.iescities.server.querymapper.datasource.query.QueriableFactory;
import eu.iescities.server.querymapper.datasource.query.QueriableFactoryException;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable;
import eu.iescities.server.querymapper.datasource.query.SQLQueriable.DataOrigin;
import eu.iescities.server.querymapper.datasource.query.serialization.ResultSet;
import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManagerException;
import eu.iescities.server.querymapper.datasource.update.SQLUpdateable;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactory;
import eu.iescities.server.querymapper.datasource.update.UpdateableFactoryException;
import eu.iescities.server.querymapper.sql.schema.translator.TranslationException;
import eu.iescities.server.querymapper.util.scheduler.QuartzScheduler;
import eu.iescities.server.querymapper.util.status.DatasetStatus;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;
import eu.welive.rest.auth.MessageResponse;
import eu.welive.rest.auth.user.UserInfo;
import eu.welive.rest.datasource.exception.QueryExecutionException;
import eu.welive.rest.datasource.exception.UpdateExecutionException;
import eu.welive.rest.datasource.logging.DatasourceLogger;
import eu.welive.rest.datasource.manager.AccessData.WeLiveRole;
import eu.welive.rest.exception.ForbiddenErrorException;
import eu.welive.rest.exception.UnexpectedErrorException;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.scheduler.WeLiveMainJob;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

public class DatasourceManager {
	
	private final AccessData accessData;
	private final DatasourceLogger datasourceLogger;
	private final CKANUtil ckanUtil;
	
	public DatasourceManager(UserInfo userInfo, String packageId, String resourceId) throws WeLiveException {
		final AccessDataManager accessDataManager = new AccessDataManager();
		this.ckanUtil = new CKANUtil();
		this.accessData = accessDataManager.getAccessData(ckanUtil, userInfo, packageId, resourceId);
		this.datasourceLogger = new DatasourceLogger();
	}
	
	protected DatasourceManager(CKANUtil ckanUtil, AccessData accessData, DatasourceLogger datasourceLogger) throws WeLiveException {
		this.ckanUtil = ckanUtil;
		this.accessData = accessData;
		this.datasourceLogger = datasourceLogger;
	}
	
	public String getMapping() throws WeLiveException {
		return accessData.getConnector().toString();
	}
	
	public DataSourceInfo getInfo() throws WeLiveException {
		try {
			final SQLQueriable queriable = QueriableFactory.createQueriable(accessData.getConnector(), accessData.getResourceInfo().getID(), false);
			return queriable.getInfo();
		} catch (DataSourceSecurityManagerException e) {
			throw new UserForbiddenException(accessData, e);
		} catch (DatabaseCreationException | QueriableFactoryException e) {
			throw new UnexpectedErrorException("Could not load datasource mapping information.", e);
		} catch (DataSourceManagementException e) {
			throw new QueryExecutionException(e);
		}
	}
	
	public ResultSet executeSQLQuery(String sql, DataOrigin origin) throws WeLiveException {				
		try {			
			return executeQuery(sql, origin);
		} catch (DataSourceSecurityManagerException e) {
			throw new UserForbiddenException(accessData, e);
		} catch (DatabaseCreationException | QueriableFactoryException e) {
			throw new UnexpectedErrorException("Could not load datasource mapping information.", e);
		} catch (TranslationException | DataSourceManagementException e) {
			throw new QueryExecutionException(e);
		}
	}
	
	public UpdateResult executeSQLUpdate(String sql, boolean transaction) throws WeLiveException {
		try {
			return executeUpdate(sql, transaction); 
		} catch (DataSourceSecurityManagerException e) {
			throw new UserForbiddenException(accessData, e);
		} catch (UpdateableFactoryException e) {
			throw new UnexpectedErrorException("Could not load datasource mapping information.", e);
		} catch (DataSourceManagementException | DatabaseCreationException e) {
			throw new UpdateExecutionException(e);
		}
	}
	
	public MessageResponse resetDataSource() throws WeLiveException {
		if (!canAdminDataset()) {
			throw new ForbiddenErrorException("User not authorized to reset dataset"); 
		}
		
		try {			
			deleteStoredData();
			
			final WeLiveMainJob mainJob = new WeLiveMainJob();
			final Scheduler sch = QuartzScheduler.getInstance().getScheduler();
			
			mainJob.deleteJob(accessData.getResourceInfo().getID(), sch);
			DatasetStatusManager.getInstance().updateStatus(accessData.getResourceInfo().getID(), DatasetStatusManager.Status.OK, "Previous job removed");
			
			mainJob.processResourceMapping(accessData.getResourceInfo(), sch, new ArrayList<String>());
			DatasetStatusManager.getInstance().updateStatus(accessData.getResourceInfo().getID(), DatasetStatusManager.Status.OK, "Datasource was manually reset");
		} catch (SchedulerException e) {
			throw new UnexpectedErrorException(String.format("Could not reset job for datasource %s", accessData.getResourceInfo().getID()), e);
		} catch (DatasetStatusManagerException e) {
			throw new UnexpectedErrorException("Could nod store status information", e);
		}
		
		return new MessageResponse(String.format("Datasource %s correctly reset", accessData.getResourceInfo().getID()));
	}
	
	public MessageResponse deleteStoredData() throws WeLiveException {
		if (!canAdminDataset()) {
			throw new ForbiddenErrorException("User not authorized to delete dataset data"); 
		}		
		
		try {
			JSONDumper.deleteData(accessData.getResourceInfo().getID());
			DatasetStatusManager.getInstance().updateStatus(accessData.getResourceInfo().getID(), DatasetStatusManager.Status.OK, "Data was manually deleted");
		} catch (IOException e) {
			throw new UnexpectedErrorException(String.format("Could not delete database for datasource %s", accessData.getResourceInfo().getID()), e); 
		} catch (DatasetStatusManagerException e) {
			throw new UnexpectedErrorException("Could not update status information for datasource %s", e);
		}
		
		return new MessageResponse(String.format("Datasource %s data correctly deleted", accessData.getResourceInfo().getID()));
	}
	
	public MessageResponse stopAssociatedJob() throws WeLiveException {
		if (!canAdminDataset()) {
			throw new ForbiddenErrorException("User not authorized to reset dataset"); 
		}
		
		try {		
			final WeLiveMainJob mainJob = new WeLiveMainJob();
			final Scheduler sch = QuartzScheduler.getInstance().getScheduler();
			if (mainJob.isJobRegistered(accessData.getResourceInfo().getID(), sch)) {
				mainJob.deleteJob(accessData.getResourceInfo().getID(), sch);
			}

			DatasetStatusManager.getInstance().updateStatus(accessData.getResourceInfo().getID(), DatasetStatusManager.Status.OK, "Data downloading job was manually stopped");
		} catch (SchedulerException e) {
			throw new UnexpectedErrorException(String.format("Could not reset job for datasource %s", accessData.getResourceInfo().getID()), e);
		}  catch (DatasetStatusManagerException e) {
			throw new UnexpectedErrorException("Could not store status information. ", e);
		}
		
		return new MessageResponse(String.format("Datasource %s data downloading job stopped", accessData.getResourceInfo().getID()));
	}
	
	public List<DatasetStatus> getAllStatus() throws WeLiveException {
		try {
			final List<DatasetStatus> statusList = DatasetStatusManager.getInstance().getAll();
			return statusList;
		} catch (DatasetStatusManagerException e) {
			throw new UnexpectedErrorException("Could not obtain dataset statuses", e);
		}
	}
	
	private boolean canAdminDataset() throws WeLiveException {
		try {
			final WeLiveRole role = ckanUtil.getUserRole(accessData.getPackageId(), accessData.getUserInfo().getName());
			
			if (role.equals(WeLiveRole.ADMIN) || role.equals(WeLiveRole.EDITOR) || role.equals(WeLiveRole.CREATOR)) {
				return true;
			}
			
			return false;
		} catch (CKANUtilException e) {
			throw new UnexpectedErrorException("Unexpected error", e);
		}
	}
	
	private UpdateResult executeUpdate(String sql, boolean transaction)
			throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException, UpdateableFactoryException {
		
		final SQLUpdateable sqlUpdateable = UpdateableFactory.createUpdateable(accessData.getConnector(), accessData.getResourceInfo().getID(), false);
		
		int modifiedRows = 0;
		if (!transaction) {
			modifiedRows = sqlUpdateable.executeSQLUpdate(sql, accessData.getUserInfo().getName());
		}
		else {
			modifiedRows = sqlUpdateable.executeSQLTransactionUpdate(sql, accessData.getUserInfo().getName());
		}

		datasourceLogger.logUpdate(accessData.getResourceInfo().getID(), accessData.getUserInfo().getName());

		return new UpdateResult(modifiedRows);
	}

	private ResultSet executeQuery(String sql, DataOrigin origin)
			throws DatabaseCreationException, DataSourceSecurityManagerException, DataSourceManagementException,
			QueriableFactoryException, QueryExecutionException, TranslationException {
		
		final SQLQueriable queriable = QueriableFactory.createQueriable(accessData.getConnector(), accessData.getResourceInfo().getID(), false);
		
		if (sql.isEmpty()) {
			throw new QueryExecutionException("Query was empty");
		} else {
			final ResultSet results = queriable.executeSQLSelect(sql, accessData.getUserInfo().getName(), origin);
			datasourceLogger.logQuery(accessData.getResourceInfo().getID(), sql, accessData.getUserInfo().getName());
			return results;
		}
	}
	
}
