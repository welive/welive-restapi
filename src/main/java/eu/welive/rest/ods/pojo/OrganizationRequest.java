/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ods.pojo;

public class OrganizationRequest {
	
	private String name;
	private String info;
	private int ccOrganizationId;
	private int leaderId;
	
	public OrganizationRequest() {
	}

	public OrganizationRequest(String title, String description, int orgID, int ccUserID) {
		super();
		this.name = title;
		this.info = description;
		this.ccOrganizationId = orgID;
		this.leaderId = ccUserID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getCcOrganizationId() {
		return ccOrganizationId;
	}

	public void setCcOrganizationId(int ccOrganizationId) {
		this.ccOrganizationId = ccOrganizationId;
	}

	public int getLeaderId() {
		return leaderId;
	}

	public void setLeaderId(int leaderId) {
		this.leaderId = leaderId;
	}
}
