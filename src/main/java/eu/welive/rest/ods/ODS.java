/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.ods;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.github.slugify.Slugify;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.DCTerms;
import com.hp.hpl.jena.vocabulary.RDF;

import eu.welive.rest.auth.annotation.SecurityFilterCheck;
import eu.welive.rest.auth.roles.UserRoles;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.ods.pojo.OrganizationRequest;
import eu.welive.rest.ods.pojo.RatingRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Created by mikel (m.emaldi at deusto dot es) on 24/09/15.
 */
@Path("ods/")
@Api(value = "/ods", description = "Contains methods for interacting with the Open Data Stack")
@SecurityFilterCheck
public class ODS {

	private static final Logger logger = Logger.getLogger(ODS.class);

	private static String ODS_API;
	private static String CKAN_URL;
	private static String API_URL;

	public ODS() throws WeLiveException {
		try {
			ODS_API = Config.getInstance().getCkanEndpoint() + "/action/";
			CKAN_URL = Config.getInstance().getCkanURL();
			API_URL = Config.getInstance().getApiURL();
		} catch (IOException e) {
            logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error reading welive.properties", e);
		}
	}

	// DATASET OPERATIONS

	@GET
	@Path("/dataset/all")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Returns all datasets stored in the Open Data Stack.")
	@Timed
	@ExceptionMetered
	public Response all(@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		return callGetAction("package_list", null, MediaType.APPLICATION_JSON, apiKey);
	}

	@POST
	@Path("/dataset")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates a new dataset.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response createDataset(String packageDict, @HeaderParam("Authorization") String apiKey)
			throws WeLiveException {
		return callPostAction("package_create", packageDict, apiKey, null, MediaType.APPLICATION_JSON);
	}

	@GET
	@Path("/dataset/{datasetID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Returns metadata about a dataset.")
	@Timed
	@ExceptionMetered
	public Response getDataset(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", datasetID));
		Response result = callGetAction("package_show", params, MediaType.APPLICATION_JSON, apiKey);

		/*
		 * if (result.getStatus() < 300) { JSONObject payload = new
		 * JSONObject(); payload.put("msg", "Dataset metadata accessed");
		 * payload.put("appId", LOGGING_SERVICE_NAME); payload.put("type",
		 * "DatasetMetadataAccessed"); payload.put("timestamp", new
		 * Date().getTime() / 1000); JSONObject customAttributes = new
		 * JSONObject(); customAttributes.put("DatasetID", datasetID);
		 * payload.put("custom_attr", customAttributes);
		 * loggingService.sendLog(LOGGING_SERVICE_NAME, payload); }
		 */

		return result;
	}

	@PUT
	@Path("/dataset/{datasetID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates an existing dataset.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response updateDataset(@PathParam("datasetID") @DefaultValue("") String datasetID, String packageDict,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", datasetID));
		return callPostAction("package_update", packageDict, apiKey, params, MediaType.APPLICATION_JSON);
	}

	@DELETE
	@Path("/dataset/{datasetID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes a dataset.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response removeDataset(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		String packageDict = String.format("{\"id\": \"%s\"}", datasetID);
		return callPostAction("package_delete", packageDict, apiKey, null, MediaType.APPLICATION_JSON);
	}

	@DELETE
    @Path("/dataset/{datasetID}/user/{ccUserID}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Deletes a dataset in behalf of a user identified by ccUserId")
    @RolesAllowed(UserRoles.BASIC_USER)
	@Timed
	@ExceptionMetered
    public Response removeDatasetBasic(@PathParam("datasetID") @DefaultValue("") String datasetID,
                                       @PathParam("ccUserID")  @DefaultValue("") String ccUserID,
                                       @HeaderParam("Authorization") String basicAuth) throws WeLiveException {
        String packageDict = String.format("{\"id\": \"%s\", \"ccUserID\": \"%s\"}", datasetID, ccUserID);

        return callPostAction("package_delete", packageDict, basicAuth, null, MediaType.APPLICATION_JSON);
    }

	@GET
	@Path("/dataset/{datasetID}/usdl")
	@Produces(MediaType.TEXT_PLAIN)
	@ApiOperation(value = "Returns a dataset USDL description.")
	@Timed
	@ExceptionMetered
	public Response getUSDL(@PathParam("datasetID") @DefaultValue("") String datasetID,
							@HeaderParam("Authorization") String auth) throws WeLiveException {
		String stringResult = generateUSDL(datasetID, auth);
		if (stringResult == null) {
			return Response.noContent().build();
		}

		return Response.ok(stringResult, MediaType.TEXT_PLAIN).build();
	}

	@PUT
	@Path("/dataset/{datasetID}/rating")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Adds a rating to a dataset")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response addRating(@PathParam("datasetID") @DefaultValue("") String datasetID, RatingRequest ratingRequest,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("package", datasetID));
		String packageDict = String.format("{\"rating\": %s}", ratingRequest.getRating());
		return callPostAction("rating_create", packageDict, apiKey, params, MediaType.APPLICATION_JSON);
	}

	@PUT
	@Path("/dataset/{datasetID}/rating/user/{ccUserID}")
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Adds a rating to a dataset (Basic Auth)")
	@RolesAllowed(UserRoles.BASIC_USER)
	@Timed
	@ExceptionMetered
	public Response addRatingBasic(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("ccUserID") @DefaultValue("") String userID, RatingRequest ratingRequest,
			@HeaderParam("Authorization") String basicAuth) throws WeLiveException {

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("package", datasetID));
		String packageDict = String.format("{\"rating\": %s, \"ccUserID\": %s}", ratingRequest.getRating(), userID);
		return callPostAction("rating_create", packageDict, basicAuth, params, MediaType.APPLICATION_JSON);
	}

	@GET
	@Path("/dataset/{datasetID}/rating")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets a rating from a dataset")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response getRating(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", datasetID));
		return callGetAction("rating_show", params, MediaType.APPLICATION_JSON, apiKey);
	}

	@GET
	@Path("/dataset/{datasetID}/rating/user/{ccUserID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Gets a rating from a dataset")
	@RolesAllowed(UserRoles.BASIC_USER)
	@Timed
	@ExceptionMetered
	public Response getRatingBasic(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("ccUserID") @DefaultValue("") String userID, @HeaderParam("Authorization") String apiKey)
			throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", datasetID));
		params.add(new BasicNameValuePair("ccUserID", userID));
		return callGetAction("rating_show", params, MediaType.APPLICATION_JSON, apiKey);
	}

	// RESOURCE OPERATIONS

	@POST
	@Path("/dataset/{datasetID}/resource")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates a new resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response createResource(
			@ApiParam("ID of the dataset in which the resource is going to be created.") @PathParam("datasetID") @DefaultValue("") String datasetID,
			@ApiParam("packageDict. A JSON dictionary containing metadata about resource to be created.") @FormDataParam("packageDict") String packageDict,
			@ApiParam("upload. The data file to be attached to the new resource.") @DefaultValue("") @FormDataParam("upload") InputStream upload,
			@FormDataParam("upload") FormDataContentDisposition fileInfo, @HeaderParam("Authorization") String apiKey)
			throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("package_id", datasetID));
		return callPostFileAction("resource_create", params, packageDict, upload, fileInfo, apiKey,
				MediaType.APPLICATION_JSON);
	}

	@GET
	@Path("/dataset/{datasetID}/resource/{resourceID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Returns a resource.")
	@Timed
	@ExceptionMetered
	public Response getResource(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		Response actionResult = callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);

		return actionResult;
	}

	@PUT
	@Path("/dataset/{datasetID}/resource/{resourceID}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates a resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response updateResource(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID,
			@FormDataParam("packageDict") String packageDict, @FormDataParam("upload") InputStream upload,
			@FormDataParam("upload") FormDataContentDisposition fileInfo, @HeaderParam("Authorization") String apiKey)
			throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		return callPostFileAction("resource_update", params, packageDict, upload, fileInfo, apiKey,
				MediaType.APPLICATION_JSON);
	}

	@DELETE
	@Path("/dataset/{datasetID}/resource/{resourceID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes a resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response deleteResource(@PathParam("datasetID") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID, @HeaderParam("Authorization") String apiKey)
			throws WeLiveException {
		String packageDict = String.format("{\"id\": \"%s\"}", resourceID);
		return callPostAction("resource_delete", packageDict, apiKey, null, MediaType.APPLICATION_JSON);
	}

	// MAPPING OPERATIONS

	@POST
	@Path("/dataset/{datasetID}/resource/{resourceID}/mapping")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates a Query Mapper's mapping for a resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response createMapping(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID, String mapping,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("mapping", mapping));
		return updateResourceParam(params, apiKey);
	}

	@GET
	@Path("/dataset/{datasetID}/resource/{resourceID}/mapping")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Returns the Query Mapper's mapping for a dataset.")
	@Timed
	@ExceptionMetered
	public Response getMapping(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", resourceID));
		Response response = callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);

		JSONObject result = getJsonObject(response.readEntity(String.class));
		JSONObject verificationRule = new JSONObject();
		if (result.has("result")) {
			if (result.getJSONObject("result").has("mapping")) {
				verificationRule.put("mapping", result.getJSONObject("result").getString("mapping"));
			} else {
				verificationRule.put("mapping", "");
			}
			return Response.ok(verificationRule.toString(), MediaType.APPLICATION_JSON).build();
		}
		return response;
	}

	@PUT
	@Path("/dataset/{datasetID}/resource/{resourceID}/mapping")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates a Query Mapper's mapping for a resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response updateMapping(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID, String mapping,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("mapping", mapping));
		return updateResourceParam(params, apiKey);
	}

	@DELETE
	@Path("/dataset/{datasetID}/resource/{resourceID}/mapping")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Deletes the Query Mapper's mapping for a dataset.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response deleteMapping(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID, @HeaderParam("Authorization") String apiKey)
			throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("mapping", ""));
		return callPostAction("resource_update", null, apiKey, params, MediaType.APPLICATION_JSON);
	}

	// VALIDATION OPERATIONS

	@POST
	@Path("/dataset/{datasetID}/resource/{resourceID}/validation")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates a new validation rule for a resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response createVerification(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID, String verification,
			@DefaultValue("") @HeaderParam("Authorization") String apiKey) throws WeLiveException {

		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("validation", verification));
		return updateResourceParam(params, apiKey);
	}

	@PUT
	@Path("/dataset/{datasetID}/resource/{resourceID}/validation")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates the verification rule for a resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response updateVerification(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID, String verification,
			@DefaultValue("") @HeaderParam("Authorization") String apiKey) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("validation", verification));
		return updateResourceParam(params, apiKey);
	}

	@DELETE
	@Path("/dataset/{datasetID}/resource/{resourceID}/validation")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates the validation rule for a resource.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response deleteVerification(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID,
			@DefaultValue("") @HeaderParam("Authorization") String apiKey) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		params.add(new BasicNameValuePair("validation", ""));
		return updateResourceParam(params, apiKey);
	}

	@GET
	@Path("/dataset/{datasetID}/resource/{resourceID}/validation")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Returns the validation schema for a dataset.")
	@Timed
	@ExceptionMetered
	public Response getVerification(@PathParam("datasetID") @DefaultValue("") String datasetID,
			@PathParam("resourceID") @DefaultValue("") String resourceID) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("id", resourceID));
		Response response = callGetAction("resource_show", params, MediaType.APPLICATION_JSON, null);
		JSONObject result = getJsonObject(response.readEntity(String.class));
		JSONObject verificationRule = new JSONObject();
		if (result.has("result")) {
			if (result.getJSONObject("result").has("validation")) {
				verificationRule.put("verification_rule", result.getJSONObject("result").getString("validation"));
			} else {
				verificationRule.put("verification_rule", "");
			}
			return Response.ok(verificationRule.toString(), MediaType.APPLICATION_JSON).build();
		}
		return response;
	}

	// User

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/user/me")
	@ApiOperation(value = "Deletes the current user.")
	@RolesAllowed(UserRoles.REGISTERED_USER)
	@Timed
	@ExceptionMetered
	public Response deleteUser(@DefaultValue("") @HeaderParam("Authorization") String apiKey,
			@QueryParam("cascade") @DefaultValue("false") String cascade) throws WeLiveException {

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("cascade", cascade));

		return callGetAction("user_delete", params, MediaType.APPLICATION_JSON, apiKey);
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/user/{ccUserID}")
	@ApiOperation(value = "Deletes the user identified by ccUserID.")
	@RolesAllowed(UserRoles.BASIC_USER)
	@Timed
	@ExceptionMetered
	public Response deleteUserBasic(@PathParam("ccUserID") @DefaultValue("") String ccUserID,
			@HeaderParam("Authorization") @DefaultValue("") String apiKey,
			@QueryParam("cascade") @DefaultValue("false") String cascade) throws WeLiveException {

		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("ccUserID", ccUserID));
		params.add(new BasicNameValuePair("cascade", cascade));

		return callGetAction("user_delete", params, MediaType.APPLICATION_JSON, apiKey);
	}

	// ORGANIZATION

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/upsert-organization")
	@ApiOperation(value = "Creates an organization in the ODS")
	@RolesAllowed(UserRoles.BASIC_USER)
	@Timed
	@ExceptionMetered
	public Response createOrganization(OrganizationRequest organizationRequest,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		Response ckanResponse;
		JSONObject organization = getOrganizationID(String.valueOf(organizationRequest.getCcOrganizationId()));
		if (organization.has("error")) {
			Slugify slg = new Slugify();
			String name = slg.slugify(organizationRequest.getName());
			String dataDict = String.format(
					"{\"name\": \"%s\", \"title\": \"%s\", \"description\": \"%s\", \"extras\": [{\"key\": \"orgID\", \"value\": %s}], \"ccUserID\": %s}",
					name, organizationRequest.getName(), organizationRequest.getInfo(),
					organizationRequest.getCcOrganizationId(), organizationRequest.getLeaderId());
			ckanResponse = callPostAction("organization_create", dataDict, apiKey, null, MediaType.APPLICATION_JSON);
		} else {
			String dataDict = String.format(
					"{\"id\": \"%s\", \"title\": \"%s\", \"description\": \"%s\", \"extras\": [{\"key\": \"orgID\", \"value\": %s}], \"ccUserID\": %s, \"users\": %s}",
					organization.getJSONObject("result").getString("id"), organizationRequest.getName(),
					organizationRequest.getInfo(), organizationRequest.getCcOrganizationId(),
					organizationRequest.getLeaderId(), organization.getJSONObject("result").getJSONArray("users"));
			ckanResponse = callPostAction("organization_update", dataDict, apiKey, null, MediaType.APPLICATION_JSON);
		}

		if (ckanResponse != null) {
			JSONObject ckanResponseJSON = new JSONObject(ckanResponse.readEntity(String.class));
			JSONObject responseJSON = new JSONObject();
			responseJSON.put("error", !ckanResponseJSON.getBoolean("success"));
			if (ckanResponseJSON.getBoolean("success")) {
				responseJSON.put("status", 200);
				responseJSON.put("message", "Organization created/updated successfully");
			} else {
				responseJSON.put("status", 500);
				responseJSON.put("message", ckanResponseJSON.getJSONObject("error").getString("message"));
			}
			return Response.ok(responseJSON.toString(), MediaType.APPLICATION_JSON).build();
		} else {
			return Response.serverError().build();
		}

	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/delete-organization/{ccOrganizationId}")
	@ApiOperation(value = "Deletes an organization in the ODS")
	@RolesAllowed(UserRoles.BASIC_USER)
	@Timed
	@ExceptionMetered
	public Response deleteOrganization(@PathParam("ccOrganizationId") String companyID,
			@HeaderParam("Authorization") String apiKey) throws WeLiveException {
		JSONObject organization = getOrganizationID(companyID);
		if (organization.has("error")) {
			JSONObject responseJSON = new JSONObject();
			responseJSON.put("error", !organization.getBoolean("success"));
			responseJSON.put("status", 404);
			responseJSON.put("message", organization.getJSONObject("error").getString("message"));

			return Response.ok(responseJSON.toString(), MediaType.APPLICATION_JSON).build();
		}
		String dataDict = String.format("{\"id\": \"%s\"}", organization.getJSONObject("result").getString("id"));
		Response ckanResponse = callPostAction("organization_delete", dataDict, apiKey, null,
				MediaType.APPLICATION_JSON);
		JSONObject ckanResponseJSON = new JSONObject(ckanResponse.readEntity(String.class));

		JSONObject responseJSON = new JSONObject();
		responseJSON.put("error", !ckanResponseJSON.getBoolean("success"));
		if (ckanResponseJSON.getBoolean("success")) {
			responseJSON.put("status", 200);
			responseJSON.put("message", "Organization deleted successfully");
		} else {
			responseJSON.put("status", 500);
			responseJSON.put("message", ckanResponseJSON.getJSONObject("error").getString("message"));
		}

		return Response.ok(responseJSON.toString(), MediaType.APPLICATION_JSON).build();
	}

	protected JSONObject getOrganizationID(String ccOrgID) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", ccOrgID));

		Response response = callGetAction("get_organization_id", params, MediaType.APPLICATION_JSON, null);
		String organization = response.readEntity(String.class);
		return new JSONObject(organization);
	}

	protected Response updateResourceParam(List<NameValuePair> params, String apiKey) throws WeLiveException {
		return callPostAction("resource_update", null, apiKey, params, MediaType.APPLICATION_JSON);
	}

	public String generateUSDL(String datasetID, String auth) throws WeLiveException {
		List<NameValuePair> params = new ArrayList<>();
		params.add(new BasicNameValuePair("id", datasetID));

		Response result = callGetAction("package_show", params, MediaType.APPLICATION_JSON, auth);
		JSONObject json = getJsonObject(result.readEntity(String.class));
		if (json.has("error")) {
			return json.getJSONObject("error").getString("message");
		}
		try {
			JSONObject jsonResult = json.getJSONObject("result");
			if (jsonResult.has("type")) {
				if (!jsonResult.getString("type").equals("harvest")) {

					Model model = ModelFactory.createDefaultModel();
					Resource resource = model.createResource(String.format("%s/ods/dataset/%s", API_URL, datasetID));
					Resource datasetType = model.createResource("http://www.welive.eu/ns/welive-core#Service");
					resource.addProperty(RDF.type, datasetType);
					Resource dcType = model.createResource("http://www.welive.eu/ns/welive-core#Dataset");
					resource.addProperty(DCTerms.type, dcType);
					if (jsonResult.has("notes")) {
						if (!jsonResult.isNull("notes")) {
							resource.addProperty(DCTerms.description, jsonResult.getString("notes"));
						}
					}
					if (jsonResult.has("title")) {
						resource.addProperty(DCTerms.title, jsonResult.getString("title"));
					}
					if (jsonResult.has("metadata_created")) {
						DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'.'SSSSSS");
						DateTime packageCreationDateTime = dtf.parseDateTime(jsonResult.getString("metadata_created"));
						DateTimeFormatter outputDTF = DateTimeFormat.forPattern("yyyy-MM-dd");

						resource.addProperty(DCTerms.created, outputDTF.print(packageCreationDateTime),
								XSDDatatype.XSDdate);
					}
					resource.addProperty(FOAF.page,
							model.createResource(String.format("%s/dataset/%s", CKAN_URL, datasetID)));

					if (jsonResult.has("tags")) {
						JSONArray tags = jsonResult.getJSONArray("tags");
						Property tagProperty = model
								.createProperty("http://www.holygoat.co.uk/owl/redwood/0.1/tags#tag");
						for (int i = 0; i < tags.length(); i++) {
							resource.addProperty(tagProperty, tags.getJSONObject(i).getString("name"));
						}
					}

					if (jsonResult.has("license_title")) {
						if (jsonResult.get("license_title") != JSONObject.NULL) {
							Resource license = model
									.createResource(String.format("%s/ods/dataset/%s/license", API_URL, datasetID));
							Resource legalCondition = model
									.createResource("http://www.linked-usdl.org/ns/usdl-core#LegalCondition");
							license.addProperty(RDF.type, legalCondition);
							license.addLiteral(DCTerms.title, jsonResult.getString("license_id"));
							license.addLiteral(DCTerms.description, jsonResult.getString("license_title"));
							if (jsonResult.has("license_url")) {
								license.addProperty(model.createProperty("http://schema.org/url"),
										jsonResult.getString("license_url"));
							}
							resource.addProperty(model.createProperty("http://welive.eu/ns#hasLegalCondition"),
									license);
						}
					}

					if (jsonResult.has("resources")) {
						JSONArray resources = jsonResult.getJSONArray("resources");
						for (int i = 0; i < resources.length(); i++) {
							JSONObject jsonResource = resources.getJSONObject(i);
							Property interactionPointProperty = model
									.createProperty("http://www.welive.eu/ns/welive-core#hasInteractionPoint");
							// Resource interactionPoint =
							// model.createResource(String.format("%s/ods/dataset/%s/resource/%s",
							// SWAGGER_URL, datasetID,
							// jsonResource.getString("id")));
							Resource interactionPoint = model
									.createResource(String.format("%s/ods/dataset/%s/interaction-point/%s", API_URL,
											datasetID, jsonResource.getString("id")));
							Property RESTWebServiceIP = model
									.createProperty("http://www.welive.eu/ns/welive-core#InteractionPoint");
							interactionPoint.addProperty(RDF.type, RESTWebServiceIP);
							if (jsonResource.has("name")) {
								if (jsonResource.get("name") != JSONObject.NULL) {
									interactionPoint.addProperty(DCTerms.title, jsonResource.getString("name"));
								} else {
									interactionPoint.addProperty(DCTerms.title, "Unnamed resource");
								}
							}
							if (jsonResource.has("description")) {
								interactionPoint.addProperty(DCTerms.description,
										jsonResource.getString("description"));
							}
							if (jsonResource.has("url")) {
								// Resource url =
								// model.createResource(jsonResource.getString("url"));
								// Resource url =
								// model.createResource(String.format("%s/ods/dataset/%s/resource/%s",
								// API_URL, datasetID,
								// jsonResource.getString("id")));
								String url = String.format("%s/ods/dataset/%s/resource/%s", API_URL, datasetID,
										jsonResource.getString("id"));
								interactionPoint.addProperty(model.createProperty("http://schema.org/url"), url);
							}

							/*
							 * Resource expectedResult = model.createResource();
							 * Property expectedResultProperty =
							 * model.createProperty(
							 * "http://www.welive.eu/ns/welive-core#ExpectedResult"
							 * ); Property parameterProperty =
							 * model.createProperty(
							 * "http://www.welive.eu/ns/welive-core#Parameter");
							 * expectedResult.addProperty(RDF.type,
							 * parameterProperty); if
							 * (jsonResource.has("format")) {
							 * expectedResult.addProperty(DCTerms.title,
							 * jsonResource.getString("format")); } // TODO:
							 * this property is not properly used Property
							 * typeProperty = model.createProperty(XSD.getURI()
							 * + "type"); if (jsonResource.has("format")) {
							 * expectedResult.addProperty(typeProperty,
							 * jsonResource.getString("format")); }
							 * 
							 * interactionPoint.addProperty(
							 * expectedResultProperty, expectedResult);
							 */
							resource.addProperty(interactionPointProperty, interactionPoint);
						}
					}

					if (jsonResult.has("organization")) {
						JSONObject organizationJSON = jsonResult.getJSONObject("organization");
						Resource organizationResource = model.createResource(
								String.format("%s/organization/%s", CKAN_URL, organizationJSON.getString("name")));
						organizationResource.addProperty(RDF.type,
								model.createProperty("http://www.welive.eu/ns/welive-core#Entity"));
						organizationResource.addProperty(DCTerms.title, organizationJSON.getString("title"));
						organizationResource.addProperty(
								model.createProperty("http://www.welive.eu/ns/welive-core#businessRole"),
								model.createProperty("http://www.welive.eu/ns/welive-core#Owner"));
						resource.addProperty(
								model.createProperty("http://www.welive.eu/ns/welive-core#hasBusinessRole"),
								organizationResource);
					}

					StringWriter out = new StringWriter();
					model.write(out, "RDF/XML");
					String stringResult = out.toString();

					return stringResult;
				}
			}
		} catch (JSONException e) {
            logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Invalid JSON received from the ODS", e);
		}
		return null;
	}

	protected Response callPostFileAction(String action, List<NameValuePair> params, String packageDict,
			InputStream upload, FormDataContentDisposition fileInfo, String apiKey, String MIMEType)
			throws WeLiveException {
		try {
			Client client = ClientBuilder.newClient().register(MultiPartFeature.class);
			WebTarget target = client.target(String.format("%s%s", ODS_API, action));

			try (MultiPart multiPart = new MultiPart()) {
				multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);

				String tempDir = System.getProperty("java.io.tmpdir");
				if (action.equals("resource_create") && fileInfo == null) {
					throw new WeLiveException(Response.Status.BAD_REQUEST, "fileInfo is null. You must upload a resource file");
				}
				if (fileInfo != null) {
					File file = new File(tempDir + "/" + fileInfo.getFileName());
					file.deleteOnExit();
					FileOutputStream out = new FileOutputStream(file);
					IOUtils.copy(upload, out);

					FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("upload", file,
							MediaType.APPLICATION_OCTET_STREAM_TYPE);
					multiPart.bodyPart(fileDataBodyPart);
				}
				if (packageDict == null) {
					throw new WeLiveException(Response.Status.BAD_REQUEST, "packageDict is null. You must provide a packageDict with metadata about resource");
				}
				JSONObject jsonPackage = new JSONObject(URLDecoder.decode(packageDict, "UTF-8"));
				if (!jsonPackage.keySet().contains("url")) {
					multiPart.bodyPart(new FormDataBodyPart("url", ""));
				}
				for (Object key : jsonPackage.keySet()) {
					multiPart.bodyPart(new FormDataBodyPart((String) key, String.valueOf(jsonPackage.get((String) key))));
				}
				for (NameValuePair pair : params) {
					multiPart.bodyPart(new FormDataBodyPart(pair.getName(), pair.getValue()));
				}

                Response response = target.request(MediaType.APPLICATION_JSON_TYPE).header("Authorization", apiKey)
                        .post(Entity.entity(multiPart, multiPart.getMediaType()));

                return response;
			}

		} catch (IOException e) {
            logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error reading result from the Open Data Stack", e);
		} catch (ProcessingException e) {
            logger.error(e.getMessage(), e);
            throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }

	}

	protected Response callPostAction(String action, String packageDict, String apiKey, List<NameValuePair> extraParams,
			String MIMEType) throws WeLiveException {
		try {
			JSONObject jsonPackage = new JSONObject();
			if (packageDict != null) {
				jsonPackage = new JSONObject(packageDict);
			}
			if (extraParams != null) {
				for (NameValuePair pair : extraParams) {
					jsonPackage.put(pair.getName(), pair.getValue());
				}
			}

			Client client = ClientBuilder.newClient();
			WebTarget target = client.target(String.format("%s%s", ODS_API, action));
			Invocation.Builder request = target.request();

			if (apiKey != null) {
				request.header("Authorization", apiKey);
			}

			Response response = request.post(Entity.json(jsonPackage.toString()));
			return response;

		} catch (JSONException e) {
            logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.BAD_REQUEST, "Provided invalid JSON", e);
		} catch (ProcessingException e) {
            logger.error(e.getMessage(), e);
            throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
	}

	protected Response callGetAction(String action, List<NameValuePair> params, String MIMEType, String apikey)
			throws WeLiveException {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(String.format("%s%s", ODS_API, action));
		if (params != null) {
			for (NameValuePair pair : params) {
				target = target.queryParam(pair.getName(), pair.getValue());
			}
		}

		Invocation.Builder request = target.request();
		if (apikey != null) {
			request.header("Authorization", apikey);
		}

        try {
            Response response = request.get();

            return response;
        } catch (ProcessingException e) {
            logger.error(e.getMessage(), e);
            throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
	}

	public JSONObject getJsonObject(String actionResult) {
		JSONObject jsonObject = new JSONObject(actionResult);
		JSONObject result = new JSONObject();
		if (!jsonObject.has("result")) {
			JSONObject error = jsonObject.getJSONObject("error");
			result.put("error", error);
			return result;
		}
		if (jsonObject.get("result") instanceof JSONArray) {
			JSONArray resultArray = jsonObject.getJSONArray("result");
			result.put("result", resultArray);
		} else if (jsonObject.get("result") instanceof JSONObject) {
			JSONObject resultObject = jsonObject.getJSONObject("result");
			result.put("result", resultObject);
		}

		return result;
	}
}
