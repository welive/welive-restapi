/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.aac;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.glassfish.jersey.server.wadl.WadlApplicationContext;
import org.glassfish.jersey.server.wadl.internal.ApplicationDescription;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.glassfish.jersey.server.wadl.internal.WadlUtils;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import eu.welive.rest.exception.WeLiveException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "/aac", description = "AAC controller.")
@Path("/aac/")
public class AACController {

	private AACService aacService;
	
	private static ObjectMapper mapper = new ObjectMapper();

	@Context
	protected UriInfo uriInfo;

	@Context
	protected WadlApplicationContext wadlContext;

	@GET
	@Path("/wadl")
	@Produces({ "application/vnd.sun.wadl+xml", MediaType.APPLICATION_XML })
	public Response wadl() {

		try {
			boolean detailedWadl = WadlUtils.isDetailedWadlRequested(uriInfo);
			String lastModified = new SimpleDateFormat(WadlResource.HTTPDATEFORMAT).format(new Date());
			ApplicationDescription applicationDescription = wadlContext.getApplication(uriInfo, detailedWadl);
			com.sun.research.ws.wadl.Application application = applicationDescription.getApplication();
			List<com.sun.research.ws.wadl.Resource> extResources = new ArrayList<com.sun.research.ws.wadl.Resource>();
			for (com.sun.research.ws.wadl.Resource temp : application.getResources().get(0).getResource()) {
				if (!temp.getPath().equalsIgnoreCase("/aac/")) {
					extResources.add(temp);
				}
			}
			application.getResources().get(0).getResource().removeAll(extResources);
			return Response.ok(application).header("Last-modified", lastModified).build();
		} catch (Exception e) {
			throw new ProcessingException("Error generating WADL", e);
		}
	}
	
	private eu.welive.rest.aac.AACService getAACServiceInstance() {
		if (aacService == null) {
			aacService = AACService.getInstance();
		}
		return aacService;
	}

	@ApiOperation(value = "Get the token data within the client credentials flow and within the authorization code flow.")
	@POST
	@Path("/oauth/token")
	@Timed
	@ExceptionMetered
	public Response generateToken(
			@ApiParam(value = "Indicates the client that is making the request. The value passed in this parameter must exactly match the value shown in the console.", required = true) @QueryParam("client_id") String clientId,
			@ApiParam(value = "The client secret obtained during application registration.", required = true) @QueryParam("client_secret") String clientSecret,
			@ApiParam(value = "The (URL encoded) authorization code returned from the initial request. Ignored in case of client_credentials flow.", required = false) @QueryParam("code") String code,
			@ApiParam(value = "Determines where the response is sent. Required in case of authorization code flow. The value of this parameter must exactly match one of the values registered in the APIs Console (including the http or https schemes, case, and trailing '/').", required = true) @QueryParam("redirect_uri") String redirectUri,
			@ApiParam(value = "Determines the authorization flow used. Possible values are authorization_code or client_credentials or refresh_token.", required = true) @QueryParam("grant_type") String grantType,
			@ApiParam(value = "Determines the refresh token necessary for updating the access token.", required = false) @QueryParam("refresh_token") String refreshToken)
			throws WeLiveException {

		String query = String.format(
				"client_id=%s&client_secret=%s&grant_type=%s&redirect_uri=%s&code=%s&refresh_token=%s", clientId,
				clientSecret, grantType, redirectUri, code, refreshToken);

		// System.out.println(redirectUri);
		// System.out.println(grantType);
		// System.out.println(query);

		String result = null;
		try {
			result = getAACServiceInstance().generateToken(query);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Verify if a token is applicable to the given scope.")
	@GET
	@Path("/resources/access")
	@Timed
	@ExceptionMetered
	public Response validateToken(
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "A scope.", required = true) @QueryParam("scope") String scope) throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().validateToken("scope=" + scope, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get information about the OAuth client app making the request, namely client app ID and client app name.")
	@GET
	@Path("/resources/clientinfo")
	@Timed
	@ExceptionMetered
	public Response getClientInfo(
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().getClientInfo(null, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get the basic profile of the current user.")
	@GET
	@Path("/basicprofile/me")
	@Timed
	@ExceptionMetered
	public Response findProfile(
			@ApiParam(value = "\"Bearer \" + a user access token.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().findProfile(token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get the basic profiles of the specified users.")
	@GET
	@Path("/basicprofile/profiles")
	@Timed
	@ExceptionMetered
	public Response findProfiles(
			@ApiParam(value = "\"Bearer \" + a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "list of user IDs.", required = true) @QueryParam("userIds") String userIds)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().findProfiles(userIds, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get the basic profile of the specified user.")
	@GET
	@Path("/basicprofile/all/{userId}")
	@Timed
	@ExceptionMetered
	public Response getProfile(
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "A user id.", required = true) @PathParam("userId") String userId)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().getUser(userId, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get the basic profiles of the users whose name/surname matches the specified string.")
	@GET
	@Path("/basicprofile/all")
	@Timed
	@ExceptionMetered
	public Response searchUsers(
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "Case insensitive substring to be searched. If not specified, all users returned.", required = false) @QueryParam("filter") String filter)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().searchUsers((filter != null) ? ("filter=" + filter) : null, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get the account profile of the current user.")
	@GET
	@Path("/accountprofile/me")
	@Timed
	@ExceptionMetered
	public Response findAccountProfile(
			@ApiParam(value = "\"Bearer \" + a user access token.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().findAccountProfile(token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get the account profiles of the specified users.")
	@GET
	@Path("/accountprofile/profiles")
	@Timed
	@ExceptionMetered
	public Response findAccountProfiles(
			@ApiParam(value = "\"Bearer \" + a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "list of user IDs.", required = true) @QueryParam("userIds") String userIds)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().findAccountProfiles(userIds, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Create a new user.")
	@POST
	@Path("/extuser/create")
	@Timed
	@ExceptionMetered
	public Response createUser(
			@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "ExtUser") ExtUser extUser) throws WeLiveException {

		String result = null;

		try {
			String body = new ObjectMapper().writeValueAsString(extUser);
			result = getAACServiceInstance().createExtUser(body, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get information about the all OAuth clients making the request, namely client app ID and client app name.")
	@GET
	@Path("/resources/clientinfo/oauth")
	@Timed
	@ExceptionMetered
	public Response getAllClientInfo(
			@ApiParam(value = "\"Bearer \" + a user or a client access token.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().getAllClientInfo(null, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get information about the all OAuth clients making the request using user id, namely client app ID and client app name.")
	@GET
	@Path("/resources/clientinfo/oauth/{userId}")
	@Timed
	@ExceptionMetered
	public Response getAllClientInfoByUserId(
			@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "A user id.", required = true) @PathParam("userId") String userId)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().getAllClientInfoByUserId(userId, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}
	
	@ApiOperation(value = "Check token.")
	@GET
	@Path("/eauth/check_token")
	@Timed
	@ExceptionMetered
	public Response checkToken(@ApiParam(value = "A user token.", required = true) @QueryParam("token") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().checkToken(token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Revoke token.")
	@DELETE
	@Path("/eauth/revoke/{token}")
	@Timed
	@ExceptionMetered
	public Response revokeToken(@ApiParam(value = "A user token.", required = true) @PathParam("token") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().revokeToken(token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Revoke token making request using client id and user id.")
	@DELETE
	@Path("/eauth/revoke/{clientId}/{userId}")
	@Timed
	@ExceptionMetered
	public Response revokeTokenUsingCientIdUserId(
			@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String basicToken,
			@ApiParam(value = "A client id.", required = true) @PathParam("clientId") String clientId,
			@ApiParam(value = "A user id.", required = true) @PathParam("userId") String userId)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().revokeTokenUsingClientUserId(clientId, userId, basicToken);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Delete user data using Basic Authentication.")
	@DELETE
	@Path("/user/{ccUserId}")
	@Timed
	@ExceptionMetered
	public Response deleteUserDataUsingBasicAuth(
			@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String basicToken,
			@ApiParam(value = "A user id", required = true) @PathParam("ccUserId") String ccUserId,
			@ApiParam(value = "Cascade", required = false) @QueryParam("cascade") Boolean cascade)
			throws WeLiveException, JsonParseException, JsonMappingException, IOException {

		String result = null;
		try {
			result = getAACServiceInstance().deleteUsingBasicAuth(ccUserId,
					(cascade != null ? "cascade=" + cascade : null), basicToken);
		} catch (Exception e2) {
			eu.welive.rest.aac.Response response = mapper.readValue(e2.getMessage(), eu.welive.rest.aac.Response.class);
			throw new WeLiveException(Response.Status.fromStatusCode(response.getCode()), response.getErrorMessage(), e2);
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Get client specification for user.")
	@GET
	@Path("/resources/clientspec")
	@Timed
	@ExceptionMetered
	public Response getClientSpec(
			@ApiParam(value = "\"Bearer \" + a client access token.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().getClientSpec(token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Create client specification for user.")
	@POST
	@Path("/resources/clientspec")
	@Timed
	@ExceptionMetered
	public Response createClientSpec(
			@ApiParam(value = "\"Bearer \" + a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "ClientModel") ClientModel model) throws WeLiveException {

		String result = null;

		try {
			String body = new ObjectMapper().writeValueAsString(model);
			result = getAACServiceInstance().createClientSpec(body, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Update client specification for user.")
	@PUT
	@Path("/resources/clientspec")
	@Timed
	@ExceptionMetered
	public Response updateClientSpec(
			@ApiParam(value = "\"Bearer \" + a client access token.", required = true) @HeaderParam("Authorization") String token,
			@ApiParam(value = "ClientModel") ClientModel model) throws WeLiveException {
		String result = null;

		try {
			String body = new ObjectMapper().writeValueAsString(model);
			result = getAACServiceInstance().updateClientSpec(body, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}
	
	@ApiOperation(value = "Get service permissions.")
	@GET
	@Path("/resources/permissions")
	@Timed
	@ExceptionMetered
	public Response getServicePermissions() throws WeLiveException {

		String result = null;
		try {
			result = getAACServiceInstance().getServicePermissions();
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	public void setAACService(AACService service) {
		this.aacService = service;

	}

}