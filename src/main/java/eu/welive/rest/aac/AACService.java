/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.aac;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.ws.rs.core.Response;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;

import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.logging.LoggingService;

public class AACService {

	private static AACService instance = null;
	private static String aacServiceURL = null;
	private static CloseableHttpClient httpClient = null;

	protected AACService() {
		try {
			aacServiceURL = Config.getInstance().getAacServiceURL();
		} catch (IOException e) {
			e.printStackTrace();
		}
		SSLContextBuilder builder = new SSLContextBuilder();
		try {
			builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(builder.build());
			httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}

	public static AACService getInstance() {
		if (instance == null) {
			instance = new AACService();
		}

		return instance;
	}

	public static void setHttpClient(CloseableHttpClient httpClient) {
		AACService.httpClient = httpClient;
	}
	
	protected String createExtUser(String body, String token) throws Exception {
		return doPost(body, "/extuser/create", null, token, true);
	}

	protected String generateToken(String query) throws Exception {
		return doPost(null, "/oauth/token", query, null, true);
	}

	public String revokeToken(String token) throws Exception {
		return doDelete("/eauth/revoke/" + token, null, null, true);
	}

	public String revokeTokenUsingClientUserId(String clientId, String userId, String basicToken) throws Exception {
		return doDelete("/eauth/revoke/" + clientId + "/" + userId, null, basicToken, true);
	}

	public String deleteUsingBasicAuth(String ccUserId, String query, String basicToken) throws Exception {
		return doDelete("/user/" + ccUserId, query, basicToken, true);
	}

	protected String validateToken(String query, String token) throws Exception {
		return doGet("/resources/access", query, token);
	}
	
	public String checkToken(String token) throws Exception {
		return doGet("/eauth/check_token?token=" + token, null, null);
	}

	protected String getClientInfo(String query, String token) throws Exception {
		return doGet("/resources/clientinfo", query, token);
	}

	public String getAllClientInfo(String query, String token) throws Exception {
		return doGet("/resources/clientinfo/oauth", query, token);
	}

	public String getAllClientInfoByUserId(String userId, String token) throws Exception {
		return doGet("/resources/clientinfo/oauth/" + userId, null, token);
	}

	protected String findProfile(String token) throws Exception {
		return doGet("/basicprofile/me", null, token);
	}

	protected String findProfiles(String userIds, String token) throws Exception {
		return doGet("/basicprofile/profiles?userIds=" + userIds, null, token);
	}

	protected String getUser(String userId, String token) throws Exception {
		return doGet("/basicprofile/all/" + userId, null, token);
	}

	protected String searchUsers(String query, String token) throws Exception {
		return doGet("/basicprofile/all", query, token);
	}

	protected String findAccountProfile(String token) throws Exception {
		return doGet("/accountprofile/me", null, token);
	}

	protected String findAccountProfiles(String userIds, String token) throws Exception {
		return doGet("/accountprofile/profiles?userIds=" + userIds, null, token);
	}

	protected String getClientSpec(String token) throws Exception {
		return doGet("/resources/clientspec", null, token);
	}
	
	protected String getServicePermissions() throws Exception {
		return doGet("/resources/permissions", null, null);
	}

	protected String createClientSpec(String body, String token) throws Exception {
		return doPost(body, "/resources/clientspec", null, token, true);
	}

	protected String updateClientSpec(String body, String token) throws Exception {
		return doPut(body, "/resources/clientspec", null, token, true);
	}

	protected String doPut(String request, String operation, String query, String token, boolean getResponse)
			throws Exception {

		URIBuilder uriBuilder = new URIBuilder(aacServiceURL + operation + (query != null ? ("?" + query) : ""));

		System.out.println(query);

		HttpPut httpPut = new HttpPut(uriBuilder.build());
		if (request != null) {
			httpPut.setEntity(new StringEntity(request));
		}

		httpPut.addHeader("Content-Type", "application/json");
		httpPut.addHeader("Accept", "application/json");
		httpPut.addHeader("Authorization", token);
		CloseableHttpResponse response = httpClient.execute(httpPut);

		if (getResponse) {
			InputStream content = response.getEntity().getContent();
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			content.close();

			if (response.getStatusLine().getStatusCode() >= 400) {
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
						responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
			}

			return responseStrBuilder.toString();
		} else {
			return null;
		}
	}

	protected String doPost(String request, String operation, String query, String token, boolean getResponse)
			throws Exception {
		URIBuilder uriBuilder = new URIBuilder(aacServiceURL + operation + (query != null ? ("?" + query) : ""));

		System.out.println(query);

		HttpPost httpPost = new HttpPost(uriBuilder.build());
		if (request != null) {
			httpPost.setEntity(new StringEntity(request));
		}

		httpPost.addHeader("Content-Type", "application/json");
		httpPost.addHeader("Accept", "application/json");
		httpPost.addHeader("Authorization", token);
		CloseableHttpResponse response = httpClient.execute(httpPost);

		if (getResponse) {
			InputStream content = response.getEntity().getContent();
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			content.close();

			if (response.getStatusLine().getStatusCode() >= 400) {
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
						responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
			}

			return responseStrBuilder.toString();
		} else {
			return null;
		}
	}

	protected String doDelete(String operation, String query, String token, boolean getResponse) throws Exception {

		URIBuilder uriBuilder = new URIBuilder(aacServiceURL + operation + (query != null ? ("?" + query) : ""));

		System.out.println(query);

		HttpDelete httpDelete = new HttpDelete(uriBuilder.build());

		httpDelete.addHeader("Content-Type", "application/json");
		httpDelete.addHeader("Accept", "application/json");

		if (token != null)
			httpDelete.addHeader("Authorization", token);

		CloseableHttpResponse response = httpClient.execute(httpDelete);

		if (getResponse) {
			InputStream content = response.getEntity().getContent();
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			content.close();

			if (response.getStatusLine().getStatusCode() >= 400) {
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
						responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
			}

			return responseStrBuilder.toString();
		} else {
			return null;
		}
	}

	protected String doGet(String operation, String query, String token) throws Exception {

		URIBuilder uriBuilder = new URIBuilder(aacServiceURL + operation + (query != null ? ("?" + query) : ""));
		HttpGet httpGet = new HttpGet(uriBuilder.build());
		httpGet.addHeader("Content-Type", "application/json");
		httpGet.addHeader("Accept", "application/json");
		httpGet.addHeader("Authorization", token);

		CloseableHttpResponse response = httpClient.execute(httpGet);

		InputStream content = response.getEntity().getContent();
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
		StringBuilder responseStrBuilder = new StringBuilder();
		String inputStr;
		while ((inputStr = streamReader.readLine()) != null) {
			responseStrBuilder.append(inputStr);
		}
		content.close();

		if (response.getStatusLine().getStatusCode() >= 400) {
			throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
					responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
		}

		return responseStrBuilder.toString();
	}

}