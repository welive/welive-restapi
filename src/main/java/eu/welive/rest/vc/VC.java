/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.vc;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import eu.welive.rest.config.Config;
import eu.welive.rest.lum.UserRoleResponse;
import eu.welive.rest.oia.OIA.AuthType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.util.Json;

/**
 * Created by asirchia (antonino.sirchia at eng dot it) on 29/01/16.
 */

@Path("vc/")
@Api(value = "/vc")
@Produces({MediaType.APPLICATION_JSON})
public class VC {
	
	/*
	 * **** IMPORTANT *****
	 * These methods are only showcases for real services deployed.
	 * Nginx should be properly configured to forward all the requests to the real services. 
	 *
	 */
	
	public static enum AuthType {Basic, Oauth};
	private static String baseurl;
	
	public VC() throws Exception{
		baseurl = Config.getInstance().getVcURL();
	}
	
	public AuthType authType(String auth){
		
		if(auth.startsWith("Basic ")) return AuthType.Basic;
		else if(auth.startsWith("Bearer ")) return AuthType.Oauth;
		else return null;
	}
	
	
	public Response delete(String url, String token) {
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).delete(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}
	
public Response invoke(String url, String token){
		
		Client client = Client.create();
		WebResource webResource;
		
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}

	public Response post(String url, String token, Object data){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
		ClientResponse clientResponse = null;
      
		if(url.compareTo(baseurl+"/ideaworkgroup")==0)
       {
    	   clientResponse = webResource.type(MediaType.TEXT_PLAIN).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
       }
       
		else {
        
			clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
		}
			if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
        System.out.println(resp);       
        
		return Response.status(200).entity(resp).build();
	}

	
	@POST
	@Path("/registeruser")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Create a new VC user", 
					//hidden=true,
					response = WgResponse.class)
	public Response registerUser(@Context HttpHeaders headers,
								 @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								 @ApiParam(value = "User data") UserData userData) {
		
		/*List<String> auths = new ArrayList<String>();
			auths = headers.getRequestHeaders().get("authorization");
		if(auths==null || auths.isEmpty()){
			JSONObject json = new JSONObject();
			json.put("error", true);
			json.put("message", "Authentication required");
			return Response.status(500).entity(json.toString()).build();
		}
		String auth = auths.get(0).trim();
		if(auth.startsWith("Basic ")){*/
		
			//son.prettyPrint(userData);
			String url = baseurl+"/registeruser";
			return post(url,token,userData);
		/*}
		else{
			JSONObject json = new JSONObject();
			json.put("error", true);
			json.put("message", "Only basic authentication is allowed");
			return Response.status(500).entity(json.toString()).build();
		}*/
	}
	
	@POST
	@Path("/ideaworkgroup")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Create a new workgroup associated with an idea",
					//hidden=true,
					response = WgResponse.class)
	public Response createWorkgroupForIdea(@Context HttpHeaders headers,
										   @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
										   @ApiParam(value = "Idea information and list of members") String ideaData) {
		
		
		//ideaData = {"wgname":"VC idea","authority":"trento@welive.eu","ideaid":25402,"isTakenUp":false,"ideaname":"VC idea 2","members":[{"username":"filippo.giuffrida@eng.it"},{"username":"chiara.capizzi@demetrix.it"}],"authorityOrCompanyOrganizati"
			//	+ "                                              onId":138811,"authorityOrCompanyOrganizationName":"Trento Municipality"}
		/*List<String> auths = headers.getRequestHeaders().get("authorization");
		if(auths==null || auths.isEmpty()){
			JSONObject json = new JSONObject();
			json.put("error", true);
			json.put("message", "Authentication required");
			return Response.status(500).entity(json.toString()).build();
		}
		String auth = auths.get(0).trim();
		if(auth.startsWith("Basic ")){*/
			String url = baseurl+"/ideaworkgroup";
			System.out.println(ideaData);
			return post(url,token, ideaData);
		/*}
		else{
			JSONObject json = new JSONObject();
			json.put("error", true);
			json.put("message", "Only basic authentication is allowed");{"wgname":"VC idea ","authority":"trento@welive.eu","ideaid":25402,"isT                                                                                                                     akenUp":false,"ideaname":"VC idea 2","members":[{"username":"filippo.giuffrida@e                                                                                                                     ng.it"},{"username":"chiara.capizzi@demetrix.it"}],"authorityOrCompanyOrganizati                                                                                                                     onId":138811,"authorityOrCompanyOrganizationName":"Trento Municipality"}
			return Response.status(500).entity(json.toString()).build();
		}*/
	}
	
	@DELETE
	@Path("/user/{ccUserID}")
	@Produces("application/json")
	@ApiOperation(value = "Delete a VC user by ccUserID", 
				response = UserDeleteBean.class)
	public Response deleteUser(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								 @ApiParam(value = "The ccuserid of user to be deleted") @PathParam("ccUserID") Integer ccUserID,
	                             @ApiParam(value = "The parameter that indicates if user's contents must be deleted") @QueryParam("cascade") Boolean cascade){
		
			String url = baseurl+"/deleteuser/"
					+ ccUserID+"?cascade="+cascade; 
			
			return delete(url,token);
	}
	
	@POST
	@Path("/upsert-organization")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Create/Update an organization",
					//hidden=true,
					response = VcResponse.class)
	public Response upsertOrganization(@Context HttpHeaders headers,
										   @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
										   @ApiParam(value = "Organization information") Organization organization) {
	
		String url = baseurl+"/upsert-organization";
		return post(url,token, organization);
		
	}
	
	@DELETE
	@Path("/delete-organization/{ccOrganizationId}")
	@Produces("application/json")
	@ApiOperation(value = "Delete an organization by ccOrganizationId", 
				response = VcResponse.class)
	public Response deleteOrganization(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
								 @ApiParam(value = "The ccOrganizationId  of the organization to be deleted") @PathParam("ccOrganizationId") Integer ccOrganizationId){
		
			String url = baseurl+"/delete-organization"
					+ "/ccOrganizationId/"+ccOrganizationId; 
			
			return delete(url,token);
	}
	
	@POST
	@Path("/add-organization-members")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Add members to an existing organization",
					//hidden=true,
					response = VcResponse.class)
	public Response addMembers(@Context HttpHeaders headers,
										   @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
										   @ApiParam(value = "Organization information") Members members) {
	
		String url = baseurl+"/add-organization-members";
		return post(url,token, members);
		
	}
	
	@POST
	@Path("/remove-organization-members")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Remove members from an existing organization",
					//hidden=true,
					response = VcResponse.class)
	public Response removeMembers(@Context HttpHeaders headers,
										   @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
										   @ApiParam(value = "Organization information") Members members) {
	
		String url = baseurl+"/remove-organization-members";
		return post(url,token, members);
		
	}
	
	@POST
	@Path("/validateService")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Validate REST or SOAP services against WADL/WSDL xsd schema",
					//hidden=true,
					response = ServiceResponse.class)
	public Response validateService(@Context HttpHeaders headers,
										   @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
										   @ApiParam(value = "Organization information") Service service) {
	
		String url = baseurl+"/validateService";
		return post(url,token,service);
		
	}
	
	
	
	
	
}