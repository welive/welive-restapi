/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.vc;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import eu.welive.rest.vc.VC.AuthType;

public class VCservices {
	
public AuthType authType(String auth){
		
		if(auth.startsWith("Basic ")) return AuthType.Basic;
		else if(auth.startsWith("Bearer ")) return AuthType.Oauth;
		else return null;
	}
	
	
	public Response delete(String url, String token) {
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).delete(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}
	
public Response invoke(String url, String token){
		
		Client client = Client.create();
		WebResource webResource;
		
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}

	public Response post(String url, String token, Object data){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}


}
