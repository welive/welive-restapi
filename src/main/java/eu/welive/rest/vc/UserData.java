/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.vc;

import java.util.ArrayList;
import java.util.List;

public class UserData{
	
	Integer ccUserID;
	String referredPilot;
	String firstName;
	String lastName;
	String email;
	String liferayScreenName;
	String role;
	Boolean isDeveloper;
	List<String> userTags = new ArrayList<String>();
	Boolean isCompany;
	Boolean isMale;
	String country;
	String city;
	String employement;
	Integer id;
	List<String> languages =  new ArrayList <String>();
	String address;
	String zipCode;
	String secret;
	List<String> usersSkills = new ArrayList<String>();
	Birthdate birthDate = new Birthdate();
	
	
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public Integer getCcUserID() {
		return ccUserID;
	}
	public void setCcUserID(Integer ccUserID) {
		this.ccUserID = ccUserID;
	}
	
	public String getReferredPilot() {
		return referredPilot;
	}
	public void setReferredPilot(String referredPilot) {
		this.referredPilot = referredPilot;
	}
	public List<String> getUserTags() {
		return userTags;
	}
	
	public void setUserTags(List<String> userTags) {
		this.userTags = userTags;
	}
	public Boolean getIsCompany () {
		return isCompany;
	}
	
	public void setIsCompany (Boolean isCompany) {
		this.isCompany = isCompany;
	}
	
	public Boolean getIsMale () {
		return isMale;
	}
	
	public void setIsMale (Boolean isMale) {
		this.isMale = isMale;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getEmployement() {
		return employement;
	}
	public void setEmployement(String employement) {
		this.employement = employement;
	}
	
	public Integer getId () {
		return id;
	}
	
	public void setId (Integer id) {
		
		this.id = id;
	}
	
	public List <String> getLanguages () {
		return languages;
	}
	public void setLanguages (List<String> languages) {
		this.languages = languages;
	}
	
public Boolean getIsDeveloper () {
		
		return isDeveloper;
	}
	public void setIsDeveloper (Boolean isDeveloper) {
		
		this.isDeveloper = isDeveloper;
	}
	public String getLiferayScreenName() {
		return liferayScreenName;
	}
	public void setLiferayScreenName(String liferayScreenName) {
		this.liferayScreenName = liferayScreenName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress () {
		return address;
	}
	public void setAddress (String address) {
		this.address = address;
	}
	public String getSecret() {
		return secret;
	}
	
	public void setSecret(String secret) {
		this.secret = secret;
	}
	
	public List<String> getUsersSkills () {
		return usersSkills;
	}
	
	public void setUsersSkills (List<String> usersSkills) {
		this.usersSkills = usersSkills;
	}
	
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	public Birthdate getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate (Birthdate birthDate) {
		this.birthDate = birthDate;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
    public String getZipCode() {
    	return zipCode;
    }
    public void setZipCode (String zipCode) {
    	this.zipCode = zipCode;
    }
	
	
	
}