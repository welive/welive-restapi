/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.app;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.ApplicationPath;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.codahale.metrics.jersey2.InstrumentedResourceMethodApplicationListener;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.glassfish.jersey.server.ResourceConfig;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import eu.iescities.server.querymapper.util.scheduler.QuartzScheduler;
import eu.welive.rest.config.Config;
import eu.welive.rest.scheduler.WeLiveMainJob;
import eu.welive.rest.util.cors.CrossOriginResourceSharingFilter;
import eu.welive.rest.util.swagger.SwaggerInitializer;

@ApplicationPath("/api")
public class WeLiveApplication extends ResourceConfig {
	
	private static final Logger logger = LogManager.getLogger(WeLiveApplication.class);
	
	private static final int QUERY_MAPPER_JOB_INTERVAL = 10; 
//	private static final int USER_REMOVAL_JOB_INTERVAL = 10;

    public WeLiveApplication() {    	
        packages("eu.welive.rest");
        
        configureLogging();
        
        register(CrossOriginResourceSharingFilter.class);

        register(io.swagger.jaxrs.listing.ApiListingResource.class);
        register(io.swagger.jaxrs.listing.SwaggerSerializers.class);

        register(org.glassfish.jersey.media.multipart.MultiPartFeature.class);
        register(org.glassfish.jersey.moxy.json.MoxyJsonFeature.class);
        register(eu.welive.rest.app.resolver.JsonMoxyConfigurationContextResolver.class);

		final MetricRegistry metricRegistry = new MetricRegistry();
		register(new InstrumentedResourceMethodApplicationListener(metricRegistry));

		try {
			Config config = Config.getInstance();
			final Graphite graphite = new Graphite(new InetSocketAddress(config.getGraphiteHost(), config.getGraphitePort()));
			final GraphiteReporter reporter = GraphiteReporter.forRegistry(metricRegistry)
					.prefixedWith(config.getGraphitePrefix())
					.convertRatesTo(TimeUnit.SECONDS)
					.convertDurationsTo(TimeUnit.MILLISECONDS)
					.filter(MetricFilter.ALL)
					.build(graphite);
			reporter.start(1, TimeUnit.MINUTES);
		} catch (IOException e) {
			e.printStackTrace();
		}

        register(SwaggerInitializer.class);
    	
        try {
        	final Config config = Config.getInstance();
        	eu.iescities.server.querymapper.config.Config.getInstance().setDataDir(config.getDataDir());
        	eu.iescities.server.querymapper.config.Config.getInstance().setDefaultPermissions(config.getDefaultPermissions());
        	eu.iescities.server.querymapper.config.Config.getInstance().setSecurityManager(config.getSecurityManager());
        	eu.iescities.server.querymapper.config.Config.getInstance().setSchedulerThreadCount(config.getSchedulerThreadCount());
        	
        	registerQueryMapperJob(config);
        	//registerUserRemovalJob();
        	
    		QuartzScheduler.getInstance().getScheduler().start();
        	   	
        } catch (IOException | SchedulerException e) {
        	logger.error("Problem starting Quartz scheduler instance", e);
        }
    }

	private void registerQueryMapperJob(Config config) {
		if (eu.iescities.server.querymapper.config.Config.getInstance().checkDataDir()) {
	        try {
	        	final JobDetail queryMapperJob = JobBuilder.newJob(WeLiveMainJob.class)
        	        .withIdentity("queryMapperJob", "queryMapperGroup")
        	        .storeDurably().build();
        		
        		queryMapperJob.getJobDataMap().put("lastRevision", null);

        		final Trigger trigger = newTrigger()
        	        .withIdentity("queryMapperTrigger", "queryMapperGroup")
        	        .withSchedule(simpleSchedule().withIntervalInSeconds(QUERY_MAPPER_JOB_INTERVAL).repeatForever()).build();

        		QuartzScheduler.getInstance().getScheduler().scheduleJob(queryMapperJob, trigger);
			} catch (SchedulerException e) {
				logger.error("Could not register job for query mapper", e);
			}
        } else {
        	logger.error("Could not start scheduler for query mapper. Data dir '" + config.getDataDir() + "' not found.");
        }
	}
	
//	private void registerUserRemovalJob() {
//		try {
//			final JobDetail userRemovalJob = JobBuilder.newJob(UserRemovalJob.class)
//		        .withIdentity("userRemovalJob", "userRemovalGroup")
//		        .storeDurably().build();
//	
//			final Trigger trigger = newTrigger()
//		        .withIdentity("userRemovalJob", "userRemovalGroup")
//		        .withSchedule(simpleSchedule().withIntervalInSeconds(USER_REMOVAL_JOB_INTERVAL).repeatForever()).build();
//	
//			QuartzScheduler.getInstance().getScheduler().scheduleJob(userRemovalJob, trigger);
//		} catch (SchedulerException e) {
//			logger.error("Could not register job for user removal. " + e.getMessage());
//		}
//	}
	
	private void configureLogging() {
		try {
			final Properties props = new Properties();
			final File log4Properties = new File("log4j.properties");
			if (log4Properties.exists()) {
				props.load(new FileInputStream(log4Properties));
				PropertyConfigurator.configure(props);
			}
			logger.info("Correctly loaded external log4j.properties file");
		} catch (IOException e) {
			logger.error("Error loading external log4j.properties file");
		}
	}
}
