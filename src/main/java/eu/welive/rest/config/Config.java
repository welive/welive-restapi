/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.iescities.server.querymapper.BasicConfig;

public class Config extends BasicConfig {

	private static final Logger logger = LogManager.getLogger(Config.class);

	private static Config instance = null;

	private String dataDir = "";
	private String defaultPermissions = "";
	private String securityManager = "";

	private int schedulerThreadCount = 10;

	private String swaggerURL = "";
	private String swaggerAppClientId= "";
	private String apiURL = "";
	
	private String ckanEndpoint = "";
	private String ckanSearchApi = "";
	private String ckanURL = "";
	private String virtuosoURI = "";
	private String virtuosoUser = "";
	private String virtuosoPassword = "";
	private String virtuosoEndpoint = "";

	private String httpProxy = "";
	private String httpProxyPort = "";

	private String loggingServiceURL = "";
	private String aacServiceURL = "";

	private String decisionEngineURL = "";
	private String udeustoDEURL;

	private String adminUser = "";
	private String adminPass = "";

	private String sesameServer = "";
	private String sesameRepository = "";

	private String welivePlayerURL = "";

	private String liferayURL = "";
	private String mkppath = "";
	private String oiapath = "";
	private String cdvpath = "";
	private String lumpath = "";
	private String vcURL = "";

	// ADS
	private String URL_WELIVE_LOGIN = "";
	private String BASE_URL = "";
	private String KPI1_1 = "";
	private String KPI4_3 = "";
	private String KPI11_1 = "";
	private String KPI11_2 = "";
	private String WELIVE_LOGGIN_QUERY = "";
	private String WELIVE_LOG_INSERT = "";

	private boolean loggingEnabled = true;

	private String graphiteHost = "";
	private int graphitePort = 2003;
	private String graphitePrefix = "graphite";

	public static Config getInstance() throws IOException {
		if (instance == null) {
			instance = new Config();
		}
		return instance;
	}

	private Config() throws IOException {
		load("welive.properties");
	}

	public void load(String file) throws IOException {
		final Properties p = new Properties();
		try (FileInputStream is = new FileInputStream(file)) {
			p.load(is);

			dataDir = getProperty(p, "datadir", dataDir);
			defaultPermissions = getProperty(p, "defaultpermissions", "eu.welive.rest.security.WeLiveDefaultPermissions");
			securityManager = getProperty(p, "securitymanager", "eu.iescities.server.querymapper.datasource.security.ACLSecurityManager");
			
			schedulerThreadCount = getProperty(p, "schedulerthreadcount", schedulerThreadCount);

			swaggerURL = getProperty(p, "swaggerurl", "");
			swaggerAppClientId = getProperty(p, "swaggerappclientid", "");
			apiURL = getProperty(p, "apiurl", apiURL);

			ckanURL = getProperty(p, "ckanurl", "");
			ckanEndpoint = ckanURL + "/api/3";
			ckanSearchApi = ckanURL + "/api/search";

			ckanEndpoint = getProperty(p, "ckanendpoint", ckanEndpoint);
			ckanSearchApi = getProperty(p, "ckansearchapi", ckanSearchApi);

			virtuosoURI = getProperty(p, "virtuosouri", virtuosoURI);
			virtuosoUser = getProperty(p, "virtuosouser", virtuosoUser);
			virtuosoPassword = getProperty(p, "virtuosopassword", virtuosoPassword);
			virtuosoEndpoint = getProperty(p, "virtuosoendpoint", virtuosoEndpoint);

			httpProxy = getProperty(p, "httpproxy", httpProxy);
			httpProxyPort = getProperty(p, "httpproxyport", httpProxyPort);

			setProxy();

			loggingServiceURL = getProperty(p, "loggingserviceurl", "");
			aacServiceURL = getProperty(p, "aacserviceurl", "");

			decisionEngineURL = getProperty(p, "decisionengineurl", "");
			udeustoDEURL = getProperty(p, "udeustodeurl", "");

			welivePlayerURL = getProperty(p, "wlplayerurl", "");

			adminUser = getProperty(p, "adminuser", adminUser);
			adminPass = getProperty(p, "adminpass", adminPass);

			if (adminUser.isEmpty() || adminPass.isEmpty()) {
				logger.error("Internal administration user & pass not set. The REST API will not connect with other WeLive modules.");
			}

			sesameServer = getProperty(p, "sesameserver", sesameServer);
			sesameRepository = getProperty(p, "sesamerepository", sesameRepository);

			liferayURL = getProperty(p, "liferayURL", liferayURL);
			mkppath = getProperty(p, "mkppath", mkppath);
			oiapath = getProperty(p, "oiapath", oiapath);
			cdvpath = getProperty(p, "cdvpath", cdvpath);
			lumpath = getProperty(p, "lumpath", lumpath);
			vcURL = getProperty(p, "vcURL", vcURL);

			loggingEnabled = getProperty(p, "loggingenabled", true);

			// ADS
			URL_WELIVE_LOGIN = getProperty(p, "URL_WELIVE_LOGIN", URL_WELIVE_LOGIN);
			BASE_URL = getProperty(p, "BASE_URL", BASE_URL);
			KPI1_1 = getProperty(p, "KPI1_1", KPI1_1);
			KPI4_3 = getProperty(p, "KPI4_3", KPI4_3);
			KPI11_1 = getProperty(p, "KPI11_1", KPI11_1);
			KPI11_2 = getProperty(p, "KPI11_2", KPI11_2);
			WELIVE_LOGGIN_QUERY = getProperty(p, "WELIVE_LOGGIN_QUERY", WELIVE_LOGGIN_QUERY);
			WELIVE_LOG_INSERT = getProperty(p, "WELIVE_LOG_INSERT", WELIVE_LOG_INSERT);

			graphiteHost = getProperty(p, "graphiteHost", graphiteHost);
			graphitePort = getProperty(p, "graphitePort", graphitePort);
			graphitePrefix = getProperty(p, "graphitePrefix", graphitePrefix);
		}
	}

	protected boolean getProperty(Properties p, String propertyName, boolean current) {
		String value = p.getProperty(propertyName);

		boolean boolValue;
		if (value == null || value.isEmpty()) {
			boolValue = current;
		} else {
			boolValue = Boolean.parseBoolean(value);
		}
		return boolValue;
	}

	private void setProxy() {
		if (!httpProxy.isEmpty() && !httpProxyPort.isEmpty()) {
			System.getProperties().put("proxySet", "true");
			System.getProperties().put("proxyHost", httpProxy);
			System.getProperties().put("proxyPort", httpProxyPort);
		}
	}

	public String getDataDir() {
		return dataDir;
	}
	
	public String getHost() throws MalformedURLException {
		final URL baseURL = new URL(apiURL);
		return baseURL.getHost() + ((baseURL.getPort() == -1)?"":(":" + baseURL.getPort()));
	}

	public String getDefaultPermissions() {
		return defaultPermissions;
	}

	public String getSecurityManager() {
		return securityManager;
	}

	public int getSchedulerThreadCount() {
		return schedulerThreadCount;
	}

	public String getSwaggerURL() {
		return swaggerURL;
	}
	
	public String getSwaggerAppClientId() {
		return swaggerAppClientId;
	}

	public String getVirtuosoURI() {
		return virtuosoURI;
	}

	public String getVirtuosoUser() {
		return virtuosoUser;
	}

	public String getVirtuosoPassword() {
		return virtuosoPassword;
	}

	public String getHttpProxy() {
		return httpProxy;
	}

	public String getHttpProxyPort() {
		return httpProxyPort;
	}

	public String getLoggingServiceURL() {
		return loggingServiceURL;
	}

	public String getAacServiceURL() {
		return aacServiceURL;
	}

	public String getWeLivePlayerServiceURL() {
		return welivePlayerURL;
	}

	public String getCkanURL() {
		return ckanURL;
	}

	public String getCkanSearchApi() {
		return ckanSearchApi;
	}

	public String getCkanEndpoint() {
		return ckanEndpoint;
	}

	public String getDecisionEngineURL() {
		return decisionEngineURL;
	}

	public String getUdeustoDEURL() {
		return udeustoDEURL;
	}

	public String getApiURL() {
		return apiURL;
	}

	public String getAdminUser() {
		return adminUser;
	}

	public String getAdminPass() {
		return adminPass;
	}

	public boolean getLoggingEnabled() {
		return loggingEnabled;
	}

	public String getSesameServer() {
		return sesameServer;
	}

	public String getSesameRepository() {
		return sesameRepository;
	}

	public String getLiferayURL() {
		return liferayURL;
	}

	public String getMkppath() {
		return mkppath;
	}

	public String getLumpath() {
		return lumpath;
	}

	public String getOiapath() {
		return oiapath;
	}

	public String getVcURL() {
		return vcURL;
	}

	public String getCdvPath() {
		return cdvpath;
	}

	// ADS
	public String getURL_WELIVE_LOGIN() {
		return URL_WELIVE_LOGIN;
	}

	public String getBASE_URL() {
		return BASE_URL;
	}

	public String getKPI1_1() {
		return KPI1_1;
	}

	public String getKPI4_3() {
		return KPI4_3;
	}

	public String getKPI11_1() {
		return KPI11_1;
	}

	public String getKPI11_2() {
		return KPI11_2;
	}

	public String getWELIVE_LOGGIN_QUERY() {
		return WELIVE_LOGGIN_QUERY;
	}

	public String getWELIVE_LOG_INSERT() {
		return WELIVE_LOG_INSERT;
	}

	public String getGraphiteHost() {
		return graphiteHost;
	}

	public int getGraphitePort() {
		return graphitePort;
	}

	public String getGraphitePrefix() {
		return graphitePrefix;
	}
}