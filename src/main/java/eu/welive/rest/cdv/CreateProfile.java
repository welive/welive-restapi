/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

import eu.welive.rest.lum.Role;

public class CreateProfile{
	
	@XmlElement(required = true)
	Integer ccUserID;
	@XmlElement(required = true)
	String referredPilot;
	@XmlElement(required = true)
	String firstName;
	@XmlElement(required = true)
	String lastName;
	@XmlElement(required = true)
	Boolean isMale;
	@XmlElement(required = true)
	DMYdate birthdate;
	@XmlElement(required = true)
	String email;
	@XmlElement(required = true)
	Boolean isDeveloper;
	@XmlElement(required = false)
	String address;
	@XmlElement(required = false)
	String city;
	@XmlElement(required = false)
	String country;
	@XmlElement(required = false)
	String zipCode;
	@XmlElement(required = false)
	List<String> languages;
	@XmlElement(required = false)
	List<String> userTags;
	@XmlElement(required = true)
	Role role;
	@XmlElement(required = true)
	String liferayScreenName;
	
	public Integer getCcUserID() {
		return ccUserID;
	}
	public void setCcUserID(Integer ccUserID) {
		this.ccUserID = ccUserID;
	}
	public String getReferredPilot() {
		return referredPilot;
	}
	public void setReferredPilot(String referredPilot) {
		this.referredPilot = referredPilot;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Boolean getIsMale() {
		return isMale;
	}
	public void setIsMale(Boolean isMale) {
		this.isMale = isMale;
	}
	public DMYdate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(DMYdate birthdate) {
		this.birthdate = birthdate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Boolean getIsDeveloper() {
		return isDeveloper;
	}
	public void setIsDeveloper(Boolean isDeveloper) {
		this.isDeveloper = isDeveloper;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	public List<String> getUserTags() {
		return userTags;
	}
	public void setUserTags(List<String> userTags) {
		this.userTags = userTags;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	public String getLiferayScreenName() {
		return liferayScreenName;
	}
	public void setLiferayScreenName(String liferayScreenName) {
		this.liferayScreenName = liferayScreenName;
	}
	
}