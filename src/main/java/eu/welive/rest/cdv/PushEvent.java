/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class PushEvent{
	
	@XmlElement(required=true)
	Integer ccUserID;
	@XmlElement(required=true)
	String eventName;
	
	@XmlElement(required=false)
	List<PushEntry> entries;
	
	public Integer getCcUserID() {
		return ccUserID;
	}
	public void setCcUserID(Integer ccUserID) {
		this.ccUserID = ccUserID;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public List<PushEntry> getEntries() {
		return entries;
	}
	public void setEntries(List<PushEntry> entries) {
		this.entries = entries;
	}
	
	
}