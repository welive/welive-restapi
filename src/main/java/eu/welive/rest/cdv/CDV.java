/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;

import com.codahale.metrics.ConsoleReporter.Builder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import eu.welive.rest.config.Config;
import eu.welive.rest.mkp.Pilot;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Created by asirchia (antonino.sirchia at eng dot it) on 29/01/16.
 */

@Path("cdv/")
@Api(value = "/cdv")
@Produces({MediaType.APPLICATION_JSON})
public class CDV {
	
	public static enum AuthType {Basic, Oauth};
	private static String baseurl;
	
	public AuthType authType(String auth){
		
		if(auth.startsWith("Basic ")) return AuthType.Basic;
		else if(auth.startsWith("Bearer ")) return AuthType.Oauth;
		else return null;
	}
	
	
	public Response invoke(String url, String token){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
        return Response.status(200).entity(resp).build();
	}
	
	public Response post(String url, String token, Object data){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		switch(authtype){
			case Basic:
				String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
				String[] credentials = decoded.split(":");
				client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
				webResource = client.resource(url);
				break;
			case Oauth:
				webResource = client.resource(url);
				webResource.header("Authorization", token);
				break;
			default:
				webResource = client.resource(url);
				break;
		}
		System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
        return Response.status(200).entity(resp).build();
        
	}

	public Response delete(String url, String token) {
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		com.sun.jersey.api.client.WebResource.Builder builder;
		switch(authtype){
			case Basic:
				String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
				String[] credentials = decoded.split(":");
				client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
				webResource = client.resource(url);
				break;
			case Oauth:
				webResource = client.resource(url);
				webResource.header("Authorization", token);
				break;
			default:
				webResource = client.resource(url);
				break;
		}
		System.out.println("Invoking "+url);
       
		builder = webResource.accept(MediaType.WILDCARD);
		
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).delete(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
        return Response.status(200).entity(resp).build();
	}
	
	public CDV() throws Exception {
        baseurl = Config.getInstance().getLiferayURL() + Config.getInstance().getCdvPath();
    }
	@GET
	@Path("/getuserprofile")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get user profile data",
				  notes = "This API requires Oauth Authentication.",
				  response = UserProfile.class)
	public Response getUserProfile(@ApiParam(value = "\"Bearer \" + a user access token.", required = true) @HeaderParam("Authorization") String token) {
		
		
			String url = baseurl+"/getuserprofile";
			return invoke(url,token);
		}
	@GET
	@Path("/getusermetadata/{ccUserID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get user metadata (birthdate, address, city, skills, used apps and last known location) data by ccUserId",
				response = UserMetadata.class)
	public Response getUserMetadata(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
									@ApiParam(value = "The cross-components user id") @PathParam("ccUserID") Integer ccUserID) throws UnsupportedEncodingException {
		
			String url = baseurl+"/getusermetadata/ccuserid/"+URLEncoder.encode(ccUserID.toString(), "UTF-8");
			return invoke(url,token);
	}
	
	@GET
	@Path("/getusersskills")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get users skills and languages",
				response = UserSkill[].class)
	public Response getUsersSkills(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token) {
		
			String url = baseurl+"/getusersskills";
			return invoke(url,token);
	}
	
	@POST
	@Path("/adduser")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Add a new user to the CDV", response = ResponseMessage.class)
	public Response addUser(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
							@ApiParam(value = "Profile data") CreateProfile profile) {
		
			String url = baseurl+"/adduser";
			return post(url, token, profile); }
	
	@POST
	@Path("/updateuser")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Update user profile", response = ResponseMessage.class)
	public Response updateUser( @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
								@ApiParam(value = "Profile data") UpdateProfile profile) {
		
		
			String url = baseurl+"/updateuser";
			return post(url, token, profile);
		}
	
	@POST
	@Path("/push")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Push data about platform usage by the user", response = ResponseMessage.class)
	public Response push(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
						 @ApiParam(value = "Event data") PushEvent[] profile) {
		
		
			String url = baseurl+"/push";
			return post(url, token, profile);
	}
	
	@GET
	@Path("/getKPI_1.1/{pilotID}/from/{from}/to/{to}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the number of users (per pilot) who created or collaborated any need/idea",
					response = KPI_11_Response.class)
	public Response getKPI_11(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
							  @ApiParam(value = "The pilot id") @PathParam("pilotID") Pilot pilotID,
							  @ApiParam(value = "The starting date") @PathParam("from") String from,
							  @ApiParam(value = "The end date") @PathParam("to") String to) throws UnsupportedEncodingException {
			String url = baseurl+"/getkpi11/pilotid/"+URLEncoder.encode(pilotID.toString(), "UTF-8")+"/from/"+URLEncoder.encode(from, "UTF-8")+"/to/"+URLEncoder.encode(to, "UTF-8");
			return invoke(url,token);
	}

	@GET
	@Path("/getKPI_4.3/{pilotID}/from/{from}/to/{to}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the number of registered users (per pilot)",
					response = KPI_43_Response.class)
	public Response getKPI_43(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
							  @ApiParam(value = "The pilot id") @PathParam("pilotID") Pilot pilotID,
							  @ApiParam(value = "The starting date in format yyyy-MM-dd") @PathParam("from") String from,
							  @ApiParam(value = "The end date in format yyyy-MM-dd") @PathParam("to") String to) throws UnsupportedEncodingException {

		
			String url = baseurl+"/getkpi43/pilotid/"+URLEncoder.encode(pilotID.toString(), "UTF-8")+"/from/"+URLEncoder.encode(from, "UTF-8")+"/to/"+URLEncoder.encode(to, "UTF-8");
			return invoke(url,token);
	}

	@GET
	@Path("/getKPI_11.1/{pilotID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the classification of users per age range (per pilot)",
					response = KPI_111_Response.class)
	public Response getKPI_111(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
			 				   @ApiParam(value = "The pilot id") @PathParam("pilotID") Pilot pilotID) throws UnsupportedEncodingException {

		
			String url = baseurl+"/getkpi111/pilotid/"+URLEncoder.encode(pilotID.toString(), "UTF-8");
			return invoke(url,token);
	}

	@GET
	@Path("/getKPI_11.2/{pilotID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the classification of users per gender (per pilot)",
					response = KPI_112_Response.class)
	public Response getKPI_112(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
								@ApiParam(value = "The pilot id") @PathParam("pilotID") Pilot pilotID) throws UnsupportedEncodingException {

		
			String url = baseurl+"/getkpi112/pilotid/"+URLEncoder.encode(pilotID.toString(), "UTF-8");
			return invoke(url,token);
	}
	
	@GET
	@Path("/getKPI_11.3/{pilotID}/from/{from}/to/{to}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the classification of users per Work status (per pilot)",
					response = KPI_113_Response.class)
	public Response getKPI_113(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
								@ApiParam(value = "The pilot id") @PathParam("pilotID") Pilot pilotID,
								@ApiParam(value = "The starting date in format yyyy-MM-dd") @PathParam("from") String from,
								@ApiParam(value = "The end date in format yyyy-MM-dd") @PathParam("to") String to) throws UnsupportedEncodingException {

		
			String url = baseurl+"/getkpi113/pilotid/"+URLEncoder.encode(pilotID.toString(), "UTF-8")+"/from/"+URLEncoder.encode(from, "UTF-8")+"/to/"+URLEncoder.encode(to, "UTF-8");
			return invoke(url,token);
	}
	@GET
	@Path("/getKPI_11.4/{pilotID}/from/{from}/to/{to}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the classification of users per role (per pilot)",
					response = KPI_114_Response.class)
	public Response getKPI_114(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
								@ApiParam(value = "The pilot id") @PathParam("pilotID") Pilot pilotID,
								@ApiParam(value = "The starting date in format yyyy-MM-dd") @PathParam("from") String from,
								@ApiParam(value = "The end date in format yyyy-MM-dd") @PathParam("to") String to) throws UnsupportedEncodingException {

			String url = baseurl+"/getkpi114/pilotid/"+URLEncoder.encode(pilotID.toString(), "UTF-8")+"/from/"+URLEncoder.encode(from, "UTF-8")+"/to/"+URLEncoder.encode(to, "UTF-8");
			return invoke(url,token);
	}
	
	@GET
	@Path("/getKPI_11.5/{pilotID}/from/{from}/to/{to}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get the number of developers per per pilot",
					response = KPI_115_Response.class)
	public Response getKPI_115(@ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token, 
								@ApiParam(value = "The pilot id") @PathParam("pilotID") Pilot pilotID,
								@ApiParam(value = "The starting date in format yyyy-MM-dd") @PathParam("from") String from,
								@ApiParam(value = "The end date in format yyyy-MM-dd") @PathParam("to") String to) throws UnsupportedEncodingException {

		
			String url = baseurl+"/getkpi115/pilotid/"+URLEncoder.encode(pilotID.toString(), "UTF-8")+"/from/"+URLEncoder.encode(from, "UTF-8")+"/to/"+URLEncoder.encode(to, "UTF-8");
			return invoke(url,token);
	}
	
	@GET
	@Path("/userdata/get/{dataID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Get user custom data",
				  notes = "This API requires Oauth Authentication.",
				  response = UserDataBean.class,
				  responseContainer = "List")
	public Response getUserData(@ApiParam(value = "\"Bearer \" + an access token who identifies both user and client.", required = true) @HeaderParam("Authorization") String token,
									@ApiParam(value = "The dataID or \"all\" to retrieve all user data for the application.") @PathParam("dataID") String dataid) throws UnsupportedEncodingException {
		
		
			String url = baseurl+"/userdataget/dataid/"+URLEncoder.encode(dataid, "UTF-8");
			return invoke(url,token);
	}
	
	@POST
	@Path("/userdata/update/{dataID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Create or update user custom data",
				  notes = "This API requires Oauth Authentication.",
				  response = UserDataCreateUpdateBean.class)
	
	public Response userDataUpdate(@ApiParam(value = "\"Bearer \" + an access token who identifies both user and client.", required = true) @HeaderParam("Authorization") String token,
									@ApiParam(value = "The data to be added or updated") IndexedUserDataBean body) {
		 
			String url = baseurl+"/userdataupdate";
			return post(url, token, body);	
	}
	
	@DELETE
	@Path("/userdatadelete/dataid/{dataID}/uid/{uid}/client_id/{clientid}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Delete a user custom data",
				  notes = "This API requires Oauth Authentication.",
				  response = UserDataBean.class,
				  responseContainer = "List")
	public Response deleteUserData(@ApiParam(value = "\"Bearer \" + an access token who identifies both user and client.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "The ID of the user data to be deleted.") @PathParam("dataid") String dataid,
	        @ApiParam(value = "The ID of the user.") @PathParam("uid") String uid,
    @ApiParam(value = "The ID of the application.") @PathParam("clientid") String clientid)throws UnsupportedEncodingException {
		
		
			String url = baseurl+"/userdatadelete/dataid/"+URLEncoder.encode(dataid, "UTF-8") + "/uid/" + URLEncoder.encode(uid, "UTF-8") + "/client_id/" + URLEncoder.encode(clientid, "UTF-8") ;
			return delete(url, token);		
	}
	
	@POST
	@Path("/userdatasearch/{tags}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Search user custom data by keyword",
				  notes = "This API requires Oauth Authentication.",
				  response = UserDataBean.class,
				  responseContainer = "List")
	public Response getUserDataSearch(@ApiParam(value = "\"Bearer \" + an access token who identifies both user and client.", required = true) @HeaderParam("Authorization") String token,
								@ApiParam(value = "The keywords (separeted by comma) that should be used to search data.") @PathParam("tags") String tags) throws UnsupportedEncodingException {
		
		
			String url = baseurl+"/userdatasearch/tags/"+URLEncoder.encode(tags, "UTF-8");
			return post(url, token, "");		
	}
}

