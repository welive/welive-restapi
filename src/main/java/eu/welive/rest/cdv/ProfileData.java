/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import javax.xml.bind.annotation.XmlElement;


public class ProfileData{
	
	@XmlElement(required=true)
	Integer numCreatedIdeas;
	@XmlElement(required=true)
	Integer numCollaborationsInIdeas;
	@XmlElement(required=true)
	Float reputationScore;
	
	public Integer getNumCreatedIdeas() {
		return numCreatedIdeas;
	}
	public void setNumCreatedIdeas(Integer numCreatedIdeas) {
		this.numCreatedIdeas = numCreatedIdeas;
	}
	public Integer getNumCollaborationsInIdeas() {
		return numCollaborationsInIdeas;
	}
	public void setNumCollaborationsInIdeas(Integer numCollaborationsInIdeas) {
		this.numCollaborationsInIdeas = numCollaborationsInIdeas;
	}
	public Float getReputationScore() {
		return reputationScore;
	}
	public void setReputationScore(Float reputationScore) {
		this.reputationScore = reputationScore;
	}
	
	
}