/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class UpdateProfile{
	
	@XmlElement(required=true)
	Integer ccUserID;
	@XmlElement(required=false)
	Boolean isMale;
	@XmlElement(required=false)
	DMYUpdateDate birthdate;
	@XmlElement(required=false)
	String address;
	@XmlElement(required=false)
	String city;
	@XmlElement(required=false)
	String country;
	@XmlElement(required=false)
	String zipCode;
	@XmlElement(required=false)
	List<String> languages;
	@XmlElement(required=false)
	List<String> userTags;
	
	public Integer getCcUserID() {
		return ccUserID;
	}
	public void setCcUserID(Integer ccUserID) {
		this.ccUserID = ccUserID;
	}
	public Boolean getIsMale() {
		return isMale;
	}
	public void setIsMale(Boolean isMale) {
		this.isMale = isMale;
	}
	public DMYUpdateDate getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(DMYUpdateDate birthdate) {
		this.birthdate = birthdate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	public List<String> getUserTags() {
		return userTags;
	}
	public void setUserTags(List<String> userTags) {
		this.userTags = userTags;
	}	
	
	
}