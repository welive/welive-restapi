/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import javax.xml.bind.annotation.XmlElement;

public class KPI_111_Response
{
	@XmlElement(required=false)
	AgeRanges bilbao;
	@XmlElement(required=false)
	AgeRanges helsinki;
	@XmlElement(required=false)
	AgeRanges novisad;
	@XmlElement(required=false)
	AgeRanges trento;
	
	public AgeRanges getBilbao() {
		return bilbao;
	}
	public void setBilbao(AgeRanges bilbao) {
		this.bilbao = bilbao;
	}
	public AgeRanges getHelsinki() {
		return helsinki;
	}
	public void setHelsinki(AgeRanges helsinki) {
		this.helsinki = helsinki;
	}
	public AgeRanges getNovisad() {
		return novisad;
	}
	public void setNovisad(AgeRanges novisad) {
		this.novisad = novisad;
	}
	public AgeRanges getTrento() {
		return trento;
	}
	public void setTrento(AgeRanges trento) {
		this.trento = trento;
	}
	
	
}