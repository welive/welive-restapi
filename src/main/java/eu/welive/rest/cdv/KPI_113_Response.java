/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import javax.xml.bind.annotation.XmlElement;

public class KPI_113_Response{
	
	@XmlElement(required=false)
	WorkClassification Bilbao;
	@XmlElement(required=false)
	WorkClassification Uusimaa;
	@XmlElement(required=false)
	WorkClassification Novisad;
	@XmlElement(required=false)
	WorkClassification Trento;
	
	
	public WorkClassification getBilbao() {
		return Bilbao;
	}
	public void setBilbao(WorkClassification bilbao) {
		this.Bilbao = bilbao;
	}
	public WorkClassification getUusimaa() {
		return Uusimaa;
	}
	public void setHelsinki(WorkClassification helsinki) {
		this.Uusimaa = helsinki;
	}
	public WorkClassification getNovisad() {
		return Novisad;
	}
	public void setNovisad(WorkClassification novisad) {
		this.Novisad = novisad;
	}
	public WorkClassification getTrento() {
		return Trento;
	}
	public void setTrento(WorkClassification trento) {
		this.Trento = trento;
	}
	
}