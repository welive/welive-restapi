/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

public class WorkClassification {
	
	Integer noanswer;
	Integer Student;
	Integer Unemployed;
	Integer Employed_by_third_party;
	Integer Self_employed_Entrepreneur;
	Integer Retired;
	Integer Other;
	
	
	public Integer getNoanswer() {
		return noanswer;
	}
	public void setNoanswer(Integer noanswer) {
		this.noanswer = noanswer;
	}
	public Integer getStudent() {
		return Student;
	}
	public void setStudent(Integer student) {
		Student = student;
	}
	public Integer getUnemployed() {
		return Unemployed;
	}
	public void setUnemployed(Integer unemployed) {
		Unemployed = unemployed;
	}
	public Integer getEmployed_by_third_party() {
		return Employed_by_third_party;
	}
	public void setEmployed_by_third_party(Integer employed_by_third_party) {
		Employed_by_third_party = employed_by_third_party;
	}
	public Integer getSelf_employed_Entrepreneur() {
		return Self_employed_Entrepreneur;
	}
	public void setSelf_employed_Entrepreneur(Integer self_employed_Entrepreneur) {
		Self_employed_Entrepreneur = self_employed_Entrepreneur;
	}
	public Integer getRetired() {
		return Retired;
	}
	public void setRetired(Integer retired) {
		Retired = retired;
	}
	public Integer getOther() {
		return Other;
	}
	public void setOther(Integer other) {
		Other = other;
	}
	
	

}
