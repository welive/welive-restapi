/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import javax.xml.bind.annotation.XmlElement;

public class AgeRanges
{
	@XmlElement(required=true)
	Integer lt20;
	@XmlElement(required=true)
	Integer _20_40;
	@XmlElement(required=true)
	Integer _40_60;
	@XmlElement(required=true)
	Integer gt60;
	@XmlElement(required=true)
	Integer noanswer;
	
	public Integer getLt20() {
		return lt20;
	}
	public void setLt20(Integer lt20) {
		this.lt20 = lt20;
	}
	public Integer get_20_40() {
		return _20_40;
	}
	public void set_20_40(Integer _20_40) {
		this._20_40 = _20_40;
	}
	public Integer get_40_60() {
		return _40_60;
	}
	public void set_40_60(Integer _40_60) {
		this._40_60 = _40_60;
	}
	public Integer getGt60() {
		return gt60;
	}
	public void setGt60(Integer gt60) {
		this.gt60 = gt60;
	}
	public Integer getNoanswer() {
		return noanswer;
	}
	public void setNoanswer(Integer noanswer) {
		this.noanswer = noanswer;
	}
	
}