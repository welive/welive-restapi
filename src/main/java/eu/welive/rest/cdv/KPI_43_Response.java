/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import javax.xml.bind.annotation.XmlElement;

public class KPI_43_Response
{
	@XmlElement(required=false)
	Integer bilbao;
	@XmlElement(required=false)
	Integer helsinki;
	@XmlElement(required=false)
	Integer novisad;
	@XmlElement(required=false)
	Integer trento;
	
	public Integer getBilbao() {
		return bilbao;
	}
	public void setBilbao(Integer bilbao) {
		this.bilbao = bilbao;
	}
	public Integer getHelsinki() {
		return helsinki;
	}
	public void setHelsinki(Integer helsinki) {
		this.helsinki = helsinki;
	}
	public Integer getNovisad() {
		return novisad;
	}
	public void setNovisad(Integer novisad) {
		this.novisad = novisad;
	}
	public Integer getTrento() {
		return trento;
	}
	public void setTrento(Integer trento) {
		this.trento = trento;
	}
	
	
}