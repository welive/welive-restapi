/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class UserMetadata{
	
	@XmlElement(required=true)
	String birthdate;
	@XmlElement(required=true)
	String address;
	@XmlElement(required=true)
	String city;
	@XmlElement(required=true)
	List<Skill> skills;
	@XmlElement(required=true)
	List<App> usedApps;
	@XmlElement(required=true)
	Location lastKnownLocation;
	@XmlElement(required=true)
	List<String> userTags;
	
	
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public List<Skill> getSkills() {
		return skills;
	}
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	public List<App> getUsedApps() {
		return usedApps;
	}
	public void setUsedApps(List<App> usedApps) {
		this.usedApps = usedApps;
	}
	public Location getLastKnownLocation() {
		return lastKnownLocation;
	}
	public void setLastKnownLocation(Location lastKnownLocation) {
		this.lastKnownLocation = lastKnownLocation;
	}
	public List<String> getUserTags() {
		return userTags;
	}
	public void setUserTags(List<String> userTags) {
		this.userTags = userTags;
	}
}