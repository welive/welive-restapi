/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

public class Role_item{
	
	Integer Citizen;
	Integer Academy;
	Integer Business;
	Integer Entrepreneur;
	
	
	public Integer getCitizen() {
		return Citizen;
	}
	public void setCitizen(Integer citizen) {
		Citizen = citizen;
	}
	public Integer getAcademy() {
		return Academy;
	}
	public void setAcademy(Integer academy) {
		Academy = academy;
	}
	public Integer getBusiness() {
		return Business;
	}
	public void setBusiness(Integer business) {
		Business = business;
	}
	public Integer getEntrepreneur() {
		return Entrepreneur;
	}
	public void setEntrepreneur(Integer entrepreneur) {
		Entrepreneur = entrepreneur;
	}
	
	
}