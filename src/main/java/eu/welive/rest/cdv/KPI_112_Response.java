/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import javax.xml.bind.annotation.XmlElement;

public class KPI_112_Response{
	
	@XmlElement(required=false)
	GenderClassification bilbao;
	@XmlElement(required=false)
	GenderClassification helsinki;
	@XmlElement(required=false)
	GenderClassification novisad;
	@XmlElement(required=false)
	GenderClassification trento;
	
	
	public GenderClassification getBilbao() {
		return bilbao;
	}
	public void setBilbao(GenderClassification bilbao) {
		this.bilbao = bilbao;
	}
	public GenderClassification getHelsinki() {
		return helsinki;
	}
	public void setHelsinki(GenderClassification helsinki) {
		this.helsinki = helsinki;
	}
	public GenderClassification getNovisad() {
		return novisad;
	}
	public void setNovisad(GenderClassification novisad) {
		this.novisad = novisad;
	}
	public GenderClassification getTrento() {
		return trento;
	}
	public void setTrento(GenderClassification trento) {
		this.trento = trento;
	}
	
	
	
}