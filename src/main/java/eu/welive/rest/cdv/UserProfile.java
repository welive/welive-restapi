/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class UserProfile{
	
	@XmlElement(required=true)
	Integer ccUserID;
	@XmlElement(required=true)
	String name;
	@XmlElement(required=true)
	String surname;
	@XmlElement(required=true)
	String gender;
	@XmlElement(required=true)
	String birthdate;
	@XmlElement(required=true)
	String address;
	@XmlElement(required=true)
	String city;
	@XmlElement(required=true)
	String country;
	@XmlElement(required=true)
	String zipCode;
	@XmlElement(required=true)
	Boolean isDeveloper;
	@XmlElement(required=true)
	List<String> languages;
	@XmlElement(required=true)
	List<Skill> skills;
	@XmlElement(required=true)
	List<App> usedApps;
	@XmlElement(required=true)
	ProfileData profileData;
	@XmlElement(required=true)
	Location lastKnownLocation;
	@XmlElement(required=true)
	String referredPilot;
	@XmlElement(required=true)
	String email;
	@XmlElement(required=true)
	List<String> userTags;
	
	public Integer getCcUserID() {
		return ccUserID;
	}
	public void setCcUserID(Integer ccUserID) {
		this.ccUserID = ccUserID;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public Boolean getIsDeveloper() {
		return isDeveloper;
	}
	public void setIsDeveloper(Boolean isDeveloper) {
		this.isDeveloper = isDeveloper;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
	public List<Skill> getSkills() {
		return skills;
	}
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	public List<App> getUsedApps() {
		return usedApps;
	}
	public void setUsedApps(List<App> usedApps) {
		this.usedApps = usedApps;
	}
	public ProfileData getProfileData() {
		return profileData;
	}
	public void setProfileData(ProfileData profileData) {
		this.profileData = profileData;
	}
	public Location getLastKnownLocation() {
		return lastKnownLocation;
	}
	public void setLastKnownLocation(Location lastKnownLocation) {
		this.lastKnownLocation = lastKnownLocation;
	}
	public String getReferredPilot() {
		return referredPilot;
	}
	public void setReferredPilot(String referredPilot) {
		this.referredPilot = referredPilot;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<String> getUserTags() {
		return userTags;
	}
	public void setUserTags(List<String> userTags) {
		this.userTags = userTags;
	}
	
}