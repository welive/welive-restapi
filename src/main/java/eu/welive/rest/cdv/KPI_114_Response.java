/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.cdv;

import javax.xml.bind.annotation.XmlElement;

public class KPI_114_Response{
	
	@XmlElement(required=false)
	Role_item Bilbao;
	@XmlElement(required=false)
	Role_item Uusimaa;
	@XmlElement(required=false)
	Role_item Novisad;
	@XmlElement(required=false)
	Role_item Trento;
	
	
	public Role_item getBilbao() {
		return Bilbao;
	}
	public void setBilbao(Role_item bilbao) {
		this.Bilbao = bilbao;
	}
	public Role_item getUusimaa() {
		return Uusimaa;
	}
	public void setHelsinki(Role_item helsinki) {
		this.Uusimaa = helsinki;
	}
	public Role_item getNovisad() {
		return Novisad;
	}
	public void setNovisad(Role_item novisad) {
		this.Novisad = novisad;
	}
	public Role_item getTrento() {
		return Trento;
	}
	public void setTrento(Role_item trento) {
		this.Trento = trento;
	}
	
}