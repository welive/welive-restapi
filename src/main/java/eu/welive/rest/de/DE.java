/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.de;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse.Status;

import eu.welive.rest.auth.annotation.SecurityFilterCheck;
import eu.welive.rest.auth.roles.UserRoles;
import eu.welive.rest.config.Config;
import eu.welive.rest.de.pojo.AppRequest;
import eu.welive.rest.de.pojo.ArtifactRequest;
import eu.welive.rest.de.pojo.IdeaRequest;
import eu.welive.rest.de.pojo.UserRecommendRequest;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.logging.LoggingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * Created by mikel (m.emaldi at deusto dot es) on 26/11/15.
 */
@Path("de/")
@Api(value = "/de", description = "Contains methods for interacting with the Decision Engine")
@SecurityFilterCheck
public class DE {
	
	private static final Logger logger = Logger.getLogger(DE.class);

	private static String DE_URL;
	private static String UDEUSTO_DE_URL;
	private static LoggingService loggingService;
	private static String LOGGING_SERVICE_NAME = "de";

	protected enum ArtifactType {
		DATASET("Dataset"), BUILDING_BLOCK("Building Block"), APP("Public Service");

		private String name;

		ArtifactType(String name) {
			this.name = name;
		}

		public String getName() {
			return this.name;
		}
	}

	public DE() throws WeLiveException {
		try {
			DE_URL = Config.getInstance().getDecisionEngineURL();
			UDEUSTO_DE_URL = Config.getInstance().getUdeustoDEURL();
			loggingService = LoggingService.getInstance();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error reading welive.properties", e);
		}
	}

	// All artifacts together

	@GET
	@Path("/dataset/{datasetID}/recommend/artifacts")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends artifacts related to the artifact provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendDatasetArtifact(@PathParam("datasetID") String artifactID) throws WeLiveException {
		try {
			JSONObject jsonResponse = new JSONObject();

			Response response = callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("dataset", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("dataset"),
						ArtifactType.DATASET.getName(), ArtifactType.DATASET.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			response = callGET(String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("building_block", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("building_block"),
						ArtifactType.DATASET.getName(), ArtifactType.BUILDING_BLOCK.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			response = callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("app", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("app"), ArtifactType.DATASET.getName(),
						ArtifactType.APP.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			return Response.ok(jsonResponse.toString(), MediaType.APPLICATION_JSON).build();
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Path("/building-block/{buildingBlockID}/recommend/artifacts")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends artifacts related to the artifact provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendBuildingBlockArtifact(@PathParam("buildingBlockID") String artifactID)
			throws WeLiveException {
		try {
			JSONObject jsonResponse = new JSONObject();

			Response response = callGET(
					String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("dataset", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("dataset"),
						ArtifactType.BUILDING_BLOCK.getName(), ArtifactType.DATASET.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			response = callGET(
					String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("building_block", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("building_block"),
						ArtifactType.BUILDING_BLOCK.getName(), ArtifactType.BUILDING_BLOCK.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			response = callGET(String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("app", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("app"),
						ArtifactType.BUILDING_BLOCK.getName(), ArtifactType.APP.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			return Response.ok(jsonResponse.toString(), MediaType.APPLICATION_JSON).build();
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Path("/app/{appID}/recommend/artifacts")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends artifacts related to the artifact provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendAppArtifact(@PathParam("appID") String artifactID) throws WeLiveException {
		try {
			JSONObject jsonResponse = new JSONObject();

			Response response = callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("dataset", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("dataset"), ArtifactType.APP.getName(),
						ArtifactType.DATASET.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			response = callGET(String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("building_block", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("building_block"),
						ArtifactType.APP.getName(), ArtifactType.BUILDING_BLOCK.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			response = callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				jsonResponse.put("app", new JSONArray(response.readEntity(String.class)));
				logRecommendedArtifacts(artifactID, jsonResponse.getJSONArray("app"), ArtifactType.APP.getName(),
						ArtifactType.APP.getName());
			} else if (response.getStatus() == 404) {
				return Response.status(Status.NOT_FOUND).entity(response.readEntity(String.class)).build();
			}

			return Response.ok(jsonResponse.toString(), MediaType.APPLICATION_JSON).build();
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	// Datasets

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/dataset/{datasetID}/recommend/datasets")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends datasets related to the dataset provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendDatasetDataset(@PathParam("datasetID") String artifactID) throws WeLiveException {
		try {
			Response response = callGET(String.format("%s/dataset/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.DATASET.getName(),
						ArtifactType.DATASET.getName());
				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}

		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/dataset/{datasetID}/recommend/building-blocks")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends building blocks related to the dataset provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendDatasetBuildingBlock(@PathParam("datasetID") String artifactID) throws WeLiveException {
		try {
			Response response = callGET(
					String.format("%s/dataset/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.DATASET.getName(),
						ArtifactType.BUILDING_BLOCK.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/dataset/{datasetID}/recommend/apps")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends apps to the dataset provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendDatasetApp(@PathParam("datasetID") String artifactID) throws WeLiveException {
		try {
			Response response = callGET(String.format("%s/dataset/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.DATASET.getName(),
						ArtifactType.APP.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@PUT
	@RolesAllowed(UserRoles.BASIC_USER)
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/dataset/{datasetID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates or updates the dataset metadata.")
	@Timed
	@ExceptionMetered
	public Response updateDataset(@PathParam("datasetID") String artifactID, ArtifactRequest request,
			@HeaderParam("Authorization") String auth) throws WeLiveException {
		try {
			if (request == null) {
                throw new WeLiveException(Response.Status.BAD_REQUEST, "The body of the request is empty!");
            }
			JSONObject modJSON = new JSONObject();
			modJSON.put("lang", request.getLang());
			modJSON.put("tags", new JSONArray(request.getTags()));
			modJSON.put("id", artifactID);

			Response response = callGET(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 404) {
				logArtifact(artifactID, ArtifactType.DATASET.getName(), "Artifact Published", "ArtifactPublished");
				return callPOST(String.format("%s/dataset/", UDEUSTO_DE_URL), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			} else {
				return callPUT(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@DELETE
	@RolesAllowed(UserRoles.BASIC_USER)
	@Path("/dataset/{datasetID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Removes the dataset metadata from the recommender engine.")
	@Timed
	@ExceptionMetered
	public Response removeDataset(@PathParam("datasetID") String artifactID, @HeaderParam("Authorization") String auth)
			throws WeLiveException {
		Response response = callDelete(String.format("%s/dataset/%s/", UDEUSTO_DE_URL, artifactID), auth);
		if (response.getStatus() == 204) {
			try {
				logArtifact(artifactID, ArtifactType.DATASET.getName(), "Artifact Removed", "ArtifactRemoved");
			} catch (URISyntaxException e) {
                logger.error(e.getMessage(), e);
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}
		}
		return response;
	}

	// Building Blocks

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/building-block/{buildingBlockID}/recommend/datasets")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends datasets related to the building block provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendBuildingBlockDataset(@PathParam("buildingBlockID") String artifactID)
			throws WeLiveException {
		try {
			Response response = callGET(
					String.format("%s/buildingblock/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.BUILDING_BLOCK.getName(),
						ArtifactType.DATASET.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/building-block/{buildingBlockID}/recommend/building-blocks")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends building blocks related to the building block provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendBuildingBlockBuildingBlock(@PathParam("buildingBlockID") String artifactID)
			throws WeLiveException {
		try {
			Response response = callGET(
					String.format("%s/buildingblock/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.BUILDING_BLOCK.getName(),
						ArtifactType.BUILDING_BLOCK.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/building-block/{buildingBlockID}/recommend/apps")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends apps to the building block provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendBuildingBlockApp(@PathParam("buildingBlockID") String artifactID) throws WeLiveException {
		try {
			Response response = callGET(
					String.format("%s/buildingblock/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.BUILDING_BLOCK.getName(),
						ArtifactType.APP.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@PUT
	@RolesAllowed(UserRoles.BASIC_USER)
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/building-block/{buildingBlockID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates or opdates the building block metadata.")
	@Timed
	@ExceptionMetered
	public Response updateBuildingBlock(@PathParam("buildingBlockID") String artifactID, ArtifactRequest request,
			@HeaderParam("Authorization") String auth) throws WeLiveException {
		try {
            if (request == null) {
                throw new WeLiveException(Response.Status.BAD_REQUEST, "The body of the request is empty!");
            }
			JSONObject modJSON = new JSONObject();
			modJSON.put("lang", request.getLang());
			modJSON.put("tags", request.getTags());
			modJSON.put("id", artifactID);

			Response response = callGET(String.format("%s/buildingblock/%s/", UDEUSTO_DE_URL, artifactID));

			if (response.getStatus() == 404) {
				logArtifact(artifactID, ArtifactType.BUILDING_BLOCK.getName(), "Artifact Published",
						"ArtifactPublished");
				return callPOST(String.format("%s/buildingblock/", UDEUSTO_DE_URL), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			} else {
				return callPUT(String.format("%s/buildingblock/%s/", UDEUSTO_DE_URL, artifactID), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@DELETE
	@RolesAllowed(UserRoles.BASIC_USER)
	@Path("/building-block/{buildingBlockID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Removes the building block metadata from the recommender engine.")
	@Timed
	@ExceptionMetered
	public Response removeBuildingBlock(@PathParam("buildingBlockID") String artifactID,
			@HeaderParam("Authorization") String auth) throws WeLiveException {
		Response response = callDelete(String.format("%s/buildingblock/%s", UDEUSTO_DE_URL, artifactID), auth);
		if (response.getStatus() == 204) {
			try {
				logArtifact(artifactID, ArtifactType.BUILDING_BLOCK.getName(), "Artifact Removed", "ArtifactRemoved");
			} catch (URISyntaxException e) {
                logger.error(e.getMessage(), e);
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}
		}
		return response;
	}

	// Apps

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/app/{appID}/recommend/datasets")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends datasets related to the app provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendAppDataset(@PathParam("appID") String artifactID) throws WeLiveException {
		try {
			Response response = callGET(String.format("%s/app/%s/recommend/dataset/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.APP.getName(),
						ArtifactType.DATASET.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/app/{appID}/recommend/building-blocks")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends building blocks related to the app provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendAppBuildingBlock(@PathParam("appID") String artifactID) throws WeLiveException {
		try {
			Response response = callGET(
					String.format("%s/app/%s/recommend/buildingblock/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.APP.getName(),
						ArtifactType.BUILDING_BLOCK.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/app/{appID}/recommend/apps")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommends apps to the app provided using their tags.")
	@Timed
	@ExceptionMetered
	public Response recommendAppApp(@PathParam("appID") String artifactID) throws WeLiveException {
		try {
			Response response = callGET(String.format("%s/app/%s/recommend/app/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonObject = new JSONArray(response.readEntity(String.class));

				logRecommendedArtifacts(artifactID, jsonObject, ArtifactType.APP.getName(), ArtifactType.APP.getName());

				return Response.status(response.getStatus()).entity(jsonObject.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@PUT
	@RolesAllowed(UserRoles.BASIC_USER)
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/app/{appID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates or updates the app metadata.")
	@Timed
	@ExceptionMetered
	public Response updateApp(@PathParam("appID") String artifactID, AppRequest request,
			@HeaderParam("Authorization") String auth) throws WeLiveException {
		try {
            if (request == null) {
                throw new WeLiveException(Response.Status.BAD_REQUEST, "The body of the request is empty!");
            }
			JSONObject modJSON = new JSONObject();
			modJSON.put("lang", request.getLang());
			modJSON.put("tags", new JSONArray(request.getTags()));
			modJSON.put("scope", request.getScope());
			modJSON.put("min_age", request.getMinimumAge());
			modJSON.put("id", artifactID);

			Response response = callGET(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 404) {
				logArtifact(artifactID, ArtifactType.APP.getName(), "Artifact Published", "ArtifactPublished");
				return callPOST(String.format("%s/app/", UDEUSTO_DE_URL), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			} else {
				return callPUT(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@DELETE
	@RolesAllowed(UserRoles.BASIC_USER)
	@Path("/app/{appID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Removes the building block metadata from the recommender engine.")
	@Timed
	@ExceptionMetered
	public Response removeApp(@PathParam("appID") String artifactID, @HeaderParam("Authorization") String auth)
			throws WeLiveException {
		Response response = callDelete(String.format("%s/app/%s/", UDEUSTO_DE_URL, artifactID), auth);
		if (response.getStatus() == 204) {
			try {
				logArtifact(artifactID, ArtifactType.APP.getName(), "Artifact Removed", "ArtifactRemoved");
			} catch (URISyntaxException e) {
                logger.error(e.getMessage(), e);
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
			}
		}
		return response;
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("user/{userID}/apps")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recommend applications to a user based on his/her age, location and preferences.")
	@Timed
	@ExceptionMetered
	public Response appRecommendation(@PathParam("userID") String userID,
			@ApiParam(value = "current latitude of the user", required = false) @QueryParam("lat") double lat,
			@ApiParam(value = "current long of the user", required = false) @QueryParam("lon") double lon,
			@QueryParam("radius") double radius) throws WeLiveException {
		try {

			Response response = callGET(String.format("%s/user/%s/apps/?lat=%s&lon=%s&radius=%s", UDEUSTO_DE_URL,
					userID, lat, lon, radius));
			if (response.getStatus() == 200) {
				JSONArray jsonResult = new JSONArray(response.readEntity(String.class));
				logApp(userID, lat, lon, radius, jsonResult);

				return Response.status(response.getStatus()).entity(jsonResult.toString())
						.type(MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	// Ideas

	@PUT
	@RolesAllowed(UserRoles.BASIC_USER)
	@Consumes(MediaType.APPLICATION_JSON + "; charset=utf-8")
	@Path("/idea/{ideaID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Creates or updates the idea metadata.")
	@Timed
	@ExceptionMetered
	public Response updateIdea(@PathParam("ideaID") String artifactID, IdeaRequest request,
			@HeaderParam("Authorization") String auth) throws WeLiveException {
		try {
            if (request == null) {
                throw new WeLiveException(Response.Status.BAD_REQUEST, "The body of the request is empty!");
            }
			JSONObject modJSON = new JSONObject();
			modJSON.put("lang", request.getLang());
			modJSON.put("tags", request.getTags());
			modJSON.put("id", artifactID);

			Response response = callGET(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 404) {
				logIdea(artifactID);
				return callPOST(String.format("%s/idea/", UDEUSTO_DE_URL), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			} else {
				return callPUT(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID), modJSON.toString(),
						MediaType.APPLICATION_JSON, auth);
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	@DELETE
	@RolesAllowed(UserRoles.BASIC_USER)
	@Path("/idea/{ideaID}")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Removes the idea metadata from the recommender engine.")
	@Timed
	@ExceptionMetered
	public Response removeIdea(@PathParam("ideaID") String artifactID, @HeaderParam("Authorization") String auth)
			throws WeLiveException {
		return callDelete(String.format("%s/idea/%s/", UDEUSTO_DE_URL, artifactID), auth);
	}

	@GET
	@Path("/idea/{ideaID}/recommend/ideas")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Recomends ideas related to the one provided using Latent Dirichlet Allocation.")
	@Timed
	@ExceptionMetered
	public Response recommendIdea(@PathParam("ideaID") String artifactID) throws WeLiveException {
		try {

			Response response = callGET(String.format("%s/idea/%s/recommend/idea/", UDEUSTO_DE_URL, artifactID));
			if (response.getStatus() == 200) {
				JSONArray jsonResult = new JSONArray(response.readEntity(String.class));
				logIdeaRecommended(artifactID, jsonResult);

				return Response.ok(jsonResult.toString(), MediaType.APPLICATION_JSON).build();
			} else {
				return Response.status(response.getStatus()).entity(response.readEntity(String.class))
						.type(MediaType.APPLICATION_JSON).build();
			}
		} catch (JSONException | URISyntaxException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	protected Response callGET(String path) throws WeLiveException {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(path);
		Invocation.Builder request = target.request();
		request.header("Accept", MediaType.APPLICATION_JSON);
		request.header("Content-type", MediaType.APPLICATION_JSON);
		try {
			Response response = request.get();
			return response;
		} catch (ProcessingException e) {
			logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
		}
		
	}

	protected Response callPOST(String path, String inputJSON, String MIMEType, String auth) throws WeLiveException {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(path);
		Invocation.Builder request = target.request();
		request.header("Accept", MIMEType);
		request.header("Content-type", MIMEType);
		if (auth != null) {
			request.header("Authorization", auth);
		}

		try {
			Response response = request.post(Entity.entity(inputJSON, MIMEType));
			if (response.getStatus() >= 400) {
				String responseStr = response.readEntity(String.class);
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatus()), responseStr);
			} else {
				return response;
			}
		} catch (ProcessingException e) {
			logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
		}
	}

	protected Response callPUT(String path, String inputJSON, String MIMEType, String auth) throws WeLiveException {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(path);
		Invocation.Builder request = target.request();
		if (auth != null) {
			request.header("Authorization", auth);
		}
		request.header("Accept", MIMEType);
		request.header("Content-type", MIMEType);
		Response response = request.put(Entity.entity(inputJSON, MIMEType));

		try {
			if (response.getStatus() >= 400) {
				String responseStr = response.readEntity(String.class);
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatus()), responseStr);
			} else {
				return response;
			}
		} catch (ProcessingException e) {
			logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
		}
	}

	protected Response callDelete(String path, String auth) throws WeLiveException {
		Client client = ClientBuilder.newClient();
		WebTarget target = client.target(path);
		Invocation.Builder request = target.request();
		if (auth != null) {
			request.header("Authorization", auth);
		}
		request.header("Accept", MediaType.APPLICATION_JSON);
		try {
			Response response = request.delete();
			if (response.getStatus() >= 400) {
				String responseStr = response.readEntity(String.class);
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatus()), responseStr);
			} else {
				return response;
			}
		} catch (ProcessingException e) {
			logger.error(e.getMessage(), e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage(), e);
		}
	}

	@POST
	@RolesAllowed(UserRoles.BASIC_USER)
	@Path("/idea/recommend/users_by_skills")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	@ApiOperation(value = "Recommends users whose skills match the tags and/or categories of an idea as collaborators.")
	@Timed
	@ExceptionMetered
	public Response recommendUsersBySkills(
			@ApiParam(value = "\"Basic \" + credentials hash.", required = true) @HeaderParam("Authorization") String auth,
			UserRecommendRequest request) throws WeLiveException {
		try {
			if (auth == null || !auth.startsWith("Basic ")) {
				JSONObject json = new JSONObject();
				json.put("error", true);
				json.put("message", "Basic authentication required");
				return Response.status(403).entity(json.toString()).build();
			}
			Gson gson = new Gson();
			String jsonRequest = gson.toJson(request);
			return callPOST(String.format("%s/de/recommend_users_by_skills", DE_URL), jsonRequest,
					MediaType.APPLICATION_JSON, auth);
		} catch (JSONException e) {
            logger.error("Error when connecting to the Decision Engine", e);
			throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Error when connecting to the Decision Engine", e);
		}
	}

	protected void logArtifact(String artifactID, String artifactType, String msg, String type)
			throws URISyntaxException {
		JSONObject logJSON = new JSONObject();
		logJSON.put("msg", msg);
		logJSON.put("appId", LOGGING_SERVICE_NAME);
		logJSON.put("type", type);
		logJSON.put("timestamp", new Date().getTime() / 1000);
		JSONObject customAttributes = new JSONObject();
		customAttributes.put("artifactid", artifactID);
		customAttributes.put("artifacttype", artifactType);
		logJSON.put("custom_attr", customAttributes);
		sendLog(LOGGING_SERVICE_NAME, logJSON);
	}

	protected void logRecommendedArtifacts(String artifactID, JSONArray resultJSON, String sourceArtifactType,
			String recommendedArtifactType) throws URISyntaxException {
		JSONObject logJSON = new JSONObject();
		logJSON.put("msg", "Artifact Recommended");
		logJSON.put("appId", LOGGING_SERVICE_NAME);
		logJSON.put("type", "ArtifactRecommended");
		logJSON.put("timestamp", new Date().getTime() / 1000);
		JSONObject customAttributes = new JSONObject();
		customAttributes.put("sourceartifactid", artifactID);
		customAttributes.put("sourceartifacttype", sourceArtifactType);
		customAttributes.put("recommendedartifacttype", recommendedArtifactType);
		customAttributes.put("recommendedartifactlist", resultJSON);
		logJSON.put("custom_attr", customAttributes);
		sendLog(LOGGING_SERVICE_NAME, logJSON);
	}

	protected void logApp(String userID, double lat, double lon, double radius, JSONArray jsonResult)
			throws URISyntaxException {
		JSONObject logJSON = new JSONObject();
		logJSON.put("msg", "User Recommendation");
		logJSON.put("appId", LOGGING_SERVICE_NAME);
		logJSON.put("type", "UserRecommendation");
		logJSON.put("timestamp", new Date().getTime() / 1000);

		JSONObject customAttributes = new JSONObject();
		customAttributes.put("userid", userID);
		customAttributes.put("lat", lat);
		customAttributes.put("lon", lon);
		customAttributes.put("radius", radius);
		customAttributes.put("recommendedapplist", jsonResult);
		logJSON.put("custom_attr", customAttributes);
		sendLog(LOGGING_SERVICE_NAME, logJSON);
	}

	protected void logIdea(String artifactID) throws URISyntaxException {
		JSONObject logJSON = new JSONObject();
		logJSON.put("msg", "Idea Published");
		logJSON.put("appId", LOGGING_SERVICE_NAME);
		logJSON.put("type", "IdeaPublished");
		logJSON.put("timestamp", new Date().getTime() / 1000);
		JSONObject customAttributes = new JSONObject();
		customAttributes.put("ideaid", artifactID);
		logJSON.put("custom_attr", customAttributes);
		sendLog(LOGGING_SERVICE_NAME, logJSON);
	}

	protected void logIdeaRecommended(String artifactID, JSONArray jsonResult) throws URISyntaxException {
		JSONObject logJSON = new JSONObject();
		logJSON.put("msg", "Idea Recommended");
		logJSON.put("appId", LOGGING_SERVICE_NAME);
		logJSON.put("type", "IdeaRecommended");
		logJSON.put("timestamp", new Date().getTime() / 1000);
		JSONObject customAttributes = new JSONObject();
		customAttributes.put("sourceideaid", artifactID);
		customAttributes.put("recommendedidealist", jsonResult);
		logJSON.put("custom_attr", customAttributes);

		sendLog(LOGGING_SERVICE_NAME, logJSON);
	}

	protected void sendLog(String serviceName, JSONObject jsonObject) throws URISyntaxException {
		loggingService.sendLog(serviceName, jsonObject);
	}
}