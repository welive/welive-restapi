/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.de.pojo;

import java.util.ArrayList;
import java.util.List;

public class UserRecommendRequest {
    private String lang;
    private List<String> tag_list;
    private List<String> category_list;
    
    public UserRecommendRequest() {
    	lang = "";
    	tag_list = new ArrayList<String>();
    	category_list = new ArrayList<String>();
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<String> getTag_list() {
        return tag_list;
    }

    public void setTag_list(List<String> tag_list) {
        this.tag_list = tag_list;
    }

    public List<String> getCategory_list() {
        return category_list;
    }

    public void setCategory_list(List<String> category_list) {
        this.category_list = category_list;
    }
}

