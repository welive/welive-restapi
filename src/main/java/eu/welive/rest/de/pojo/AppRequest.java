/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.de.pojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mikel on 16/03/16.
 */
public class AppRequest {
    String lang;
    List<String> tags;
    String scope;
    int minimumAge;

    public AppRequest() {
        this.lang = "";
        this.tags = new ArrayList<String>();
        this.scope = "";
        this.minimumAge = 0;
    }

    public AppRequest(String lang, List<String> tags, String scope, int minimumAge) {
        this.lang = lang;
        this.tags = tags;
        this.scope = scope;
        this.minimumAge = minimumAge;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public int getMinimumAge() {
        return minimumAge;
    }

    public void setMinimumAge(int minimumAge) {
        this.minimumAge = minimumAge;
    }
}
