/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.scheduler;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.quartz.SchedulerException;

import eu.iescities.server.querymapper.util.scheduler.QuartzScheduler;
import eu.welive.rest.exception.UnexpectedErrorException;
import eu.welive.rest.exception.WeLiveException;
import eu.welive.rest.scheduler.WeLiveMainJob;

@Path("scheduler/")
public class SchedulerInfo {

	@GET
	@Path("/info")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSwaggerConf() throws WeLiveException {
		final Set<String> registeredJobs = getRegisteredJobs();
		final Set<String> executingJobs = getExecutingJobs();
		
		final StringBuilder strBuilder = new StringBuilder();
		
		strBuilder.append(String.format("Registered jobs: %d\n", registeredJobs.size()));
		for (String job : registeredJobs) {
			strBuilder.append(String.format("\t%s\n", job));
		}
		
		strBuilder.append("\n");
		
		strBuilder.append(String.format("Executing jobs: %d\n", executingJobs.size()));
		for (String job : executingJobs) {
			strBuilder.append(String.format("\t%s", job));
		}
		
		return strBuilder.toString();
	}
	
	private Set<String> getRegisteredJobs() throws UnexpectedErrorException {
		try {
			final WeLiveMainJob mainJob = new WeLiveMainJob();
			return mainJob.getRegisteredJobs(QuartzScheduler.getInstance().getScheduler());
		} catch (SchedulerException e) {
			throw new UnexpectedErrorException("Could not obtain registered jobs from scheduler", e);
		}
	}
	
	private Set<String> getExecutingJobs() throws UnexpectedErrorException {
		try {
			final WeLiveMainJob mainJob = new WeLiveMainJob();
			return mainJob.getExecutingJobs(QuartzScheduler.getInstance().getScheduler());
		} catch (SchedulerException e) {
			throw new UnexpectedErrorException("Could not obtain executing jobs from scheduler", e);
		}
	}
}
