/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.ckan.data;

import org.json.JSONObject;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import eu.iescities.server.querymapper.datasource.ConnectorFactory;
import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;

public class CKANResource {

	public static final String ID = "id";
	public static final String PACKAGE_ID = "package_id";
	public static final String MAPPING = "mapping";
	public static final String PERMISSIONS = "permissions";

	public static final String EMPTY_MAPPING = "{}";	
	
	private String id = "";
	private String packageId = "";
	private String mapping = "";
	private String permissions = "";
	
	private String mergedMapping;
	
	public CKANResource(String id, String packageId, String mapping, String permissions) {
		this.id = id;
		this.packageId = packageId;
		this.mapping = mapping;
		this.permissions = permissions;
		
		mergeMapping();
	}
	
	public CKANResource(JSONObject resourceInfo) {
		this.id = resourceInfo.getString(ID);
		this.packageId = resourceInfo.getString(PACKAGE_ID);
		
		if (resourceInfo.has(MAPPING)) {
			this.mapping = resourceInfo.getString(MAPPING);
		}
		
		if (resourceInfo.has(PERMISSIONS)) {
			this.permissions = resourceInfo.getString(PERMISSIONS);
		}
		
		mergeMapping();
	}

	private void mergeMapping() {
		if (hasMapping() && hasPermissions() && !getPermissions().isEmpty()) {
			if (hasMapping() && hasPermissions()) {
				final Gson gson = new GsonBuilder().create();
				
				final JsonObject jsonMapping = gson.fromJson(mapping, JsonObject.class);
				final JsonObject jsonPermissions = gson.fromJson(permissions, JsonObject.class);
				
				jsonMapping.add(PERMISSIONS, jsonPermissions);
				this.mergedMapping = jsonMapping.toString();
			}
		} else {
			this.mergedMapping = mapping;
		}
	}
	
	public String getMergedMapping() {
		return this.mergedMapping;
	}
	
	public String getID() {
		return id;
	}
	
	public String getPackageId() {
		return packageId;
	}
	
	public boolean hasMapping() {
		return !mapping.isEmpty();
	}
	
	public String getMapping() {
		return mapping;
	}
	
	public DataSourceConnector getConnector() throws DataSourceConnectorException {
		final DataSourceConnector connector = ConnectorFactory.load(mergedMapping);
		return connector;
	}
	
	public boolean hasPermissions() {
		return !permissions.isEmpty();
	}
	
	public String getPermissions() {
		return permissions;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof CKANResource)) {
			return false;
		}
		
		final CKANResource resourceInfo = (CKANResource)o;
		
		return this.getID().equals(resourceInfo.getID())
			&& this.getPackageId().equals(resourceInfo.getPackageId())
			&& this.getMapping().equals(resourceInfo.getMapping())
			&& this.getPermissions().equals(resourceInfo.getPermissions());
	}

	@Override
	public int hashCode() {
		return this.getID().hashCode();
	}
}
