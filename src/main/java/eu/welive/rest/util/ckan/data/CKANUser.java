/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.ckan.data;

import org.json.JSONObject;

public class CKANUser {

	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String CAPACITY = "capacity";
	public static final String SUCCESS = "success";
	
	private final String id;
	private final String name;
	private final String capacity;
	
	public CKANUser(JSONObject userInfo) {
		if (userInfo.has(ID)) {
			this.id = userInfo.getString(ID);
		} else {
			this.id = "";
		}
		
		if (userInfo.has(NAME)) {
			this.name = userInfo.getString(NAME);
		} else {
			this.name = "";
		}
		
		if (userInfo.has(CAPACITY)) {
			this.capacity = userInfo.getString(CAPACITY);
		} else {
			this.capacity = "";
		}
	}
	
	public String getID() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getCapacity() {
		return capacity;
	}
}
