/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.ckan.data;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class CKANOrganization {
	
	public static final String ID = "id";
	public static final String USERS = "users";
	 
	private String id = "";
	private List<CKANUser> users = new ArrayList<CKANUser>();
	
	public CKANOrganization(String id) {
		this.id = id;
	}
	
	public CKANOrganization(JSONObject organizationInfo) {
		if (organizationInfo.has(ID)) {
			this.id = organizationInfo.getString(ID);
		}
		
		if (organizationInfo.has(USERS)) {			
			final JSONArray userArray = organizationInfo.getJSONArray(USERS);
			for (int i = 0; i < userArray.length(); i++) {
				final JSONObject userInfo = userArray.getJSONObject(i);
				users.add(new CKANUser(userInfo));
			}
		} 
	}
	
	public String getID() {
		return id;
	}
	
	public List<CKANUser> getUsers() {
		return users;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof CKANOrganization)) {
			return false;
		}
		
		final CKANOrganization organization = (CKANOrganization)o;
		return this.id.equals(organization.id)
			&& this.users.equals(organization.users);
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
}
