/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.ckan;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.http.client.utils.URIBuilder;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import com.sciamlab.ckan4j.CKANApiClient;
import com.sciamlab.ckan4j.CKANApiClient.CKANApiClientBuilder;
import com.sciamlab.ckan4j.exception.CKANException;

import eu.iescities.server.querymapper.datasource.security.DataSourceSecurityManager;
import eu.welive.rest.config.Config;
import eu.welive.rest.datasource.manager.AccessData.WeLiveRole;
import eu.welive.rest.util.ckan.data.CKANOrganization;
import eu.welive.rest.util.ckan.data.CKANPackage;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.data.CKANUser;
import eu.welive.rest.util.ckan.exception.CKANConnectionException;
import eu.welive.rest.util.ckan.exception.CKANUtilException;
import eu.welive.rest.util.ckan.exception.CKANUtilNotFoundException;

public class CKANUtil {
	
	private final CKANApiClient ckanAPIClient;
	private final String searchAPI;
	
	public CKANUtil() throws CKANConnectionException {
		try {
			final Config config = Config.getInstance();
			this.searchAPI = config.getCkanSearchApi();
			final CKANApiClientBuilder builder = CKANApiClientBuilder.init(config.getCkanEndpoint());
			this.ckanAPIClient = builder.build();
		} catch (IOException e) {
			throw new CKANConnectionException(e);
		}
	}
	
	public CKANResource getResourceInfo(String resourceId) throws CKANUtilException {
		try {
			final JSONObject resourceInfo = ckanAPIClient.resourceShow(resourceId);
			return new CKANResource(resourceInfo);
		} catch (CKANException e) {
			if (e.getError().getString("__type").equals("Not Found Error")) {
				throw new CKANUtilNotFoundException("Resource " + resourceId + " not found");
			}
			throw new CKANUtilException(e);
		}
	}
	
	public CKANPackage getPackageInfo(String packageId) throws CKANUtilException {
		try {
			final JSONObject packageInfo = ckanAPIClient.packageShow(packageId);
			return new CKANPackage(packageInfo);
		} catch (CKANException e) {
			if (e.getError().getString("__type").equals("Not Found Error")) {
				System.out.println("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
				throw new CKANUtilNotFoundException("Package " + packageId + " not found");
			}
			throw new CKANUtilException(e);
		}
	}

	public List<String> getAllPackages() throws CKANUtilException {
		try {
			final List<String> allPackages = new ArrayList<String>();
			final JSONArray packages = ckanAPIClient.packageList();
			for (int packageIndex = 0; packageIndex < packages.length(); packageIndex++) {
				final String packageName = packages.getString(packageIndex);
				allPackages.add(packageName);
			}
			return allPackages;
		} catch (CKANException e) {
			throw new CKANUtilException(e);
		}
	}
	
	public List<String> getUpdatedPackages(DateTime lastRevision) throws CKANUtilException {
		if (lastRevision == null) {
			return getAllPackages();
		} else {
			try {
				final Client client = ClientBuilder.newClient();
				final URIBuilder uriBuilder = new URIBuilder(String.format("%s/revision?since_time=%s", searchAPI, lastRevision.toString("YYYY-MM-dd'T'HH:mm:ss")));
				
				final String response = client.target(uriBuilder.build())
											.request()
											.get(String.class);

				final JSONArray revisionList = new JSONArray(response);

				final Set<String> packageIDSet = new TreeSet<String>();
				for (int i = 0; i < revisionList.length(); i++) {
					final String revisionID = revisionList.getString(i);
					
					final MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
					params.add("id", revisionID);
					final JSONObject revision = (JSONObject) ckanAPIClient.actionGET("revision_show", params);
					if (revision.has("packages")) {
						final JSONArray revisionPackages = revision.getJSONArray("packages");
						for (int j = 0; j < revisionPackages.length(); j++) {
							packageIDSet.add(revisionPackages.getString(j));
						}
					}
				}
				
				return new ArrayList<String>(packageIDSet);
				
			} catch (URISyntaxException | CKANException e) {
				throw new CKANUtilException(e);
			}
		}
	}
	
	
	public WeLiveRole getUserRole(String packageId, String userId) throws CKANUtilException {
		final CKANPackage packageInfo = getPackageInfo(packageId);
		
		if (!userId.isEmpty()) {
			if (userId.equals(DataSourceSecurityManager.ADMIN_USER)) {
				return WeLiveRole.ADMIN;
			}
			
			final CKANUser ckanUser = getUserByUserId(userId);
			if (packageInfo.hasCreatorUserID() && ckanUser.getID().equals(packageInfo.getCreatorUserID())) {
				return WeLiveRole.CREATOR;
			}
				
			if (packageInfo.hasOrganization()) {
				final String organizationId = packageInfo.getOrganization().getID();
				
				final CKANOrganization organization = getOrganization(organizationId);
				for (CKANUser orgUsers : organization.getUsers()) {
					if (ckanUser.getID().equals(orgUsers.getID())) {
						try {
							return WeLiveRole.valueOf(orgUsers.getCapacity().toUpperCase()); 
						} catch (IllegalArgumentException | NullPointerException e) {
							return WeLiveRole.NONE;
						}
					}
				}
			}
		}
		
		return WeLiveRole.NONE;
	}
	
	public CKANOrganization getOrganization(String organizationId) throws CKANUtilException {
		try {
			final JSONObject organizationInfo = ckanAPIClient.organizationShow(organizationId, false);
			return new CKANOrganization(organizationInfo);
		} catch (CKANException e) {
			throw new CKANUtilException(e);
		}
	}

	public CKANUser getUser(String userID) throws CKANUtilException {
		try {
			final JSONObject userInfo = ckanAPIClient.userShow(userID);
			return new CKANUser(userInfo);
		} catch (CKANException e) {
			throw new CKANUtilException(e);
		}
	}
	
	public CKANUser getUserByUserId(String userId) throws CKANUtilException {
		try {
			final MultivaluedMap<String, String> params = new MultivaluedHashMap<>();
			params.add("cc_user_id", userId);
			final JSONObject response = (JSONObject) ckanAPIClient.actionGET("user_by_ccuserid", params);
			return new CKANUser(response);
		} catch (CKANException e) {
			throw new CKANUtilException(e);
		}
	}
}
