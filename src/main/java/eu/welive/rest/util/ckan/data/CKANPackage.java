/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.ckan.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class CKANPackage {

	public static final String ID = "id";
	public static final String CREATOR_USER_ID = "creator_user_id";
	public static final String ORGANIZATION = "organization";
	public static final String RESOURCES = "resources";
	
	private String id = "";
	private String creatorUserId = "";
	private CKANOrganization organization = null;
	private List<CKANResource> resources = Collections.emptyList();
	
	public CKANPackage(String id, String creatorUserId, String organizationId, List<CKANResource> resources) {
		this.id = id;
		this.creatorUserId = creatorUserId;
		this.organization = new CKANOrganization(organizationId);
		this.resources = resources;
	}
	
	public CKANPackage(JSONObject packageInfo) {		
		this.id = packageInfo.getString(ID);
		
		if (packageInfo.has(CREATOR_USER_ID)) {
			this.creatorUserId = packageInfo.getString(CREATOR_USER_ID);
		}
		
		if (packageInfo.has(ORGANIZATION)) {
			final JSONObject organizationInfo = packageInfo.getJSONObject(ORGANIZATION);
			this.organization = new CKANOrganization(organizationInfo);
		}
		
		if (packageInfo.has(RESOURCES)) {
			final List<CKANResource> resources = new ArrayList<CKANResource>();
			final JSONArray resourceArray = packageInfo.getJSONArray(RESOURCES);
			for (int resourceIndex = 0; resourceIndex < resourceArray.length(); resourceIndex++) {
				final JSONObject resourceInfo = resourceArray.getJSONObject(resourceIndex);
				resources.add(new CKANResource(resourceInfo));
			}
			
			this.resources = resources;
		}
	}
	
	public String getID() {
		return id;
	}
	
	public String getCreatorUserID() {
		return creatorUserId;
	}
	
	public boolean hasCreatorUserID() {
		return !creatorUserId.isEmpty();
	}
	
	public boolean hasOrganization() {
		return organization != null;
	}
	
	public CKANOrganization getOrganization() {
		return organization;
	}
	
	public boolean hasResources() {
		return !resources.isEmpty();
	}
	
	public List<CKANResource> getResources() {
		return resources;
	}
	
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof CKANPackage)) {
			return false;
		}
		
		final CKANPackage packageInfo = (CKANPackage)o;
		return this.id.equals(packageInfo.id)
			&& this.creatorUserId.equals(packageInfo.creatorUserId)
			&& this.organization.equals(packageInfo.organization)
			&& this.resources.equals(packageInfo.resources);
	}

	@Override
	public int hashCode() {
		return this.id.hashCode();
	}
}
