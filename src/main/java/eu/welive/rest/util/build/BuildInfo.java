/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.build;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import eu.welive.rest.exception.UnexpectedErrorException;
import eu.welive.rest.exception.WeLiveException;

@Path("build/")
public class BuildInfo {

	@GET
	@Path("/info")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSwaggerConf() throws WeLiveException {
		try (final BufferedReader reader = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(("/git.properties"))))) {
			final StringBuilder strBuilder = new StringBuilder();
			
			String line = reader.readLine();
		    while (line != null) {
		    	strBuilder.append(line + "\n");
		    	line = reader.readLine();
		    }
			
			return strBuilder.toString();
		} catch (IOException e) {
			throw new UnexpectedErrorException("Could not load git.properties file", e);
		}
	}
}
