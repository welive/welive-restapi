/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.swagger;

import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import eu.welive.rest.config.Config;
import eu.welive.rest.exception.UnexpectedErrorException;
import eu.welive.rest.exception.WeLiveException;

@Path("swagger/")
public class SwaggerAppConf {

	@GET
	@Path("/conf")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSwaggerConf() throws WeLiveException {
		try {
			final Config config = Config.getInstance();
			return config.getSwaggerAppClientId();
		} catch (IOException e) {
			throw new UnexpectedErrorException("Could not read properties", e); 
		}
	}
}
