/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.util.swagger;

import java.io.IOException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import eu.welive.rest.config.Config;
import io.swagger.jaxrs.config.BeanConfig;

@WebListener
public class SwaggerInitializer implements ServletContextListener {
	
	private static final Logger logger = LogManager.getLogger(SwaggerInitializer.class);

	public void contextInitialized(ServletContextEvent servletContextEvent) {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setTitle("WeLive REST API");
		beanConfig.setVersion("1.0.0");
		beanConfig.setResourcePackage("eu.welive.rest");
		
		try {
			final Config config = Config.getInstance();
        	eu.iescities.server.querymapper.config.Config.getInstance().setDataDir(config.getDataDir());
    		beanConfig.setBasePath(config.getSwaggerURL());
    		beanConfig.setHost(config.getHost());
        } catch (IOException e) {
        	logger.error("Problem reading config file information", e);
        }
		
		beanConfig.setScan(true);
	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {

	}
}
