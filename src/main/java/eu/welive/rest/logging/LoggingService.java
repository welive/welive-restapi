/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.logging;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.glassfish.jersey.server.wadl.WadlApplicationContext;
import org.glassfish.jersey.server.wadl.internal.ApplicationDescription;
import org.glassfish.jersey.server.wadl.internal.WadlResource;
import org.glassfish.jersey.server.wadl.internal.WadlUtils;
import org.json.JSONObject;

import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Timed;

import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "/log", description = "Log controller.")
@Path("/log/")
public class LoggingService {

	public static final String MESSAGE_KEY = "msg";
	public static final String APP_ID_KEY = "appid";
	public static final String TYPE_KEY = "type";
	public static final String TIMESTAMP_KEY = "timestamp";
	public static final String CUSTOM_ATTR_KEY = "custom_attr";
	private static LoggingService instance = null;
	private static String loggingServiceURL = null;
	private static CloseableHttpClient httpClient = null;
	private boolean enabled = true;

	@Context
	protected UriInfo uriInfo;

	@Context
	protected WadlApplicationContext wadlContext;

	@GET
	@Path("/wadl")
	@Produces({ "application/vnd.sun.wadl+xml", MediaType.APPLICATION_XML })
	public Response wadl() {

		try {
			boolean detailedWadl = WadlUtils.isDetailedWadlRequested(uriInfo);
			String lastModified = new SimpleDateFormat(WadlResource.HTTPDATEFORMAT).format(new Date());
			ApplicationDescription applicationDescription = wadlContext.getApplication(uriInfo, detailedWadl);
			com.sun.research.ws.wadl.Application application = applicationDescription.getApplication();
			List<com.sun.research.ws.wadl.Resource> extResources = new ArrayList<com.sun.research.ws.wadl.Resource>();
			for (com.sun.research.ws.wadl.Resource temp : application.getResources().get(0).getResource()) {
				if (!temp.getPath().equalsIgnoreCase("/log/")) {
					extResources.add(temp);
				}
			}
			application.getResources().get(0).getResource().removeAll(extResources);
			return Response.ok(application).header("Last-modified", lastModified).build();
		} catch (Exception e) {
			throw new ProcessingException("Error generating WADL", e);
		}
	}
	
	public LoggingService() {
		try {
			loggingServiceURL = Config.getInstance().getLoggingServiceURL();
			enabled = Config.getInstance().getLoggingEnabled();
			httpClient = HttpClientBuilder.create().build();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	

	public static void setHttpClient(CloseableHttpClient httpClient) {
		LoggingService.httpClient = httpClient;
	}

	public LoggingService(String url) {
		loggingServiceURL = url;
	}

	public static LoggingService getInstance() {
		if (instance == null) {
			instance = new LoggingService();
			
		}
		return instance;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@ApiOperation(value = "Save a log message on the service.")
	@POST
	@Path("/{appId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Timed
	@ExceptionMetered
	public Response pushLogOp(
			@ApiParam(value = "Application identifier", required = true) @PathParam("appId") @DefaultValue("") String appId,
			@ApiParam(value = "Log message", required = true) String msg,
			@ApiParam(value = "\"Bearer \" + client access token or basic.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {
		try {
			pushLog(msg, appId, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok().build();
	}

	@ApiOperation(value = "Update log schema in service.")
	@POST
	@Path("/update/schema/{appId}/{type}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Timed
	@ExceptionMetered
	public Response udpateLogEventSchema(@ApiParam(value = "Schema of log msg", required = true) String schema,
			@ApiParam(value = "Application identifier", required = true) @PathParam("appId") String appId,
			@ApiParam(value = "Type of log msg", required = true) @PathParam("type") String type,
			@ApiParam(value = "\"Bearer \" + client access token or basic.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {
		try {
			updateSchema(schema, appId, type, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok().build();
	}

	@ApiOperation(value = "Read log schema in service.")
	@GET
	@Path("/read/schema/{appId}")
	@Timed
	@ExceptionMetered
	public Response readLogEventSchema(
			@ApiParam(value = "Application identifier", required = true) @PathParam("appId") String appId,
			@ApiParam(value = "\"Bearer \" + client access token or basic.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {
		String result = null;
		try {
			result = readSchema(appId, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Return the paginate list of result matching query criteria.")
	@GET
	@Path("/{appId}")
	@Timed
	@ExceptionMetered
	public Response query(
			@ApiParam(value = "Application identifier", required = true) @PathParam("appId") @DefaultValue("") String appId,
			@ApiParam(value = "Timerange start. Express it in millis", required = false) @QueryParam("from") Long from,
			@ApiParam(value = "Timerange end. Express it in millis", required = false) @QueryParam("to") Long to,
			@ApiParam(value = "Log type to search", required = false) @QueryParam("type") @DefaultValue("") String type,
			@ApiParam(value = "Search criteria on custom fields using Lucene syntax. Put in logical AND clause with msgPattern if present.", required = false) @QueryParam("pattern") @DefaultValue("") String pattern,
			@ApiParam(value = "Search the pattern in log text. Put in logical AND clause with pattern if present.s", required = false) @QueryParam("msgPattern") @DefaultValue("") String msgPattern,
			@ApiParam(value = "Maximum number of messages to return. Default value is 150", required = false) @QueryParam("limit") Integer limit,
			@ApiParam(value = "Index of first message to return. Default value is 0", required = false) @QueryParam("offset") Integer offset,
			@ApiParam(value = "\"Bearer \" + client access token or basic.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String query = String.format("type=%s&pattern=%s&msgPattern=%s", type, pattern, msgPattern);
		if (from != null) {
			query = query + "&from=" + from;
		}
		if (to != null) {
			query = query + "&to=" + to;
		}
		if (limit != null) {
			query = query + "&limit=" + limit;
		}
		if (offset != null) {
			query = query + "&offset=" + offset;
		}

		String result = null;
		try {
			result = getLog(appId, query, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}
		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Count log entries.")
	@GET
	@Path("/count/{appId}")
	@Timed
	@ExceptionMetered
	public Response count(
			@ApiParam(value = "Application identifier", required = true) @PathParam("appId") @DefaultValue("") String appId,
			@ApiParam(value = "Timerange start. Express it in millis", required = false) @QueryParam("from") Long from,
			@ApiParam(value = "Timerange end. Express it in millis", required = false) @QueryParam("to") Long to,
			@ApiParam(value = "Log type to search", required = false) @QueryParam("type") @DefaultValue("") String type,
			@ApiParam(value = "Search criteria on custom fields using Lucene syntax. Put in logical AND clause with msgPattern if present.", required = false) @QueryParam("pattern") @DefaultValue("") String pattern,
			@ApiParam(value = "Search the pattern in log text. Put in logical AND clause with pattern if present.s", required = false) @QueryParam("msgPattern") @DefaultValue("") String msgPattern,
			@ApiParam(value = "Maximum number of messages to return. Default value is 150", required = false) @QueryParam("limit") Integer limit,
			@ApiParam(value = "Index of first message to return. Default value is 0", required = false) @QueryParam("offset") Integer offset,
			@ApiParam(value = "\"Bearer \" + client access token or basic.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {

		String query = String.format("type=%s&pattern=%s&msgPattern=%s", type, pattern, msgPattern);
		if (from != null) {
			query = query + "&from=" + from;
		}
		if (to != null) {
			query = query + "&to=" + to;
		}
		if (limit != null) {
			query = query + "&limit=" + limit;
		}
		if (offset != null) {
			query = query + "&offset=" + offset;
		}

		String result = null;
		try {
			result = countLog(appId, query, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}

		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	@ApiOperation(value = "Return one or more elasticsearch aggregation(s). See https://www.elastic.co/guide/en/elasticsearch/reference/current/search-search.html and https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations.html")
	@POST
	@Path("aggregate")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Timed
	@ExceptionMetered
	public Response aggregate(String request,
			@ApiParam(value = "\"Bearer \" + client access token or basic.", required = true) @HeaderParam("Authorization") String token)
			throws WeLiveException {
		String result = null;
		try {
			result = aggregateLog(request, token);
		} catch (Exception e2) {
			if (e2 instanceof WeLiveException) {
				WeLiveException wle = (WeLiveException) e2;
				throw new WeLiveException(wle.getStatus(), wle.getMessage(), wle.getCause());
			} else {
				throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, e2.getMessage(), e2.getCause());	
			}
		}

		return Response.ok(result, MediaType.APPLICATION_JSON).build();
	}

	public void sendLog(String service, JSONObject attributes) throws URISyntaxException {
		if (enabled) {
			try {
				CloseableHttpClient httpClientT = HttpClientBuilder.create().build();
				URIBuilder uriBuilder = new URIBuilder(String.format("%s/%s", loggingServiceURL, service));
				HttpPost httpPost = new HttpPost(uriBuilder.build());
				httpPost.setEntity(new StringEntity(attributes.toString()));
				httpPost.addHeader("Content-type", "application/json");
				String authentication = String.format("%s:%s", Config.getInstance().getAdminUser(),
						Config.getInstance().getAdminPass());
				byte[] bytesEncoded = Base64.encodeBase64(authentication.getBytes());
				httpPost.addHeader("Authorization", String.format("Basic %s", new String(bytesEncoded)));
				httpClientT.execute(httpPost);
				httpClientT.close();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	protected String pushLog(String request, String appId, String token) throws Exception {
		return doPost(request, "/" + appId, null, false, token);
	}

	public String updateSchema(String schema, String appId, String type, String token) throws Exception {
		return doPost(schema, "/update/schema/" + appId + "/" + type, null, false, token);

	}

	public String readSchema(String appId, String token) throws Exception {
		return doGet("/read/schema/" + appId, null, token);

	}

	protected String getLog(String appId, String query, String token) throws Exception {
		return doGet("/" + appId, query, token);
	}

	protected String countLog(String appId, String query, String token) throws Exception {
		return doGet("/count/" + appId, query, token);
	}

	protected String aggregateLog(String request, String token) throws Exception {
		String result = doPost(request, "/aggregate", null, true, token);
		return result;
	}

	protected String doPost(String request, String operation, String query, boolean getResponse, String token)
			throws Exception {
//		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		URIBuilder uriBuilder = new URIBuilder(loggingServiceURL + operation + (query != null ? ("?" + query) : ""));

		HttpPost httpPost = new HttpPost(uriBuilder.build());
		httpPost.setEntity(new StringEntity(request));
		httpPost.addHeader("Content-Type", "application/json");
		httpPost.addHeader("Accept", "application/json");
		httpPost.addHeader("Authorization", token);
		CloseableHttpResponse response = httpClient.execute(httpPost);

		if (getResponse) {
			InputStream content = response.getEntity().getContent();
			BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null) {
				responseStrBuilder.append(inputStr);
			}
			content.close();

			if (response.getStatusLine().getStatusCode() >= 400) {
				throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
						responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
			}
			response.close();
//			httpClient.close();
			return responseStrBuilder.toString();
		} else {
			return null;
		}
	}

	protected String doGet(String operation, String query, String token) throws Exception {
//		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		URIBuilder uriBuilder = new URIBuilder(loggingServiceURL + operation + (query != null ? ("?" + query) : ""));
		HttpGet httpGet = new HttpGet(uriBuilder.build());
		httpGet.addHeader("Content-Type", "application/json");
		httpGet.addHeader("Accept", "application/json");
		httpGet.addHeader("Authorization", token);
		CloseableHttpResponse response = httpClient.execute(httpGet);

		InputStream content = response.getEntity().getContent();
		BufferedReader streamReader = new BufferedReader(new InputStreamReader(content));
		StringBuilder responseStrBuilder = new StringBuilder();
		String inputStr;
		while ((inputStr = streamReader.readLine()) != null) {
			responseStrBuilder.append(inputStr);
		}
		content.close();

		if (response.getStatusLine().getStatusCode() >= 400) {
			throw new WeLiveException(Response.Status.fromStatusCode(response.getStatusLine().getStatusCode()),
					responseStrBuilder.toString() + " " + response.getStatusLine().getReasonPhrase());
		}
//		httpClient.close();
		return responseStrBuilder.toString();
	}

	public long createTimestamp() {
		return new Date().getTime() / 1000;
	}

	public JSONObject createMainPayload(String message, String appID, String type) {
		final JSONObject payload = new JSONObject();

		payload.put(MESSAGE_KEY, message);
		payload.put(APP_ID_KEY, appID);
		payload.put(TYPE_KEY, type);
		payload.put(TIMESTAMP_KEY, createTimestamp());

		return payload;
	}

	
	// protected String ping() {
	// return "pong";
	// }
}
