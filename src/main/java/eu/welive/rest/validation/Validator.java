/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.validation;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.media.multipart.FormDataParam;

import com.hp.hpl.jena.rdf.model.InfModel;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.NsIterator;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;
import com.hp.hpl.jena.reasoner.Reasoner;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;
import com.hp.hpl.jena.reasoner.ValidityReport;
import com.hp.hpl.jena.reasoner.ValidityReport.Report;
import com.hp.hpl.jena.util.FileManager;
import com.hp.hpl.jena.vocabulary.RDF;

import io.swagger.annotations.ApiOperation;
import uk.gov.nationalarchives.csv.validator.api.java.CsvValidator;
import uk.gov.nationalarchives.csv.validator.api.java.ErrorMessage;
import uk.gov.nationalarchives.csv.validator.api.java.FailMessage;
import uk.gov.nationalarchives.csv.validator.api.java.Substitution;


/**
 * Created by mikel (m.emaldi at deusto dot es) on 17/07/15.
 */
@Path("validation")
// @Api(value = "/validation", description = "Validators for ckanext-validator extension")
public class Validator {

    @POST
    @Path("/csv")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Validates CSV files",
            notes = "Validates a CSV file againts its CSV schema",
            response = ValidatorResponse.class)
    public ValidatorResponse csv(@FormDataParam("csv") InputStream csvInputStream, @FormDataParam("schema") InputStream schemaInputStream) {

        Reader csvReader = new InputStreamReader(csvInputStream);
        Reader schemaReader = new InputStreamReader(schemaInputStream);

        Boolean failFast = false;
        List<Substitution> pathSubstitutions = new ArrayList<Substitution>();


        List<FailMessage> messages = CsvValidator.validate(csvReader, schemaReader, failFast, pathSubstitutions, true);

        if (messages.isEmpty()) {
            return new ValidatorResponse("true", null);
        } else {
            List<String> errors = new ArrayList<>();
            for(FailMessage message : messages) {
                if(message instanceof ErrorMessage) {
                    errors.add(message.getMessage());
                }
            }
            return new ValidatorResponse("false", errors);
        }
    }

    @POST
    @Path("/rdf")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Validates RDF files",
            notes = "Validates a RDF file againts its ontological model",
            response = ValidatorResponse.class)
    public ValidatorResponse rdf(@FormDataParam("rdf") InputStream rdfInputStream) {

        List<String> errors = new ArrayList<>();

        Model data = ModelFactory.createDefaultModel();
        //TODO: replace http://welive.eu/data
        data.read(rdfInputStream, "http://welive.eu/data/");
        NsIterator nsIterator = data.listNameSpaces();
        Model ontoModel = ModelFactory.createDefaultModel();
        while(nsIterator.hasNext()) {
            String ns = nsIterator.nextNs();
            Model model = FileManager.get().loadModel(ns);
            ontoModel.add(model);
        }

        StmtIterator stmtIterator = data.listStatements();
        while(stmtIterator.hasNext()) {
            Statement statement = stmtIterator.nextStatement();
            boolean contains = ontoModel.contains(statement.getPredicate(), null, (RDFNode) null);
            if (!contains) {
                errors.add(String.format("Property %s not found in schema", statement.getPredicate()));
            }
            if (statement.getPredicate().equals(RDF.type) && statement.getObject().isResource()) {
                contains = ontoModel.contains(statement.getObject().asResource(), null, (RDFNode) null);
                if (!contains) {
                    errors.add(String.format("Class %s not found in schema", statement.getObject()));
                }
            }
        }

        Reasoner owlReasoner = ReasonerRegistry.getOWLReasoner();
        owlReasoner = owlReasoner.bindSchema(ontoModel);
        InfModel owlInfModel = ModelFactory.createInfModel(owlReasoner, data);

        ValidityReport owlValidityReport = owlInfModel.validate();
        Iterator<Report> owlReportIterator = owlValidityReport.getReports();

        while (owlReportIterator.hasNext()) {
            Report report = owlReportIterator.next();
            errors.add(report.getDescription());
        }

        Reasoner rdfsReasoner = ReasonerRegistry.getRDFSReasoner();
        rdfsReasoner = rdfsReasoner.bindSchema(ontoModel);
        InfModel rdfsInfModel = ModelFactory.createInfModel(rdfsReasoner, data);
        ValidityReport rdfsValidityReport = rdfsInfModel.validate();
        Iterator<Report> rdfsReportIterator = rdfsValidityReport.getReports();
        while (rdfsReportIterator.hasNext()) {
            Report report = rdfsReportIterator.next();
            errors.add(report.getDescription());
        }
        if (errors.size() > 0 ) {
            return new ValidatorResponse("false", errors);
        } else {
            return new ValidatorResponse("true", null);
        }
    }
}
