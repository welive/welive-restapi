/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.oia;

import javax.xml.bind.annotation.XmlElement;

public class VCProjectAssociation {

	@XmlElement(required = true)
	String vcProjectId;
	@XmlElement(required = true)
	String vcProjectName;
	@XmlElement(required = true)
	Boolean isMockup; 
	@XmlElement(required = true)
	String ideaId;
	
	
	public String getVcProjectId() {
		return vcProjectId;
	}
	public void setVcProjectId(String vcProjectId) {
		this.vcProjectId = vcProjectId;
	}
	public String getVcProjectName() {
		return vcProjectName;
	}
	public void setVcProjectName(String vcProjectName) {
		this.vcProjectName = vcProjectName;
	}
	public Boolean isMockup() {
		return isMockup;
	}
	public void setMockup(Boolean isMockup) {
		this.isMockup = isMockup;
	}
	public String getIdeaId() {
		return ideaId;
	}
	public void setIdeaId(String ideaId) {
		this.ideaId = ideaId;
	}
	
	
	
}
