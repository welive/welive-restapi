/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.oia;

import java.util.List;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.util.Base64;
import org.json.JSONObject;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

import eu.welive.rest.config.Config;
import eu.welive.rest.vc.VC.AuthType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("oia/")
@Api(value = "/oia")
@Produces({MediaType.APPLICATION_JSON})
public class OIA {

	public static enum AuthType {Basic, Oauth};
	private static String baseurl;
	
	public AuthType authType(String auth){
		
		if(auth.startsWith("Basic ")) return AuthType.Basic;
		else if(auth.startsWith("Bearer ")) return AuthType.Oauth;
		else return null;
	}
	
	
	
	public Response invoke(String url, String token){
		
		Client client = Client.create();
		WebResource webResource;
		
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		//System.out.println("Invoking "+url);
       
        ClientResponse clientResponse = webResource.accept(MediaType.WILDCARD).get(ClientResponse.class);
        if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        }
        String resp = clientResponse.getEntity(String.class);
               
		return Response.status(200).entity(resp).build();
	}
	
	
public Response post(String url, String token, Object data){
		
		Client client = Client.create();
		WebResource webResource;
		AuthType authtype = authType(token);
		if(authtype!=null){
			switch(authtype){
				case Basic:
					String decoded = new String(Base64.decode(token.replace("Basic ", "").getBytes()));
					String[] credentials = decoded.split(":");
					client.addFilter(new HTTPBasicAuthFilter(credentials[0], credentials[1]));
					webResource = client.resource(url);
					break;
				case Oauth:
					webResource = client.resource(url);
					webResource.header("Authorization", token);
					break;
				default:
					webResource = client.resource(url);
					break;
			}
		}
		else{
			return Response.status(400).entity("Invalid token").build();
		}
		System.out.println("Invoking "+url);
		ClientResponse clientResponse = null;
      
		
        
			clientResponse = webResource.type(MediaType.APPLICATION_JSON).accept(MediaType.WILDCARD).post(ClientResponse.class, data);
	
			if (clientResponse.getStatus() != 200) {
        	System.out.println("Failed - HTTP Error Code :" + clientResponse.getStatus());
        
        	
        	
        }
        String resp = clientResponse.getEntity(String.class);
        
        System.out.println(resp);       
        
		return Response.status(200).entity(resp).build();
	}
	

	
	
	public OIA() throws Exception {
		baseurl = Config.getInstance().getLiferayURL() + Config.getInstance().getOiapath();
        
    }
	
	public JSONObject isValidNewScreenshotRequest(Long ideaId, NewScreenshot screenshot){
		
		JSONObject json = new JSONObject();
		json.put("error", false);
		
		if(ideaId==null){
			json.put("error", true);
			json.put("message", "IdeaId is mandatory");
		}
		
		if(screenshot.getEncodedScreenShot()==null || screenshot.getEncodedScreenShot().equals("")){
			json.put("message", "Screenshot content is mandatory");
			json.put("error", true);
		}
		
		if(screenshot.getImgMimeType()==null || screenshot.getImgMimeType().equals("")){
			json.put("message", "Img mime type is mandatory");
			json.put("error", true);
		}
		
		if(screenshot.getUsername()==null || screenshot.getUsername().equals("")){
			json.put("message", "Username is mandatory");
			json.put("error", true);
		}
		
		return json;
		
	}
	
	public Response publishScreenShotAction(String ideaId, NewScreenshot screenshot, String auth){
		String url = baseurl+"/publish-screen-shot";
				//+ "/idea_id/"+ideaId;
				//+ "/encoded-screen-shot/"+screenshot.getEncodedScreenShot();
				
		/*if(screenshot.getDescription()==null || screenshot.getDescription().equals("")){
			url += "/-description/";
		}
		else{ url += "/description/"+screenshot.getDescription(); }
		
		url += "/img-type/"+screenshot.getImgMimeType()
				+"/username/"+screenshot.getUsername();*/
		
		
		screenshot.setIdea_id(ideaId);
		
		//JSONObject j = isValidNewScreenshotRequest()
		
		return Response.status(200).entity(post(url,auth,screenshot).toString()).build();
	}

public JSONObject isValidVCProjectAssociation(VCProjectAssociation vcprj){
		
		JSONObject json = new JSONObject();
		json.put("error", false);
		
		if(vcprj.getIdeaId()==null || vcprj.getIdeaId().equals("")){
			json.put("message", "IdeaId is mandatory");
			json.put("error", true);
		}
		
		if(vcprj.getVcProjectId()==null || vcprj.getVcProjectId().equals("")){
			json.put("message", "vcProjectId is mandatory");
			json.put("error", true);
			
		}
		
		if(vcprj.isMockup()==null || vcprj.isMockup().equals("")){
			json.put("message", "isMockup is mandatory");
			json.put("error", true);
		}
		
		if(vcprj.getVcProjectName()==null || vcprj.getVcProjectName().equals("")){
			json.put("message", "vcProjectName is mandatory");
			json.put("error", true);
		}
		
		return json;
		
	}
	
	public Response vcProjectUpdateAction(VCProjectAssociation vcprj, String auth){
		String url = baseurl+"/vc-project-update"
				+ "/vmeprojectid/"+vcprj.getVcProjectId()
				+ "/vmeprojectname/"+vcprj.getVcProjectName()
				+ "/ismockup/"+vcprj.isMockup()
				+ "/ideaid/"+vcprj.getIdeaId();
		
		return Response.status(200).entity(invoke(url,auth)).build();
	}

	@POST
	@Path("/new-screenshot")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Associates a new screenshot to a specific idea in the Welive Open Innovation Area",
					//hidden=true,
					response = AckResponse.class)
	public Response publishScreenShot(@Context HttpHeaders headers,
			                        @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
									//@ApiParam(value = "The unique id of the idea which you want to associate the screenshot") @PathParam("idea-id") String ideaId,
									@ApiParam(value = "Screenshot publication data. The 'encodedScreenshot' must be Base64 encoded") NewScreenshot screenshot) {
/*
 * 
 * 
		List<String> auths = headers.getRequestHeaders().get("authorization");
		if(auths==null || auths.isEmpty()){
			JSONObject json = new JSONObject();
			json.put("error", true);
			json.put("message", "Authentication is required");
			return Response.status(403).entity(json.toString()).build();
		}
		
		String auth = auths.get(0).trim();
		AuthType type = authType(auth);
		switch(type){
			case Basic:{
				JSONObject json = isValidNewScreenshotRequest(ideaId, screenshot);
				if(json.getBoolean("error")){
					return Response.status(400).entity(json.toString()).build();
				}*/
				
		String url = baseurl+"/publish-screen-shot";
		//screenshot.setIdea_id(ideaId);
		return post (url,token, screenshot);
		
				//return publishScreenShotAction(ideaId, screenshot, token);
		
	}

	
	@POST
	@Path("/addVcProject")
	@Produces(MediaType.APPLICATION_JSON)
	@ApiOperation(value = "Updates the Mashup and Mockup projects list associated with an Idea",
					//hidden=true,
					response = AckResponse.class)
	public Response vcProjectUpdate(@Context HttpHeaders headers,
			                        @ApiParam(value = "\"Basic \" + a user access token.", required = true) @HeaderParam("Authorization") String token,
									@ApiParam(value = "Association data") VCProjectAssociation vcprj) {
		
		/*JSONObject json = new JSONObject();
		json.put("error", true);
		
		List<String> auths = headers.getRequestHeaders().get("authorization");
		if(auths==null || auths.isEmpty()){
			json.put("message", "Authentication is required");
			return Response.status(403).entity(json.toString()).build();
		}
		
		String auth = auths.get(0).trim();
		AuthType type = authType(auth);
		switch(type){
			case Basic:
				JSONObject j = isValidVCProjectAssociation(vcprj);
				if(j.getBoolean("error")){
					return Response.status(400).entity(j.toString()).build();
				}*/
		
		String url = baseurl+"/vc-project-update"
				+ "/vmeprojectid/"+vcprj.getVcProjectId()
				+ "/vmeprojectname/"+vcprj.getVcProjectName()
				+ "/ismockup/"+vcprj.isMockup()
				+ "/ideaid/"+vcprj.getIdeaId();
				
				return invoke(url,token);
			/*case Oauth:
			default:
				json.put("message", "Only Basic Authentication allowed");
				return Response.status(403).entity(json.toString()).build();
		}*/
		
	}

}
