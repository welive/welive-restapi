/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.oia;

import javax.xml.bind.annotation.XmlElement;

public class NewScreenshot {

	@XmlElement(required = true)
	String encodedScreenShot;
	
	String description;
	
	@XmlElement(required = true)
	String imgMimeType;
	
	@XmlElement(required = true)
	String username;
	
	@XmlElement(required = true)
	String idea_id;
	
	public String getIdea_id() {
		return idea_id;
	}
	public void setIdea_id(String idea_id) {
		this.idea_id = idea_id;
	}
	
	public String getEncodedScreenShot() {
		return encodedScreenShot;
	}
	public void setEncodedScreenShot(String encodedScreenShot) {
		this.encodedScreenShot = encodedScreenShot;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImgMimeType() {
		return imgMimeType;
	}
	public void setImgMimeType(String imgMimeType) {
		this.imgMimeType = imgMimeType;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	
	
}
