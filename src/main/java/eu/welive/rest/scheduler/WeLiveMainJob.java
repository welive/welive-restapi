/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.scheduler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

import eu.iescities.server.querymapper.datasource.DataSourceConnector;
import eu.iescities.server.querymapper.datasource.DataSourceConnectorException;
import eu.iescities.server.querymapper.datasource.DataSourceManagementException;
import eu.iescities.server.querymapper.datasource.json.DatabaseCreationException;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaConnector;
import eu.iescities.server.querymapper.datasource.json.schema.JSONSchemaDataSource;
import eu.iescities.server.querymapper.util.scheduler.MainJob;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager;
import eu.iescities.server.querymapper.util.status.DatasetStatusManager.Status;
import eu.iescities.server.querymapper.util.status.DatasetStatusManagerException;
import eu.welive.rest.scheduler.exception.ResourceJobException;
import eu.welive.rest.util.ckan.CKANUtil;
import eu.welive.rest.util.ckan.data.CKANPackage;
import eu.welive.rest.util.ckan.data.CKANResource;
import eu.welive.rest.util.ckan.exception.CKANConnectionException;
import eu.welive.rest.util.ckan.exception.CKANUtilException;

public class WeLiveMainJob extends MainJob {
	
	public static final String LAST_REVISION = "lastRevision";

	private static final Logger logger = LogManager.getLogger(WeLiveMainJob.class);

	private CKANUtil ckanUtil;

	@Override
	public void checkJobs(Scheduler sch) {		
		try {
			ckanUtil = new CKANUtil();

			final DateTime lastRevision = (DateTime) sch.getContext().get(LAST_REVISION);
			final DateTime currentTime = new DateTime().withZone(DateTimeZone.forID("UTC"));
            sch.getContext().put(LAST_REVISION, currentTime);
            
			try {
				final List<String> runningMappings = new ArrayList<String>();

				for (String packageName : ckanUtil.getUpdatedPackages(lastRevision)) {					
					try {
						processPackage(sch, runningMappings, packageName);
					} catch (DatasetStatusManagerException e) {
						logger.error("Could not update status information. ", e);
					}

				}
	
				try {
					stopJobs(runningMappings, sch);
				} catch (SchedulerException e) {
					logger.error("Unexpected problem stopping running jobs.", e);
				}
            } catch (CKANUtilException e) {
            	logger.error("Problem obtaning updated packages with lastRevision=" + lastRevision, e);
            }
		} catch (CKANConnectionException e) {
			logger.error("Problem connecting to CKAN instance", e);
		} catch (SchedulerException e) {
			logger.error("Problem with query mapper job scheduling", e);
		}
	}

	private void processPackage(Scheduler sch, final List<String> runningMappings, final String packageName)
			throws DatasetStatusManagerException {
		try {
			final CKANPackage packageInfo = ckanUtil.getPackageInfo(packageName);
			for (CKANResource resource : packageInfo.getResources()) {
				processResourceMapping(resource, sch, runningMappings);
			}			
		} catch (CKANUtilException e) {
			DatasetStatusManager.getInstance().updateStatus(packageName, Status.ERROR, "Problem obtaining package information. " + e.getMessage());
		}
	}

	public void processResourceMapping(final CKANResource resource, Scheduler sch, final List<String> runningMappings) throws DatasetStatusManagerException {
		final String resourceID = resource.getID();
		if (resource.hasMapping()) {
			try {
				final DataSourceConnector connector = resource.getConnector();
				switch (connector.getType()) {
					case json_schema:	createUserDataset(connector, resourceID);
										break;
																		
					case json:
					case csv:			createPeriodicJob(sch, runningMappings, resourceID, connector);
										break;
										
					default:			DatasetStatusManager.getInstance().updateStatus(resourceID, Status.OK, "Mapping type is not compatible");
										break;
				}
			} catch (DataSourceConnectorException e) {
				DatasetStatusManager.getInstance().updateStatus(resourceID, Status.ERROR, "Problem loading mapping. " + e.getMessage());
			} catch (SchedulerException e) {
				DatasetStatusManager.getInstance().updateStatus(resourceID, Status.ERROR, "Problem checking running job.");
			} catch (ResourceJobException e) {
				DatasetStatusManager.getInstance().updateStatus(resourceID, Status.ERROR, e.getMessage());
			}
		} else {
			DatasetStatusManager.getInstance().updateStatus(resourceID, Status.OK, "Mapping is empty");
		}
	}

	private void createPeriodicJob(Scheduler sch, final List<String> runningMappings, final String resourceID,
			final DataSourceConnector connector)
			throws SchedulerException, DatasetStatusManagerException, ResourceJobException {
		if (!isJobRegistered(resourceID, sch)) {
			try {
				DatasetStatusManager.getInstance().updateStatus(resourceID, Status.PROCESSING, "Data download job created");
				createDatasourceJob(resourceID, connector, sch);
				runningMappings.add(resourceID);
			} catch (SchedulerException | IOException e) {
				throw new ResourceJobException("Problem starting scheduler job", e);										
			}
		} else {
			runningMappings.add(resourceID);
		}
	}

	private void createUserDataset(DataSourceConnector connector, String resourceID) throws ResourceJobException, DatasetStatusManagerException {
		try {
			DatasetStatusManager.getInstance().updateStatus(resourceID, Status.PROCESSING, "Started creation of user storage");
			initializeUserDataset((JSONSchemaConnector)connector, resourceID);
			DatasetStatusManager.getInstance().updateStatus(resourceID, Status.OK, "User storage created");
		} catch (DatabaseCreationException | DataSourceManagementException e) {
			throw new ResourceJobException("Problem creating user dataset", e);
		}
	}  
	
	public void initializeUserDataset(JSONSchemaConnector connector, String resourceID) throws DatabaseCreationException, DataSourceManagementException {
		new JSONSchemaDataSource((JSONSchemaConnector) connector, resourceID);
	}
}
