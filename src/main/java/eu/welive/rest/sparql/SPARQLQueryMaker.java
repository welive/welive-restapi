/*
Copyright 2015-2018 WeLive Consortium

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.rest.sparql;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataParam;

import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.query.ResultSetFormatter;
import com.hp.hpl.jena.rdf.model.Model;

import eu.iescities.server.querymapper.datasource.sparql.SPARQLConnector;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilder;
import eu.iescities.server.querymapper.sql.schema.builder.SchemaBuilderException;
import eu.welive.rest.config.Config;
import eu.welive.rest.exception.WeLiveException;
import io.swagger.annotations.ApiOperation;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * Created by mikel (m.emaldi at deusto dot es) on 18/09/15.
 */
@Path("sparql-query-maker")
//@Api(value = "/sparql-query-maker", description = "Gateway for Openlink Virtuoso")
public class SPARQLQueryMaker {


    @POST
    @Path("/create")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Creates a RDF graph and inserts given triples",
            notes = "Creates a RDF graph and inserts given triples",
            response = SPARQLResponse.class)
    public SPARQLResponse create(@FormDataParam("data") @DefaultValue("") InputStream data, @FormDataParam("graphName") @DefaultValue("") String graphName) throws WeLiveException {
        try {
            final Config config = Config.getInstance();
            Model model = VirtModel.openDatabaseModel(graphName, config.getVirtuosoURI(), config.getVirtuosoUser(), config.getVirtuosoPassword());
            model.read(new InputStreamReader(data), null);
            model.close();

            return new SPARQLResponse("Triples inserted successfully");
        } catch (IOException e) {
            throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Connection timeout when querying the SPARQL endpoint", e);
        }
    }

    @GET
    @Path("/query")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Queries a graph",
            notes = "Queries a graph",
            response = SPARQLResponse.class)
    public Response query(@QueryParam("query") @DefaultValue("") String query, @QueryParam("graphName") @DefaultValue("") String graphName) throws WeLiveException {
        try {
            Config config = Config.getInstance();
            VirtGraph set = new VirtGraph (graphName, config.getVirtuosoURI(), config.getVirtuosoUser(), config.getVirtuosoPassword());
            Query SPARQLQuery = QueryFactory.create(query);
            VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(SPARQLQuery, set);
            ResultSet rs = vqe.execSelect();
            File temp = File.createTempFile(graphName, ".tmp");
            OutputStream out = new FileOutputStream(temp);
            ResultSetFormatter.outputAsJSON(out, rs);
            byte[] encoded = Files.readAllBytes(Paths.get(temp.getAbsolutePath()));
            //return new SPARQLResponse(new String(encoded));
            return Response.ok(new String(encoded), MediaType.APPLICATION_JSON).build();
        } catch (IOException e) {
            throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Connection timeout when querying the SPARQL endpoint", e);
        }
    }


    @GET
    @Path("/mapping")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Generates the mapping for a RDF file automatically")
    public Response getMapping(@QueryParam("sparqlEndpoint") @DefaultValue("") String sparqlEndpoint,
                                     @QueryParam("graph") @DefaultValue("") String graph) throws WeLiveException {
        try {
            SchemaBuilder schemaBuilder = new SchemaBuilder(sparqlEndpoint, graph, false);
            SPARQLConnector schema = schemaBuilder.getSchema();
            System.out.println(schema.toString());
            // return new SPARQLResponse(schema.toString());
            return Response.ok(schema.toString(), MediaType.APPLICATION_JSON).build();
        } catch (IOException e) {
            throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Connection timeout when querying the SPARQL endpoint", e);
        } catch (SchemaBuilderException e) {
            throw new WeLiveException(Response.Status.INTERNAL_SERVER_ERROR, "Failed when reading the schema", e);
        }
    }
}
